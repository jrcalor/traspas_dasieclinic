unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JvExControls, JvXPCore, JvXPButtons, StdCtrls, ExtCtrls, DB, HTTPApp,
  MyAccess, MemDS, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery, System.JSON,
  Grids, DBGrids, FIBDataSet, pFIBDataSet, iexColorPalette, DBAccess, ComCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, System.JSONConsts,
  System.Net.URLClient, System.Net.HttpClient, System.Net.HttpClientComponent,
  RVScroll, RichView, RVStyle, System.AnsiStrings, DBRV, SRVSkinManager, PtblRV,
  RVReport, gtXportIntf, gtRichViewIntf, gtClasses3, gtCstDocEng, gtCstPlnEng,
  gtCstHTMLEng, gtExHTMLEng, gtHTMLEng, IdGlobal, Vcl.Samples.Spin,
  JvExStdCtrls, JvRichEdit, FileCtrl;

type
  TForm2 = class(TForm)
    Panel1: TPanel;
    qE1: TMyQuery;
    FIBDades: TpFIBDatabase;
    FIBPrincipal: TpFIBTransaction;
    pFIBRelacions: TpFIBDatabase;
    pFIBTrans_Relacions: TpFIBTransaction;
    qD1: TpFIBQuery;
    qR1: TpFIBQuery;
    qR2e: TpFIBQuery;
    tDPob: TpFIBDataSet;
    msEClinic: TMyConnection;
    TEscritura: TpFIBTransaction;
    DataSource1: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    CheckBox1: TCheckBox;
    sbPoblacions: TJvXPButton;
    Panel4: TPanel;
    Label3: TLabel;
    Edit3: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Edit9: TEdit;
    JvXPButton2: TJvXPButton;
    JvXPButton1: TJvXPButton;
    Label2: TLabel;
    Label5: TLabel;
    sbReiniciar: TJvXPButton;
    Panel5: TPanel;
    tD: TpFIBDataSet;
    Memo2: TMemo;
    tD1: TpFIBDataSet;
    Memo3: TMemo;
    Label6: TLabel;
    Edit4: TEdit;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    Memo1: TMemo;
    RichEdit1: TRichEdit;
    Panel7: TPanel;
    sbEspe: TJvXPButton;
    sbTipusDoc: TJvXPButton;
    sbEmpresa: TJvXPButton;
    Panel8: TPanel;
    sbAntecedents: TJvXPButton;
    sbProcedencies: TJvXPButton;
    sbOrigens: TJvXPButton;
    Panel9: TPanel;
    Panel10: TPanel;
    sbGrupsITtes: TJvXPButton;
    sbTarifesGRP: TJvXPButton;
    sbTarifes: TJvXPButton;
    Panel11: TPanel;
    sbPacients: TJvXPButton;
    sbAntecedentsPAc: TJvXPButton;
    Panel12: TPanel;
    sbCites: TJvXPButton;
    Memo4: TMemo;
    Panel13: TPanel;
    sbAbsencies: TJvXPButton;
    Edit6: TEdit;
    Label7: TLabel;
    sbRelacionsC: TJvXPButton;
    sbApertures: TJvXPButton;
    Panel14: TPanel;
    sbVisites: TJvXPButton;
    sbTipusCita: TJvXPButton;
    sbEpisodis: TJvXPButton;
    sbBoxes: TJvXPButton;
    ckExtern: TCheckBox;
    Panel15: TPanel;
    Panel16: TPanel;
    sbBonus: TJvXPButton;
    RVStyle1: TRVStyle;
    Label8: TLabel;
    sbCarrecs: TJvXPButton;
    Panel17: TPanel;
    sbCobros: TJvXPButton;
    sbInformes: TJvXPButton;
    tInformes: TpFIBDataSet;
    tInformesINFREGISTRE: TFIBIntegerField;
    tInformesINFORME: TFIBMemoField;
    RichView1: TRichView;
    DBRichViewPDF: TDBRichView;
    dsInformes: TDataSource;
    gtHTMLEngine1: TgtHTMLEngine;
    gtRichViewInterface1: TgtRichViewInterface;
    RVReportHelper1: TRVReportHelper;
    tInformesINFTITULO: TFIBStringField;
    sbCaixes: TJvXPButton;
    sbConConta: TJvXPButton;
    sbHoraris: TJvXPButton;
    sbCies: TJvXPButton;
    sbPreus: TJvXPButton;
    sbEntitats: TJvXPButton;
    sbFP: TJvXPButton;
    sbBancs: TJvXPButton;
    Label9: TLabel;
    Label4: TLabel;
    Edit8: TEdit;
    JvXPButton3: TJvXPButton;
    Edit7: TEdit;
    Label10: TLabel;
    Button1: TButton;
    Edit1: TEdit;
    sbUsuaris: TJvXPButton;
    sbProf: TJvXPButton;
    Button2: TButton;
    Memo5: TMemo;
    tD2: TpFIBDataSet;
    tD0: TpFIBDataSet;
    qE2: TMyQuery;
    sbTutors: TJvXPButton;
    sbImatgesDocs: TJvXPButton;
    sbClasImg: TJvXPButton;
    NetHTTPClient1: TNetHTTPClient;
    Label11: TLabel;
    Edit5: TEdit;
    sbFraMutues: TJvXPButton;
    sbCobMutua: TJvXPButton;
    sp1: TSpinEdit;
    CheckBox2: TCheckBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Edit10: TEdit;
    tInformesINFDATA: TFIBDateTimeField;
    tInformesPACREGISTRE: TFIBIntegerField;
    tInformesUSRREGISTRE: TFIBIntegerField;
    procedure JvXPButton2Click(Sender: TObject);
    procedure sbPoblacionsClick(Sender: TObject);
    procedure RevisarPob;
    procedure JvXPButton1Click(Sender: TObject);
    procedure Botons(Sender: TObject);
    procedure Botons2(Sender: TObject);
    procedure sbReiniciarClick(Sender: TObject);
    procedure sbProcedenciesClick(Sender: TObject);
    procedure sbTipusDocClick(Sender: TObject);
    function TaulaOK(Taula: String): Boolean;
    procedure PassadaOK(Taula: String);
    procedure FormCreate(Sender: TObject);
    procedure sbTarifesGRPClick(Sender: TObject);
    procedure sbTarifesClick(Sender: TObject);
    procedure sbEspeClick(Sender: TObject);
    procedure sbGrupsITtesClick(Sender: TObject);
    procedure sbPreusClick(Sender: TObject);
    procedure sbProfClick(Sender: TObject);
    function CogNoms(var Cadena: string; Separador : string) : String;
    procedure sbEmpresaClick(Sender: TObject);
    procedure sbHorarisClick(Sender: TObject);
    procedure sbAbsenciesClick(Sender: TObject);
    procedure sbOrigensClick(Sender: TObject);
    procedure sbPacientsClick(Sender: TObject);
    procedure sbCitesClick(Sender: TObject);
    procedure sbEpisodisClick(Sender: TObject);
    procedure sbVisitesClick(Sender: TObject);
    procedure sbUsuarisClick(Sender: TObject);
    procedure sbRelacionsCClick(Sender: TObject);
    function DonemPass(Clau : string; var Password: string; var Salt : string) : Boolean;
    procedure sbAntecedentsPAcClick(Sender: TObject);
    procedure NetHTTPClient1ValidateServerCertificate(const Sender: TObject;
      const ARequest: TURLRequest; const Certificate: TCertificate;
      var Accepted: Boolean);
    procedure sbAperturesClick(Sender: TObject);
    procedure sbTipusCitaClick(Sender: TObject);
    procedure sbBoxesClick(Sender: TObject);
    procedure sbBonusClick(Sender: TObject);
    procedure sbAntecedentsClick(Sender: TObject);
    procedure sbCiesClick(Sender: TObject);
    procedure sbFraMutuesClick(Sender: TObject);
    procedure sbCarrecsClick(Sender: TObject);
    procedure sbCobrosClick(Sender: TObject);
    procedure sbCobMutuaClick(Sender: TObject);
    procedure sbInformesClick(Sender: TObject);
    procedure sbCaixesClick(Sender: TObject);
    procedure sbConContaClick(Sender: TObject);
    procedure sbEntitatsClick(Sender: TObject);
    procedure sbBancsClick(Sender: TObject);
    procedure sbFPClick(Sender: TObject);
    procedure JvXPButton3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    function QuitarCaracteres(Str: String): String;
    procedure sbTutorsClick(Sender: TObject);
    procedure sbImatgesDocsClick(Sender: TObject);
    procedure sbClasImgClick(Sender: TObject);
    function base64encode(f:TStream):string;
    function RastreaDir( Dir: string; idInforme : integer ) : string;
  private
    EsProd : Boolean;
  public
    idCentre : integer;
    idRegistre : String;
  end;

var
  Form2: TForm2;

implementation

uses UFuncions, UFValors;

{$R *.dfm}

procedure TForm2.JvXPButton1Click(Sender: TObject);
var Sepudo : boolean;
begin
  SePudo := True;
  if Trim(Edit2.Text) <> '' then msEClinic.Server := Trim(Edit2.Text);

  try
    msEClinic.Connect;
  except
    SePudo := False;
    raise;
  end;

  if SePudo then
  begin
    Edit1.Text := Trim(Edit9.Text);
    // primer busquem per l'ID_REGISTRE a la taula users
{    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('select c.idCentre from user a '+
                'left join usuaris u on a.usuari=u.idUsuari '+
                'left join usuarisCentres c on u.idUsuari=c.idUsuari '+
                'where a.username=:idregistre');
    qE1.ParamByName('idregistre').AsString := idregistre;
    qE1.Execute;
    Edit1.Text := IntToStr(qE1.FieldByName('idCentre').AsInteger);  }
    idCentre := StrToInt(Trim(Edit1.Text));//.FieldByName('idCentre').AsInteger;

    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('SELECT centres.idcentre, centres.dadescentre, IFNULL(CONVERT(AES_DECRYPT( persones.nom ,"9w7kM5GwbJSsRnvS") USING utf8), "") nom '+
                'FROM dasieclinicDB.centres '+
                'left join dasieclinicDB.persones on centres.dadescentre=persones.idPersona '+
                'where centres.idcentre=:centre');
    qE1.ParamByName('centre').AsInteger := idCentre;
    qE1.Execute;
    if qE1.RecordCount > 0 then
    begin
      Label5.Caption := qE1.FieldByName('nom').AsString  + '  TEST  centre ' + IntToStr(idCentre);
      JvXPButton1.Enabled := False;
      JvXPButton3.Enabled := False;
      idCentre := StrToInt(Edit1.Text);
      Botons(Sender);
      EsProd := False;
    end else Showmessage('No existeix aquest centre');
  end else Showmessage('No s''ha pogut connectar amb la base de dades Dest�');
end;

function TForm2.TaulaOK(Taula: string): Boolean;
begin
  qR1.Close;
  qR1.SQL.Clear;
  qR1.SQL.Add('select id from taules_passades where id_centre=:id_centre and taula=:taula');
  qR1.ParamByName('id_centre').AsInteger := idCentre;
  qR1.ParamByName('taula').AsString := Taula;
  qR1.ExecQuery;
  Result := (qR1.RecordCount=0);
end;

procedure TForm2.PassadaOK(Taula: string);
begin
  qR2e.Close;
  qR2e.SQL.Clear;
  qR2e.SQL.Add('insert into taules_passades(id_centre, taula) values(:id_centre,:taula)');
  qR2e.ParamByName('id_centre').AsInteger := idCentre;
  qR2e.ParamByName('taula').AsString := Taula;
  qR2e.ExecQuery;
  Botons2(Self);
end;

procedure TForm2.Botons(Sender: TObject);
begin
  sbReiniciar.Enabled := ((JvXPButton2.Enabled=False) and (JvXPButton1.Enabled=False));
  if sbReiniciar.Enabled then
  begin
    if label2.Caption<>label5.Caption then
    begin
      if ConfirmaN('SEMBLA QUE ELS CENTRES NO SON EL MATEIX CLIENT, CONTINUES?') then Botons2(Sender);
    end else Botons2(Sender);
  end;
end;

procedure TForm2.Botons2(Sender: TObject);
begin
  sbPacients.Enabled := TaulaOK('S_PACIENTS');
  sbOrigens.Enabled := TaulaOK('S_ORIGENES');
  sbAntecedents.Enabled := TaulaOK('ANTECEDENTES');
  sbPoblacions.Enabled := TaulaOK('S_POBLACIONS');
  sbProcedencies.Enabled := TaulaOK('S_PROCEDENCIES');
  sbTipusDoc.Enabled := TaulaOK('TIPUS_DOCUMENT');
  sbTarifesGRP.Enabled := TaulaOK('TARIFES_GRUPS');
  sbTarifes.Enabled := TaulaOK('TARIFES');
  sbEspe.Enabled := TaulaOK('MODULS');
  sbGrupsITtes.Enabled := TaulaOK('TRACTAMENTS');
  sbPreus.Enabled := TaulaOK('TARIFES_TRACTAMENT');
  sbEmpresa.Enabled := TaulaOK('S_EMPRESES');
  sbProf.Enabled := TaulaOK('S_PROFESSIONALS');
  sbHoraris.Enabled := (TaulaOK('DIES_FESTIUS') or TaulaOK('PERIODES') or
                        TaulaOK('S_ESTRUCTURA'));
  sbAbsencies.Enabled := TaulaOK('FESTIUS');
  sbApertures.Enabled := TaulaOK('FESTIUS_EXCEPCIONS');
  sbRelacionsC.Enabled := TaulaOK('S_USUARIS_RC');
  sbPacients.Enabled := TaulaOK('S_PACIENTS');
  sbAntecedentsPac.Enabled := TaulaOK('ANTECEDENTES_PAC');
  sbCites.Enabled := TaulaOK('S_CITACIONS');
  sbEpisodis.Enabled := TaulaOK('EPISODIS');
  sbVisites.Enabled := TaulaOK('VISITES');
  sbUsuaris.Enabled := TaulaOK('S_USUARIS');
  sbRelacionsC.Enabled := TaulaOK('S_USUARIS_RC');
  sbTipusCita.Enabled := TaulaOK('TIPUS_CITA');
  sbBoxes.Enabled := TaulaOK('S_BOXES');
  sbCies.Enabled := TaulaOK('CIES');
  sbCarrecs.Enabled := TaulaOK('CARREGS');
  sbFraMutues.Enabled := TaulaOK('TARIFES_FACTURES');
  sbCobros.Enabled := TaulaOK('COBRAMENTS');
  sbCobMutua.Enabled := TaulaOK('TARIFES_COBRAMENTS');
  sbInformes.Enabled := TaulaOK('INFORMES');
  sbEntitats.Enabled := TaulaOK('S_ENTITATS');
  sbBancs.Enabled := TaulaOK('BANCS');
  sbCaixes.Enabled := TaulaOK('MOVIMIENTOS');
  sbFP.Enabled := TaulaOK('FORMAS_PAGO');
  sbConConta.Enabled := TaulaOK('CONCEPTES');
  sbTutors.Enabled := TaulaOK('S_TUTORS');
  sbClasImg.Enabled := TaulaOK('CLASSIFICACIONS_IMG');
  sbImatgesDocs.Enabled := TaulaOK('IMATGES_MINIATURES');
  sbBonus.Enabled := TaulaOK('S_BONUS');
end;

procedure TForm2.Button1Click(Sender: TObject);
var idBono, idVisitaTra : integer;
begin
  tD.Close;
  tD.SelectSQL.Clear;
  tD.SelectSQL.Add('select bonregistre, trvregistre from visites_tractaments where bonregistre>0');
  tD.Open;
  while not tD.Eof do
  begin
    // cerca BONUS a taula de relacions_centres
    qR1.Close;
    qR1.SQL.Clear;
    qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TRACTAMENTS_BONUS'' and D_ID=:bonregistre and e_Centre=:idCentre');
    qR1.ParamByName('idcentre').AsInteger := 13892;
    qR1.ParamByName('bonregistre').AsInteger := tD.FieldByName('bonregistre').AsInteger;
    qR1.ExecQuery;
    if qR1.RecordCount = 0 then idBono := -1
      else idBono := qR1.FieldByName('E_ID').AsVariant;

    // cerca TRVREGISTRE a taula de relacions_centres
    qR1.Close;
    qR1.SQL.Clear;
    qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''VISITES_TRACTAMENTS'' and D_ID=:trvregistre and e_Centre=:idCentre');
    qR1.ParamByName('idcentre').AsInteger := 13892;
    qR1.ParamByName('trvregistre').AsInteger := tD.FieldByName('trvregistre').AsInteger;
    qR1.ExecQuery;
    if qR1.RecordCount = 0 then idVisitaTra := -1
      else idVisitaTra := qR1.FieldByName('E_ID').AsVariant;

    // actualitzem visita_tractament
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('update visitesTractaments set bonus=:bonus where id=:idVisitaTra and traspas=:traspas');
    qE1.ParamByName('idVisitaTra').AsInteger := idVisitaTra;
    qE1.ParamByName('bonus').AsInteger := idBono;
    qE1.ParamByName('traspas').AsString := 'TRASPAS_08516Y10A0013892';
    qE1.ExecSQL;

    tD.Next;
  end;
  tD.Close;
{
  try

    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('select idPacient, tarifaPreferida from pacients where idCentre=:idCentre');
    qE1.ParamByName('idCentre').AsInteger := idCentre;
    qE1.ExecSQL ;
    while not qE1.Eof do
    begin
      qE2.Close;
      qE2.SQL.Clear;
      qE2.SQL.Add('INSERT INTO pacientsTarifaMutua( idPacient, idTarifaMutua) '+
                  'VALUES( :idPacient, :idTarifaMutua)');
      qE2.ParamByName('idTarifaMutua').AsInteger := qE1.FieldByName('tarifaPreferida').AsInteger;
      qE2.ParamByName('idPacient').AsInteger := qE1.FieldByName('idPacient').AsInteger;
      qE2.ExecSQL ;

      qE1.Next;
    end;
  except
    raise;
  end;}
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  FValors.ValorsApp.Idioma := 0;
  EsProd := False;
end;

procedure TForm2.JvXPButton2Click(Sender: TObject);
var Sepudo : boolean;
begin
  SePudo := True;
  if Trim(Edit3.Text) <> '' then FibDades.DatabaseName := Edit3.Text;

  try
    FibDades.Open;
    FIBPrincipal.StartTransaction;
    try
      pFIBRelacions.Open;
      pFIBTrans_Relacions.StartTransaction;
    except
      SePudo := False;
      raise;
    end;
  except
    SePudo := False;
    raise;
  end;

  if SePudo then
  begin
    qD1.Close;
    qD1.SQL.Clear;
    qD1.SQL.Add('select nomclinica from parametres where terminal=1');
    qD1.ExecQuery;
    Label2.Caption := qD1.FieldByName('nomclinica').AsString;

    qD1.Close;
    qD1.SQL.Clear;
    qD1.SQL.Add('select id_registro from ut_dasi where master=1');
    qD1.ExecQuery;
    Edit4.Text := qD1.FieldByName('id_registro').AsString;
    idRegistre := qD1.FieldByName('id_registro').AsString;

    JvXPButton2.Enabled := False;
    Botons(Sender);
  end else Showmessage('No s''ha pogut connectar amb la base de dades Origen');
end;

procedure TForm2.JvXPButton3Click(Sender: TObject);
var Sepudo : boolean;
begin
  SePudo := True;
  if Trim(Edit7.Text) <> '' then msEClinic.Server := Trim(Edit7.Text);

  try
    msEClinic.Connect;
  except
    SePudo := False;
    raise;
  end;

  if SePudo then
  begin
    Edit1.Text := Trim(Edit8.Text);
    // primer busquem per l'ID_REGISTRE a la taula users
{    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('select c.idCentre from user a '+
                'left join usuaris u on a.usuari=u.idUsuari '+
                'left join usuarisCentres c on u.idUsuari=c.idUsuari '+
                'where a.username=:idregistre');
    qE1.ParamByName('idregistre').AsString := idregistre;
    qE1.Execute;
    Edit1.Text := IntToStr(qE1.FieldByName('idCentre').AsInteger);  }
    idCentre := StrToInt(Trim(Edit8.Text));//.FieldByName('idCentre').AsInteger;

    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('SELECT centres.idcentre, centres.dadescentre, IFNULL(CONVERT(AES_DECRYPT( persones.nom ,"9w7kM5GwbJSsRnvS") USING utf8), "") nom '+
                'FROM dasieclinicDB.centres '+
                'left join dasieclinicDB.persones on centres.dadescentre=persones.idPersona '+
                'where centres.idcentre=:centre');
    qE1.ParamByName('centre').AsInteger := idCentre;
    qE1.Execute;
    if qE1.RecordCount > 0 then
    begin
      Label5.Caption := qE1.FieldByName('nom').AsString + '  PRODUCCIO  centre ' + IntToStr(idCentre);
      JvXPButton1.Enabled := False;
      JvXPButton3.Enabled := False;
      idCentre := StrToInt(Edit8.Text);
      Botons(Sender);
      EsProd := True;

      // agafem id max d'imatges
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select max(d_id) registre from relacions_centre where e_centre=:idCentre and e_taula=''arxiusdoc'' ');
      qR1.ParamByName('idcentre').AsInteger := idCentre;
      qR1.ExecQuery;
      Edit5.Text := IntToStr(qR1.FieldByName('registre').AsInteger);
    end else Showmessage('No existeix aquest centre');
  end else Showmessage('No s''ha pogut connectar amb la base de dades Dest�');
end;

procedure TForm2.sbEntitatsClick(Sender: TObject);
begin
  // ENTITATS
  // esborrem entitats creades del centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''S_ENTITATS'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // S'HA DE MIRAR PER IDCENTRE + DASICLINICREG>0
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from dasieclinicDB.bancEntitat where idcentre=:idcentre');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from s_entitats where entregistre>0 and '+
                     'entregistre in (select entregistre from bancs) order by entcodi');
    tD.Open;
    while not tD.Eof do
    begin
      // crear ENTITAT
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO dasieclinicDB.bancEntitat( entitat, nom, idCentre, bic) '+
                  'VALUES( :entitat, :nom, :idCentre, :bic)');
      qE1.ParamByName('entitat').AsString := tD.FieldByName('ENTCODI').AsString;
      qE1.ParamByName('nom').AsString := tD.FieldByName('ENTNOM').AsString;
      qE1.ParamByName('bic').AsString := tD.FieldByName('ENTBIC').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_ENTITATS'',:D_ID,''bancEntitat'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('ENTREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := StrToInt(tD.FieldByName('ENTCODI').AsString);
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbEntitats.Enabled := False;
    PassadaOK('S_ENTITATS');
  except
    raise;
  end;
  tD.Close;
end;

procedure TForm2.sbBancsClick(Sender: TObject);
var idEntitat, idEmpresa, idBancCompte : integer;
begin
  // BANCS
  // esborrem BANCS creats de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''BANCS'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // S'HA DE MIRAR PER IDCENTRE + DASICLINICREG>0
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from dasieclinicDB.bancCompte where idcentre=:idcentre and dasiclinicReg>0');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from bancs where banregistre>0 order by banregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca ENTITAT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_ENTITATS'' and D_ID=:entregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('entregistre').AsInteger := tD.FieldByName('entregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idEntitat := -1
        else idEntitat := qR1.FieldByName('E_ID').AsVariant;

      // cerca EMPRESA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_EMPRESES'' and D_ID=:empregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('empregistre').AsInteger := tD.FieldByName('empregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idEmpresa := -1
        else idEmpresa := qR1.FieldByName('E_ID').AsVariant;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO dasieclinicDB.bancCompte( entitat, idBancCompte, nom,'+
                  'iban, ccontable, idCentre, ccost, dasiclinicReg, idEmpresa) '+
                  'VALUES( :entitat, :idBancCompte, :nom, :iban, :ccontable, :idCentre,'+
                  ':ccost, :dasiclinicReg, :idEmpresa)');
      if idEntitat > 0 then
        qE1.ParamByName('entitat').AsString := StrZero(idEntitat,4);
      qE1.ParamByName('nom').AsString := tD.FieldByName('DESCRIPCIO').AsString;
      qE1.ParamByName('iban').AsString := tD.FieldByName('IBAN').AsString;
      qE1.ParamByName('ccontable').AsString := tD.FieldByName('CUENTA').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('ccost').AsString := tD.FieldByName('CCOSTE').AsString;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('banregistre').AsInteger;
      if idEmpresa > 0 then
        qE1.ParamByName('idEmpresa').AsInteger := idEmpresa;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idBancCompte := qE1.FieldByName('registre').AsInteger;


      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''BANCS'',:D_ID,''bancCompte'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('banregistre').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idBancCompte;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbBancs.Enabled := False;
    PassadaOK('BANCS');
  except
    raise;
  end;
  tD.Close;

end;

procedure TForm2.sbCaixesClick(Sender: TObject);
var idMoviment, idBancCompte, idProfessional, idConcepteCont : integer;
begin
  // CAIXES
  // esborrem CAIXES creades de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''MOVIMIENTOS'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // S'HA DE MIRAR PER IDCENTRE + DASICLINICREG>0
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from dasieclinicDB.moviment where idcentre=:idcentre and dasiclinicReg>0');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from movimientos where movregistre>0 order by data, movregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca BANC a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''BANCS'' and D_ID=:banregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('banregistre').AsInteger := tD.FieldByName('banregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idBancCompte := -1
        else idBancCompte := qR1.FieldByName('E_ID').AsVariant;

      // cerca PROFESSIONAL a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idProfessional := -1
        else idProfessional := qR1.FieldByName('E_ID').AsVariant;

      // cerca CONCEPTE a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''CONCEPTES'' and D_ID=:conregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('conregistre').AsInteger := tD.FieldByName('conregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idConcepteCont := -1
        else idConcepteCont := qR1.FieldByName('E_ID').AsVariant;

      // crear MOVIMENT
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO dasieclinicDB.moviment( idMoviment, data, ultmodifica,'+
                  'textmov, import, debe, haber, exercici, mes, idConcepteCont, idBancCompte,'+
                  'idProfessional, idCentre, dasiclinicReg, traspas) VALUES( :idMoviment,'+
                  ':data, :ultmodifica, :textmov, :import, :debe, :haber, :exercici, :mes,'+
                  ':idConcepteCont, :idBancCompte, :idProfessional, :idCentre, :dasiclinicReg,'+
                  ':traspas)');
      qE1.ParamByName('data').AsDateTime := tD.FieldByName('DATA').AsDateTime;
      qE1.ParamByName('textmov').AsString := tD.FieldByName('TEXTO').AsString;
      qE1.ParamByName('import').AsFloat := tD.FieldByName('IMPORTE').AsFloat;
      qE1.ParamByName('debe').AsFloat := tD.FieldByName('DEBE').AsFloat;
      qE1.ParamByName('haber').AsFloat := tD.FieldByName('HABER').AsFloat;
      qE1.ParamByName('exercici').AsInteger := tD.FieldByName('EXERCICI').AsInteger;
      qE1.ParamByName('mes').AsInteger := tD.FieldByName('MES').AsInteger;
      if idConcepteCont > 0 then
        qE1.ParamByName('idConcepteCont').AsInteger := idConcepteCont;
      if idBancCompte > 0 then
        qE1.ParamByName('idBancCompte').AsInteger := idBancCompte;
      if idProfessional > 0 then
        qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('DasiclinicReg').AsInteger := tD.FieldByName('movregistre').AsInteger;
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idMoviment := qE1.FieldByName('registre').AsInteger;

      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from COBRAMENTS where cierre=:cierre order by cobregistre');
      tD1.ParamByName('cierre').AsInteger := tD.FieldByName('movregistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('update dasieclinicDB.cobrament set moviment=:moviment '+
                    'where dasiclinicReg=:dasiclinicReg and idCentre=:idCentre');
        qE1.ParamByName('moviment').AsInteger := idMoviment;
        qE1.ParamByName('DasiclinicReg').AsInteger := tD1.FieldByName('cobregistre').AsInteger;
        qE1.ParamByName('idCentre').AsInteger := idCentre;
        qE1.ExecSQL ;

        tD1.Next;
      end;
      tD1.Close;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''MOVIMIENTOS'',:D_ID,''moviment'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('MOVREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idMoviment;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbCaixes.Enabled := False;
    PassadaOK('MOVIMIENTOS');
  except
    raise;
  end;
  tD.Close;
end;

procedure TForm2.sbCarrecsClick(Sender: TObject);
var idProfessional, idPacient, idTarifaMutua, idEmpresa, idCarrec, idTutor, idVisitaTractament,
    idFra : integer;
    tipusCarrec :string;
begin
  // CARRECS
  // esborrem CARRECS creats de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''CARRECS'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // COMPTE NO EXISTEIX EL CAMP TRASPASS... S'HA DE MIRAR PER IDCENTRE + DASICLINICREG>0
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from dasieclinicDB.carrec where idcentre=:idcentre and dasiclinicReg>0');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;

    // ESBORREM FACTURES PERQUE NO FA EL CASCADE
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from dasieclinicDB.fra where traspas=:traspas and dasiclinicreg>0');
    qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
//    tD.SelectSQL.Add('select * from carregs where carregistre>0 and anticipo=0 and reintegro=0 '+
//                     'order by carregistre');
    tD.SelectSQL.Add('select * from carregs where carregistre>0 order by carregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca PROFESSIONAL a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idProfessional := -1
        else idProfessional := qR1.FieldByName('E_ID').AsVariant;

      // cerca PACIENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and D_ID=:pacregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idPacient := -1
        else idPacient := qR1.FieldByName('E_ID').AsVariant;

      // cerca TARIFA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES'' and D_ID=:trfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('trfregistre').AsInteger := tD.FieldByName('trfregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idTarifaMutua := -1
        else idTarifaMutua := qR1.FieldByName('E_ID').AsVariant;

      // cerca EMPRESA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_EMPRESES'' and D_ID=:empregistre and e_centre=:idcentre');
      qR1.ParamByName('empregistre').AsInteger := tD.FieldByName('empregistre').AsInteger;
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idEmpresa := -1
        else idEmpresa := qR1.FieldByName('E_ID').AsVariant;

      // cerca a eClinic el idClient del tutor si el carrec es facturar al tutor
      if tD.FieldByName('TUTOR').AsInteger > 0 then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select idClient from clients where dasiclinicReg=:Registre '+
                    'and traspas=:traspas');
        qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
        qE1.ParamByName('Registre').AsInteger := tD.FieldByName('empregistre').AsInteger;
        qE1.ExecSQL ;
        if qE1.RecordCount = 0 then idTutor := -1
          else idTutor := qE1.FieldByName('idClient').AsInteger;
      end else idTutor := -1;

      // tipus de CARREC estat 0 - carrec, 1 - factura, 2 - mutua, 3 - bestreta, 4 - abonament, 5 - reintegrament
      case tD.FieldByName('ESTAT').AsInteger of
        0 : tipusCarrec := 'CAR';
        1 : tipusCarrec := 'CAR';     // son factures
        2 : tipusCarrec := 'MUT';
        3 : tipusCarrec := 'ANT';
        4 : tipusCarrec := 'ABO';
        5 : tipusCarrec := 'RET';
      end;

      // crear CARREC
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO dasieclinicDB.carrec( idCarrec, tipusCarrec, codi,'+
                  'import, totalBase, totalImpost, baseExenta, conta, dataCarrec,'+
                  'dataDoc, exercici, mes, deposito, costMaterial, totalLiquidacio,'+
                  'liquidat, totalCobrat, pendentCobrar, diposit, idPacient,'+
                  'idProfessional, idTarifaMutua, idCentre, idFra, client, empresa,'+
                  'grupTractaments, irpf, export, dasiclinicReg, concepte, contracteSoci,'+
                  'idFormaPagamentClient, quota, percIRPF, netMutua) VALUES( :idCarrec,'+
                  ':tipusCarrec, :codi, :import, :totalBase, :totalImpost, :baseExenta,'+
                  ':conta, :dataCarrec, :dataDoc, :exercici, :mes, :deposito, :costMaterial,'+
                  ':totalLiquidacio, :liquidat, :totalCobrat, :pendentCobrar, :diposit,'+
                  ':idPacient, :idProfessional, :idTarifaMutua, :idCentre, :idFra,'+
                  ':client, :empresa, :grupTractaments, :irpf, :export, :dasiclinicReg,'+
                  ':concepte, :contracteSoci, :idFormaPagamentClient, :quota, :percIRPF,'+
                  ':netMutua)');
      qE1.ParamByName('tipusCarrec').AsString := tipusCarrec;
      qE1.ParamByName('codi').AsString := tD.FieldByName('CODI').AsString;
      qE1.ParamByName('import').AsFloat := tD.FieldByName('total').AsFloat;
      qE1.ParamByName('totalBase').AsFloat := tD.FieldByName('TOTAL_BASE').AsFloat;
      qE1.ParamByName('totalImpost').AsFloat := tD.FieldByName('TOTAL_IVA').AsFloat;
      qE1.ParamByName('baseExenta').AsFloat := tD.FieldByName('BASE_EXENTA').AsFloat;
      qE1.ParamByName('conta').AsInteger := tD.FieldByName('conta').AsInteger;
      qE1.ParamByName('dataCarrec').AsDateTime := tD.FieldByName('CARDATA').AsDateTime;
      qE1.ParamByName('dataDoc').AsDateTime := tD.FieldByName('FRADATA').AsDateTime;
      qE1.ParamByName('exercici').AsInteger := tD.FieldByName('EXERCICI').AsInteger;
      qE1.ParamByName('mes').AsInteger := DameMes(tD.FieldByName('FRADATA').AsDateTime);
      qE1.ParamByName('deposito').AsFloat := tD.FieldByName('deposito').AsFloat;
      qE1.ParamByName('costMaterial').AsFloat := tD.FieldByName('COST_MATERIAL').AsFloat;
//      qE1.ParamByName('totalLiquidacio').AsFloat := tD.FieldByName('TOTAL_LIQUIDACIO').AsFloat;  es passa a visites tractaments
      if tD.FieldByName('LIQUIDAT').AsString = '1' then
        qE1.ParamByName('liquidat').AsInteger := 1;
      qE1.ParamByName('totalCobrat').AsFloat := 0; //                                 'TOTAL_COBRAT'   L'ACTUALITZARE AL FER EL COBRAMENT
      qE1.ParamByName('pendentCobrar').AsFloat := tD.FieldByName('total').AsFloat; // 'PENDENT_COBRAR' IGUAL
      qE1.ParamByName('diposit').AsFloat := tD.FieldByName('DEPOSITO').AsFloat;  // en carrecs i abonaments sempre ser� 0
      qE1.ParamByName('idPacient').AsInteger := idPacient;
      if idProfessional > 0 then
        qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      if idTarifaMutua > 0 then
        qE1.ParamByName('idTarifaMutua').AsInteger := idTarifaMutua;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      // idFra            l'actualitzarem despr�s si generem una factura
      if idTutor > 0 then
        qE1.ParamByName('client').AsInteger := idTutor;
      if idEmpresa > 0 then
        qE1.ParamByName('empresa').AsInteger := idEmpresa;
      // grupTractaments  no se perque es fa servir
      // irpf             aix� ha d'anar a la taula fra
      // export           no es traspassa
      qE1.ParamByName('DasiclinicReg').AsInteger := tD.FieldByName('carregistre').AsInteger;
      // concepte         no se perque es fa servir
      // contracteSoci          es tema socis no tenim aquest valor
      // idFormaPagamentClient  es tema socis no tenim aquest valor
      // quota                  es tema socis no tenim aquest valor
      // percIRPF               aix� ha d'anar a la taula fra
      // netMutua               aix� no ha d'anar en lloc
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idCarrec := qE1.FieldByName('registre').AsInteger;

      // Marquem totes les visites del carrec i Insertem relacio visitesTractamentCarrec
      if tD.FieldByName('carregistre').AsInteger > 0 then
      begin
        tD1.Close;
        tD1.SelectSQL.Clear;
        tD1.SelectSQL.Add('select trvregistre from visites_tractaments where carregistre=:carrec '+
                          'or carregistre_mutua=:carrec');
        tD1.ParamByName('carrec').AsInteger := tD.FieldByName('carregistre').AsInteger;
        tD1.Open;
        while not tD1.Eof do
        begin
          // agafem el idVisita
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('select id from visitesTractaments where dasiclinicReg=:registre and '+
                      'traspas=:traspas');
          qE1.ParamByName('registre').AsInteger := tD1.FieldByName('trvregistre').AsInteger;
          qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
          qE1.ExecSQL ;
          idVisitaTractament := qE1.FieldByName('id').AsInteger;

          if idVisitaTractament > 0 then
          begin
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO visitesTractamentsCarrecs(idVisitaTractament, idCarrec) '+
                        'VALUES(:idVisitaTractament, :idCarrec)');
            qE1.ParamByName('idVisitaTractament').AsInteger := idVisitaTractament;
            qE1.ParamByName('idCarrec').AsInteger := idCarrec;
            qE1.ExecSQL ;
          end;
          tD1.Next;
        end;
      end;

      // si tenim FRAREGISTRE hem de mirar si ja existeix la factura i si no hi es
      // crear-la en aquest cas de privats
      // fins que es modifiqui partim de la base que nomes tenim un impost AIXO S'HA DE REPLANTEJAR
      if tD.FieldByName('FRAREGISTRE').AsInteger > 0 then
      begin
        idFra := 0;
        // Mirem si existeix per el codi (de moment a eClinic solament tenim aquest camp) i traspas
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select idFra from dasieclinicDB.fra where codi=:codi and '+
                    'traspas=:traspas');
        qE1.ParamByName('codi').AsString := tD.FieldByName('CODI').AsString;
        qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
        qE1.ExecSQL ;
        if qE1.RecordCount = 0 then
        begin
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('INSERT INTO dasieclinicDB.fra( idFra, codi, irpfPer, irpfImport,'+
                      'import, total, cobrat, importCobrat, proforma, dataDoc,'+
                      'notaPeu, groupTrac, periodeIni, periodeFi, observacions, dasiclinicReg,'+
                      'rebut, traspas, base, totalImpost) VALUES( :idFra, :codi, :irpfPer,'+
                      ':irpfImport, :import, :total, :cobrat, :importCobrat, :proforma,'+
                      ':dataDoc, :notaPeu, :groupTrac, :periodeIni, :periodeFi, :observacions,'+
                      ':dasiclinicReg, :rebut, :traspas, :base, :totalImpost)');
          qE1.ParamByName('codi').AsString := tD.FieldByName('CODI').AsString;
          qE1.ParamByName('irpfPer').AsFloat := 0;    // A PRIVATS NO HI HA IRPF
          qE1.ParamByName('irpfImport').AsFloat := 0; // A PRIVATS NO HI HA IRPF
          qE1.ParamByName('import').AsFloat := tD.FieldByName('total').AsFloat;
          qE1.ParamByName('cobrat').AsFloat := 0;         // ES MARCARA AL FER EL COBRAMENT
          qE1.ParamByName('importCobrat').AsInteger := 0; // ES MARCARA AL FER EL COBRAMENT
          qE1.ParamByName('dataDoc').AsDateTime := tD.FieldByName('FRADATA').AsDateTime;
          qE1.ParamByName('groupTrac').AsInteger := 0; // NO TE VALOR AL DASICLINIC
          qE1.ParamByName('dasiclinicReg').AsInteger :=  tD.FieldByName('FRAREGISTRE').AsInteger;
          qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
          qE1.ParamByName('base').AsFloat := tD.FieldByName('TOTAL_BASE').AsFloat;
          qE1.ParamByName('totalImpost').AsFloat := tD.FieldByName('TOTAL_IVA').AsFloat;
          qE1.ExecSQL ;

          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
          qE1.ExecSQL ;
          idFra := qE1.FieldByName('registre').AsInteger;
        end else idFra := qE1.FieldByName('idFra').AsInteger;

        // actualitzem el idFra a la taula carrec
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('update dasieclinicDB.carrec set idFra=:idFra where idCarrec=:idCarrec');
        qE1.ParamByName('idFra').AsInteger := idFra;
        qE1.ParamByName('idCarrec').AsInteger := idCarrec;
        qE1.ExecSQL ;
      end;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''CARREGS'',:D_ID,''carrec'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('CARREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idCarrec;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbCarrecs.Enabled := False;
    PassadaOK('CARREGS');
  except
    raise;
  end;
  tD.Close;
end;

procedure TForm2.sbCiesClick(Sender: TObject);
var registre : integer;
begin
  //  aquest es especial per un client
//  try
    // esborrem els boxes que tingui el centre
{    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from ciesCentre where idCentre=:idCentre');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;   }

{    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from A_CIES');  // s'haura de revisar
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO ciesCentre (id, tipus, nom, codi, idCentre, grup,'+
                  'idIdioma) VALUES( :id, :tipus, :nom, :codi, :idCentre, :grup, :idIdioma)');
      qE1.ParamByName('tipus').AsString := 'D';
      qE1.ParamByName('codi').AsString := tD.FieldByName('codi').AsString;
      qE1.ParamByName('nom').AsString := tD.FieldByName('nom').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('idIdioma').AsInteger := 2;// 2 castell�, 3 catal�
      qE1.ExecSQL ;       }

{      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_BOXES'',:D_ID,''boxes'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('boxregistre').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;
}
{      tD.Next;
    end;
    sbCies.Enabled := False;
    PassadaOK('CIES');
  except
    raise;
  end; }

end;

procedure TForm2.sbBonusClick(Sender: TObject);
var idPacient, Registre, idTractament : Integer;
begin
  // BONOS
  try
    // esborrem els bonos que tingui el centre va amb TRASPAS
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''TRACTAMENTS_BONUS'' and e_Centre=:idCentre '+
                   'and D_ID>0');    //////////  HA DE SER ZERO
      qR2e.ParamByName('idcentre').AsInteger := idcentre;
      qR2e.ExecQuery;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from bonus where traspas=:traspas and dasiclinicreg>0');
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ExecSQL ;
    end;
  except
    raise;
  end;

  // generem bonos
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from TRACTAMENTS_BONUS');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca PACIENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and '+
                  'D_ID=:pacregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount > 0 then
      begin
        idPacient := qR1.FieldByName('E_ID').AsVariant;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO bonus (id, dataCreat, dataIniciat, consumides, sessions,'+
                    'dataCancelat, motiuCancelat, idPacient,  dasiclinicReg, traspas) '+
                    'VALUES( :id, :dataCreat, :dataIniciat, :consumides, :sessions,'+
                    ':dataCancelat, :motiuCancelat, :idPacient, :dasiclinicReg, :traspas)');
        qE1.ParamByName('dataCreat').AsDateTime := tD.FieldByName('INICIAT').AsDateTime;
        qE1.ParamByName('dataIniciat').AsDateTime := tD.FieldByName('INICIAT').AsDateTime;
        qE1.ParamByName('consumides').AsInteger := tD.FieldByName('SESSIONS_UTILITZADES').AsInteger;
        qE1.ParamByName('sessions').AsInteger := tD.FieldByName('SESSIONS_INICIALS').AsInteger;
        if not tD.FieldByName('CANCELAT').IsNull then
          qE1.ParamByName('dataCancelat').AsDateTime := tD.FieldByName('CANCELAT').AsDateTime;
        if not tD.FieldByName('CANCELAT').IsNull then
          qE1.ParamByName('motiuCancelat').AsString := tD.FieldByName('CANCELAT_MOTIU').AsString;
        qE1.ParamByName('idPacient').AsInteger := idPacient;
        qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('BONREGISTRE').AsInteger;
        qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
        qE1.ExecSQL ;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        Registre := qE1.FieldByName('registre').AsInteger;

        // generem relacio bonus-tractament
        // cerca TRACTAMENT a taula de relacions_centres
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TRACTAMENTS'' and D_ID=:traregistre and e_Centre=:idCentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('traregistre').AsInteger := tD.FieldByName('traregistre').AsInteger;
        qR1.ExecQuery;
        if qR1.RecordCount > 0 then
        begin
          idTractament := qR1.FieldByName('E_ID').AsVariant;

          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('update tractaments set bonus=:bonus where idTractament=:idTractament and '+
                      'idCentre=:idCentre');
          qE1.ParamByName('bonus').AsInteger := Registre;
          qE1.ParamByName('idTractament').AsInteger := idTractament;
          qE1.ParamByName('idCentre').AsInteger := idCentre;
          qE1.ExecSQL ;
        end;

        // generem realcions bonus compartits si ni ha
        tD1.Close;
        tD1.SelectSQL.Clear;
        tD1.SelectSQL.Add('select pacregistre from TRACTAMENTS_BONUS_COMP where bonregistre=:bonregistre');
        tD1.ParamByName('bonregistre').AsInteger := tD.FieldByName('BONREGISTRE').AsInteger;
        tD1.Open;
        while not tD1.Eof do
        begin
          // cerca PACIENT a taula de relacions_centres
          qR1.Close;
          qR1.SQL.Clear;
          qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and '+
                      'D_ID=:pacregistre and e_Centre=:idCentre');
          qR1.ParamByName('idcentre').AsInteger := idcentre;
          qR1.ParamByName('pacregistre').AsInteger := tD1.FieldByName('pacregistre').AsInteger;
          qR1.ExecQuery;
          if qR1.RecordCount > 0 then
          begin
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO bonusPacients (bonus_id, idPacient) VALUES( :bonus_id, :idPacient)');
            qE1.ParamByName('consumides').AsInteger := Registre;
            qE1.ParamByName('idPacient').AsInteger := qR1.FieldByName('E_ID').AsInteger;
            qE1.ExecSQL ;
          end;
          tD1.Next;
        end;

        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''TRACTAMENTS_BONUS'',:D_ID,''bonus'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('bonregistre').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := registre;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;
      end;
      tD.Next;
    end;
    sbBonus.Enabled := False;
    PassadaOK('S_BONUS');
  except
    raise;
  end;
end;

procedure TForm2.sbBoxesClick(Sender: TObject);
var registre : integer;
begin
  // BOXES
  try
    // esborrem els boxes que tingui el centre
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from boxes where idCentre=:idCentre');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;
  except
    raise;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from S_BOXES');
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO boxes (idBox, actiu, especific, nom, codi, ordre,'+
                  'idCentre, dasiclinicReg) VALUES( :idBox, 1, 0, :nom, :codi, :ordre,'+
                  ':idCentre, :dasiclinicReg)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('BOXDESCRIPCION').AsString;
      qE1.ParamByName('codi').AsString := tD.FieldByName('BOXNOM').AsString;
      qE1.ParamByName('ordre').AsInteger := tD.FieldByName('BOXORDRE').AsInteger;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('BOXREGISTRE').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_BOXES'',:D_ID,''boxes'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('boxregistre').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbBoxes.Enabled := False;
    PassadaOK('S_BOXES');
  except
    raise;
  end;
end;

procedure TForm2.NetHTTPClient1ValidateServerCertificate(const Sender: TObject;
  const ARequest: TURLRequest; const Certificate: TCertificate;
  var Accepted: Boolean);
begin
  Accepted := True;
end;

procedure TForm2.sbProfClick(Sender: TObject);
var registre, idAdreca, idEmail, idPersona, idTarifaMutua, idPeriodeEclinic, idTipusDocument,
    idEmpresa, idEspecialitat, idProfessional, idCategoria : integer;
    pais, cognom1, cognom2, cadena : string;
    idZona : Variant;
begin
  // PROFESSIONALS
  // esborrem professionals creats del centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // S'HA DE MIRAR PER IDCENTRE + DASICLINICREG>0
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from dasieclinicDB.professionals where idcentre=:idcentre and dasiclinicReg>0');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from s_professionals where prfregistre>0');
    tD.Open;
    while not tD.Eof do
    begin
      Cadena := tD.FieldByName('PRFCOGNOMS').AsString;
      Cognom1 := Cognoms(Cadena, ' ');
      Cognom2 := Cadena;

      // crear persona + mails + adreces
      // buscar poblacio->zona a taula de relacions
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from relacions where d_taula=''S_POBLACIONS'' and D_ID=:pobregistre');
      qR1.ParamByName('pobregistre').AsInteger := tD.FieldByName('POBREGISTRE').AsInteger;
      qR1.ExecQuery;
      IDZona := qR1.FieldByName('E_ID').AsVariant;

      // cercar pais de la zona a MySql
      if idZona >= 0 then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select pais from zones where idzona=:idzona');
        qE1.ParamByName('idzona').AsInteger := idzona;
        qE1.ExecSQL ;
        Pais := qE1.FieldByName('pais').AsString;
      end;

      // cercar TipusDocument
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select idTipus from dasieclinicDB.tipusDocument where idcentre=:idcentre and nom like :tipus');
      qE1.ParamByName('idcentre').AsInteger := idcentre;
      qE1.ParamByName('tipus').AsString := '%DNI%';
      qE1.ExecSQL ;
      idTipusDocument := qE1.FieldByName('idTipus').AsInteger;

      // cerca ESPECIALITAT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''MODULS'' and D_ID=:modregistre and e_centre=:idcentre');
      qR1.ParamByName('modregistre').AsInteger := tD.FieldByName('modregistre').AsInteger;
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ExecQuery;
      idEspecialitat := qR1.FieldByName('E_ID').AsVariant;

      // CREAR adre�a
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO adreces (idAdreca, direccio1, cp, pais, idZona) '+
                  'VALUES(:idAdreca, AES_ENCRYPT(:direccio1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:cp,"9w7kM5GwbJSsRnvS"), :pais, :idzona)');
      qE1.ParamByName('direccio1').AsString := tD.FieldByName('PRFADRECA').AsString;
      qE1.ParamByName('cp').AsString := tD.FieldByName('PRFCCP').AsString;
      qE1.ParamByName('pais').AsString := pais;
      if idZona >= 0 then qE1.ParamByName('idZona').AsInteger := idZona;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idAdreca := qE1.FieldByName('registre').AsInteger;

      // CREAR email
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO emails (idEmail, email) VALUES(:idEmail, AES_ENCRYPT(:email,"9w7kM5GwbJSsRnvS"))');
      qE1.ParamByName('email').AsString := tD.FieldByName('PRFEMAIL').AsString;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idEmail := qE1.FieldByName('registre').AsInteger;

      // crear persona
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO persones (idPersona, nom, nomComplert, numDocument,'+
                  'telefon1, telefon2, cognom1, cognom2, idAdreca, idEmail, TipusDocument) '+
                  'VALUES(:idPersona, AES_ENCRYPT(:nom,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:nomComplert,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:numDocument,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon2,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:cognom1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:cognom2,"9w7kM5GwbJSsRnvS"), :idAdreca, :idEmail, :TipusDocument)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('PRFNOM').AsString;
      qE1.ParamByName('cognom1').AsString := cognom1;
      qE1.ParamByName('cognom2').AsString := cognom2;
      qE1.ParamByName('nomComplert').AsString := Trim(Trim(cognom1) + ' ' + Trim(cognom2)) + ', ' + tD.FieldByName('PRFNOM').AsString;
      qE1.ParamByName('numDocument').AsString := tD.FieldByName('DNI').AsString;
      qE1.ParamByName('telefon1').AsString := tD.FieldByName('PRFTELEFON1').AsString;
      qE1.ParamByName('telefon2').AsString := tD.FieldByName('PRFTELEFONS2').AsString;
      qE1.ParamByName('TipusDocument').AsInteger := idTipusDocument;
      qE1.ParamByName('idAdreca').AsInteger := idAdreca;
      qE1.ParamByName('idEmail').AsInteger := idEmail;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idPersona := qE1.FieldByName('registre').AsInteger;

      // crear PROFESSIONAL
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO professionals (idProfessional, dataAlta, actiu, acronim,'+
                  'numColegiat, observacions, idCentre, idPersona, isProfessionalDemo,'+
                  'dasiclinicId, dasiclinicReg, obsAgenda, color_agenda, alertAgenda,'+
                  'veureAgendaEnWidget, mateixesTarifesCentre) '+
                  'VALUES(:idProfessional, :dataAlta, :actiu, :acronim, :numColegiat,'+
                  ':observacions, :idCentre, :idPersona, 0, :dasiclinicId, :dasiclinicReg,'+
                  ':obsAgenda, :color_agenda, 0, 0, 0)');
      qE1.ParamByName('dataAlta').AsDateTime := Now;
      qE1.ParamByName('actiu').AsInteger := tD.FieldByName('ACTIVO').AsInteger;
      qE1.ParamByName('acronim').AsString := tD.FieldByName('PRFCODI').AsString;
      qE1.ParamByName('numColegiat').AsString := tD.FieldByName('COLEGIADO').AsString;
      qE1.ParamByName('observacions').AsString := tD.FieldByName('PRFOBSERVACIONS').AsString;
      qE1.ParamByName('obsAgenda').AsString := tD.FieldByName('PRFOBS_AGENDA').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('idPersona').AsInteger := idPersona;
      qE1.ParamByName('color_agenda').AsString := TColorToHex(tD.FieldByName('PRFCOLOR').AsInteger);
      qE1.ParamByName('dasiclinicId').AsInteger := tD.FieldByName('ID').AsInteger;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idProfessional := qE1.FieldByName('registre').AsInteger;

      // crear EMPRESES_PROFESSIONAL       !!!!  DE MOMENT A LA 350 NOMES HI HA 1 EMP x PRF

      // cerca EMPRESA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_EMPRESES'' and D_ID=:empregistre and e_centre=:idcentre');
      qR1.ParamByName('empregistre').AsInteger := tD.FieldByName('empregistre').AsInteger;
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ExecQuery;
      idEmpresa := qR1.FieldByName('E_ID').AsVariant;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO empresaProfessional ( Professional, empresa) '+
                  'VALUES(:Professional, :empresa)');
      qE1.ParamByName('Professional').AsInteger := idProfessional;
      qE1.ParamByName('empresa').AsInteger := idEmpresa;
      qE1.ExecSQL ;

      // crear ESPECIALITATS_PROFESSIONAL
      // PRIMER ES CREA LA PRINCIPAL I LA CATEGORIA

      // cerca ESPECIALITAT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''MODULS'' and D_ID=:modregistre and e_centre=:idcentre');
      qR1.ParamByName('modregistre').AsInteger := tD.FieldByName('modregistre').AsInteger;
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ExecQuery;
      idEspecialitat := qR1.FieldByName('E_ID').AsVariant;

      // cerca CATEGORIA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''MODULS_CATEGORIES'' and D_ID=:catregistre and e_centre=:idcentre');
      qR1.ParamByName('catregistre').AsInteger := tD.FieldByName('catregistre').AsInteger;
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ExecQuery;
      if qR1.RecordCount > 0 then idCategoria := qR1.FieldByName('E_ID').AsVariant else idCategoria := -1;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO especialitatsProfessional ( idProfessional, idEspecialitat,'+
                  'idCategoria, principal) VALUES(:idProfessional, :idEspecialitat, :idCategoria, 1)');
      qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
      if idCategoria <> -1 then qE1.ParamByName('idCategoria').AsInteger := idCategoria;
      qE1.ExecSQL ;

      // ARA AFEGIM LA RESTA DE ESPECIALITATS
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from S_PROFESSIONALS_ESP where prfregistre=:prfregistre and PRINCIPAL=0');
      tD1.ParamByName('prfregistre').AsInteger :=  tD.FieldByName('PRFREGISTRE').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // cerca ESPECIALITAT a taula de relacions_centres
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''MODULS'' and D_ID=:modregistre and e_centre=:idcentre');
        qR1.ParamByName('modregistre').AsInteger := tD1.FieldByName('modregistre').AsInteger;
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ExecQuery;
        idEspecialitat := qR1.FieldByName('E_ID').AsVariant;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO especialitatsProfessional ( idProfessional, idEspecialitat,'+
                    'principal) VALUES(:idProfessional, :idEspecialitat, 0)');
        qE1.ParamByName('idProfessional').AsInteger := idProfessional;
        qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
        qE1.ExecSQL ;

        tD1.Next;
      end;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_PROFESSIONALS'',:D_ID,''professionals'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('PRFREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idProfessional;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      // Ara passem les mutues amb les que col�labora el professional
      // IMPORTANT a l'eClinic funciona al reves, s'han de posar amb quines NO col�labora
      // per tant la select sera de totes les tarifes (excepte la 0) que no estiguin
      // a la taula s_professionals_mutues del professional en curs
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select t.trfregistre from tarifes t where t.trfregistre >0 '+
                        'and t.trfregistre not in (select trfregistre from s_professionals_mutuas where prfregistre=:prfregistre)');
      tD1.ParamByName('prfregistre').AsInteger :=  tD.FieldByName('PRFREGISTRE').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // cerca ID tarifa a taula de relacions_centres
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES'' and D_ID=:trfregistre and e_centre=:idcentre');
        qR1.ParamByName('trfregistre').AsInteger := tD1.FieldByName('trfregistre').AsInteger;
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ExecQuery;
        idTarifaMutua := qR1.FieldByName('E_ID').AsVariant;

        // afegim dades a la taula professionalsTarifaMutua
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO professionalsTarifaMutua ( idProfessional, idTarifaMutua) '+
                    'VALUES(:idProfessional, :idTarifaMutua)');
        qE1.ParamByName('idProfessional').AsInteger := idProfessional;
        qE1.ParamByName('idTarifaMutua').AsInteger := idTarifaMutua;
        qE1.ExecSQL ;

        tD1.Next;
      end;
      tD1.Close;

      tD.Next;
    end;
    sbProf.Enabled := False;
    PassadaOK('S_PROFESSIONALS');
  except
    raise;
  end;
end;

procedure TForm2.sbAbsenciesClick(Sender: TObject);
var registre, idProfessional, idPeriode : integer;
begin
  // PERIODES_ABSENCIA BLOQUEJOS
  // primer passem el de la taula FESTIUS
 try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from FESTIUS');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca professional a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:PRFREGISTRE and e_centre=:idCentre');
      qR1.ParamByName('PRFREGISTRE').AsInteger := tD.FieldByName('PRFREGISTRE').AsInteger;
      qR1.ParamByName('idCentre').AsInteger := idCentre;
      qR1.ExecQuery;
      if qR1.RecordCount > 0 then idProfessional := qR1.FieldByName('E_ID').AsVariant
        else idProfessional := 0;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO periodesAbsencia (idPeriodeAbsencia, inici, final,'+
                  'actiu, nom, idProfessional, tipus, hora_inici, hora_final, dasiclinicReg)'+
                  'VALUES(:idPeriodeAbsencia, :inici, :final, 1, :nom,'+
                  ':idProfessional, :tipus, :hora_inici, :hora_final, :dasiclinicReg)');
      qE1.ParamByName('inici').AsDateTime := tD.FieldByName('FESDATAINICI').AsDateTime;
      qE1.ParamByName('final').AsDateTime := tD.FieldByName('FESDATAFINAL').AsDateTime;
      qE1.ParamByName('nom').AsString := tD.FieldByName('FESNOM').AsString;
      if idProfessional > 0 then
        qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      qE1.ParamByName('tipus').AsInteger := iif(tD.FieldByName('FESTIPO').AsInteger=0,0,1);
      qE1.ParamByName('hora_inici').AsInteger :=  iif(tD.FieldByName('FESTIPO').AsInteger=1,540,iif(tD.FieldByName('FESTIPO').AsInteger=2,900,0));
      qE1.ParamByName('hora_final').AsInteger :=  iif(tD.FieldByName('FESTIPO').AsInteger=1,900,iif(tD.FieldByName('FESTIPO').AsInteger=2,1260,0));
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('FESREGISTRE').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''FESTIUS'',:D_ID,''periodesAbscencia'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('FESREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
  except
    raise;
  end;
  tD.Close;
  dbGrid1.Refresh;

  // BLOQUEJOS hores sueltes
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from S_CITACIONS where pacregistre=-1');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca professional a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:PRFREGISTRE and e_centre=:idCentre');
      qR1.ParamByName('PRFREGISTRE').AsInteger := tD.FieldByName('PRFREGISTRE').AsInteger;
      qR1.ParamByName('idCentre').AsInteger := idCentre;
      qR1.ExecQuery;
      idProfessional := qR1.FieldByName('E_ID').AsVariant;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO bloquejosAgenda (idBloqueig, data, allDay, hora,'+
                  'temps, motiu, idProfessional, isBloqueigDemo, dasiclinicId) '+
                  'VALUES(:idBloqueig, :data, 0, :hora, :temps, :motiu,'+
                  ':idProfessional, 0, :dasiclinicId)');
      qE1.ParamByName('data').AsDateTime := tD.FieldByName('CITDATA').AsDateTime;
      qE1.ParamByName('hora').AsInteger := HoraMinuts(tD.FieldByName('CITHORA').AsString);
      qE1.ParamByName('temps').AsInteger := tD.FieldByName('CITTEMPS').AsInteger;
      qE1.ParamByName('motiu').AsString := tD.FieldByName('CITOBSERVACIONS').AsString;
      qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      qE1.ParamByName('dasiclinicId').AsInteger := tD.FieldByName('CITREGISTRE').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_CITACIONS'',:D_ID,''bloquejosAgenda'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('CITREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbAbsencies.Enabled := False;
    PassadaOK('FESTIUS');
  except
    raise;
  end;
end;

procedure TForm2.sbAperturesClick(Sender: TObject);
var idProfessional, idPeriode, idHorari : integer;
begin
  // APERTURES
  // Com que tinc les apertures de dies complerts en dos registres, primer faig
  // un group by data+professional i aix� creant un sol per�ode m�s horari puc
  // pasar els dos registres en un que es com esta a l'eClinic
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select data, prfregistre from festius_excepcions group by data, prfregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // Cerquem profesional a la taula relacions_centre
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_centre=:idCentre');
      qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      qR1.ParamByName('idCentre').AsInteger := idCentre;
      qR1.ExecQuery;
      idProfessional := qR1.FieldByName('E_ID').AsVariant;

      // creo un periode per aquest dia i professional
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO periodes (idPeriode, inici, final, actiu, especific,'+
                  'nom, diaObert, idProfessional, dasiclinicId, dasiclinicReg)'+
                  'VALUES(:idPeriode, :inici, :final, 1, 0, :nom, 1, :idProfessional, 0, 0)');
      qE1.ParamByName('nom').AsString := 'sistema';
      qE1.ParamByName('inici').AsDateTime := tD.FieldByName('data').AsDateTime;
      qE1.ParamByName('final').AsDateTime := tD.FieldByName('data').AsDateTime;
      qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idPeriode := qE1.FieldByName('registre').AsInteger;

      // creo un horari per aquest periode
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO horaris (idHorari, setmana, actiu, idPeriode, diaSetmana,'+
                  'dasiclinicId, dasiclinicReg) VALUES(:idHorari, 0, 1, :idPeriode,'+
                  ':diaSetmana, 0, 0)');
      qE1.ParamByName('diaSetmana').AsInteger := DayOfWeek(tD.FieldByName('data').AsDateTime)-1;
      qE1.ParamByName('idPeriode').AsInteger := idPeriode;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idHorari := qE1.FieldByName('registre').AsInteger;

      // actualitzo l'horari de mati i/o tarda
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from festius_excepcions where data=:data and prfregistre=:prfregistre');
      tD1.ParamByName('data').AsDateTime := tD.FieldByName('data').AsDateTime;
      tD1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // faig update segons sigui mati o tarda
        if tD1.FieldByName('tipo').AsInteger = 1 then  // matins
        begin
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('UPDATE horaris SET mati=1, h_interval=:interval, matiInici=:Inici, '+
                      'matiFinal=:final where idHorari=:idHorari');
          qE1.ParamByName('interval').AsInteger := tD1.FieldByName('INTERVAL').AsInteger;
          qE1.ParamByName('Inici').AsInteger := tD1.FieldByName('HORA1').AsInteger;
          qE1.ParamByName('final').AsInteger := tD1.FieldByName('HORA2').AsInteger;
          qE1.ParamByName('idHorari').AsInteger := idHorari;
          qE1.ExecSQL ;
        end;

        if tD1.FieldByName('tipo').AsInteger = 2 then  // tardes
        begin
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('UPDATE horaris SET tarda=1, h_interval=:interval, tardaInici=:Inici, '+
                      'tardaFinal=:final where idHorari=:idHorari');
          qE1.ParamByName('interval').AsInteger := tD1.FieldByName('INTERVAL').AsInteger;
          qE1.ParamByName('Inici').AsInteger := tD1.FieldByName('HORA1').AsInteger;
          qE1.ParamByName('final').AsInteger := tD1.FieldByName('HORA2').AsInteger;
          qE1.ParamByName('idHorari').AsInteger := idHorari;
          qE1.ExecSQL ;
        end;

        tD1.Next;
      end;
      tD.Next;
    end;
    sbApertures.Enabled := False;
    PassadaOK('FESTIUS_EXCEPCIONS');
  except
    raise;
  end;
end;

procedure TForm2.sbAntecedentsClick(Sender: TObject);
var posicio, posicio2 : integer;
begin
  //ANTECEDENTS
  try
    // esborrem antecedents inicials
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from antecedents where idCentre=:idCentre');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;

    // passem els dos grups BASE (f�rmacs i antecedents)
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from ANTECEDENTES where padre=0 order by antregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // CREAR grups BASE
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO antecedents (id, ordre, nom, idCentre, dasiclinicReg) '+
                  'VALUES(:id, :ordre, :nom, :idCentre, :dasiclinicReg)');
      qE1.ParamByName('ordre').AsInteger := tD.FieldByName('antregistre').AsInteger * 100000;
      qE1.ParamByName('nom').AsString := tD.FieldByName('descripcion').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('antregistre').AsInteger;
      qE1.ExecSQL ;

      // passem el nivell 1
      posicio := 0;
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from ANTECEDENTES where padre=:padre order by antregistre');
      tD1.ParamByName('padre').AsInteger := tD.FieldByName('antregistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        Inc(Posicio,1000);
        // CREAR grups NIVELL 1
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO antecedents (id, ordre, nom, idCentre, dasiclinicReg) '+
                    'VALUES(:id, :ordre, :nom, :idCentre, :dasiclinicReg)');
        qE1.ParamByName('ordre').AsInteger := tD1.FieldByName('padre').AsInteger * 100000 + posicio;
        qE1.ParamByName('nom').AsString := tD1.FieldByName('descripcion').AsString;
        qE1.ParamByName('idCentre').AsInteger := idCentre;
        qE1.ParamByName('dasiclinicReg').AsInteger := tD1.FieldByName('antregistre').AsInteger;
        qE1.ExecSQL ;

        // passem el nivell 2
        posicio2 := tD1.FieldByName('padre').AsInteger * 100000 + posicio;
        tD2.Close;
        tD2.SelectSQL.Clear;
        tD2.SelectSQL.Add('select * from ANTECEDENTES where padre=:padre order by antregistre');
        tD2.ParamByName('padre').AsInteger := tD1.FieldByName('antregistre').AsInteger;
        tD2.Open;
        while not tD2.Eof do
        begin
          Inc(Posicio2,1);
          // CREAR grups NIVELL 2
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('INSERT INTO antecedents (id, ordre, nom, idCentre, dasiclinicReg) '+
                      'VALUES(:id, :ordre, :nom, :idCentre, :dasiclinicReg)');
          qE1.ParamByName('ordre').AsInteger := posicio2;
          qE1.ParamByName('nom').AsString := tD2.FieldByName('descripcion').AsString;
          qE1.ParamByName('idCentre').AsInteger := idCentre;
          qE1.ParamByName('dasiclinicReg').AsInteger := tD2.FieldByName('antregistre').AsInteger;
          qE1.ExecSQL ;

          tD2.Next;
        end;
        tD1.Next;
      end;
      tD.Next;
    end;
    sbAntecedents.Enabled := False;
    PassadaOK('ANTECEDENTES');
  except
    raise;
  end;
end;

procedure TForm2.sbAntecedentsPAcClick(Sender: TObject);
var posicio0, posicio, posicio2, idPacient : integer;
begin
  //ANTECEDENTS PACIENTS
  try
    // esborrem antecedents inicials
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from antecedentPacient where idCentre=:idCentre');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;

    // mirem quins pacients tenen antecedents creats
    tD0.Close;
    tD0.SelectSQL.Clear;
    tD0.SelectSQL.Add('select distinct(pacregistre) from antecedentes_pac where pacregistre>0 and padre>0 order by pacregistre');
    tD0.Open;
    while not tD0.Eof do
    begin
      // cerca PACIENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and D_ID=:pacregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('pacregistre').AsInteger := tD0.FieldByName('pacregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idPacient := -1
        else idPacient := qR1.FieldByName('E_ID').AsVariant;

      if idPAcient > 0 then
      begin
        tD.Close;
        tD.SelectSQL.Clear;
        tD.SelectSQL.Add('select * from antecedentes_pac where pacregistre=:pacregistre and padre=0 order by antregistre');
        tD.ParamByName('pacregistre').AsInteger := tD0.FieldByName('pacregistre').AsInteger;
        tD.Open;
        while not tD.Eof do
        begin
          // NO INSERTEM GRUP BASE, NO CAL
          posicio0 := tD.FieldByName('antregistre').AsInteger * 100000;

          // passem el nivell 1
          posicio := 0;
          tD1.Close;
          tD1.SelectSQL.Clear;
          tD1.SelectSQL.Add('select * from antecedentes_pac where pacregistre=:pacregistre and padre=:padre order by antregistre');
          tD1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
          tD1.ParamByName('padre').AsInteger := tD.FieldByName('antregistre').AsInteger;
          tD1.Open;
          while not tD1.Eof do
          begin
            Inc(Posicio,1000);
            // CREAR grups NIVELL 1
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO antecedentPacient (id, ordre, nom, observacions,'+
                        'idPacient, idCentre) VALUES(:id, :ordre, :nom, :observacions,'+
                        ':idPacient, :idCentre)');
            qE1.ParamByName('ordre').AsInteger := posicio0 + posicio;
            qE1.ParamByName('nom').AsString := tD1.FieldByName('descripcion').AsString;
            qE1.ParamByName('observacions').AsString := tD1.FieldByName('texte').AsString;
            qE1.ParamByName('idCentre').AsInteger := idCentre;
            qE1.ParamByName('idPacient').AsInteger := idPacient;
            qE1.ExecSQL ;

            // passem el nivell 2
            posicio2 := posicio0 + posicio;
            tD2.Close;
            tD2.SelectSQL.Clear;
            tD2.SelectSQL.Add('select * from antecedentes_pac where pacregistre=:pacregistre and padre=:padre order by antregistre');
            tD2.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
            tD2.ParamByName('padre').AsInteger := tD1.FieldByName('antregistre').AsInteger;
            tD2.Open;
            while not tD2.Eof do
            begin
              Inc(Posicio2,1);
              // CREAR grups NIVELL 2
              qE1.Close;
              qE1.SQL.Clear;
              qE1.SQL.Add('INSERT INTO antecedentPacient (id, ordre, nom, observacions,'+
                          'idPacient, idCentre) VALUES(:id, :ordre, :nom, :observacions,'+
                          ':idPacient, :idCentre)');
              qE1.ParamByName('ordre').AsInteger := posicio2;
              qE1.ParamByName('nom').AsString := tD2.FieldByName('descripcion').AsString;
              qE1.ParamByName('observacions').AsString := tD2.FieldByName('texte').AsString;
              qE1.ParamByName('idCentre').AsInteger := idCentre;
              qE1.ParamByName('idPacient').AsInteger := idPacient;
              qE1.ExecSQL ;

              tD2.Next;
            end;
            tD1.Next;
          end;
          tD.Next;
        end;
      end;
      tD0.Next;
    end;
    sbAntecedentsPAc.Enabled := False;
    PassadaOK('ANTECEDENTES_PAC');
  except
    raise;
  end;
end;

procedure TForm2.sbEmpresaClick(Sender: TObject);
var registre, idAdreca, idEmail, idPersona, idEmpresa, idTipusDocument  : integer;
    pais : string;
    idZona : Variant;
begin
  // EMPRESES
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from s_empreses where empregistre>=0');
    tD.Open;
    while not tD.Eof do
    begin
      // crear persona + mails + adreces
      // buscar poblacio->zona a taula de relacions
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from relacions where d_taula=''S_POBLACIONS'' and D_ID=:pobregistre');
      qR1.ParamByName('pobregistre').AsInteger := tD.FieldByName('POBREGISTRE').AsInteger;
      qR1.ExecQuery;
      IDZona := qR1.FieldByName('E_ID').AsVariant;

      // cercar pais de la zona a MySql
      if idZona >= 0 then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select pais from zones where idzona=:idzona');
        qE1.ParamByName('idzona').AsInteger := idzona;
        qE1.ExecSQL ;
        Pais := qE1.FieldByName('pais').AsString;
      end;

      // cercar TipusDocument
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select idTipus from dasieclinicDB.tipusDocument where idcentre=:idcentre and nom like :tipus');
      qE1.ParamByName('idcentre').AsInteger := idcentre;
      qE1.ParamByName('tipus').AsString := '%NIF%';
      qE1.ExecSQL ;
      idTipusDocument := qE1.FieldByName('idTipus').AsInteger;

      // CREAR adre�a
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO adreces (idAdreca, direccio1, cp, pais, idZona) '+
                  'VALUES(:idAdreca, AES_ENCRYPT(:direccio1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:cp,"9w7kM5GwbJSsRnvS"), :pais, :idzona)');
      qE1.ParamByName('direccio1').AsString := tD.FieldByName('EMPADRECA').AsString;
      qE1.ParamByName('cp').AsString := tD.FieldByName('EMPCCP').AsString;
      qE1.ParamByName('pais').AsString := pais;
      if idZona >= 0 then qE1.ParamByName('idZona').AsInteger := idZona;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idAdreca := qE1.FieldByName('registre').AsInteger;

      // CREAR email
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO emails (idEmail, email) VALUES(:idEmail, AES_ENCRYPT(:email,"9w7kM5GwbJSsRnvS"))');
      qE1.ParamByName('email').AsString := tD.FieldByName('EMPEMAIL').AsString;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idEmail := qE1.FieldByName('registre').AsInteger;

      // crear persona
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO persones (idPersona, nom, nomComplert, numDocument,'+
                  'telefon1, telefon2, cognom1, cognom2, idAdreca, idEmail, TipusDocument) '+
                  'VALUES(:idPersona, AES_ENCRYPT(:nom,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:nomComplert,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:numDocument,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon2,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:cognom1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:cognom2,"9w7kM5GwbJSsRnvS"), :idAdreca, :idEmail, :idTipusDocument)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('EMPNOM').AsString;
      qE1.ParamByName('nomComplert').AsString := tD.FieldByName('EMPNOM').AsString;
      qE1.ParamByName('cognom1').AsString := tD.FieldByName('EMPRAOSOCIAL').AsString;
      qE1.ParamByName('numDocument').AsString := tD.FieldByName('EMPNIF').AsString;
      qE1.ParamByName('telefon1').AsString := tD.FieldByName('EMPTELEFON1').AsString;
      qE1.ParamByName('telefon2').AsString := tD.FieldByName('EMPTELEFON2').AsString;
      qE1.ParamByName('idAdreca').AsInteger := idAdreca;
      qE1.ParamByName('idTipusDocument').AsInteger := idTipusDocument;
      qE1.ParamByName('idEmail').AsInteger := idEmail;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idPersona := qE1.FieldByName('registre').AsInteger;

      // crear EMPRESA
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO empresa (id, web, dataAlta, actiu, serieFac, serieRec,'+
                  'serieFacMut, serieRecMut, percIRPF, contaDigits, dadesEmpresa,'+
                  'idCentre, comentaris, DasiClinicReg) VALUES(:id, :web, :dataAlta,'+
                  ':actiu, :serieFac, :serieRec, :serieFacMut, :serieRecMut, :percIRPF,'+
                  ':contaDigits, :dadesEmpresa, :idCentre, :comentaris, :DasiClinicReg)');
      qE1.ParamByName('web').AsString := tD.FieldByName('EMPWEB').AsString;
//      qE1.ParamByName('dataAlta').AsDateTime := '';
      qE1.ParamByName('actiu').AsInteger := tD.FieldByName('ACTIVA').AsInteger;
      qE1.ParamByName('serieFac').AsInteger := tD.FieldByName('SERIE_FRA').AsInteger;
      qE1.ParamByName('serieRec').AsInteger := tD.FieldByName('SERIE_FRE').AsInteger;
      qE1.ParamByName('serieFacMut').AsInteger := tD.FieldByName('SERIE_FRA_M').AsInteger;
      qE1.ParamByName('serieRecMut').AsInteger := tD.FieldByName('SERIE_FRE_M').AsInteger;
      qE1.ParamByName('percIRPF').AsFloat := tD.FieldByName('EMPIRPF').AsFloat;
      qE1.ParamByName('contaDigits').AsInteger := tD.FieldByName('DIGITS_CONTA').AsInteger;
      qE1.ParamByName('dadesEmpresa').AsInteger := idPersona;
      qE1.ParamByName('comentaris').AsString := tD.FieldByName('EMPOBSERVACIONS').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('DasiClinicReg').AsInteger := tD.FieldByName('EMPREGISTRE').AsInteger;;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idEmpresa := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_EMPRESES'',:D_ID,''empresa'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('EMPREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idEmpresa;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbTarifes.Enabled := False;
    PassadaOK('S_EMPRESES');
  except
    raise;
  end;
end;

procedure TForm2.sbEspeClick(Sender: TObject);
var registre, idCategoria : integer;
begin
  // Especialitats
  try
    // esborrem les especialitats que crea l'eclinic per defecte
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from especialitats where idCentre=:idCentre and dasiclinicreg>0');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;

    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from moduls');
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO especialitats (idEspecialitat, nom, actiu, idCentre, DasiClinicID, DasiClinicReg) '+
                  'VALUES(:idEspecialitat, :nom, :actiu, :idCentre, :DasiClinicID, :DasiClinicReg)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('MODNOM').AsString;
      qE1.ParamByName('actiu').AsInteger := tD.FieldByName('ACTIVO').AsInteger;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('DasiClinicID').AsInteger := tD.FieldByName('modregistre').AsInteger;
      qE1.ParamByName('DasiClinicReg').AsInteger := tD.FieldByName('modregistre').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''MODULS'',:D_ID,''especialitats'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('MODREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from moduls_categories where catregistre > 0 '+
                        'and catregistre in (select distinct(catregistre) from s_professionals) '+
                        'and modregistre=:modregistre');
      tD1.ParamByName('modregistre').AsInteger := tD.FieldByName('MODREGISTRE').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO categories (idCategoria, idEspecialitat, nom, actiu, DasiClinicReg) '+
                    'VALUES(:idCategoria, :idEspecialitat, :nom, 1, :DasiClinicReg)');
        qE1.ParamByName('idEspecialitat').AsInteger := Registre;
        qE1.ParamByName('nom').AsString := tD1.FieldByName('CATNOM').AsString;
        qE1.ParamByName('DasiClinicReg').AsInteger := tD1.FieldByName('catregistre').AsInteger;
        qE1.ExecSQL ;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        idCategoria := qE1.FieldByName('registre').AsInteger;

        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''MODULS_CATEGORIES'',:D_ID,''categories'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD1.FieldByName('CATREGISTRE').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := idCategoria;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;

        tD1.Next;
      end;
      tD.Next;
    end;

    sbEspe.Enabled := False;
    PassadaOK('MODULS');
  except
    raise;
  end;
end;

procedure TForm2.sbFPClick(Sender: TObject);
var idFP, idbancCompte : integer;
begin
  // FORMES PAGAMENT
  // esborrem FORMES creats de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''FORMAS_PAGO'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // ESBORREM FORMES DE PAGAMENT
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from formaPagamentClient where idCentre=:idCentre and dasiclinicreg>0');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from FORMAS_PAGO order by FORMA_PAGO');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca BANC a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''BANCS'' and D_ID=:banregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('banregistre').AsInteger := tD.FieldByName('banregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idbancCompte := -1
        else idbancCompte := qR1.FieldByName('E_ID').AsVariant;

      // insertem Formes pagament
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO dasieclinicDB.formaPagamentClient( idFormaPagamentClient,'+
                  'nom, perComisio, diposit, bancCompte, idCentre, pare, dasiclinicReg, inactiu) '+
                  'VALUES( :idFormaPagamentClient, :nom, :perComisio, :diposit, :bancCompte,'+
                  ':idCentre, :pare, :dasiclinicReg, :inactiu)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('NOMBRE').AsString;
      qE1.ParamByName('perComisio').AsFloat := tD.FieldByName('COMISION').AsFloat;
      qE1.ParamByName('diposit').AsInteger := StrToInt(tD.FieldByName('DEPOSITO').AsString);
      if idbancCompte > 0 then
        qE1.ParamByName('bancCompte').AsInteger := idbancCompte;
      case tD.FieldByName('FORMA_PAGO').AsInteger of
        0 : qE1.ParamByName('pare').AsString := 'tpag_efectivo';
        1 : qE1.ParamByName('pare').AsString := 'tpag_targeta1';
        2 : qE1.ParamByName('pare').AsString := 'tpag_talon';
        3 : qE1.ParamByName('pare').AsString := 'tpag_targeta2';
        4 : qE1.ParamByName('pare').AsString := 'tpag_financiacion';
        5 : qE1.ParamByName('pare').AsString := 'tpag_cargo_dep';
        6 : qE1.ParamByName('pare').AsString := 'tpag_ent_efec';
      end;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('FORMA_PAGO').AsInteger;
      qE1.ParamByName('inactiu').AsInteger := iif(tD.FieldByName('INACTIVA').IsNull,0,tD.FieldByName('INACTIVA').AsInteger);
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idFP := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''FORMAS_PAGO'',:D_ID,''formaPagamentClient'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('FORMA_PAGO').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idFP;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbFP.Enabled := False;
    PassadaOK('FORMAS_PAGO');
  except
    raise;
  end;
end;

procedure TForm2.sbFraMutuesClick(Sender: TObject);
var idTarifaMutua, idEmpresa, idFra, idVisitaTractament, idCarrec : integer;
begin
  // FACTURES DE MUTUA
  // esborrem CARRECS creats de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''TARIFES_FACTURES'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // ESBORREM FACTURES PERQUE NO FA EL CASCADE
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete f.* from dasieclinicDB.fra f '+
                'left join dasieclinicDB.carrec c on f.idFra=c.idFra '+
                'where f.traspas=:traspas and c.tipusCarrec=:tipus');
    qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
    qE1.ParamByName('tipus').AsString := 'MUT';
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from TARIFES_FACTURES where frmregistre>0 order by frmregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca TARIFA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES'' and D_ID=:trfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('trfregistre').AsInteger := tD.FieldByName('trfregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idTarifaMutua := -1
        else idTarifaMutua := qR1.FieldByName('E_ID').AsVariant;

      // cerca EMPRESA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_EMPRESES'' and D_ID=:empregistre and e_centre=:idcentre');
      qR1.ParamByName('empregistre').AsInteger := tD.FieldByName('empregistre').AsInteger;
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idEmpresa := -1
        else idEmpresa := qR1.FieldByName('E_ID').AsVariant;

      // insertem Factures Mutua
      // fins que es modifiqui partim de la base que nomes tenim un impost AIXO S'HA DE REPLANTEJAR
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO dasieclinicDB.fra( idFra, codi, irpfPer, irpfImport,'+
                  'import, total, cobrat, importCobrat, proforma, dataDoc,'+
                  'notaPeu, groupTrac, periodeIni, periodeFi, observacions, dasiclinicReg,'+
                  'rebut, traspas, base, totalImpost) VALUES( :idFra, :codi, :irpfPer,'+
                  ':irpfImport, :import, :total, :cobrat, :importCobrat, :proforma,'+
                  ':dataDoc, :notaPeu, :groupTrac, :periodeIni, :periodeFi, :observacions,'+
                  ':dasiclinicReg, :rebut, :traspas, :base, :totalImpost)');
      qE1.ParamByName('codi').AsString := tD.FieldByName('FRMNUMEROTXT').AsString;
      qE1.ParamByName('irpfPer').AsFloat := tD.FieldByName('FRMIRPF_P').AsFloat;
      qE1.ParamByName('irpfImport').AsFloat := tD.FieldByName('FRMIRPF_IMPORT').AsFloat;
      qE1.ParamByName('import').AsFloat := tD.FieldByName('FRMTOTAL').AsFloat;
      qE1.ParamByName('cobrat').AsFloat := 0;         // ES MARCARA AL FER EL COBRAMENT
      qE1.ParamByName('importCobrat').AsInteger := 0; // ES MARCARA AL FER EL COBRAMENT
      qE1.ParamByName('dataDoc').AsDateTime := tD.FieldByName('FRMDATA').AsDateTime;
      qE1.ParamByName('groupTrac').AsInteger := 0;    // NO TE VALOR AL DASICLINIC
      qE1.ParamByName('periodeIni').AsDateTime := tD.FieldByName('PERIODE_INI').AsDateTime;
      qE1.ParamByName('periodeFi').AsDateTime := tD.FieldByName('PERIODE_FIN').AsDateTime;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('FRMREGISTRE').AsInteger;
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ParamByName('base').AsFloat := tD.FieldByName('FRMIMPORT').AsFloat;
      qE1.ParamByName('totalImpost').AsFloat := 0;    // NO HI HA IVA AL DASICLINIC
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idFra := qE1.FieldByName('registre').AsInteger;

      // MIREM totes les visites_tractament que afectan aquesta factura i agafem
      // el idCarrec un cop el tenim actualitzem a "carrec" el idFra i el codi
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select trvregistre from visites_tractaments where FACTURA_MUTUA=:factura');
      tD1.ParamByName('factura').AsInteger := tD.FieldByName('FRMREGISTRE').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // agafem el idVisita
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select id from visitesTractaments where dasiclinicReg=:registre and '+
                    'traspas=:traspas');
        qE1.ParamByName('registre').AsInteger := tD1.FieldByName('trvregistre').AsInteger;
        qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
        qE1.ExecSQL ;
        idVisitaTractament := qE1.FieldByName('id').AsInteger;

        if idVisitaTractament > 0 then
        begin
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('select idCarrec from visitesTractamentsCarrecs where idVisitaTractament=:idVisitaTractament');
          qE1.ParamByName('idVisitaTractament').AsInteger := idVisitaTractament;
          qE1.ExecSQL ;
          idCarrec := qE1.FieldByName('idCarrec').AsInteger;

          // actualitzem el idFra a la taula carrec
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('update dasieclinicDB.carrec set idFra=:idFra, codi=:codi where idCarrec=:idCarrec');
          qE1.ParamByName('idFra').AsInteger := idFra;
          qE1.ParamByName('codi').AsString := tD.FieldByName('FRMNUMEROTXT').AsString;
          qE1.ParamByName('idCarrec').AsInteger := idCarrec;
          qE1.ExecSQL ;

          // actualitzem el idFraMutua a la taula visitesTractaments
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('update dasieclinicDB.visitesTractaments set fra_mutua=:fra_mutua where id=:idVisitaTractament');
          qE1.ParamByName('fra_mutua').AsInteger := idFra;
          qE1.ParamByName('idVisitaTractament').AsInteger := idVisitaTractament;
          qE1.ExecSQL ;
        end;
        tD1.Next;
      end;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''TARIFES_FACTURES'',:D_ID,''fra'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('FRMREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idFra;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbCarrecs.Enabled := False;
    PassadaOK('TARIFES_FACTURES');
  except
    raise;
  end;
end;

procedure TForm2.sbGrupsITtesClick(Sender: TObject);
var registre, idGrup, idEspecialitat, idImpost, idTractament : integer;
begin
  // GRUPS DE TRACTAMENTS
  try
    // esborrem tractaments demo
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from tractaments where isTractamentDemo=''1'' and idCentre=:idCentre');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from tractaments where dasiclinicreg>0 and idCentre=:idCentre');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;

    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from grups_tractaments where modregistre >= 0 '+
                     'and modregistre in (select distinct(modregistre) from s_professionals where prfregistre>0)');
    tD.Open;
    while not tD.Eof do
    begin
      // cercar id especialitat eClinic
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select e_id from RELACIONS_CENTRE where e_centre=:idCentre '+
                  'and d_id=:especialitat and d_taula=''MODULS''');
      qR1.ParamByName('idCentre').AsInteger := idCentre;
      qR1.ParamByName('especialitat').AsInteger := tD.FieldByName('MODREGISTRE').AsInteger;
      qR1.ExecQuery;
      idEspecialitat := qR1.FieldByName('e_id').AsInteger;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO grupTractaments (idGrup, idEspecialitat, nom, DasiclinicReg) '+
                  'VALUES(:idGrup, :idEspecialitat, :nom, :DasiclinicReg)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('grtnom').AsString;
      qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
      qE1.ParamByName('DasiClinicReg').AsInteger := tD.FieldByName('grtregistre').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idGrup := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''GRUPS_TRACTAMENTS'',:D_ID,''grupTractaments'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('grtregistre').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idGrup;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      // PASSEM TRACTAMENTS DEL GRUP
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from tractaments where grtregistre=:grtregistre');
      tD1.ParamByName('grtregistre').AsInteger := tD.FieldByName('grtregistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // cerco IMPOST
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select e_id from RELACIONS where d_id=:impost and d_taula=''IVA''');
        qR1.ParamByName('impost').AsInteger := tD1.FieldByName('IVA').AsInteger;
        qR1.ExecQuery;
        idImpost := qR1.FieldByName('e_id').AsInteger;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO tractaments (idTractament, idImpost, idEspecialitat,'+
                    'idGrup, idCentre, nom, codi, duracio, actiu, import, bonus,'+
                    'costMaterial, DasiClinicID, DasiClinicReg, IsTractamentDemo, DataCreat) '+
                    'VALUES(:idTractament, :idImpost, :idEspecialitat, :idGrup,'+
                    ':idCentre, :nom, :codi, :duracio, :actiu, :import, :bonus,'+
                    ':costMaterial, 0, 0, 0, :Data)');
        qE1.ParamByName('idImpost').AsInteger := idImpost;
        qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
        qE1.ParamByName('idGrup').AsInteger := idGrup;
        qE1.ParamByName('idCentre').AsInteger := idCentre;
        qE1.ParamByName('nom').AsString := tD1.FieldByName('tranom').AsString;
        qE1.ParamByName('codi').AsString := tD1.FieldByName('tracodi').AsString;
        qE1.ParamByName('duracio').AsInteger := tD1.FieldByName('traminuts').AsInteger;
        qE1.ParamByName('actiu').AsInteger := iif(tD1.FieldByName('INACTIVO').AsInteger=1,0,1);
        qE1.ParamByName('import').AsFloat := tD1.FieldByName('TRAPVP').AsFloat;
        qE1.ParamByName('costMaterial').AsFloat := tD1.FieldByName('TRAPREUCOSTMATERIAL').AsFloat;
        qE1.ParamByName('data').AsDateTime := Now;
        qE1.ExecSQL ;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        idTractament := qE1.FieldByName('registre').AsInteger;

        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''TRACTAMENTS'',:D_ID,''tractaments'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD1.FieldByName('traregistre').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := idTractament;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;

        // FALTA PASSAR ELS BONUS

        tD1.Next;
      end;
      tD.Next;
    end;
    sbGrupsITtes.Enabled := False;
    PassadaOK('TRACTAMENTS');
  except
    raise;
  end;
end;

procedure TForm2.sbHorarisClick(Sender: TObject);
var registre, idProfessional, idPeriode : integer;
begin
  // FESTIUS
  // esborrem DIES_FESTIUS creats de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''DIES_FESTIUS'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // S'HA DE MIRAR PER IDCENTRE + DASICLINICREG>0
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from dasieclinicDB.diesFestius where idcentre=:idcentre');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from DIES_FESTIUS');
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO diesFestius (ID, nom, data, any, periodicitat, idCentre)'+
                  'VALUES(:id, :nom, :data, :any, 1, :idCentre)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('fesnom').AsString;
      qE1.ParamByName('data').AsDateTime := tD.FieldByName('fesdata').AsDateTime;
      qE1.ParamByName('any').AsInteger := Exercici(tD.FieldByName('fesdata').AsDateTime);
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''DIES_FESTIUS'',:D_ID,''diesFestius'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('id').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbProcedencies.Enabled := False;
    PassadaOK('DIES_FESTIUS');
  except
    raise;
  end;
  tD.Close;
  DBGrid1.Refresh;

  // PERIODES
  // esborrem PERIODES creats de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''PERIODES'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // S'HA DE MIRAR PER TRASPAS + DASICLINICREG>0
{    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from dasieclinicDB.periodes where traspas=:traspas');
    qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
    qE1.ExecSQL ;}
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from PERIODES where prfregistre>0');
    tD.Open;
    while not tD.Eof do
    begin
      // Cerquem profesional a la taula relacions_centre
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_Centre=:idCentre');
      qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      qR1.ParamByName('idCentre').AsInteger := idCentre;
      qR1.ExecQuery;
      idProfessional := qR1.FieldByName('E_ID').AsVariant;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO periodes (idPeriode, inici, final, actiu, especific,'+
                  'nom, diaObert, idProfessional, dasiclinicId, dasiclinicReg)'+
                  'VALUES(:idPeriode, :inici, :final, 1, 0, :nom, 0, :idProfessional, 0, 0)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('PERNOM').AsString;
      qE1.ParamByName('inici').AsDateTime := tD.FieldByName('PERINICI').AsDateTime;
      qE1.ParamByName('final').AsDateTime := tD.FieldByName('PERFINAL').AsDateTime;
      qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''PERIODES'',:D_ID,''periodes'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('PERREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbProcedencies.Enabled := False;
    PassadaOK('PERIODES');
  except
    raise;
  end;

  // S_ESTRUCTURA
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select e.* from S_ESTRUCTURA e '+
                     'left join periodes p on e.perregistre=p.perregistre '+
                     'where e.estregistre>=0 and p.prfregistre>0');
    tD.Open;
    while not tD.Eof do
    begin
      // Cerquem periode a la taula relacions_centre
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''PERIODES'' and D_ID=:perregistre and e_centre=:idCentre');
      qR1.ParamByName('perregistre').AsInteger := tD.FieldByName('perregistre').AsInteger;
      qR1.ParamByName('idCentre').AsInteger := idCentre;
      qR1.ExecQuery;
      idPeriode := qR1.FieldByName('E_ID').AsVariant;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO horaris (idHorari, setmana, diaSetmana, actiu, mati,'+
                  'tarda, continu, h_interval, matiInici, matiFinal, tardaInici, tardaFinal,'+
                  'idPeriode, dasiclinicId, dasiclinicReg)'+
                  'VALUES(:idHorari, 0, :diaSetmana, :actiu, :mati, :tarda, 0, :h_interval,'+
                  ':matiInici, :matiFinal, :tardaInici, :tardaFinal, :idPeriode, 0, 0)');
      qE1.ParamByName('diaSetmana').AsInteger := tD.FieldByName('ESTDIA').AsInteger-1;
      qE1.ParamByName('actiu').AsInteger := StrToInt(tD.FieldByName('ESTACTIVAT1').AsString);
      qE1.ParamByName('mati').AsInteger := StrToInt(tD.FieldByName('ESTACTIVAT2').AsString);
      qE1.ParamByName('tarda').AsInteger := StrToInt(tD.FieldByName('ESTACTIVAT3').AsString);
      qE1.ParamByName('h_interval').AsInteger := tD.FieldByName('ESTINTERVAL').AsInteger;
      qE1.ParamByName('matiInici').AsInteger := tD.FieldByName('ESTHORA1').AsInteger;
      qE1.ParamByName('matiFinal').AsInteger := tD.FieldByName('ESTHORA2').AsInteger;
      qE1.ParamByName('tardaInici').AsInteger := tD.FieldByName('ESTHORA3').AsInteger;
      qE1.ParamByName('tardaFinal').AsInteger := tD.FieldByName('ESTHORA4').AsInteger;
      qE1.ParamByName('idPeriode').AsInteger := idPeriode;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_ESTRUCTURA'',:D_ID,''horaris'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('ESTREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbProcedencies.Enabled := False;
    PassadaOK('S_ESTRUCTURA');
  except
    raise;
  end;
end;

procedure TForm2.sbImatgesDocsClick(Sender: TObject);
var url, cadena, Pare, data, arxiu, ARXNom, ARXDes, JSon, Enviament : string;
    response : string;
    params : TStrings;
    stream : TStream;
    JSA, JSA1 : TJSONArray;
    JSP : TJSONPair;
    JSObj, JSO, JSONObject : TJSONObject;
    JSonToSend : TStringStream;
    Arrel, Salto, n, nPacients, UltimPacient, bucleImatges : integer;
    HiHaPacients : Boolean;
begin
  // Tot el traspas va ordenat per pacients, per tant primer fem una select dels
  // diferents pacients que tenen imatges i afegim o esborrem (si cal) a nivel de pacient

  // per evitar colapssar el servidor del eClinic, fem un bucle amb x pacients (10) i un cop
  // fets els n pacients esperem 1 minut i "REINICIEM", Connectem les dues bbdd i tornem a
  // iniciar el proces amb n pacients m�s.
  bucleImatges := iif(CheckBox2.Checked,Sp1.Value,300000);
  nPacients := iif(CheckBox2.Checked,1,Sp1.Value);
  UltimPacient := StrToInt(Edit5.Text);
  HiHaPacients := False;
  while bucleImatges > 0 do
  begin
    try
      tD0.Close;
      tD0.SelectSQL.Clear;
      tD0.SelectSQL.Add('select first '+ IntToStr(nPacients) +' DISTINCT(PACREGISTRE) from IMATGES_MINIATURES where pacregistre>:pacregistre order by pacregistre');
      tD0.ParamByName('pacregistre').AsInteger := UltimPacient;
      tD0.Open;
      while not tD0.Eof do
      begin
        HiHaPacients := True;
        if EsProd then URL := 'https://www.dasieclinic.com/delete-files-dc'
          else URL := 'http://test.dasieclinic.com/delete-files-dc';

        // esborrem el pacient de la taula de relacions (en aquest cas solament serveix de guia de quins pacients s'han passat
        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''IMATGES_MINIATURES'' and e_Centre=:idCentre '+
                     'and D_ID=:D_ID');
        qR2e.ParamByName('idcentre').AsInteger := idcentre;
        qR2e.ParamByName('D_ID').AsInteger := tD0.FieldByName('PACREGISTRE').AsInteger;
        qR2e.ExecQuery;

        // ESBORREM a partir de un pacient per si ha petat el programa
        try
          JSObj := TJsonObject.Create();    // objecte JSON principal a enviar
          JSA   := TJSONArray.Create;       // array inferior amb els valors de cada registre
          JSA1  := TJSONArray.Create;       // array superior amb l'array dels registres

          JSO   := TJsonObject.Create();    // objecte temporal
          jso.AddPair(TJsonPair.Create('PACREGISTRE',TJSONNumber.Create(tD0.FieldByName('PACREGISTRE').AsInteger)));
          jsa.AddElement(jso);              // afegim dades al registre

          JSO   := TJsonObject.Create();    // objecte temporal
          jso.AddPair(TJsonPair.Create('0',JSA));
          JSA1.AddElement(jso);             // afegim tots els registres...  en el nostre cas sempre es un

          JSObj.AddPair('user','dasi');
          JSObj.AddPair('password','avmISAD34');
          JSObj.AddPair('center',TJSONNumber.Create(idCentre));
          JSObj.AddPair('files',JSA1);
          JsonToSend := TStringStream.Create(QuitaSigno('[]',JSObj.ToString), TEncoding.UTF8);

          NetHTTPClient1.ContentType := 'application/json';
          NetHTTPClient1.AcceptCharset := 'utf-8';

          Response := NetHTTPClient1.post(url,JSonToSend).ContentAsString;
        except;
          raise;
        end;


        // ARA PASSEM CARPETES, IMATGES I DOCUMENTS
        if EsProd then URL := 'https://www.dasieclinic.com/upload-files-dc'
          else URL := 'http://test.dasieclinic.com/upload-files-dc';
        // AGAFO CARPETA ARREL
        qD1.Close;
        qD1.SQL.Clear;
        qD1.SQL.Add('select first 1 MINREGISTRE from IMATGES_MINIATURES where pacregistre=:pacregistre and claregistre=-2 and paperera=2');
        qD1.ParamByName('PACREGISTRE').AsInteger := tD0.FieldByName('PACREGISTRE').AsInteger;
        qD1.ExecQuery;
        Arrel := qD1.FieldByName('MINREGISTRE').AsInteger;

        // passem primer carpetes
        tD.Close;
        tD.SelectSQL.Clear;
        tD.SelectSQL.Add('select * from IMATGES_MINIATURES where pacregistre=:pacregistre and TIPUS=-1 and pare>0 order by minregistre');
        tD.ParamByName('PACREGISTRE').AsInteger := tD0.FieldByName('PACREGISTRE').AsInteger;
        tD.Open;
        while not tD.Eof do
        begin
          // convertim format data
          Data := YyyyMmDd(tD.FieldByName('IMGDATA').AsDateTime);
          Data := Copy(Data,1,4)+'/'+Copy(Data,5,2)+'/'+Copy(Data,7,2);

          JSObj := TJsonObject.Create();    // objecte JSON principal a enviar
          JSA   := TJSONArray.Create;       // array inferior amb els valors de cada registre
          JSA1  := TJSONArray.Create;       // array superior amb l'array dels registres

          JSO   := TJsonObject.Create();    // objecte temporal
          jso.AddPair(TJsonPair.Create('MINREGISTRE',TJSONNumber.Create(tD.FieldByName('MINREGISTRE').AsInteger)));
          jso.AddPair(TJsonPair.Create('PACREGISTRE',TJSONNumber.Create(tD.FieldByName('PACREGISTRE').AsInteger)));
  //        jso.AddPair(TJsonPair.Create('USRREGISTRE',TJSONNumber.Create(tD.FieldByName('USRREGISTRE').AsInteger)));      afegir a la 352.3
          jso.AddPair(TJsonPair.Create('IMGDESCRIPCIO',tD.FieldByName('IMGDESCRIPCIO').AsString));
          jso.AddPair(TJsonPair.Create('IMGNOM',tD.FieldByName('IMGNOM').AsString));
          jso.AddPair(TJsonPair.Create('IMGDATA',Data));
          if tD.FieldByName('PARE').AsInteger<>Arrel then jso.AddPair(TJsonPair.Create('PARE',TJSONNumber.Create(tD.FieldByName('PARE').AsInteger)));
          jsa.AddElement(jso);              // afegim dades al registre

          JSO   := TJsonObject.Create();    // objecte temporal
          jso.AddPair(TJsonPair.Create('0',JSA));
          JSA1.AddElement(jso);             // afegim tots els registres...  en el nostre cas sempre es un

          JSObj.AddPair('user','dasi');
          JSObj.AddPair('password','avmISAD34');
          JSObj.AddPair('center',TJSONNumber.Create(idCentre));
          JSObj.AddPair('files',JSA1);

          JsonToSend := TStringStream.Create(QuitaSigno('[]',JSObj.ToString), TEncoding.UTF8);
          try
            NetHTTPClient1.ContentType := 'application/json';
            NetHTTPClient1.AcceptCharset := 'utf-8';

            Response := NetHTTPClient1.post(url,JSonToSend).ContentAsString;
  //          showmessage(Response);
//            JsonToSend.Free;
          except;
            raise;
          end;
          tD.Next;
//          JSObj.Free;
        end;
        tD.Close;

        // ARA PASSEM IMATGES i DOCUMENTS
        tD.Close;
        tD.SelectSQL.Clear;
        tD.SelectSQL.Add('select m.*, d.document, i.imagen, v.hash from IMATGES_MINIATURES m '+
                         'left join imatges i on i.imgregistre=m.imgregistre '+
                         'left join documents d on d.docregistre=m.docregistre '+
                         'left join s_se_documents v on v.minregistre=m.minregistre '+
                         'where m.pacregistre=:pacregistre and m.TIPUS>=0 order by m.minregistre');
        tD.ParamByName('PACREGISTRE').AsInteger := tD0.FieldByName('PACREGISTRE').AsInteger;
        tD.Open;
        while not tD.Eof do
        begin
          // convertim arxiu
          Stream := TStream.Create;
          Arxiu := '';
          if tD.FieldByName('DOCREGISTRE').AsInteger > 0 then
          begin
            // Agafem document
            try
              if not TD.FieldByName('DOCUMENT').IsNull then
              begin
                Stream := TD.CreateBlobStream(TD.FieldByName('DOCUMENT'), bmRead);
                Arxiu := base64encode(stream);
              end;
            except
              raise;
            end;
          end;
          if tD.FieldByName('IMGREGISTRE').AsInteger > 0 then
          begin
            // Agafem imatge
            try
              if not TD.FieldByName('IMAGEN').IsNull then
              begin
                Stream := TD.CreateBlobStream(TD.FieldByName('IMAGEN'), bmRead);
                Arxiu := base64encode(stream);
              end;
            except
              raise;
            end;
          end;
          stream.Free;

          if Arxiu > '' then
          begin
            // convertim format data
            Data := YyyyMmDd(tD.FieldByName('IMGDATA').AsDateTime);
            Data := Copy(Data,1,4)+'-'+Copy(Data,5,2)+'-'+Copy(Data,7,2);

            // controlo de posar alguna cosa si no te nom o descripcio
            ARXNom := iif(tD.FieldByName('IMGNOM').AsString>'',tD.FieldByName('IMGNOM').AsString,'Sin_nombre');
            ARXDes := iif(tD.FieldByName('IMGDESCRIPCIO').AsString>'',tD.FieldByName('IMGDESCRIPCIO').AsString,'Sin_nombre');

            // Prepara par�metres
            params := TStringList.Create;
            params.Add('user=dasi');
            params.Add('password=avmISAD34');
            params.Add('center='+IntToStr(idCentre));
            params.Add('MINREGISTRE='+IntToStr(tD.FieldByName('MINREGISTRE').AsInteger));
            params.Add('PACREGISTRE='+IntToStr(tD.FieldByName('PACREGISTRE').AsInteger));
  //          params.Add('USRREGISTRE='+IntToStr(tD.FieldByName('USRREGISTRE').AsInteger));    afegir a la 352.3
            if tD.FieldByName('IMGREGISTRE').AsInteger > 0 then params.Add('IMGREGISTRE='+IntToStr(tD.FieldByName('IMGREGISTRE').AsInteger));
            if tD.FieldByName('DOCREGISTRE').AsInteger > 0 then params.Add('DOCREGISTRE='+IntToStr(tD.FieldByName('DOCREGISTRE').AsInteger));
            params.Add('IMGDESCRIPCIO='+HTTPEncode(ARXDes));
            params.Add('IMGNOM='+HTTPEncode(ARXNom));
            params.Add('IMGDATA='+Data);
            if tD.FieldByName('PARE').AsInteger<>Arrel then params.Add('PARE='+IntToStr(tD.FieldByName('PARE').AsInteger));
            if tD.FieldByName('DOCREGISTRE').AsInteger > 0 then params.Add('DOCUMENT='+Arxiu);
            if tD.FieldByName('IMGREGISTRE').AsInteger > 0 then params.Add('IMAGEN='+Arxiu);
            if tD.FieldByName('HASH').AsString > '' then params.Add('VIDHASH='+tD.FieldByName('HASH').AsString);

  //          memo1.clear;
  //          memo1.text := Params.Text;
  //          memo1.Lines.savetofile('c:\proves\cadena.txt');
  //          Showmessage(Params.Text);
            try
              NetHTTPClient1.ContentType := 'application/x-www-form-urlencoded';
              NetHTTPClient1.AcceptCharset := 'utf-8';

              Response := NetHTTPClient1.post(url,Params).ContentAsString;
              Params.Clear;
  //            showmessage(Response);
            except;
              raise;
            end;
          end;
          tD.Next;
        end;
//        Params.Free;

        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''IMATGES_MINIATURES'',:D_ID,''arxiusdoc'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD0.FieldByName('PACREGISTRE').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := 0;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;

        tD0.Next;
        if checkBox2.Checked then UltimPacient := tD0.FieldByName('PACREGISTRE').AsInteger;
      end;
    except;
      raise;
    end;

    if ((HiHaPacients) and (checkBox2.Checked)) then
    begin
      Dec(BucleImatges);  // descomptem bucle
      if BucleImatges = 0 then
      begin
        // Reiniciem el proc�s amb n pacients m�s
        sbReiniciar.Click;
        Label13.Visible := True;
        Self.Refresh;
        Sleep(300000); // aturem 5 minuts
        Label13.Visible := False;
        Self.Refresh;
        JvXPButton2.Click;
        Self.Refresh;
        Sleep(2000); // aturem 2 segons
        JvXPButton3.Click;
        Self.Refresh;
        Sleep(2000); // aturem 2 segons
        sbImatgesDocs.Click;
        Self.Refresh;
      end;
    end else
    begin
      BucleImatges := 0;
      sbImatgesDocs.Enabled := False;
      PassadaOK('IMATGES_MINIATURES');
    end;
  end;
end;

procedure TForm2.sbInformesClick(Sender: TObject);
var idUsuari, idVisita, idProfessional, idPacient, idTag, idInforme, e : integer;
    url, cadena, JSon, Enviament, Path, Response : string;
    JSA, JSA1 : TJSONArray;
    JSP : TJSONPair;
    JSObj, JSO, JSONObject : TJSONObject;
    JSonToSend : TStringStream;
    Arrel, Salto, n, nPacients, UltimPacient, BucleInformes : integer;
    HiHaPacients : Boolean;
begin
  // Tot el traspas va ordenat per pacients, per tant primer fem una select dels
  // diferents pacients que tenen INFORMES i afegim o esborrem (si cal) a nivell de pacient
  UltimPacient := StrToInt(Edit5.Text);

  // per evitar colapsar el servidor del eClinic, fem un bucle amb x pacients (10) i un cop
  // fets els n pacients esperem 5 minuts i "REINICIEM", Connectem les dues bbdd i tornem a
  // iniciar el proces amb n pacients m�s.
  BucleInformes := iif(CheckBox2.Checked,Sp1.Value,300000);
  nPacients := iif(CheckBox2.Checked,1,Sp1.Value);
  HiHaPacients := False;
  Path := Edit10.Text;
  if not DirectoryExists(Path) then CreateDir(Path);

  while BucleInformes > 0 do
  begin
    try
      tD0.Close;
      tD0.SelectSQL.Clear;
//      tD0.SelectSQL.Add('select first '+ IntToStr(nPacients) +' DISTINCT(PACREGISTRE) from INFORMES where pacregistre>:pacregistre order by pacregistre');
//      tD0.ParamByName('pacregistre').AsInteger := UltimPacient;
      tD0.SelectSQL.Add('select first '+ IntToStr(nPacients) +' DISTINCT(PACREGISTRE) from INFORMES where pacregistre=:pacregistre order by pacregistre');
      tD0.ParamByName('pacregistre').AsInteger := 37;
      tD0.Open;
      while not tD0.Eof do
      begin
        HiHaPacients := True;
        // URL per esborrar imatges dels informes del pacient
        if EsProd then URL := 'https://www.dasieclinic.com/delete-files-dc'
          else URL := 'http://test.dasieclinic.com/delete-files-dc';

        // esborrem el pacient de la taula de relacions (en aquest cas solament serveix de guia de quins pacients s'han passat
        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''INFORMES'' and e_Centre=:idCentre '+
                     'and D_ID=:D_ID');
        qR2e.ParamByName('idcentre').AsInteger := idcentre;
        qR2e.ParamByName('D_ID').AsInteger := tD0.FieldByName('PACREGISTRE').AsInteger;
        qR2e.ExecQuery;

        // ESBORREM a partir de un pacient per si ha petat el programa
        try
          JSObj := TJsonObject.Create();    // objecte JSON principal a enviar
          JSA   := TJSONArray.Create;       // array inferior amb els valors de cada registre
          JSA1  := TJSONArray.Create;       // array superior amb l'array dels registres

          JSO   := TJsonObject.Create();    // objecte temporal
          jso.AddPair(TJsonPair.Create('PACREGISTRE',TJSONNumber.Create(tD0.FieldByName('PACREGISTRE').AsInteger)));
          jso.AddPair(TJsonPair.Create('INFORME',TJSONNumber.Create(1)));
          jsa.AddElement(jso);              // afegim dades al registre

          JSO   := TJsonObject.Create();    // objecte temporal
          jso.AddPair(TJsonPair.Create('0',JSA));
          JSA1.AddElement(jso);             // afegim tots els registres...  en el nostre cas sempre es un

          JSObj.AddPair('user','dasi');
          JSObj.AddPair('password','avmISAD34');
          JSObj.AddPair('center',TJSONNumber.Create(idCentre));
          JSObj.AddPair('files',JSA1);
          JsonToSend := TStringStream.Create(QuitaSigno('[]',JSObj.ToString), TEncoding.UTF8);

          NetHTTPClient1.ContentType := 'application/json';
          NetHTTPClient1.AcceptCharset := 'utf-8';

          Response := NetHTTPClient1.post(url,JSonToSend).ContentAsString;
          JsonToSend.Free;
        except;
          raise;
        end;
        JSObj.Free;

        // GENEREM L'INFORME EN FORMAT HTML A UNA CARPETA TEMPORAL
        try
          tD.Close;
          tD.SelectSQL.Clear;
          tD.SelectSQL.Add('select * from informes where pacregistre=:pacregistre order by infregistre');
          tD.ParamByName('pacregistre').AsInteger := tD0.FieldByName('PACREGISTRE').AsInteger;
          tD.Open;

          while not tD.Eof do
          begin
            // capturem INFORME
            tInformes.Close;
            tInformes.ParamByName('infregistre').AsInteger := tD.FieldByName('infregistre').AsInteger;
            tInformes.Open;

            // cerca USUARIS a taula de relacions_centres
            qR1.Close;
            qR1.SQL.Clear;
            qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_USUARIS'' and D_ID=:usrregistre and e_Centre=:idCentre');
            qR1.ParamByName('idcentre').AsInteger := idcentre;
            qR1.ParamByName('usrregistre').AsInteger := tD.FieldByName('usrregistre').AsInteger;
            qR1.ExecQuery;
            if qR1.RecordCount = 0 then idUsuari := -1
              else idUsuari := qR1.FieldByName('E_ID').AsVariant;

            // cerca PROFESSIONAL a taula de relacions_centres
            qR1.Close;
            qR1.SQL.Clear;
            qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_Centre=:idCentre');
            qR1.ParamByName('idcentre').AsInteger := idcentre;
            qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
            qR1.ExecQuery;
            if qR1.RecordCount = 0 then idProfessional := -1
              else idProfessional := qR1.FieldByName('E_ID').AsVariant;

            // cerca PACIENT a taula de relacions_centres
            qR1.Close;
            qR1.SQL.Clear;
            qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and D_ID=:pacregistre and e_Centre=:idCentre');
            qR1.ParamByName('idcentre').AsInteger := idcentre;
            qR1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
            qR1.ExecQuery;
            if qR1.RecordCount = 0 then idPacient := -1
              else idPacient := qR1.FieldByName('E_ID').AsVariant;

            // cerca VISITA a taula de relacions_centres
            qR1.Close;
            qR1.SQL.Clear;
            qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''VISITES'' and D_ID=:visregistre and e_Centre=:idCentre');
            qR1.ParamByName('idcentre').AsInteger := idcentre;
            qR1.ParamByName('visregistre').AsInteger := tD.FieldByName('visregistre').AsInteger;
            qR1.ExecQuery;
            if qR1.RecordCount = 0 then idVisita := -1
              else idVisita := qR1.FieldByName('E_ID').AsVariant;

            // mirem tag  1 - general (administratiu) NO S'USA   2 - m�dico   3 - administatiu   4 - proves m�diques
            // si es administatiu es posa un 3 i surt nom�s a la fitxa del pacient
            case tD.FieldByName('INFTIPODOCUMENTO').AsInteger of
              0 : idTag := 1;
              1 : idTag := 2;
              2 : idTag := 3;
            end;

            // crear INFORME SENSE PASSAR L'INFORME (nom�s per tenir el ID
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO informes (id, plantilla, visita, usuari, centre, tag, '+
                        '  titol, contingut, dataAlta, actiu, mostrarLogo, mostrarInfoCentre,'+
                        '  idProfessional, idPacient, mostrarFechaUser, mostraPortalPacient,'+
                        '  dasiclinicReg, traspas) values(:id, :plantilla, :visita,'+
                        '  :usuari, :centre, :tag, :titol, :contingut, :dataAlta, 1, 0, 0,'+
                        '  :idProfessional, :idPacient, 0, 0, :DasiclinicReg, :traspas)');
            if idVisita > 0 then
              qE1.ParamByName('visita').AsInteger := idVisita;
            if idUsuari > 0 then
              qE1.ParamByName('usuari').AsInteger := idUsuari;
            qE1.ParamByName('centre').AsInteger := idCentre;
            qE1.ParamByName('tag').AsInteger := idTag;
            qE1.ParamByName('titol').AsString := tD.FieldByName('INFTITULO').AsString;
            qE1.ParamByName('contingut').AsString := '';
            qE1.ParamByName('dataAlta').AsDateTime := tD.FieldByName('INFDATA').AsDateTime;
            if idProfessional > 0 then
              qE1.ParamByName('idProfessional').AsInteger := idProfessional;
            qE1.ParamByName('idPacient').AsInteger := idPacient;
            qE1.ParamByName('DasiclinicReg').AsInteger := tD.FieldByName('infregistre').AsInteger;
            qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
            qE1.ExecSQL ;

            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
            qE1.ExecSQL ;
            idInforme := qE1.FieldByName('registre').AsInteger;

            // guardem en format HTML a carpeta temporal
            DBRichViewPDF.SaveHTML(Path+'informe'+StrZero(tD.FieldByName('infregistre').AsInteger,6)+'.html',tInformesINFTITULO.AsString,'',[]);

            // cerquem i pujem possibles imatges del directori
            RastreaDir( Path, idInforme );

            // guardo document encriptat
//            memo1.lines.SaveToFile('c:\traspasos\dasi\arxiu.html');
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('update informes set contingut=AES_ENCRYPT(CONVERT(:contingut USING utf8),"9w7kM5GwbJSsRnvS") where id=:idinforme');
            qE1.ParamByName('idinforme').AsInteger := idinforme;
            qE1.ParamByName('contingut').AsString := Memo1.Text;
            qE1.ExecSQL ;

            tD.Next;
          end;
        except
          raise;
        end;
        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''INFORMES'',:D_ID,''informes'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD0.FieldByName('PACREGISTRE').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := 0;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;

        tD0.Next;
        if checkBox2.Checked then UltimPacient := tD0.FieldByName('PACREGISTRE').AsInteger;
      end;
    except
      raise;
    end;

    if ((HiHaPacients) and (checkBox2.Checked)) then
    begin
      Dec(BucleInformes);  // descomptem bucle
      if BucleInformes = 0 then
      begin
        // Reiniciem el proc�s amb n pacients m�s
        sbReiniciar.Click;
        Label13.Visible := True;
        Self.Refresh;
        Sleep(120000); // aturem 2 minuts
        Label13.Visible := False;
        Self.Refresh;
        JvXPButton2.Click;
        Self.Refresh;
        Sleep(2000); // aturem 2 segons
        JvXPButton3.Click;
        Self.Refresh;
        Sleep(2000); // aturem 2 segons
        sbImatgesDocs.Click;
        Self.Refresh;
      end;
    end else
    begin
      BucleInformes := 0;
//      sbImatgesDocs.Enabled := False;
//      PassadaOK('INFORMES');
    end;
  end;
end;

function TForm2.RastreaDir( Dir: string; idInforme : integer ) : String;
var FileSearch:  TSearchRec;
    NomArxiu, Extensio, data, Arxiu, ARXNom, Url, UrlImg, TextHtml : string;
    response : string;
    params : TStrings;
    stream : TMemoryStream;
    JSONObject, currcond: TJSONObject;
begin
  // url per pujar imatges
  if EsProd then URL := 'https://www.dasieclinic.com/upload-files-dc'
    else URL := 'http://test.dasieclinic.com/upload-files-dc';

  memo1.Clear;
  ChDir ( Dir );
  //  primer agafem el TEXT
  if FindFirst ( '*.html', faDirectory, FileSearch )=0 then
  repeat
    if ((FileSearch.Attr and fadirectory) = fadirectory) then
    begin
      if (FileSearch.Name <> '.') and (FileSearch.Name <> '..') then
      begin
//        RastreaDir( Dir + '\' + FileSearch.Name );   AQUI MAI HI HAURA SUBDIRECTORIS
      end;
    end else
    begin
      NomArxiu := FileSearch.Name;
      Extensio := Quitasigno('.',ExtractFileExt(NomArxiu));

      if Extensio = 'html' then
      begin
        // agafo text
        memo1.Lines.LoadFromFile(Dir + FileSearch.Name);
        // esborro arxiu text
        DeleteFile(Dir + FileSearch.Name);
      end;
    end;
  until FindNext( FileSearch ) <> 0;
  FindClose( FileSearch );

  //  Agafem les IMATGES
  if FindFirst ( '*.*', faDirectory, FileSearch )=0 then
  repeat
    if ((FileSearch.Attr and fadirectory) = fadirectory) then
    begin
      if (FileSearch.Name <> '.') and (FileSearch.Name <> '..') then
      begin
//        RastreaDir( Dir + '\' + FileSearch.Name );   AQUI MAI HI HAURA SUBDIRECTORIS
      end;
    end else
    begin
      NomArxiu := FileSearch.Name;
      Extensio := Quitasigno('.',ExtractFileExt(NomArxiu));

      if Extensio <> 'html' then
      begin
        try
          // convertim arxiu i pujem a S3
          Arxiu := '';
          Stream := TMemoryStream.Create;
          Stream.LoadFromFile(NomArxiu);
          Arxiu := base64encode(stream);
          if Arxiu > '' then
          begin
            // convertim format data
            Data := YyyyMmDd(tD.FieldByName('INFDATA').AsDateTime);
            Data := Copy(Data,1,4)+'-'+Copy(Data,5,2)+'-'+Copy(Data,7,2);

            // controlo de posar alguna cosa si no te nom o descripcio
            ARXNom := iif(tInformes.FieldByName('INFTITULO').AsString>'',tInformes.FieldByName('INFTITULO').AsString,'Sin_nombre');

            // Prepara par�metres
            params := TStringList.Create;
            params.Add('user=dasi');
            params.Add('password=avmISAD34');
            params.Add('center='+IntToStr(idCentre));
            params.Add('HIDDEN=1');
            params.Add('IDINFORME='+IntToStr(idInforme));
            params.Add('PACREGISTRE='+IntToStr(tInformes.FieldByName('PACREGISTRE').AsInteger));
            params.Add('USRREGISTRE='+IntToStr(tInformes.FieldByName('USRREGISTRE').AsInteger));
            params.Add('IMGDESCRIPCIO='+HTTPEncode(ARXNom));
            params.Add('IMGNOM='+HTTPEncode(ARXNom));
            params.Add('IMGDATA='+Data);
            params.Add('INFORME=1');
            params.Add('IMAGEN='+Arxiu);
//            params.SaveToFile('params.txt');
//            Memo1.Lines.SaveToFile('html_sense_imatge.html');

            try
              NetHTTPClient1.ContentType := 'application/x-www-form-urlencoded';
              NetHTTPClient1.AcceptCharset := 'utf-8';

              Response := NetHTTPClient1.post(url,Params).ContentAsString;
              JSONObject := TJSONObject.ParseJSONValue(Response) as TJSONObject;
//              currcond := JSONObject.GetValue('result') as TJSONObject;
              UrlImg := JSONObject.GetValue('url').Value;

              // substitueixo hash al text
              if UrlImg > '' then
              begin
                Memo1.Text := AnsiReplaceStr(Memo1.Text, NomArxiu, UrlImg);
//                Memo1.Lines.SaveToFile('html_amb_imatge.html');
              end;

            except;
              raise;
            end;
          end;
          stream.Free;
        except
          raise;
        end;

        // esborro arxiu imatge
        showmessage('esborro arxiu imatge');
        DeleteFile(Dir + FileSearch.Name);
      end;
    end;
  until FindNext( FileSearch ) <> 0;
  FindClose( FileSearch );

  showmessage('he acabat');
end;


procedure TForm2.sbPoblacionsClick(Sender: TObject);
begin
  if CheckBox1.Checked then RevisarPob;
end;

procedure TForm2.RevisarPob;
begin
  memo1.clear;
  tDPob.Open;

  while not tDPob.Eof do
  begin
    // miro si esta la poblacio a la taula de relacions
    qR1.Close;
    qR1.SQL.Clear;
    qR1.SQL.Add('select id from relacions where d_taula=''S_POBLACIONS'' and D_ID=:D_ID');
    qR1.ParamByName('D_ID').AsInteger := tDPob.FieldByName('POBREGISTRE').AsInteger;
    qR1.ExecQuery;

    // si no esta miro si quina poblacio es correspon amb eClinic i l'afageixo a relacions
    if qR1.RecordCount = 0 then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select idzona from zones where nom=:nom and pais=:pais and pare=:pare and nivell=2');
      qE1.ParamByName('nom').AsString := tDPob.FieldByName('pobnom').AsString;
      qE1.ParamByName('pais').AsString := tDPob.FieldByName('paia2').AsString;
      qE1.ParamByName('pare').AsInteger := tDPob.FieldByName('prvregistre').AsInteger;
      qE1.ExecSQL ;
      if qE1.RecordCount > 0 then
      begin
        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS (D_TAULA, D_ID, E_TAULA, E_ID, E_ZONA) '+
                     'values (''S_POBLACIONS'',:D_ID,''zones'',:E_ID,2)');
        qR2e.ParamByName('D_ID').AsInteger := tDPob.FieldByName('pobregistre').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := qE1.FieldByName('idzona').AsInteger;
        qR2e.ExecQuery;
      end else
      begin
        memo1.Lines.Add(tDPob.FieldByName('pobnom').AsString);
      end;
    end;

    tDPob.Next;
  end;
end;

procedure TForm2.sbPreusClick(Sender: TObject);
var idTractament, idTarifaMutua : integer;
begin
  // TARIFES_TRACTAMENTS
  // si TRFREGISTRE=0 es la tarifa particular, actualitzem el preu a tractaments
  // si es diferent actualitzem el preu de TarifaTractament
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from tarifes_periodes order by tperegistre');
    tD.Open;
    while not tD.Eof do
    begin
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from TARIFES_TRACTAMENT where tperegistre=:periode '+
                        'order by traregistre');
      tD1.ParamByName('periode').AsInteger := tD.FieldByName('tperegistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // cerco idTractament
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TRACTAMENTS'' '+
                    'and D_ID=:TRAREGISTRE and E_CENTRE=:idCentre');
        qR1.ParamByName('TRAREGISTRE').AsInteger := tD1.FieldByName('TRAREGISTRE').AsInteger;
        qR1.ParamByName('idCentre').AsInteger := idCentre;
        qR1.ExecQuery;
        if qR1.RecordCount > 0 then
        begin
          idTractament := qR1.FieldByName('E_ID').AsVariant;

          if tD1.FieldByName('trfregistre').AsInteger=0 then
          begin
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('Update tractaments set import=:import where idTractament=:idTractament '+
                        'and idCentre=:idCentre');
            qE1.ParamByName('idCentre').AsInteger := idCentre;
            qE1.ParamByName('idTractament').AsInteger := idTractament;
            qE1.ParamByName('import').AsFloat := tD1.FieldByName('PREU').AsFloat;
            qE1.ExecSQL ;
          end else
          begin
            // cerco idTarifaMutua
            qR1.Close;
            qR1.SQL.Clear;
            qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES'' '+
                        'and D_ID=:TRFREGISTRE and E_CENTRE=:idCentre');
            qR1.ParamByName('TRFREGISTRE').AsInteger := tD1.FieldByName('TRFREGISTRE').AsInteger;
            qR1.ParamByName('idCentre').AsInteger := idCentre;
            qR1.ExecQuery;
            idTarifaMutua := qR1.FieldByName('E_ID').AsVariant;

            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO tarifaTractament (concepteMutua, codiMutua, actiu,'+
                        'preu, importMutua, pagarMutua, facturarMutua, comisioFixaPrf,'+
                        'demanaVolant, idTractament, idTarifaMutua) VALUES(:concepteMutua,'+
                        ':codiMutua, 1, :preu, :importMutua, :pagarMutua, :facturarMutua,'+
                        ':comisioFixaPrf, :demanaVolant, :idTractament, :idTarifaMutua)');
            qE1.ParamByName('concepteMutua').AsString := tD1.FieldByName('CONCEPTE_MUTUA').AsString;
            qE1.ParamByName('codiMutua').AsString := tD1.FieldByName('CODI_MUTUA').AsString;
            qE1.ParamByName('preu').AsFloat := tD1.FieldByName('PREU').AsFloat;
            if tD1.FieldByName('FACTURAR_MUTUA').AsInteger = 1 then
            begin
              qE1.ParamByName('facturarMutua').AsFloat := tD1.FieldByName('IMPORT_MUTUA').AsFloat; // en teoria aix� hauria de ser un flag pero al eclinic repeteis el preu
              qE1.ParamByName('importMutua').AsFloat := tD1.FieldByName('IMPORT_MUTUA').AsFloat;
              qE1.ParamByName('pagarMutua').AsFloat := tD1.FieldByName('PAGAR_MUTUA').AsFloat;
            end;
            qE1.ParamByName('comisioFixaPrf').AsFloat := tD1.FieldByName('COMISION_FIJA_PRF').AsFloat;
            qE1.ParamByName('demanaVolant').AsInteger := tD1.FieldByName('PIDE_VOLANTE').AsInteger;
            qE1.ParamByName('idTractament').AsInteger := idTractament;
            qE1.ParamByName('idTarifaMutua').AsInteger := idTarifaMutua;
            qE1.ExecSQL ;
          end;
        end;
        tD1.Next;
      end;
      tD.Next;
    end;
    sbPreus.Enabled := False;
    PassadaOK('TARIFES_TRACTAMENT');
  except
    raise;
  end;
end;

procedure TForm2.sbProcedenciesClick(Sender: TObject);
var registre : integer;
begin
  // PROCEDENCIES
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select procregistre, procnom, coalesce(otros,0) otros from s_procedencias '+
                     'where procregistre>0 order by procregistre');
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO procedencies (idProcedencia, nom, dadesAdicionals,'+
                  'idCentre, traspas, DasiClinicReg) VALUES(:idProcedencia, :nom,'+
                  ':dadesAdicionals, :idCentre, :traspas, :DasiClinicReg)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('procnom').AsString;
      qE1.ParamByName('dadesAdicionals').AsInteger := tD.FieldByName('otros').AsInteger;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ParamByName('DasiClinicReg').AsInteger := tD.FieldByName('procregistre').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_PROCEDENCIES'',:D_ID,''procedencies'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('procregistre').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbProcedencies.Enabled := False;
    PassadaOK('S_PROCEDENCIES');
  except
    raise;
  end;
end;

procedure TForm2.sbOrigensClick(Sender: TObject);
var registre : integer;
begin
  // ORIGENES
  // aquest s'ha de fer perque no te Taula esta a una taula gen�rica i no te lo de OTROS
  try
    sbOrigens.Enabled := False;
    PassadaOK('S_ORIGENES');
  except
    raise;
  end;
end;

procedure TForm2.sbReiniciarClick(Sender: TObject);
begin
  FIBDades.Close;
  msEClinic.Close;
  Label2.Caption := 'Cl�nica Origen FIREBIRD';
  Edit4.Text := '';
  Edit5.Text := '0';
  JvXPButton2.Enabled := True;
  Label5.Caption := 'Cl�nica Destino MySQL';
  JvXPButton1.Enabled := True;
  JvXPButton3.Enabled := True;
  Botons(Sender);
end;

procedure TForm2.sbRelacionsCClick(Sender: TObject);
var idUsuari, idProfessional : integer;
begin
  // USUARIS_RC
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select rc.* from S_USUARIS_RC rc '+
                     'left join s_usuaris u on rc.usrregistre=u.usrregistre '+
                     'where u.usractiu=''1''');
    tD.Open;
    while not tD.Eof do
    begin
      // agafem el ID del usuari al eClinic
      if not tD.FieldByName('usrregistre').IsNull then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_USUARIS'' and D_ID=:usrregistre and e_centre=:idcentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('usrregistre').AsInteger := tD.FieldByName('usrregistre').AsInteger;
        qR1.ExecQuery;
        idUsuari := qR1.FieldByName('E_ID').AsVariant;
      end else idUsuari := -1;

      // agafem el id del professinal al eClinic
      if not tD.FieldByName('prfregistre').IsNull then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_centre=:idcentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
        qR1.ExecQuery;
        idProfessional := qR1.FieldByName('E_ID').AsVariant;
      end else idProfessional := -1;

      // insertem relacion de confian�a
      if ((idProfessional>0) and (idUsuari>0)) then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO professionals_rc (actiu, agenda, visites, cursclinic,'+
                    'editcurs, editagenda, idUsuari, idProfessional, idCentre, dasiclinicId) '+
                    'VALUES( 1, 1, 1, 1, 1, 1, :idUsuari, :idProfessional, :idCentre,'+
                    ':dasiclinicId)');
        qE1.ParamByName('idUsuari').AsInteger := idUsuari;
        qE1.ParamByName('idProfessional').AsInteger := idProfessional;
        qE1.ParamByName('idCentre').AsInteger := idCentre;
        qE1.ParamByName('dasiclinicId').AsInteger := tD.FieldByName('ID').AsInteger;
        qE1.ExecSQL ;
      end;
      tD.Next;
    end;
    sbRelacionsC.Enabled := False;
    PassadaOK('S_USUARIS_RC');
  except
    raise;
  end;
end;

procedure TForm2.sbTarifesClick(Sender: TObject);
var registre, idAdreca, idEmail, idPersona, idTarifaMutua, idPeriodeEclinic  : integer;
    pais : string;
    idZona, idGrup, creat, inici, fi, idPeriode  : Variant;
begin
  // TARIFES
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from tarifes where trfregistre>=0');
    tD.Open;
    while not tD.Eof do
    begin
      // crear persona + mails + adreces
      // buscar poblacio->zona a taula de relacions
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from relacions where d_taula=''S_POBLACIONS'' and D_ID=:pobregistre');
      qR1.ParamByName('pobregistre').AsInteger := tD.FieldByName('POBREGISTRE').AsInteger;
      qR1.ExecQuery;
      IDZona := qR1.FieldByName('E_ID').AsVariant;

      // cercar tarifa grup
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES_GRUPS'' '+
                  'and D_ID=:TRGREGISTRE and E_CENTRE=:idCentre');
      qR1.ParamByName('TRGREGISTRE').AsInteger := tD.FieldByName('TRGREGISTRE').AsInteger;
      qR1.ParamByName('idCentre').AsInteger := idCentre;
      qR1.ExecQuery;
      idGrup := qR1.FieldByName('E_ID').AsVariant;

      // cercar pais de la zona a MySql
      if idZona >= 0 then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select pais from zones where idzona=:idzona');
        qE1.ParamByName('idzona').AsInteger := idzona;
        qE1.ExecSQL ;
        Pais := qE1.FieldByName('pais').AsString;
      end;

      // cercar dates del primer per�ode TARIFES_PERIODE Creaci�, Inici, f�
      qD1.Close;
      qD1.SQL.Clear;
      qD1.SQL.Add('select first 1 TPEREGISTRE, TPEDATA, TPEDATAINI, TPEDATAFI from TARIFES_PERIODES '+
                  'where TRFREGISTRE=:TRFREGISTRE order by TPEREGISTRE');
      qD1.ParamByName('TRFREGISTRE').AsInteger := tD.FieldByName('TRFREGISTRE').AsInteger;
      qD1.ExecQuery;
      Creat := qD1.FieldByName('TPEDATA').AsVariant;
      inici := qD1.FieldByName('TPEDATAINI').AsVariant;
      fi := qD1.FieldByName('TPEDATAFI').AsVariant;
      idPeriode := qD1.FieldByName('TPEREGISTRE').AsVariant;

      // CREAR adre�a
      if ((tD.FieldByName('TRFADRECA').AsString > '') and
          (tD.FieldByName('TRFCP').AsString > '')) then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO adreces (idAdreca, direccio1, cp, pais, idZona) '+
                    'VALUES(:idAdreca, AES_ENCRYPT(:direccio1,"9w7kM5GwbJSsRnvS"),'+
                    'AES_ENCRYPT(:cp,"9w7kM5GwbJSsRnvS"), :pais, :idzona)');
        qE1.ParamByName('direccio1').AsString := tD.FieldByName('TRFADRECA').AsString;
        qE1.ParamByName('cp').AsString := tD.FieldByName('TRFCP').AsString;
        qE1.ParamByName('pais').AsString := pais;
        if idZona >= 0 then qE1.ParamByName('idZona').AsInteger := idZona;
        qE1.ExecSQL ;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        idAdreca := qE1.FieldByName('registre').AsInteger;
      end else idAdreca := -1;

      // CREAR email
      if tD.FieldByName('TRFEMAIL').AsString > '' then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO emails (idEmail, email) VALUES(:idEmail, AES_ENCRYPT(:email,"9w7kM5GwbJSsRnvS"))');
        qE1.ParamByName('email').AsString := tD.FieldByName('TRFEMAIL').AsString;
        qE1.ExecSQL ;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        idEmail := qE1.FieldByName('registre').AsInteger;
      end else idEmail := -1;

      // crear persona
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO persones (idPersona, nom, nomComplert, numDocument,'+
                  'telefon1, telefon2, idAdreca, idEmail) '+
                  'VALUES(:idPersona, AES_ENCRYPT(CONVERT(:nom USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:nomComplert USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:numDocument,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon2,"9w7kM5GwbJSsRnvS"), :idAdreca, :idEmail)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('TRFDESCRIPCIO').AsString;
      qE1.ParamByName('nomComplert').AsString := tD.FieldByName('TRFDESCRIPCIO').AsString;
      qE1.ParamByName('numDocument').AsString := tD.FieldByName('TRFNIF').AsString;
      qE1.ParamByName('telefon1').AsString := tD.FieldByName('TRFTELEFON1').AsString;
      qE1.ParamByName('telefon2').AsString := tD.FieldByName('TRFTELEFON2').AsString;
      if idAdreca >= 0 then
        qE1.ParamByName('idAdreca').AsInteger := idAdreca;
      if idEmail >= 0 then
        qE1.ParamByName('idEmail').AsInteger := idEmail;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idPersona := qE1.FieldByName('registre').AsInteger;

      // crear mutua
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO tarifaMutua (idTarifaMutua, actiu, esmutua, modificapreumutua,'+
                  'ccoste, cuenta, observacions, dataCreat, dataIni, dataFi, idPersona,'+
                  'idTarifaGrup, esPeriode, idCentre, dasiclinicid, DasiClinicReg) '+
                  ' VALUES (:idTarifaMutua, :actiu, :esmutua, :modificapreumutua, '+
                  ':ccoste, :cuenta, :observacions, :dataCreat, :dataIni, :dataFi, '+
                  ':idPersona, :idTarifaGrup, :esPeriode, :idCentre, :DasiclinicID, :DasiClinicReg)');
      qE1.ParamByName('actiu').AsInteger := iif(tD.FieldByName('INACTIVA').AsInteger=1,0,1);
      qE1.ParamByName('esmutua').AsInteger := 0;
      qE1.ParamByName('modificapreumutua').AsInteger := iif(tD.FieldByName('MODIFICA_PRECIO_MUTUA').AsString='1',1,0);
      qE1.ParamByName('ccoste').AsString := tD.FieldByName('CCOSTE').AsString;
      qE1.ParamByName('cuenta').AsString := tD.FieldByName('CUENTA').AsString;
      qE1.ParamByName('observacions').AsString := tD.FieldByName('TRFOBERVACIONS').AsString;
      if Creat <> null then qE1.ParamByName('dataCreat').AsDateTime := Creat;
      if Inici <> null then qE1.ParamByName('dataIni').AsDateTime := Inici;
      if Fi <> null then qE1.ParamByName('dataFi').AsDateTime := Fi;
      qE1.ParamByName('idPersona').AsInteger := idPersona;
      if idGrup >= 0 then qE1.ParamByName('idTarifaGrup').AsInteger := idGrup;
//      qE1.ParamByName('esPeriode').AsInteger := 0;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('dasiclinicid').AsInteger := tD.FieldByName('ID').AsInteger;
      qE1.ParamByName('DasiClinicReg').AsInteger := tD.FieldByName('TRFREGISTRE').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idTarifaMutua := qE1.FieldByName('registre').AsInteger;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO tarifesCentres (idTarifaMutua, idCentre) values(:idTarifaMutua, :idCentre)');
      qE1.ParamByName('idTarifaMutua').AsInteger := idTarifaMutua;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''TARIFES'',:D_ID,''tarifaMutua'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('TRFREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idTarifaMutua;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''TARIFES_PERIODES'',:D_ID,''tarifaMutua'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := idPeriode;
      qR2e.ParamByName('E_ID').AsInteger := idTarifaMutua;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      // crear per�odes si ni ha m�s de 1 (el b�sic)
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from TARIFES_PERIODES where trfregistre=:tarifa '+
                        'and tperegistre<>:idperiode order by tperegistre');
      tD1.Open;
      while not tD1.Eof do
      begin
        // crear Periode que al eClinic es una nova mutua
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO tarifaMutua (idTarifaMutua, actiu, esmutua, modificapreumutua,'+
                    'ccoste, cuenta, observacions, dataCreat, dataIni, dataFi, idPersona,'+
                    'idTarifaGrup, esPeriode, idCentre) VALUES (:idTarifaMutua, :actiu,'+
                    ':esmutua, :modificapreumutua, :ccoste, :cuenta, :observacions,'+
                    ':dataCreat, :dataIni, :dataFi, :idPersona, :idTarifaGrup,'+
                    ':esPeriode, :idCentre)');
        qE1.ParamByName('actiu').AsInteger := iif(tD.FieldByName('INACTIVA').AsInteger=1,0,1);
        qE1.ParamByName('esmutua').AsInteger := 0;
        qE1.ParamByName('modificapreumutua').AsInteger := iif(tD.FieldByName('MODIFICA_PRECIO_MUTUA').AsString='1',1,0);
        qE1.ParamByName('ccoste').AsString := tD.FieldByName('CCOSTE').AsString;
        qE1.ParamByName('cuenta').AsString := tD.FieldByName('CUENTA').AsString;
        qE1.ParamByName('observacions').AsString := tD.FieldByName('TRFOBERVACIONS').AsString;
        if not tD1.FieldByName('TPEDATA').IsNull then
          qE1.ParamByName('dataCreat').AsDateTime := tD1.FieldByName('TPEDATA').AsDateTime;
        if not tD1.FieldByName('TPEDATAINI').IsNull then
          qE1.ParamByName('dataIni').AsDateTime := tD1.FieldByName('TPEDATAINI').AsDateTime;
        if not tD1.FieldByName('TPEDATAFI').IsNull then
          qE1.ParamByName('dataFi').AsDateTime := tD1.FieldByName('TPEDATAFI').AsDateTime;
        qE1.ParamByName('idPersona').AsInteger := idPersona;
        if idGrup >= 0 then qE1.ParamByName('idTarifaGrup').AsInteger := idGrup;
        qE1.ParamByName('esPeriode').AsInteger := idTarifaMutua;
        qE1.ParamByName('idCentre').AsInteger := idCentre;
        qE1.ExecSQL ;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        idPeriodeEclinic := qE1.FieldByName('registre').AsInteger;

        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''TARIFES_PERIODES'',:D_ID,''tarifaMutua'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD1.FieldByName('TPEREGISTRE').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := idPeriodeEclinic;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;

        tD1.Next;
      end;

      tD.Next;
    end;
    sbTarifes.Enabled := False;
    PassadaOK('TARIFES');
  except
    raise;
  end;
end;

procedure TForm2.sbTarifesGRPClick(Sender: TObject);
var registre : integer;
begin
  // GRUPS de Tarifes
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from TARIFES_GRUPS where TRGREGISTRE>0');
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO tarifaGrup (idTarifaGrup, nom, observacions, actiu) '+
                  'VALUES(:idTarifaGrup, :nom, :observacions, :actiu)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('TRGDESCRIPCIO').AsString;
      qE1.ParamByName('observacions').AsString := tD.FieldByName('TRGOBSERVACIONS').AsString;
      qE1.ParamByName('actiu').AsInteger := iif(tD.FieldByName('TRGINACTIVO').AsInteger=1,0,1);
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''TARIFES_GRUPS'',:D_ID,''tarifaGrup'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('TRGREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbTarifesGRP.Enabled := False;
    PassadaOK('TARIFES_GRUPS');
  except
    raise;
  end;
end;

procedure TForm2.sbTipusCitaClick(Sender: TObject);
var registre : integer;
begin
  // tipus de cita
  try
    // esborrem els tipus de cita/visita que tingui el centre
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from tipusVisita where idCentre=:idCentre');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;

    // les dades estan a la taula S_COMBOS
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select cmbordre, cmbvalor, ID from S_COMBOS '+
                     'where CMBTAULA=''S_CITACIONS'' and CMBCOMBO=''TIPUS_CITA'' ');
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO tipusVisita (id, nom, descripcio, actiu, idCentre, '+
                  'pacient, privada, dasiclinicReg) VALUES( :id, :nom, :descripcio,'+
                  '1, :idCentre, 1, 0, :dasiclinicReg)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('cmbvalor').AsString;
      qE1.ParamByName('descripcio').AsString := tD.FieldByName('cmbvalor').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('id').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''TIPUS_CITA'',:D_ID,''tipusVisita'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('cmbordre').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbTipusCita.Enabled := False;
    PassadaOK('TIPUS_CITA');
  except
    raise;
  end;
end;

procedure TForm2.sbTipusDocClick(Sender: TObject);
var registre : integer;
begin
  // tipus documents
  try
    // esborrem els tipus de document que crea l'eclinic per defecte
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from tipusDocument where idCentre=:idCentre');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;

    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select a.PDCREGISTRE, a.DESCRIPCION, a.OBLIGATORIO '+
                     'from TIPUS_DOCUMENT a '+
                     'where a.idioma=(select idioma from parametres where terminal=1)');
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO tipusDocument (pais, idTipus, nom, descripcio,obligatori,actiu,'+
                  'idCentre) VALUES(''ES'', :idTipus, :nom, :nom, :obligatori, 1, :idCentre)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('DESCRIPCION').AsString;
      qE1.ParamByName('obligatori').AsInteger := iif(tD.FieldByName('OBLIGATORIO').AsInteger>=1,1,0);
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''TIPUS_DOCUMENT'',:D_ID,''tipusDocument'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('PDCREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbTipusDoc.Enabled := False;
    PassadaOK('TIPUS_DOCUMENT');
  except
    raise;
  end;
end;

procedure TForm2.sbTutorsClick(Sender: TObject);
var registre, idAdreca, idEmail, idPersona, idClient, idPacient : integer;
    pais : string;
    idZona, idTipusDocument : Variant;
begin
  // TUTORS
  try
    // ESBORREM CLIENTS DEMO o petades traspas
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''S_TUTORS'' and e_Centre=:idCentre');
      qR2e.ParamByName('idcentre').AsInteger := idcentre;
      qR2e.ExecQuery;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from clients where idCentre=:idCentre and dasiclinicreg>0');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;
  except
    raise;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from s_pacients where pacregistre>0 and pacfratutor=1 order by pachistorian');
    tD.Open;
    while not tD.Eof do
    begin
      // crear persona + mails + adreces
      // buscar poblacio->zona a taula de relacions
      if tD.FieldByName('POBREGISTRE').AsInteger > 0 then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from relacions where d_taula=''S_POBLACIONS'' and D_ID=:pobregistre');
        qR1.ParamByName('pobregistre').AsInteger := tD.FieldByName('POBREGISTRE').AsInteger;
        qR1.ExecQuery;
        IDZona := qR1.FieldByName('E_ID').AsVariant;
      end else IDZona := 0;

      // cercar pais de la zona a MySql
      if idZona > 0 then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select pais from zones where idzona=:idzona');
        qE1.ParamByName('idzona').AsInteger := idzona;
        qE1.ExecSQL ;
        Pais := qE1.FieldByName('pais').AsString;
      end else Pais := 'ES';

      // cercar TipusDocument a taula de relacions_centres
      if not tD.FieldByName('PDCREGISTRE').IsNull then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TIPUS_DOCUMENT'' and D_ID=:PDCREGISTRE and e_centre=:idcentre');
        qR1.ParamByName('PDCREGISTRE').AsInteger := tD.FieldByName('PDCREGISTRE').AsInteger;
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ExecQuery;
        idTipusDocument := qR1.FieldByName('E_ID').AsVariant;
      end else idTipusDocument := null;

      // CREAR adre�a
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO adreces (idAdreca, direccio1, cp, pais, idZona) '+
                  'VALUES(:idAdreca, AES_ENCRYPT(CONVERT(:direccio1 USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:cp,"9w7kM5GwbJSsRnvS"), :pais, :idzona)');
      qE1.ParamByName('direccio1').AsString := tD.FieldByName('PACADRECA').AsString;
      qE1.ParamByName('cp').AsString := tD.FieldByName('PACCP').AsString;
      qE1.ParamByName('pais').AsString := pais;
      if idZona >= 0 then qE1.ParamByName('idZona').AsInteger := idZona;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idAdreca := qE1.FieldByName('registre').AsInteger;

      // CREAR email
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO emails (idEmail, email) VALUES(:idEmail, AES_ENCRYPT(:email,"9w7kM5GwbJSsRnvS"))');
      qE1.ParamByName('email').AsString := tD.FieldByName('PACEMAIL').AsString;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idEmail := qE1.FieldByName('registre').AsInteger;

      // crear persona
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO persones (idPersona, nom, nomComplert, numDocument,'+
                  'telefon1, mobil, cognom1, cognom2, idAdreca, idEmail, TipusDocument) '+
                  'VALUES(:idPersona, AES_ENCRYPT(CONVERT(:nom USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:nomComplert USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:numDocument,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:mobil,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:cognom1 USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:cognom2 USING utf8),"9w7kM5GwbJSsRnvS"), :idAdreca, :idEmail, :TipusDocument)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('PACNOM').AsString;
      qE1.ParamByName('cognom1').AsString := tD.FieldByName('PACCOGNOM1').AsString;
      qE1.ParamByName('cognom2').AsString := tD.FieldByName('PACCOGNOM2').AsString;
      qE1.ParamByName('nomComplert').AsString := Trim(Trim(tD.FieldByName('PACCOGNOM1').AsString) + ' ' +
                                                      Trim(tD.FieldByName('PACCOGNOM2').AsString) + ', ' +
                                                      Trim(tD.FieldByName('PACNOM').AsString));
      qE1.ParamByName('numDocument').AsString := tD.FieldByName('PACNIF').AsString;
      qE1.ParamByName('telefon1').AsString := tD.FieldByName('PACTELEFON1').AsString;
      qE1.ParamByName('mobil').AsString := tD.FieldByName('PACTELEFON2').AsString;
      if idTipusDocument > 0 then
        qE1.ParamByName('TipusDocument').AsInteger := idTipusDocument;
      qE1.ParamByName('idAdreca').AsInteger := idAdreca;
      qE1.ParamByName('idEmail').AsInteger := idEmail;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idPersona := qE1.FieldByName('registre').AsInteger;

      RichEdit1.Clear;
      RichEdit1.Text := tD.FieldByName('PACOBSERVACIONS').AsString;

      // crear CLIENT
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO clients( idClient, dataAlta, fax, relacio, actiu, observacions,'+
                  'idCentre, idPersona, empresa, percIRPF, iban, dasiclinicReg, traspas,'+
                  'idFormaPagamentClient) VALUES( :idClient, :dataAlta, :fax, :relacio,'+
                  ':actiu, :observacions, :idCentre, :idPersona, :empresa, :percIRPF,'+
                  ':iban, :dasiclinicReg, :traspas, :idFormaPagamentClient)');
      if not tD.FieldByName('pacdatainici').IsNull
        then qE1.ParamByName('dataAlta').AsDateTime := tD.FieldByName('pacdatainici').AsDateTime
        else qE1.ParamByName('dataAlta').AsDateTime := Date;
      // fax i relacio no ho tenim al dasiclinic
      qE1.ParamByName('actiu').AsInteger := iif(tD.FieldByName('inactiu').AsInteger=1,0,1);
      qE1.ParamByName('observacions').AsString := RTF2PlainText(RichEdit1.Text);
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('idPersona').AsInteger := idPersona;
      qE1.ParamByName('empresa').AsInteger := 0;  // indica si es persona o empresa
      qE1.ParamByName('percIRPF').AsFloat := 0;
      if not tD.FieldByName('dom_iban').IsNull then
        qE1.ParamByName('iban').AsString := tD.FieldByName('dom_iban').AsString;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idClient := qE1.FieldByName('registre').AsInteger;

      // crear RELACIO TUTOR - PACIENTS
      // s'ha de buscar quins registres tenen en el camp PACTUTOR1 O PACTUTOR2 aquest PACREGISTRE
      // dels que el tinguin, cerquem el idPacient a la taula RELACIONS_CENTRE i insertem
      // un registre per cada un a la taula "clientsPacients" en el primer posarem
      // el camp defecte a 1, si en te dos el segon posarem el valor a 0
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select pacregistre from s_pacients where pactutor=:tutor');
      tD1.ParamByName('tutor').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // cerca idPacient a taula de relacions
        if tD1.FieldByName('PACREGISTRE').AsInteger > 0 then
        begin
          qR1.Close;
          qR1.SQL.Clear;
          qR1.SQL.Add('select E_ID from relacions where d_taula=''S_PACIENTS'' and D_ID=:pacregistre');
          qR1.ParamByName('pacregistre').AsInteger := tD1.FieldByName('PACREGISTRE').AsInteger;
          qR1.ExecQuery;

          // crear CLIENT/PACIENT
          if qR1.RecordCount > 0 then
          begin
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO clientsPacients( idClient, idPacient, defecte) '+
                        'VALUES( :idClient, :idPacient, :defecte)');
            qE1.ParamByName('idClient').AsInteger := idClient;
            qE1.ParamByName('idPacient').AsInteger := qR1.FieldByName('E_ID').AsInteger;
            qE1.ParamByName('defecte').AsInteger := 1;
            qE1.ExecSQL ;
          end;
        end;
        tD1.Next;
      end;

      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select pacregistre from s_pacients where pactutor2=:tutor');
      tD1.ParamByName('tutor').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // cerca idPacient a taula de relacions
        if tD1.FieldByName('PACREGISTRE').AsInteger > 0 then
        begin
          qR1.Close;
          qR1.SQL.Clear;
          qR1.SQL.Add('select E_ID from relacions where d_taula=''S_PACIENTS'' and D_ID=:pacregistre');
          qR1.ParamByName('pacregistre').AsInteger := tD1.FieldByName('PACREGISTRE').AsInteger;
          qR1.ExecQuery;

          // crear CLIENT/PACIENT
          if qR1.RecordCount > 0 then
          begin
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO clientsPacients( idClient, idPacient, defecte) '+
                        'VALUES( :idClient, :idPacient, :defecte)');
            qE1.ParamByName('idClient').AsInteger := idClient;
            qE1.ParamByName('idPacient').AsInteger := qR1.FieldByName('pacregistre').AsInteger;
            qE1.ParamByName('defecte').AsInteger := 0;
            qE1.ExecSQL ;
          end;
          tD1.Next;
        end;
      end;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_TUTORS'',:D_ID,''clients'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('PACREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idClient;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbTutors.Enabled := False;
    PassadaOK('S_TUTORS');
  except
    raise;
  end;
end;

procedure TForm2.sbUsuarisClick(Sender: TObject);
var idIdioma, idEmail, idPersona, idTipusDocument, idProfessional, idUsuari,
    User_ID, Rol, idClientDasi, idRolClient, idRolCentre, ExisteixUSR : integer;
    Pais, Password, Salt, Tipus, Clau : string;
begin
  // agafem idClient de la taula centres
  qE1.Close;
  qE1.SQL.Clear;
  qE1.SQL.Add('select clientDasi from dasieclinicDB.centres where idcentre=:idcentre');
  qE1.ParamByName('idcentre').AsInteger := idcentre;
  qE1.ExecSQL ;
  idClientDasi := qE1.FieldByName('clientDasi').AsInteger;

  // USUARIS
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from s_usuaris where usrregistre>=0');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca PROFESSIONAL a taula de relacions_centres
      if not tD.FieldByName('Prfregistre').IsNull then
      begin
        if tD.FieldByName('Prfregistre').AsInteger > 0 then
        begin
          qR1.Close;
          qR1.SQL.Clear;
          qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:Prfregistre and e_centre=:idcentre');
          qR1.ParamByName('idcentre').AsInteger := idcentre;
          qR1.ParamByName('Prfregistre').AsInteger := tD.FieldByName('Prfregistre').AsInteger;
          qR1.ExecQuery;
          idProfessional := qR1.FieldByName('E_ID').AsVariant;
        end else idProfessional := -1;
      end else idProfessional := -1;

      // crear persona + mails + adreces
      // cercar pais de la zona a MySql
      Pais := 'ES';

      // cercar TipusDocument
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select idTipus from dasieclinicDB.tipusDocument where idcentre=:idcentre and nom like :tipus');
      qE1.ParamByName('idcentre').AsInteger := idcentre;
      qE1.ParamByName('tipus').AsString := '%DNI%';
      qE1.ExecSQL ;
      idTipusDocument := qE1.FieldByName('idTipus').AsInteger;

      // CREAR email
      if not tD.FieldByName('usr_mail').IsNull then
      begin
        if Trim(tD.FieldByName('usr_mail').AsString)>'' then
        begin
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('INSERT INTO emails (idEmail, email) VALUES(:idEmail, AES_ENCRYPT(CONVERT(:email USING utf8),"9w7kM5GwbJSsRnvS"))');
          qE1.ParamByName('email').AsString := tD.FieldByName('usr_mail').AsString;
          qE1.ExecSQL ;

          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
          qE1.ExecSQL ;
          idEmail := qE1.FieldByName('registre').AsInteger;
        end else idEmail := -1;
      end else idEmail := -1;

      // crear persona
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO persones (idPersona, nom, nomComplert, numDocument,'+
                  'telefon1, mobil, cognom1, cognom2, idAdreca, idEmail, TipusDocument) '+
                  'VALUES(:idPersona, AES_ENCRYPT(CONVERT(:nom USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:nomComplert USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:numDocument,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:mobil,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:cognom1 USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:cognom2 USING utf8),"9w7kM5GwbJSsRnvS"), :idAdreca, :idEmail, :TipusDocument)');
      qE1.ParamByName('nom').AsString := iif(tD.FieldByName('USR_NOM').IsNull,tD.FieldByName('USRNOM').AsString,tD.FieldByName('USR_NOM').AsString);
      qE1.ParamByName('cognom1').AsString := tD.FieldByName('USR_COGNOM1').AsString;
      qE1.ParamByName('cognom2').AsString := tD.FieldByName('USR_COGNOM2').AsString;
      qE1.ParamByName('nomComplert').AsString := iif(tD.FieldByName('USR_NOMCOMPLERT').IsNull,tD.FieldByName('USRNOM').AsString,tD.FieldByName('USR_NOMCOMPLERT').AsString);
      qE1.ParamByName('telefon1').AsString := tD.FieldByName('USR_TELEFON').AsString;
      if idTipusDocument > 0 then
        qE1.ParamByName('TipusDocument').AsInteger := idTipusDocument;
      //qE1.ParamByName('idAdreca').AsInteger := idAdreca;        no tenim adre�a al Dasiclinic
      if idEmail > 0 then
        qE1.ParamByName('idEmail').AsInteger := idEmail;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idPersona := qE1.FieldByName('registre').AsInteger;

      // agafem idioma del centre per defecte
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select idIdioma from dasieclinicDB.centres where idcentre=:idcentre');
      qE1.ParamByName('idcentre').AsInteger := idcentre;
      qE1.ExecSQL ;
      idIdioma := qE1.FieldByName('idIdioma').AsInteger;

      // crear usuaris
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO usuaris (Idioma, professional, idUsuari, dataAlta,' +
                  'actiu, centreActiu, idPersona, Clientdasi, Getstarted,'+
                  'dasiclinicId, dasiclinicReg) VALUES(:Idioma, :professional, :idUsuari,'+
                  ':dataAlta, :actiu, :centreActiu, :idPersona, :Clientdasi, 1,'+
                  ':dasiclinicId, :dasiclinicReg)');
      qE1.ParamByName('Idioma').AsInteger := idIdioma;
      if idProfessional > 0 then
        qE1.ParamByName('professional').AsInteger := idProfessional;
      qE1.ParamByName('dataAlta').AsDateTime := Date;
      qE1.ParamByName('centreActiu').AsInteger := idcentre;
      qE1.ParamByName('idPersona').AsInteger := idPersona;
      qE1.ParamByName('actiu').AsInteger := StrToInt(tD.FieldByName('USRACTIU').AsString);
      qE1.ParamByName('Clientdasi').AsInteger := idClientDasi;
      qE1.ParamByName('dasiclinicId').AsInteger := tD.FieldByName('ID').AsInteger;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('USRREGISTRE').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idUsuari := qE1.FieldByName('registre').AsInteger;

      // crear usuarisCentres
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO usuarisCentres (Id, dataInici, idUsuari, idCentre) '+
                  'VALUES(:Id, :dataInici, :idUsuari, :idCentre)');
      qE1.ParamByName('dataInici').AsDateTime := Date-1;
      qE1.ParamByName('idCentre').AsInteger := idcentre;
      qE1.ParamByName('idUsuari').AsInteger := idUsuari;
      qE1.ExecSQL ;

      // crear USER
      // mirem si existeis un usuari a eClinic amb el mateix nom, si existeix afegirem acronim
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select count(*) existeix from user where username=:username');
      qE1.ParamByName('username').AsString := LowerCase(tD.FieldByName('USRNOM').AsString);
      qE1.ExecSQL ;
      ExisteixUSR := qE1.FieldByName('existeix').AsInteger;

      // desencriptem clau
      Clau := Desencriptar(tD.FieldByName('usrclau').AsString,tD.FieldByName('USRNIVELL').AsInteger);

      // agafem Password i Salt
      Password := '';
      Salt := '';
      DonemPass(Clau, Password, Salt);

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO user ( id, usuari, username, password, salt, actiu, email,'+
                  'emailConfirmat, usuariConfirmat) VALUES(:id , :usuari, :username,'+
                  ':password, :salt, 1, :email, 1, 1)');
      qE1.ParamByName('usuari').AsInteger := idUsuari;
      qE1.ParamByName('username').AsString := iif(ExisteixUSR=0,'',Edit6.Text+'-') + tD.FieldByName('USRNOM').AsString;
      qE1.ParamByName('password').AsString := Password;
      qE1.ParamByName('salt').AsString := Salt;
      qE1.ParamByName('email').AsString := tD.FieldByName('usr_mail').AsString;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      User_ID := qE1.FieldByName('registre').AsInteger;

      // insertem ROL usuari de eClinic a USER_ROLE
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO user_role ( user_id, role_id) VALUES(:usuari , 1)');
      qE1.ParamByName('usuari').AsInteger := User_ID;
      qE1.ExecSQL ;

      // assignem ROLS de CLIENT i CENTRE per usuari
      // primer cerquem el id dels rols corresponents
      if tD.FieldByName('PEFREGISTRE').AsInteger <= 0 then Tipus := 'aux'
      else if tD.FieldByName('PEFREGISTRE').AsInteger = 1 then Tipus := 'prof'
      else if tD.FieldByName('PEFREGISTRE').AsInteger > 1 then Tipus := 'admin';

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select id from dasieclinicDB.rols where clientDasi=:ClientDasi '+
                  'and tipus=:tipus and CentreOGeneral=0');
      qE1.ParamByName('ClientDasi').AsInteger := idClientDasi;
      qE1.ParamByName('tipus').AsString := Tipus;
      qE1.ExecSQL ;
      idRolClient := qE1.FieldByName('id').AsInteger;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select id from dasieclinicDB.rols where clientDasi=:ClientDasi '+
                  'and tipus=:tipus and CentreOGeneral=1');
      qE1.ParamByName('ClientDasi').AsInteger := idClientDasi;
      qE1.ParamByName('tipus').AsString := Tipus;
      qE1.ExecSQL ;
      idRolCentre := qE1.FieldByName('id').AsInteger;

      // crea rolsUsuarisCentres
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO rolsUsuarisCentres ( id, usuari, rol, dataInici, idCentre) '+
                  'VALUES(:id , :usuari, :rol, :dataInici, :idCentre)');
      qE1.ParamByName('usuari').AsInteger := idUsuari;
      qE1.ParamByName('rol').AsInteger := idRolClient;
      qE1.ParamByName('dataInici').AsDateTime := Date - 1;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO rolsUsuarisCentres ( id, usuari, rol, dataInici, idCentre) '+
                  'VALUES(:id , :usuari, :rol, :dataInici, :idCentre)');
      qE1.ParamByName('usuari').AsInteger := idUsuari;
      qE1.ParamByName('rol').AsInteger := idRolCentre;
      qE1.ParamByName('dataInici').AsDateTime := Date - 1;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      // insertem relacions USUARIS
      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_USUARIS'',:D_ID,''user'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('USRREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := User_ID;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbPreus.Enabled := False;
    PassadaOK('S_USUARIS');
  except
    raise;
  end;
end;

procedure TForm2.sbPacientsClick(Sender: TObject);
var registre, idAdreca, idPersona, idPacient : integer;
    pais, cognom1, cognom2, cadena, Origen : string;
    idZona, idTarifaMutua, idProcedencia, idTipusDocument, idEmail : Variant;
begin
  // PACIENTS
  try
    // ESBORREM PACIENTS DEMO op petades traspas
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and e_Centre=:idCentre');
      qR2e.ParamByName('idcentre').AsInteger := idcentre;
      qR2e.ExecQuery;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from dasieclinicDB.pacientsTarifaMutua ' +
                  'where idPacient in (select idPacient from dasieclinicDB.pacients where idCentre=:idCentre and dasiclinicreg>0)');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from pacients where idCentre=:idCentre and dasiclinicreg>0');
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;
  except
    raise;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select p.* from s_pacients p where p.pacregistre=>0 and '+
                     '(((p.pacfratutor=0) or (select count(*) from visites where pacregistre=p.pacregistre)>0) or '+
                     ' ((p.pacfratutor=0) or (select count(*) from s_citacions where pacregistre=p.pacregistre)>0)) '+
                     'order by p.pacregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // crear persona + mails + adreces
      // buscar poblacio->zona a taula de relacions
      if tD.FieldByName('POBREGISTRE').AsInteger > 0 then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from relacions where d_taula=''S_POBLACIONS'' and D_ID=:pobregistre');
        qR1.ParamByName('pobregistre').AsInteger := tD.FieldByName('POBREGISTRE').AsInteger;
        qR1.ExecQuery;
        IDZona := qR1.FieldByName('E_ID').AsVariant;
      end else IDZona := 0;

      // cercar pais de la zona a MySql
      if idZona > 0 then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select pais from zones where idzona=:idzona');
        qE1.ParamByName('idzona').AsInteger := idzona;
        qE1.ExecSQL ;
        Pais := qE1.FieldByName('pais').AsString;
      end else Pais := 'ES';

      // cercar TipusDocument a taula de relacions_centres
      if not tD.FieldByName('PDCREGISTRE').IsNull then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TIPUS_DOCUMENT'' and D_ID=:PDCREGISTRE and e_centre=:idcentre');
        qR1.ParamByName('PDCREGISTRE').AsInteger := tD.FieldByName('PDCREGISTRE').AsInteger;
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ExecQuery;
        idTipusDocument := qR1.FieldByName('E_ID').AsVariant;
      end else idTipusDocument := null;

      // cerca TARIFA a taula de relacions_centres
      if not tD.FieldByName('trfregistre').IsNull then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES'' and D_ID=:trfregistre and e_centre=:idcentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('trfregistre').AsInteger := tD.FieldByName('trfregistre').AsInteger;
        qR1.ExecQuery;
        idTarifaMutua := qR1.FieldByName('E_ID').AsVariant;
      end else idTarifaMutua := null;

      // cerca PROCEDENCIA a taula de relacions_centres
      if not tD.FieldByName('procregistre').IsNull then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROCEDENCIES'' and D_ID=:procregistre and e_centre=:idcentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('procregistre').AsInteger := tD.FieldByName('procregistre').AsInteger;
        qR1.ExecQuery;
        idProcedencia := qR1.FieldByName('E_ID').AsVariant;
      end else idProcedencia := null;

      // COMPTE NO HI HA ORIGENS AL eCLinic, es pot passar tamb� com a procedencia
      // de moment es passa a observacions
      Origen := '';
      if tD.FieldByName('orgregistre').AsInteger>0 then
      begin
        qD1.Close;
        qD1.SQL.Clear;
        qD1.SQL.Add('select ORGNOM from s_origenes where orgregistre=:orgregistre');
        qD1.ParamByName('orgregistre').AsInteger := tD.FieldByName('orgregistre').AsInteger;
        qD1.ExecQuery ;
        if qD1.RecordCount > 0 then Origen := qD1.FieldByName('ORGNOM').AsString;
      end;

      // CREAR adre�a
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO adreces (idAdreca, direccio1, cp, pais, idZona) '+
                  'VALUES(:idAdreca, AES_ENCRYPT(CONVERT(:direccio1 USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:cp,"9w7kM5GwbJSsRnvS"), :pais, :idzona)');
      qE1.ParamByName('direccio1').AsString := tD.FieldByName('PACADRECA').AsString;
      qE1.ParamByName('cp').AsString := tD.FieldByName('PACCP').AsString;
      qE1.ParamByName('pais').AsString := pais;
      if idZona >= 0 then qE1.ParamByName('idZona').AsInteger := idZona;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idAdreca := qE1.FieldByName('registre').AsInteger;

      // CREAR email
      if not tD.FieldByName('procregistre').IsNull then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO emails (idEmail, email) VALUES(:idEmail, AES_ENCRYPT(:email,"9w7kM5GwbJSsRnvS"))');
        qE1.ParamByName('email').AsString := tD.FieldByName('PACEMAIL').AsString;
        qE1.ExecSQL ;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        idEmail := qE1.FieldByName('registre').AsVariant;
      end else idEmail := null;

      // crear persona
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO persones (idPersona, nom, nomComplert, numDocument,'+
                  'telefon1, mobil, cognom1, cognom2, idAdreca, idEmail, TipusDocument) '+
                  'VALUES(:idPersona, AES_ENCRYPT(CONVERT(:nom USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:nomComplert USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:numDocument,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:telefon1,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(:mobil,"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:cognom1 USING utf8),"9w7kM5GwbJSsRnvS"),'+
                  'AES_ENCRYPT(CONVERT(:cognom2 USING utf8),"9w7kM5GwbJSsRnvS"), :idAdreca, :idEmail, :TipusDocument)');
      qE1.ParamByName('nom').AsString := tD.FieldByName('PACNOM').AsString;
      qE1.ParamByName('cognom1').AsString := tD.FieldByName('PACCOGNOM1').AsString;
      qE1.ParamByName('cognom2').AsString := tD.FieldByName('PACCOGNOM2').AsString;
      qE1.ParamByName('nomComplert').AsString := Trim(Trim(tD.FieldByName('PACCOGNOM1').AsString) + ' ' +
                                                      Trim(tD.FieldByName('PACCOGNOM2').AsString) + ', ' +
                                                      Trim(tD.FieldByName('PACNOM').AsString));
      qE1.ParamByName('numDocument').AsString := tD.FieldByName('PACNIF').AsString;
      qE1.ParamByName('telefon1').AsString := tD.FieldByName('PACTELEFON1').AsString;
      qE1.ParamByName('mobil').AsString := tD.FieldByName('PACTELEFON2').AsString;
      if idTipusDocument > 0 then
        qE1.ParamByName('TipusDocument').AsInteger := idTipusDocument;
      qE1.ParamByName('idAdreca').AsInteger := idAdreca;
      qE1.ParamByName('idEmail').AsInteger := idEmail;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idPersona := qE1.FieldByName('registre').AsInteger;

      RichEdit1.Clear;
      RichEdit1.Text := tD.FieldByName('PACOBSERVACIONS').AsString;

      memo1.Clear;
      if not tD.FieldByName('PACNSS').IsNull then
        if Trim(tD.FieldByName('PACNSS').AsString)>'' then
          memo1.Lines.Add(Trim(tD.FieldByName('PACNSS').AsString));
      if not tD.FieldByName('PACPOLISSA').IsNull then
        if Trim(tD.FieldByName('PACPOLISSA').AsString)>'' then
          memo1.Lines.Add('NSS/P�liza  '+ Trim(tD.FieldByName('PACPOLISSA').AsString));
      if Origen > '' then
          memo1.Lines.Add('Origen      '+ Origen);
{      if not tD.FieldByName('PACESTCIVIL').IsNull then
        if Trim(tD.FieldByName('PACESTCIVIL').AsString)>'' then
          memo1.Lines.Add('Estado civil  '+ Trim(tD.FieldByName('PACESTCIVIL').AsString));
      if not tD.FieldByName('PACPROFESSIO').IsNull then
        if Trim(tD.FieldByName('PACPROFESSIO').AsString)>'' then
          memo1.Lines.Add('Profesi�n  '+ Trim(tD.FieldByName('PACPROFESSIO').AsString));
//      if trim(memo1.Text)>'' then showmessage(memo1.Text);
      }
      memo1.Lines.Add(tD.FieldByName('PACOBS_SITBUCAL').AsString);

      // crear PACIENT
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO pacients( historia, dataNaixement, sexe,'+
                  'dataAlta, altresProd, dataBaixa, motiuBaixa, actiu, mailing,'+
                  'sms, smsPubli, lopd, observacions, dadesAdicionals,'+
                  'idCentre, idPersona, idProcedencia,'+
                  'tarifaPreferida, fraPacient, dasiclinicId, dasiclinicReg, traspas,'+
                  'altresDades, iban) VALUES( :historia, :dataNaixement,'+
                  ':sexe, :dataAlta, :altresProd, :dataBaixa, :motiuBaixa, :actiu,'+
                  ':mailing, :sms, :smsPubli, :lopd, :observacions, :dadesAdicionals,'+
                  ':idCentre, :idPersona, :idProcedencia, :tarifaPreferida, :fraPacient,'+
                  ':dasiclinicId, :dasiclinicReg, :traspas, :altresDades, :iban)');
      qE1.ParamByName('historia').AsInteger := iif(tD.FieldByName('pachistorian').AsInteger<0,0,tD.FieldByName('pachistorian').AsInteger);
      if not tD.FieldByName('pacnaixement').IsNull then
        qE1.ParamByName('dataNaixement').AsDateTime := tD.FieldByName('pacnaixement').AsDateTime;
      if ((tD.FieldByName('pacsexe').AsString='HOMBRE') or
          (tD.FieldByName('pacsexe').AsString='MUJER')) then
        qE1.ParamByName('sexe').AsString := iif(tD.FieldByName('pacsexe').AsString='HOMBRE','m',iif(tD.FieldByName('pacsexe').AsString='MUJER','f',''))
      else if ((tD.FieldByName('pacsexe').AsString='MASCULINO') or
               (tD.FieldByName('pacsexe').AsString='FEMENINO')) then
             qE1.ParamByName('sexe').AsString := iif(tD.FieldByName('pacsexe').AsString='MASCULINO','m',iif(tD.FieldByName('pacsexe').AsString='FEMENINO','f',''));
      if not tD.FieldByName('pacdatainici').IsNull then
        qE1.ParamByName('dataAlta').AsDateTime := tD.FieldByName('pacdatainici').AsDateTime;
      qE1.ParamByName('altresProd').AsString := tD.FieldByName('procedencia_otros').AsString;
      qE1.ParamByName('actiu').AsInteger := iif(tD.FieldByName('inactiu').AsInteger=1,0,1);
      if tD.FieldByName('inactiu').AsInteger=1 then
      begin
        qE1.ParamByName('motiuBaixa').AsString := tD.FieldByName('inactiu_motiu').AsString;
        qE1.ParamByName('dataBaixa').AsDateTime := tD.FieldByName('inactiu_data').AsDateTime;
      end;
      if not tD.FieldByName('emailing').IsNull then
      begin
        if EsNumero(tD.FieldByName('emailing').AsString)
          then qE1.ParamByName('mailing').AsInteger := StrToInt(tD.FieldByName('emailing').AsString)
          else qE1.ParamByName('mailing').AsInteger := 0;
      end else qE1.ParamByName('mailing').AsInteger := 0;
      if not tD.FieldByName('aviso_sms').IsNull then
      begin
        if EsNumero(tD.FieldByName('aviso_sms').AsString)
          then qE1.ParamByName('sms').AsInteger := StrToInt(tD.FieldByName('aviso_sms').AsString)
          else qE1.ParamByName('sms').AsInteger := 0;
      end else qE1.ParamByName('sms').AsInteger := 0;
{      if not tD.FieldByName('publicitat_sms').IsNull then
      begin
        if EsNumero(tD.FieldByName('publicitat_sms').AsString)
          then qE1.ParamByName('smsPubli').AsInteger := tD.FieldByName('publicitat_sms').AsInteger
          else qE1.ParamByName('smsPubli').AsInteger := 0;
      end else qE1.ParamByName('smsPubli').AsInteger := 0;}
      if not tD.FieldByName('paclopd').IsNull then
      begin
        if EsNumero(tD.FieldByName('paclopd').AsString)
          then qE1.ParamByName('lopd').AsInteger := StrToInt(tD.FieldByName('paclopd').AsString)
          else qE1.ParamByName('lopd').AsInteger := 0;
      end else qE1.ParamByName('lopd').AsInteger := 0;
      qE1.ParamByName('observacions').AsString := RTF2PlainText(RichEdit1.Text);
//      qE1.ParamByName('dadesAdicionals').AsString := tD.FieldByName('PACOBS_SITBUCAL').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('idPersona').AsInteger := idPersona;
      if idProcedencia<>null then
        qE1.ParamByName('idProcedencia').AsInteger := idProcedencia;
      if idTarifaMutua<>null then
        qE1.ParamByName('tarifaPreferida').AsInteger := idTarifaMutua;
      qE1.ParamByName('fraPacient').AsInteger := iif(tD.FieldByName('pacvolfactura').AsString='1',1,0);
      qE1.ParamByName('dasiclinicId').AsInteger := tD.FieldByName('ID').AsInteger;
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ParamByName('altresDades').AsString := Memo1.Text;
      qE1.ParamByName('iban').AsString := tD.FieldByName('dom_iban').AsString;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idPacient := qE1.FieldByName('registre').AsInteger;

      // crear TARIFA-PACIENT
      if idTarifaMutua<>null then
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO pacientsTarifaMutua( idPacient, idTarifaMutua) '+
                    'VALUES( :idPacient, :idTarifaMutua)');
        qE1.ParamByName('idTarifaMutua').AsInteger := idTarifaMutua;
        qE1.ParamByName('idPacient').AsInteger := idPacient;
        qE1.ExecSQL ;
      end;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_PACIENTS'',:D_ID,''pacients'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('PACREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idPacient;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbPacients.Enabled := False;
    PassadaOK('S_PACIENTS');
  except
    SHOWMESSAGE('DONA ERROR');
    raise;
  end;
end;

procedure TForm2.sbCitesClick(Sender: TObject);
var idEspecialitat, idProfessional, idPacient, idCita, idBox, estat, idTipus,
    idTractament, idOrigen, idTarifa : integer;
    AlertaObs, Processada : Boolean;
    Origen : string;
begin
  // CITES
  try
    // esborrem cites creades de centre (demo o de petades)
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''S_CITACIONS'' and e_Centre=:idCentre '+
                   'and D_ID>0');    //////////  HA DE SER ZERO
      qR2e.ParamByName('idcentre').AsInteger := idcentre;
      qR2e.ExecQuery;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from cites where idCentre=:idCentre and DasiClinicReg>0');   ///// HA DE SER ZERO
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;
    end;

    // es crea l'origen 'Traspaso Dasiclinic' o 'Traspaso Externo' per saber de on han vingut les cites
    if ckExtern.Checked then Origen := 'Traspaso Externo' else Origen := 'Traspaso Dasiclinic';
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('select id from origenCita where nom=:Origen');
    qE1.ParamByName('Origen').AsString := Origen;
    qE1.ExecSQL ;
    if qE1.RecordCount = 0 then
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO origenCita (id, nom) VALUES (:id, :nom)');
      qE1.ParamByName('nom').AsString := Origen;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idOrigen := qE1.FieldByName('registre').AsInteger;
    end else idOrigen := qE1.FieldByName('id').AsInteger;

    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select t.ARRIBADA, t.entra_a_box, t.sortida, c.* from s_citacions c '+
                     'left join s_citacions_temps t on c.citregistre=t.citregistre '+
                     'where c.citregistre>0 and c.pacregistre>0 and c.prfregistre>0 '+     /////  CITREGISTRE HA DE SER ZERO
                     'and c.citacumulada=''0'' order by c.citregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca ESPECIALITAT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''MODULS'' and D_ID=:modregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('modregistre').AsInteger := tD.FieldByName('modregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idEspecialitat := -1
        else idEspecialitat := qR1.FieldByName('E_ID').AsVariant;

      // cerca PROFESSIONAL a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idProfessional := -1
        else idProfessional := qR1.FieldByName('E_ID').AsVariant;

      // cerca PACIENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and D_ID=:pacregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idPacient := -1
        else idPacient := qR1.FieldByName('E_ID').AsVariant;

      // cerca BOX a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_BOXES'' and D_ID=:boxregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('boxregistre').AsInteger := tD.FieldByName('boxregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idBox := -1
        else idBox := qR1.FieldByName('E_ID').AsVariant;

      // cerca TARIFA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES'' and D_ID=:trfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('trfregistre').AsInteger := tD.FieldByName('trfregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idTarifa := 0
        else idTarifa := qR1.FieldByName('E_ID').AsVariant;

      // cerca TIPUS CITA a taula de relacions_centres
      if not tD.FieldByName('cittipus').IsNull then
      begin
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TIPUS_CITA'' and D_ID=:registre and e_Centre=:idCentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('registre').AsInteger := tD.FieldByName('cittipus').AsInteger;
        qR1.ExecQuery;
        if qR1.RecordCount = 0 then idTipus := -1
          else idTipus := qR1.FieldByName('E_ID').AsVariant;
      end else idTipus := -1;

      // estats 0 - 0 pendent, 1 - 1 sala espera, 2 - 3 visitat, 3 - 4 no presentat, 4 - 2 en curs
      case tD.FieldByName('CITESTAT').AsInteger of
        0 : estat := 0;
        1 : estat := 1;
        2 : estat := 3;
        3 : estat := 4;
        4 : estat := 2;
      end;

      // acumules les observacions de les cites
      memo1.Clear;
      if tD.FieldByName('citnumacumulat').AsInteger = 0 then
      begin
        if tD.FieldByName('citobservacions').AsString > '' then
          memo1.Lines.Add(tD.FieldByName('citobservacions').AsString);
      end else
      begin
        tD1.Close;
        tD1.SelectSQL.Clear;
        tD1.SelectSQL.Add('select distinct citobservacions from s_citacions where citobservacions>'''' and citnumacumulat=:citnumacumulat');
        tD1.ParamByName('citnumacumulat').AsInteger := tD.FieldByName('citnumacumulat').AsInteger;
        tD1.Open;
        while not tD1.Eof do
        begin
          if tD.FieldByName('citobservacions').AsString > '' then
            memo1.Lines.Add(tD.FieldByName('citobservacions').AsString);
          tD1.Next;
        end;
        tD1.Close;
      end;

      // agafem les observacions de la taula S_CITACIONS_NOTES
      AlertaObs := False;
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select tipus, nota from s_citacions_notes where tipus<20 '+
                        'and citregistre=:citregistre');
      tD1.ParamByName('citregistre').AsInteger := tD.FieldByName('citregistre').AsInteger;
      tD1.Open;
      if tD1.RecordCount > 0 then
      begin
        memo1.Lines.Add(tD1.FieldByName('nota').AsString);
        AlertaObs := iif(tD1.FieldByName('tipus').AsInteger=17,True,False);
      end;

      // miro si la cita ja esta processada
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select count(*) existeix from visites where citregistre=:citregistre '+
                        'and ((carregistre>0) or (carregistre_mutua>0))');
      tD1.ParamByName('citregistre').AsInteger := tD.FieldByName('citregistre').AsInteger;
      tD1.Open;
      Processada := (tD1.FieldByName('existeix').AsInteger > 0);

      // crear cita
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO cites (idCita, data, hora, temps, exercici, mes, '+
                  '  estat, multiple, urgencia, extra, observacions, idProfessional,'+
                  '  idPacient, idEspecialitat, origen, estatadm, dasiclinicID,'+
                  '  DasiclinicReg, obsdestaca, idcentre, datacrea, tarifaCita,'+
                  '  tipusVisita, Box, data_pendent, data_espera, data_curs, data_visitat) '+
                  'values(:idCita, :data, :hora, :temps, :exercici, :mes, '+
                  '  :estat, 0, :urgencia, :extra, :observacions, :idProfessional,'+
                  '  :idPacient, :idEspecialitat, :origen, :estatadm, :dasiclinicID,'+
                  '  :DasiclinicReg, :obsdestaca, :idcentre, :datacrea, :tarifaCita,'+
                  '  :tipusVisita, :box, :data_pendent, :data_espera, :data_curs, :data_visitat)');
      qE1.ParamByName('data').AsDateTime := tD.FieldByName('CITDATA').AsDateTime;
      qE1.ParamByName('hora').AsInteger := HoraMinuts(tD.FieldByName('CITHORA').AsString);
      qE1.ParamByName('temps').AsInteger := tD.FieldByName('CITTEMPS').AsInteger;
      qE1.ParamByName('exercici').AsInteger := tD.FieldByName('EXERCICI').AsInteger;
      qE1.ParamByName('mes').AsInteger := tD.FieldByName('MES').AsInteger;
      qE1.ParamByName('estat').AsInteger := estat;
      if EsNumero(tD.FieldByName('CITEXTRA').AsString) then
      begin
        qE1.ParamByName('urgencia').AsInteger := StrToInt(tD.FieldByName('CITEXTRA').AsString);
        qE1.ParamByName('extra').AsInteger := StrToInt(tD.FieldByName('CITEXTRA').AsString);
      end else
      begin
        qE1.ParamByName('urgencia').AsInteger := 0;
        qE1.ParamByName('extra').AsInteger := 0;
      end;
      qE1.ParamByName('observacions').AsString := memo1.Text;
      qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      qE1.ParamByName('idPacient').AsInteger := idPacient;
      qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
      qE1.ParamByName('origen').AsInteger := idOrigen;
      qE1.ParamByName('estatadm').AsInteger := iif(Processada,1,0);
      qE1.ParamByName('dasiclinicID').AsInteger := tD.FieldByName('ID').AsInteger;
      qE1.ParamByName('DasiclinicReg').AsInteger := tD.FieldByName('citregistre').AsInteger;
      qE1.ParamByName('obsdestaca').AsInteger := iif(AlertaObs,1,0);
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('datacrea').AsDateTime := tD.FieldByName('emision').AsDateTime;
      qE1.ParamByName('tarifaCita').AsInteger := idTarifa;
      if idTipus > 0 then
        qE1.ParamByName('tipusVisita').AsInteger := idTipus;
      if idBox > 0 then
        qE1.ParamByName('box').AsInteger := idBox;
      if not tD.FieldByName('emision').IsNull then
        qE1.ParamByName('data_pendent').AsDateTime := tD.FieldByName('emision').AsDateTime;
      if not tD.FieldByName('arribada').IsNull then
        qE1.ParamByName('data_espera').AsDateTime := tD.FieldByName('arribada').AsDateTime;
      if not tD.FieldByName('entra_a_box').IsNull then
        qE1.ParamByName('data_curs').AsDateTime := tD.FieldByName('entra_a_box').AsDateTime;
      if not tD.FieldByName('sortida').IsNull then
        qE1.ParamByName('data_visitat').AsDateTime := tD.FieldByName('sortida').AsDateTime;
      qE1.ExecSQL ;
{
`data_np` -> seteja data actual si es canvia l'estat a no presentat,
`data_cancelat` -> seteja data actual al cancelar,
`data_procesat` -> seteja data actual al processar,
`observaPac` -> observacions q pot introdu�r el pacient desde el portal del pacient.
`data_confirmat` -> seteja data actual al passar a estat confirmat,
 }
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idCita := qE1.FieldByName('registre').AsInteger;

      // insertem tants tractaments com hi hagi || si citnumacumulat=0 solament hi ha un tractament
      // si tenim citnumacumulat fem un select distinct TRAREGISTRE d'aquest citnumacumulat
      if tD.FieldByName('citnumacumulat').AsInteger = 0 then
      begin
        if tD.FieldByName('traregistre').AsInteger > 0 then
        begin
          // cerca TRACTAMENT a taula de relacions_centres
          qR1.Close;
          qR1.SQL.Clear;
          qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TRACTAMENTS'' and D_ID=:traregistre and e_Centre=:idCentre');
          qR1.ParamByName('idcentre').AsInteger := idcentre;
          qR1.ParamByName('traregistre').AsInteger := tD.FieldByName('traregistre').AsInteger;
          qR1.ExecQuery;
          if qR1.RecordCount > 0 then
          begin
            idTractament := qR1.FieldByName('E_ID').AsVariant;

            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO citesTractaments (idCita, idTractament) '+
                        'values(:idCita, :idTractament)');
            qE1.ParamByName('idCita').AsInteger := idCita;
            qE1.ParamByName('idTractament').AsInteger := idTractament;
            qE1.ExecSQL ;
          end;
        end;
      end else
      begin
        tD1.Close;
        tD1.SelectSQL.Clear;
        tD1.SelectSQL.Add('select distinct traregistre from s_citacions where citnumacumulat=:citnumacumulat');
        tD1.ParamByName('citnumacumulat').AsInteger := tD.FieldByName('citnumacumulat').AsInteger;
        tD1.Open;
        while not tD1.Eof do
        begin
          // cerca TRACTAMENT a taula de relacions_centres
          qR1.Close;
          qR1.SQL.Clear;
          qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TRACTAMENTS'' and D_ID=:traregistre and e_Centre=:idCentre');
          qR1.ParamByName('idcentre').AsInteger := idcentre;
          qR1.ParamByName('traregistre').AsInteger := tD1.FieldByName('traregistre').AsInteger;
          qR1.ExecQuery;

          if qR1.RecordCount > 0 then
          begin
            idTractament := qR1.FieldByName('E_ID').AsVariant;

            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO citesTractaments (idCita, idTractament) '+
                        'values(:idCita, :idTractament)');
            qE1.ParamByName('idCita').AsInteger := idCita;
            qE1.ParamByName('idTractament').AsInteger := idTractament;
            qE1.ExecSQL ;
          end;

          tD1.Next;
        end;
        tD1.Close;
      end;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''S_CITACIONS'',:D_ID,''cites'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('CITREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idCita;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbCites.Enabled := False;
    PassadaOK('S_CITACIONS');
  except
    raise;
  end;
end;

procedure TForm2.sbClasImgClick(Sender: TObject);
var url, JSon, Params, cadena : string;
    response : string;
    JSONObject : TJSONObject;
    JSonToSend : TStringStream;
begin
  // PRIMER PASSEM LES CLASSIFICACIONS (TAGS) (les del usuari, les de sistema no cal)
  if EsProd then URL := 'https://www.dasieclinic.com/upload-tags-dc'
    else URL := 'http://test.dasieclinic.com/upload-tags-dc';

  // esborrem tags creats del centre
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''CLASSIFICACIONS_IMG'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // esborrem tags del centre i amb registres de traspas
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from tags where idCentre=:idCentre and dasiclinicReg>=0');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from CLASSIFICACIONS_IMG where claregistre>=0 order by claregistre');
    tD.Open;
    while not tD.Eof do
    begin
      JSon := '{"user":"dasi","password":"avmISAD34","center":"'+IntToStr(idCentre)+'",' +
              '"tags":{"0":{"CLAREGISTRE":"' + IntToStr(tD.FieldByName('CLAREGISTRE').AsInteger) +'",'+
              '"CLANOM":"' + tD.FieldByName('CLANOM').AsString + '"}}}';

      JsonToSend := TStringStream.Create(Json, TEncoding.UTF8);
      try
        NetHTTPClient1.ContentType := 'application/json';
        NetHTTPClient1.AcceptCharset := 'utf-8';

        Response := NetHTTPClient1.post(url,JSonToSend).ContentAsString;

        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''CLASSIFICACIONS_IMG'',:D_ID,''tags'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('CLAREGISTRE').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := 0;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;
      except
        raise;
      end;
      tD.Next;
    end;

    sbClasImg.Enabled := False;
    PassadaOK('CLASSIFICACIONS_IMG');
  except
    raise;
  end;
end;

procedure TForm2.sbCobMutuaClick(Sender: TObject);
var idTarifaMutua, idPacient, idFra, idFormaPagamentClient, idCarrec, idUser,
    idUsuari, idCobrament : integer;
    tipusCobrament : string;
begin
  // COBRAMENTS MUTUA
  // esborrem cobraments creats de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''TARIFES_COBRAMENTS'' '+
                 'and e_Centre=:idCentre and D_ID>143');   ////////  HA DE SER ZERO
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // esborrem COBRAMENTS del centre i amb registres de traspas
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete c.* FROM dasieclinicDB.cobrament c '+
                'left join dasieclinicDB.cobramentCarrec cc on c.idCobrament=cc.idCobrament '+
                'left join dasieclinicDB.carrec ca on cc.idCarrec=ca.idCarrec '+
                'where c.idCentre=:idCentre and ca.tipusCarrec=:tipusCarrec '+
                'and c.dasiclinicreg>0');    /////   HA DE SER ZERO
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ParamByName('tipusCarrec').AsString := 'MUT';
    qE1.ExecSQL ;

    // posem a zero imports cobrats de carrec
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('update dasieclinicDB.carrec set totalCobrat=0, '+
                'pendentCobrar=import where dasiclinicReg>0 and idCentre=:idCentre and tipusCarrec=:tipusCarrec');  /////// HA DE SER ZERO
    qE1.ParamByName('idCentre').AsInteger := idCentre;
    qE1.ParamByName('tipusCarrec').AsString := 'MUT';
    qE1.ExecSQL ;

    // posem a zero imports cobrats de fra
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('update dasieclinicDB.fra set importCobrat=0 '+
                'where idFra in (select idFra from dasieclinicDB.carrec where tipusCarrec=:tipusCarrec and idCentre=:idCentre)');
    qE1.ParamByName('idCentre').AsInteger := idCentre;
    qE1.ParamByName('tipusCarrec').AsString := 'MUT';
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from TARIFES_COBRAMENTS where cbmregistre>0 order by cbmregistre');     ////////////  HA DE SER ZERO
    tD.Open;
    while not tD.Eof do
    begin
      // cerca FORMA DE PAGAMENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''FORMAS_PAGO'' and D_ID=:FORMA_PAGO and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('FORMA_PAGO').AsInteger := tD.FieldByName('FORMA_PAGO').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idFormaPagamentClient := -1
        else idFormaPagamentClient := qR1.FieldByName('E_ID').AsVariant;

      // cerca TARIFA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES'' and D_ID=:trfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('trfregistre').AsInteger := tD.FieldByName('trfregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idTarifaMutua := -1
        else idTarifaMutua := qR1.FieldByName('E_ID').AsVariant;

      // cerca factura mutua a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES_FACTURES'' and D_ID=:frmregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('frmregistre').AsInteger := tD.FieldByName('frmregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idFra := -1
        else idFra := qR1.FieldByName('E_ID').AsVariant;

      // tipus de COBRAMENT CDP cargo al deposito, COB cobro, ANT anticipo, IDP ingreso al deposito
      //                    RET reintegrament, REG
      tipusCobrament := 'COB';

      // insertem COBRAMENTS
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO dasieclinicDB.cobrament( idCobrament, tipusCobrament,'+
                  'codi, importCobrat, esDiposit, comisioBanc, dataCobrament, exercici,'+
                  'mes, multiple, idPacient, idFormaPagamentClient, idCentre, usuari,'+
                  'moviment, observacions, export, dasiclinicReg) VALUES (:idCobrament,'+
                  ':tipusCobrament, :codi, :importCobrat, :esDiposit, :comisioBanc,'+
                  ':dataCobrament, :exercici, :mes, :multiple, :idPacient,'+
                  ':idFormaPagamentClient, :idCentre, :usuari, :moviment, :observacions,'+
                  ':export, :dasiclinicReg)');
      qE1.ParamByName('tipusCobrament').AsString := tipusCobrament;
      qE1.ParamByName('codi').AsString := tD.FieldByName('COBNUMERO').AsString;
      qE1.ParamByName('importCobrat').AsFloat := tD.FieldByName('TOTAL').AsFloat;
//      qE1.ParamByName('esDiposit').AsInteger := ;
      qE1.ParamByName('comisioBanc').AsFloat := 0;        // aqui no en tenim
      qE1.ParamByName('dataCobrament').AsDateTime := tD.FieldByName('CBMDATA').AsDateTime;
      qE1.ParamByName('exercici').AsInteger := Exercici(tD.FieldByName('CBMDATA').AsDateTime);
      qE1.ParamByName('mes').AsInteger := DameMes(tD.FieldByName('CBMDATA').AsDateTime);
//      qE1.ParamByName('multiple').AsInteger := ;
//      qE1.ParamByName('idPacient').AsInteger := idPacient;  // no correspon
      qE1.ParamByName('idFormaPagamentClient').AsInteger := idFormaPagamentClient;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
//      qE1.ParamByName('usuari').AsInteger := idUsuari;  //  aqui no en tenim
//      qE1.ParamByName('moviment').AsInteger := ;  per tema tancaments de caixa
//      qE1.ParamByName('observacions').AsString := ;
//      qE1.ParamByName('export').AsInteger := ;  per control d'exportacions a programes externs
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('CBMREGISTRE').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idCobrament := qE1.FieldByName('registre').AsInteger;

      // insertem les dades de la taula cobraments_carregs  PERO HO FEM AMB TOTS ELS CARRECS COBRATS
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select distinct(carregistre_mutua) CARREGISTRE '+
                        'from visites_tractaments where factura_mutua=:factura_mutua');
      tD1.ParamByName('factura_mutua').AsInteger := tD.FieldByName('frmregistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // cerca CARREC a taula de relacions_centres
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select idCarrec from dasieclinicDB.carrec where dasiclinicReg=:carregistre and idCentre=:idCentre');
        qE1.ParamByName('idcentre').AsInteger := idcentre;
        qE1.ParamByName('CARREGISTRE').AsInteger := tD1.FieldByName('CARREGISTRE').AsInteger;
        qE1.ExecSQL;
        if qE1.RecordCount> 0 then
        begin
          idCarrec := qE1.FieldByName('idCarrec').AsVariant;

          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('INSERT INTO dasieclinicDB.cobramentCarrec(importParcial, '+
                      'idCobrament, idCarrec, idCarrecDiposit, dipositParcial) '+
                      'VALUES(:importParcial, :idCobrament, :idCarrec, :idCarrecDiposit,'+
                      ':dipositParcial)');
          qE1.ParamByName('importParcial').AsFloat := 0;// no passem import fins que no hi hagi liquidacions
          qE1.ParamByName('idCobrament').AsInteger := idCobrament;
          qE1.ParamByName('idCarrec').AsInteger := idCarrec;
          qE1.ParamByName('idCarrecDiposit').AsInteger := idCarrec;
  //        qE1.ParamByName('dipositParcial').AsFloat := ;  es per les bestretes  s'ha de ervisar
          qE1.ExecSQL ;

          // s'ha de marcar l'import cobrat i el pendent de la taula carrec
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('update dasieclinicDB.carrec set totalCobrat=import, '+
                      'pendentCobrar=0 where idCarrec=:idCarrec '+
                      'and idCentre=:idCentre');
          qE1.ParamByName('idCarrec').AsInteger := idCarrec;
          qE1.ParamByName('idCentre').AsInteger := idCentre;
          qE1.ExecSQL ;
        end;

        tD1.next;
      end;

      // s'ha de marcar l'import cobrat de la taula fra
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('update dasieclinicDB.fra set importCobrat=importCobrat+:import '+
                  'where idFra=:idFra and traspas=:traspas');
      qE1.ParamByName('import').AsFloat := tD.FieldByName('TOTAL').AsFloat;
      qE1.ParamByName('idFra').AsInteger := idFra;
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ExecSQL ;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''TARIFES_COBRAMENTS'',:D_ID,''cobrament'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('CBMREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idCobrament;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;

    sbCobMutua.Enabled := False;
    PassadaOK('TARIFES_COBRAMENTS');
  except
    raise;
  end;
end;

procedure TForm2.sbCobrosClick(Sender: TObject);
var idTarifaMutua, idPacient, idFra, idFormaPagamentClient, idCarrec, idUser,
    idUsuari, idCobrament : integer;
    tipusCobrament : string;
begin
  // COBRAMENTS
  // esborrem cobraments creats de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''COBRAMENTS'' and e_Centre=:idCentre');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    // esborrem COBRAMENTS del centre i amb registres de traspas
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete FROM dasieclinicDB.cobrament where idCentre=:idCentre and dasiclinicReg>0');
    qE1.ParamByName('idcentre').AsInteger := idcentre;
    qE1.ExecSQL ;

    // posem a zero imports cobrats de carrec
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('update dasieclinicDB.carrec set totalCobrat=0, '+
                'pendentCobrar=import where dasiclinicReg>0 and idCentre=:idCentre');
    qE1.ParamByName('idCentre').AsInteger := idCentre;
    qE1.ExecSQL ;

    // posem a zero imports cobrats de fra
    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('update dasieclinicDB.fra set importCobrat=0 where dasiclinicReg>0 and traspas=:traspas');
    qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
    qE1.ExecSQL ;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from COBRAMENTS where cobregistre>0 order by cobregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca USUARI a taula de relacions_centres es una cerca doble perque esta a dos taules
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_USUARIS'' and D_ID=:usrregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('usrregistre').AsInteger := tD.FieldByName('usrregistre').AsInteger;
      qR1.ExecQuery;
      idUser := qR1.FieldByName('E_ID').AsVariant;
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('select usuari FROM dasieclinicDB.user where id=:idUser');
      qE1.ParamByName('idUser').AsInteger := idUser;
      qE1.ExecSQL ;
      idUsuari := qE1.FieldByName('usuari').AsInteger;

      // cerca PACIENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and D_ID=:pacregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      qR1.ExecQuery;
      idPacient := qR1.FieldByName('E_ID').AsVariant;

      // cerca FORMA DE PAGAMENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''FORMAS_PAGO'' and D_ID=:FORMA_PAGO and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('FORMA_PAGO').AsInteger := tD.FieldByName('FORMA_PAGO').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idFormaPagamentClient := -1
        else idFormaPagamentClient := qR1.FieldByName('E_ID').AsVariant;

      // tipus de COBRAMENT CDP cargo al deposito, COB cobro, ANT anticipo, IDP ingreso al deposito
      //                    RET reintegrament, REG
      if tD.FieldByName('ES_REINTEGRO').AsInteger > 0 then tipusCobrament := 'RET'
      else if tD.FieldByName('ES_ANTICIPO').AsInteger > 0 then tipusCobrament := 'ANT'
      else if tD.FieldByName('DEVOLUCION').AsInteger > 0 then tipusCobrament := 'IDP'
      else if tD.FieldByName('ES_DEPOSITO').AsInteger > 0 then tipusCobrament := 'ANT'
      else tipusCobrament := 'COB';

      // insertem COBRAMENTS
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO dasieclinicDB.cobrament( idCobrament, tipusCobrament,'+
                  'codi, importCobrat, esDiposit, comisioBanc, dataCobrament, exercici,'+
                  'mes, multiple, idPacient, idFormaPagamentClient, idCentre, usuari,'+
                  'moviment, observacions, export, dasiclinicReg) VALUES (:idCobrament,'+
                  ':tipusCobrament, :codi, :importCobrat, :esDiposit, :comisioBanc,'+
                  ':dataCobrament, :exercici, :mes, :multiple, :idPacient,'+
                  ':idFormaPagamentClient, :idCentre, :usuari, :moviment, :observacions,'+
                  ':export, :dasiclinicReg)');
      qE1.ParamByName('tipusCobrament').AsString := tipusCobrament;
      qE1.ParamByName('codi').AsString := tD.FieldByName('COBRO_TEXTO').AsString;
      qE1.ParamByName('importCobrat').AsFloat := tD.FieldByName('TOTAL').AsFloat;
//      qE1.ParamByName('esDiposit').AsInteger := ;
      qE1.ParamByName('comisioBanc').AsFloat := tD.FieldByName('COMISION_BANCO').AsFloat;
      qE1.ParamByName('dataCobrament').AsDateTime := tD.FieldByName('COBDATA').AsDateTime;
      qE1.ParamByName('exercici').AsInteger := Exercici(tD.FieldByName('COBDATA').AsDateTime);
      qE1.ParamByName('mes').AsInteger := DameMes(tD.FieldByName('COBDATA').AsDateTime);
//      qE1.ParamByName('multiple').AsInteger := ;
      qE1.ParamByName('idPacient').AsInteger := idPacient;
      qE1.ParamByName('idFormaPagamentClient').AsInteger := idFormaPagamentClient;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ParamByName('usuari').AsInteger := idUsuari;
//      qE1.ParamByName('moviment').AsInteger := ;  per tema tancaments de caixa
//      qE1.ParamByName('observacions').AsString := ;
//      qE1.ParamByName('export').AsInteger := ;  per control d'exportacions a programes externs
      qE1.ParamByName('dasiclinicReg').AsInteger := tD.FieldByName('COBREGISTRE').AsInteger;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idCobrament := qE1.FieldByName('registre').AsInteger;

      // insertem les dades de la taula cobraments_carregs
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from COBRAMENTS_CARREGS where cobregistre=:cobregistre');
      tD1.ParamByName('cobregistre').AsInteger := tD.FieldByName('COBREGISTRE').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('select idCarrec, idFra from dasieclinicDB.carrec where dasiclinicReg=:registre '+
                    'and idCentre=:idCentre');
        qE1.ParamByName('idCentre').AsInteger := idCentre;
        qE1.ParamByName('registre').AsInteger := tD1.FieldByName('CARREGISTRE').AsInteger;
        qE1.ExecSQL ;
        idCarrec :=  qE1.FieldByName('idCarrec').AsInteger;
        idFra :=  qE1.FieldByName('idFra').AsInteger;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO dasieclinicDB.cobramentCarrec(importParcial, '+
                    'idCobrament, idCarrec, idCarrecDiposit, dipositParcial) '+
                    'VALUES(:importParcial, :idCobrament, :idCarrec, :idCarrecDiposit,'+
                    ':dipositParcial)');
        qE1.ParamByName('importParcial').AsFloat := tD1.FieldByName('IMPORT_PARCIAL').AsFloat;
        qE1.ParamByName('idCobrament').AsInteger := idCobrament;
        qE1.ParamByName('idCarrec').AsInteger := idCarrec;
        qE1.ParamByName('idCarrecDiposit').AsInteger := idCarrec;
//        qE1.ParamByName('dipositParcial').AsFloat := ;  es per les bestretes  s'ha de revisar
        qE1.ExecSQL ;

        // s'ha de marcar l'import cobrat i el pendent de la taula carrec
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('update dasieclinicDB.carrec set totalCobrat=totalCobrat+:import, '+
                    'pendentCobrar=pendentCobrar-:import where dasiclinicReg=:idCarrec '+
                    'and idCentre=:idCentre');
        qE1.ParamByName('import').AsFloat := tD1.FieldByName('IMPORT_PARCIAL').AsFloat;
        qE1.ParamByName('idCarrec').AsInteger := tD1.FieldByName('CARREGISTRE').AsInteger;
        qE1.ParamByName('idCentre').AsInteger := idCentre;
        qE1.ExecSQL ;

        // s'ha de marcar l'import cobrat de la taula fra
        if idFra > 0 then
        begin
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('update dasieclinicDB.fra set importCobrat=importCobrat+:import '+
                      'where idFra=:idFra and traspas=:traspas');
          qE1.ParamByName('import').AsFloat := tD1.FieldByName('IMPORT_PARCIAL').AsFloat;
          qE1.ParamByName('idFra').AsInteger := idFra;
          qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
          qE1.ExecSQL ;
        end;

        tD1.Next;
      end;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''COBRAMENTS'',:D_ID,''cobrament'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('COBREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idCobrament;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;

    sbCarrecs.Enabled := False;
    PassadaOK('COBRAMENTS');
  except
    raise;
  end;
end;

procedure TForm2.sbConContaClick(Sender: TObject);
var registre : integer;
begin
  // CONCEPTES
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from conceptes order by conregistre');
    tD.Open;
    while not tD.Eof do
    begin
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO concepteCont (idConcepteCont, concepte, cuenta,'+
                  'idCentre) VALUES(:idConcepteCont, :concepte, :cuenta, :idCentre)');
      qE1.ParamByName('concepte').AsString := tD.FieldByName('CONDESCRIPCIO').AsString;
      qE1.ParamByName('cuenta').AsString :=  tD.FieldByName('cuenta').AsString;
      qE1.ParamByName('idCentre').AsInteger := idCentre;
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      Registre := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''CONCEPTES'',:D_ID,''concepteCont'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('conregistre').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := registre;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      tD.Next;
    end;
    sbProcedencies.Enabled := False;
    PassadaOK('CONCEPTES');
  except
    raise;
  end;
end;

procedure TForm2.sbEpisodisClick(Sender: TObject);
var idProfessional, idPacient, idEspecialitat, idEpisodi : integer;
begin
  // EPISODIS
  // esborrem episodis creats de traspas (demo o de petades)
  try
    if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
    begin
      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''EPISODIS'' and '+
                   'e_Centre=:idCentre and D_ID>=:registre');
      qR2e.ParamByName('idcentre').AsInteger := idcentre;
      qR2e.ParamByName('registre').AsInteger := 0;   //////////  HA DE SER ZERO
      qR2e.ExecQuery;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('delete from episodis where traspas=:traspas and dasiclinicReg>:registre');
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ParamByName('registre').AsInteger := 0;   //////////  HA DE SER ZERO
      qE1.ExecSQL ;
    end;
  except
    raise;
  end;

  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from episodis where epiregistre>0 order by epiregistre');  /////// HA DE SER ZERO
    tD.Open;
    while not tD.Eof do
    begin
      // cerca ESPECIALITAT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''MODULS'' and D_ID=:modregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('modregistre').AsInteger := tD.FieldByName('modregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idEspecialitat := -1
        else idEspecialitat := qR1.FieldByName('E_ID').AsInteger;

      // cerca PROFESSIONAL a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idProfessional := -1
        else idProfessional := qR1.FieldByName('E_ID').AsInteger;

      // cerca PACIENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and D_ID=:pacregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idPacient := -1
        else idPacient := qR1.FieldByName('E_ID').AsInteger;

      // crear EPISODI
      if idPacient > 0 then
      begin
        try
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('INSERT INTO episodis (idEpisodi, numEpisodi, dataInici, dataAlta,'+
                      '  exercici, mes, actiu, anamnesis, exploracio, judici, evolucio,'+
                      '  comentaris, idProfessional, idPacient, idEspecialitat, DasiclinicReg,'+
                      '  traspas) values(:idEpisodi, :numEpisodi, :dataInici, :dataAlta,'+
                      '  :exercici, :mes, :actiu, :anamnesis, :exploracio, :judici, :evolucio,'+
                      '  :comentaris, :idProfessional, :idPacient, :idEspecialitat,'+
                      '  :DasiclinicReg, :traspas)');
          qE1.ParamByName('numEpisodi').AsInteger := tD.FieldByName('EPINUMERO_PAC').AsInteger;
          qE1.ParamByName('dataInici').AsDateTime := tD.FieldByName('EPIDATA').AsDateTime;
          if not tD.FieldByName('EPIDATA_FI').IsNull then
            qE1.ParamByName('dataAlta').AsDateTime := tD.FieldByName('EPIDATA_FI').AsDateTime;
          qE1.ParamByName('exercici').AsInteger := Exercici(tD.FieldByName('EPIDATA').AsDateTime);
          qE1.ParamByName('mes').AsInteger := DameMes(tD.FieldByName('EPIDATA').AsDateTime);
          qE1.ParamByName('actiu').AsInteger := 1;
          qE1.ParamByName('idProfessional').AsInteger := idProfessional;
          qE1.ParamByName('idPacient').AsInteger := idPacient;
          qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
          qE1.ParamByName('DasiclinicReg').AsInteger := tD.FieldByName('epiregistre').AsInteger;
          qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);

          tD1.Close;
          tD1.SelectSQL.Clear;
          tD1.SelectSQL.Add('execute block (EPIREGISTRE integer=?epiregistre) '+
                            '   returns(ANNAMNESIS MEMOTXT, EXPLORACIO MEMOTXT, '+
                            '   PROVES MEMOTXT, DIAGNOSTIC MEMOTXT, JUDICI MEMOTXT, '+
                            '   TRACTAMENT MEMOTXT, EVOLUCIO MEMOTXT, REVISIO MEMOTXT) '+
                            'as '+
                            'begin '+
                            '  select text from episodis_text where tipus=1 and epiregistre=:epiregistre into :ANNAMNESIS; '+
                            '  select text from episodis_text where tipus=2 and epiregistre=:epiregistre into :EXPLORACIO; '+
                            '  select text from episodis_text where tipus=3 and epiregistre=:epiregistre into :PROVES; '+
                            '  select text from episodis_text where tipus=4 and epiregistre=:epiregistre into :DIAGNOSTIC; '+
                            '  select text from episodis_text where tipus=5 and epiregistre=:epiregistre into :JUDICI; '+
                            '  select text from episodis_text where tipus=6 and epiregistre=:epiregistre into :TRACTAMENT; '+
                            '  select text from episodis_text where tipus=7 and epiregistre=:epiregistre into :EVOLUCIO; '+
                            '  select text from episodis_text where tipus=8 and epiregistre=:epiregistre into :REVISIO; '+
                            '  suspend; '+
                            'end');
          tD1.ParamByName('epiregistre').AsInteger := tD.FieldByName('epiregistre').AsInteger;
          tD1.Open;
          while not tD1.Eof do
          begin
            RichEdit1.Clear;
            RichEdit1.Lines.LoadFromStream(TD1.CreateBlobStream(TD1.FieldByName('ANNAMNESIS'), bmRead));
            qE1.ParamByName('anamnesis').AsString := RichEdit1.Text;
            RichEdit1.Clear;
            RichEdit1.Lines.LoadFromStream(TD1.CreateBlobStream(TD1.FieldByName('EXPLORACIO'), bmRead));
            qE1.ParamByName('exploracio').AsString := RichEdit1.Text;
            RichEdit1.Clear;
            RichEdit1.Lines.LoadFromStream(TD1.CreateBlobStream(TD1.FieldByName('JUDICI'), bmRead));
            qE1.ParamByName('judici').AsString := RichEdit1.Text;
            RichEdit1.Clear;
            RichEdit1.Lines.LoadFromStream(TD1.CreateBlobStream(TD1.FieldByName('EVOLUCIO'), bmRead));
            qE1.ParamByName('evolucio').AsString := RichEdit1.Text;

            memo1.Clear;
            if trim(RTF2PlainText(tD1.FieldByName('PROVES').AsString))>'' then
            begin
              RichEdit1.Clear;
              RichEdit1.Lines.LoadFromStream(TD1.CreateBlobStream(TD1.FieldByName('PROVES'), bmRead));
              memo1.Lines.add('Pruebas complementarias');
              memo1.Lines.add(RichEdit1.Text);
            end;

            if trim(RTF2PlainText(tD1.FieldByName('DIAGNOSTIC').AsString))>'' then
            begin
              RichEdit1.Clear;
              RichEdit1.Lines.LoadFromStream(TD1.CreateBlobStream(TD1.FieldByName('DIAGNOSTIC'), bmRead));
              memo1.Lines.add('Diagn�stico');
              memo1.Lines.add(RichEdit1.Text);
            end;

            if trim(RTF2PlainText(tD1.FieldByName('TRACTAMENT').AsString))>'' then
            begin
              RichEdit1.Clear;
              RichEdit1.Lines.LoadFromStream(TD1.CreateBlobStream(TD1.FieldByName('TRACTAMENT'), bmRead));
              memo1.Lines.add('Tratamiento');
              memo1.Lines.add(RichEdit1.Text);
            end;

            if trim(RTF2PlainText(tD1.FieldByName('REVISIO').AsString))>'' then
            begin
              RichEdit1.Clear;
              RichEdit1.Lines.LoadFromStream(TD1.CreateBlobStream(TD1.FieldByName('REVISIO'), bmRead));
              memo1.Lines.add('Revisi�n');
              memo1.Lines.add(RichEdit1.Text);
            end;
            qE1.ParamByName('comentaris').AsString := memo1.text;
            RichEdit1.Clear;
            memo1.Clear;

            tD1.Next;
          end;
          tD1.Close;
        except
          raise;
        end;

        qE1.ExecSQL ;  // registrem tots els camps de la taula episodis_texts

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        idEpisodi := qE1.FieldByName('registre').AsInteger;

        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''EPISODIS'',:D_ID,''cites'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('EPIREGISTRE').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := idEpisodi;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;
      end;

      tD.Next;
    end;
    sbEpisodis.Enabled := False;
    PassadaOK('EPISODIS');
  except
    raise;
  end;
  tD.Close;
end;

procedure TForm2.sbVisitesClick(Sender: TObject);
var idEspecialitat, idProfessional, idPacient, idCita, idEpisodi, modregistre,
    idVisita, hora, temps, idVisitaTractament, idTractament, idTarifa, idBonus : integer;
begin
  // VISITES
  // esborrem visites creades de centre (demo o de petades)
  if ((idCentre>0) and (StrToInt(Edit1.Text)=idCentre)) then
  begin
    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''VISITES'' and e_Centre=:idCentre and d_id>0');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    qR2e.Close;
    qR2e.SQL.Clear;
    qR2e.SQL.Add('delete from RELACIONS_CENTRE where d_taula=''VISITES_TRACTAMENTS'' and e_Centre=:idCentre and d_id>0');
    qR2e.ParamByName('idcentre').AsInteger := idcentre;
    qR2e.ExecQuery;

    qE1.Close;
    qE1.SQL.Clear;
    qE1.SQL.Add('delete from visites where traspas=:traspas and dasiclinicreg>:dasiclinicreg');
    qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
    qE1.ParamByName('dasiclinicreg').AsInteger := 0;
    qE1.ExecSQL ;
  end;

  // SI NO FAN SERVIR EPISODIS, ES CREA UN EPISODI PER PACIENT
  try
    tD.Close;
    tD.SelectSQL.Clear;
    tD.SelectSQL.Add('select * from visites where visregistre>0  and pacregistre >0 and prfregistre>0 order by visregistre');
    tD.Open;
    while not tD.Eof do
    begin
      // cerca PROFESSIONAL a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PROFESSIONALS'' and D_ID=:prfregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
      qR1.ExecQuery;
      idProfessional := qR1.FieldByName('E_ID').AsVariant;

      // cerca PACIENT a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_PACIENTS'' and D_ID=:pacregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then
      begin
        // busquem a TUTORS
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_TUTORS'' and D_ID=:pacregistre and e_Centre=:idCentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
        qR1.ExecQuery;
        idPacient := qR1.FieldByName('E_ID').AsVariant;
      end else idPacient := qR1.FieldByName('E_ID').AsVariant;

      // cerca CITA a taula de relacions_centres
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_CITACIONS'' and D_ID=:citregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('citregistre').AsInteger := tD.FieldByName('citregistre').AsInteger;
      qR1.ExecQuery;
      if qR1.RecordCount = 0 then idCita := 0
        else idCita := qR1.FieldByName('E_ID').AsVariant;

      // cerca ESPECIALITAT  a taula de relacions_centres
      if tD.FieldByName('citregistre').AsInteger <> 0 then
      begin
        // si te cita agafem la especiliatat de la cita
        qD1.Close;
        qD1.SQL.Clear;
        qD1.SQL.Add('select modregistre from s_citacions where citregistre=:citregistre');
        qD1.ParamByName('citregistre').AsInteger := tD.FieldByName('citregistre').AsInteger;
        qD1.ExecQuery;
        if qD1.RecordCount = 0 then modregistre := 0
          else modregistre := qD1.FieldByName('modregistre').AsInteger;
      end else
      begin
        // si NO te cita agafem la especialitat del professional
        qD1.Close;
        qD1.SQL.Clear;
        qD1.SQL.Add('select modregistre from s_professionals where prfregistre=:prfregistre');
        qD1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
        qD1.ExecQuery;
        if qD1.RecordCount = 0 then modregistre := 0
          else modregistre := qD1.FieldByName('modregistre').AsInteger;
      end;
      qR1.Close;
      qR1.SQL.Clear;
      qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''MODULS'' and D_ID=:modregistre and e_Centre=:idCentre');
      qR1.ParamByName('idcentre').AsInteger := idcentre;
      qR1.ParamByName('modregistre').AsInteger := modregistre;
      qR1.ExecQuery;
      idEspecialitat := qR1.FieldByName('E_ID').AsVariant;

      // primer mirem si la visita te EPIREGISTRE, si en te agafem el ID de episodis
      idEpisodi := 0;
      if tD.FieldByName('epiregistre').AsInteger = 0 then
      begin
        // si NO en te mirem si el pacient te creats episodis de la ESPECIALITAT/PROFESSIONAL i agafem el mes nou
        qD1.Close;
        qD1.SQL.Clear;
        qD1.SQL.Add('select first 1 epiregistre from episodis where pacregistre=:pacregistre '+
                    'and modregistre=:modregistre and prfregistre=:prfregistre order by epiregistre desc');
        qD1.ParamByName('pacregistre').AsInteger := tD.FieldByName('pacregistre').AsInteger;
        qD1.ParamByName('prfregistre').AsInteger := tD.FieldByName('prfregistre').AsInteger;
        qD1.ParamByName('modregistre').AsInteger := modregistre;
        qD1.ExecQuery;

        // si el pacient no en te cap es genera un episodi nou i es guarda a la taula de relacions
        if qD1.RecordCount = 0 then
        begin
          // MIRO SI JA HE CREAT UN EPISODI NOU AL eCLINIC
          qE1.Close;
          qE1.SQL.Clear;
          qE1.SQL.Add('SELECT idEpisodi from episodis where idEspecialitat=:idEspecialitat '+
                      'and idProfessional=:idProfessional and idPacient=:idPacient order by dataInici limit 1');
          qE1.ParamByName('idProfessional').AsInteger := idProfessional;
          qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
          qE1.ParamByName('idPacient').AsInteger := idPacient;
          qE1.ExecSQL ;
          // Si NO en te cap es fa un de nou
          if qE1.RecordCount = 0 then
          begin
            // GENEREM UN EPISODI NOU per aquest pacient/especialitat (la del professional)
            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('INSERT INTO episodis (idEpisodi, numepisodi, datainici, exercici, mes, '+
                        '  actiu, idProfessional, idPacient, idEspecialitat, dasiclinicReg, traspas) '+
                        'values(:idEpisodi, :numepisodi, :datainici, :exercici, :mes, '+
                        '  :actiu, :idProfessional, :idPacient, :idEspecialitat, :DasiclinicReg, :traspas)');
            qE1.ParamByName('numepisodi').AsInteger := 1;   ////  COMPTE POTSER S'HAURAN DE CONTAR SI NI HA MES D'UN
            qE1.ParamByName('datainici').AsDateTime := tD.FieldByName('VISDATA').AsDateTime;
            qE1.ParamByName('exercici').AsInteger := Exercici(tD.FieldByName('VISDATA').AsDateTime);
            qE1.ParamByName('mes').AsInteger := DameMes(tD.FieldByName('VISDATA').AsDateTime);
            qE1.ParamByName('actiu').AsInteger := 1;
            qE1.ParamByName('idProfessional').AsInteger := idProfessional;
            qE1.ParamByName('idPacient').AsInteger := idPacient;
            qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
            qE1.ParamByName('DasiclinicReg').AsInteger := 0;
            qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
            qE1.ExecSQL ;

            qE1.Close;
            qE1.SQL.Clear;
            qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
            qE1.ExecSQL ;
            idEpisodi := qE1.FieldByName('registre').AsInteger;
          end else idEpisodi := qE1.FieldByName('idEpisodi').AsInteger;
        end else  idEpisodi := -2;
      end else idEpisodi := -1;

      // si episodi es -1  o -2 vol dir que l'hem de buscar a la taula de relacions
      if idEpisodi =  -1 then
      begin
        // cerca EPISODI a taula de relacions_centres
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''EPISODIS'' and D_ID=:epiregistre and e_Centre=:idCentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('epiregistre').AsInteger := tD.FieldByName('epiregistre').AsInteger;
        qR1.ExecQuery;
        if qR1.RecordCount = 0 then idEpisodi := 0
          else idEpisodi := qR1.FieldByName('E_ID').AsVariant;
      end;
      if idEpisodi =  -2 then
      begin
        // cerca EPISODI a taula de relacions_centres
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''EPISODIS'' and D_ID=:epiregistre and e_Centre=:idCentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('epiregistre').AsInteger := qD1.FieldByName('epiregistre').AsInteger;
        qR1.ExecQuery;
        if qR1.RecordCount = 0 then idEpisodi := 0
          else idEpisodi := qR1.FieldByName('E_ID').AsVariant;
      end;

      // agafem hora i temps de la cita del eClinic
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT hora, temps from cites where idCita=:idCita');
      qE1.ParamByName('idCita').AsInteger := idCita;
      qE1.ExecSQL ;
      hora := qE1.FieldByName('hora').AsInteger;
      temps := qE1.FieldByName('temps').AsInteger;

      // crear VISITA
      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('INSERT INTO visites (idVisita, data, hora, temps, exercici, mes, '+
                  '  successiva, observacions, provescomplementaries, idProfessional, idPacient,'+
                  '  idCita, idEspecialitat, idEpisodi, DasiclinicReg, traspas) '+
                  'values(:idVisita, :data, :hora, :temps, :exercici, :mes, '+
                  '  :successiva, :observacions, :provescomplementaries, :idProfessional, :idPacient,'+
                  '  :idCita, :idEspecialitat, :idEpisodi, :DasiclinicReg, :traspas)');
      qE1.ParamByName('data').AsDateTime := tD.FieldByName('VISDATA').AsDateTime;
      qE1.ParamByName('hora').AsInteger := Hora;
      qE1.ParamByName('temps').AsInteger := temps;
      qE1.ParamByName('exercici').AsInteger := Exercici(tD.FieldByName('VISDATA').AsDateTime);
      qE1.ParamByName('mes').AsInteger := DameMes(tD.FieldByName('VISDATA').AsDateTime);
      qE1.ParamByName('successiva').AsInteger := 1;
      qE1.ParamByName('observacions').AsString := AnsiReplaceStr(tD.FieldByName('VISOBSERVACIONS').AsString, #13#10, '<br>');
      qE1.ParamByName('provescomplementaries').AsString := '';
      qE1.ParamByName('idProfessional').AsInteger := idProfessional;
      qE1.ParamByName('idPacient').AsInteger := idPacient;
      if idCita > 0 then
        qE1.ParamByName('idCita').AsInteger := idCita;
      qE1.ParamByName('idEspecialitat').AsInteger := idEspecialitat;
      if idEpisodi > 0 then
        qE1.ParamByName('idEpisodi').AsInteger := idEpisodi;
      qE1.ParamByName('DasiclinicReg').AsInteger := tD.FieldByName('visregistre').AsInteger;
      qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
      qE1.ExecSQL ;

      qE1.Close;
      qE1.SQL.Clear;
      qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
      qE1.ExecSQL ;
      idVisita := qE1.FieldByName('registre').AsInteger;

      qR2e.Close;
      qR2e.SQL.Clear;
      qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                   'values (''VISITES'',:D_ID,''visites'',:E_ID,:idCentre)');
      qR2e.ParamByName('D_ID').AsInteger := tD.FieldByName('VISREGISTRE').AsInteger;
      qR2e.ParamByName('E_ID').AsInteger := idVisita;
      qR2e.ParamByName('idCentre').AsInteger := idCentre;
      qR2e.ExecQuery;

      //  traspassem TRACTAMENTS
      tD1.Close;
      tD1.SelectSQL.Clear;
      tD1.SelectSQL.Add('select * from visites_tractaments where visregistre=:visregistre order by trvregistre');
      tD1.ParamByName('visregistre').AsInteger := tD.FieldByName('visregistre').AsInteger;
      tD1.Open;
      while not tD1.Eof do
      begin
        // cerca TRACTAMENT a taula de relacions_centres
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TRACTAMENTS'' and D_ID=:traregistre and e_Centre=:idCentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('traregistre').AsInteger := tD1.FieldByName('traregistre').AsInteger;
        qR1.ExecQuery;
        if qR1.RecordCount = 0 then idTractament := 0
          else idTractament := qR1.FieldByName('E_ID').AsVariant;

        // cerca TARIFA a taula de relacions_centres
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''TARIFES'' and D_ID=:trfregistre and e_Centre=:idCentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('trfregistre').AsInteger := tD1.FieldByName('trfregistre').AsInteger;
        qR1.ExecQuery;
        if qR1.RecordCount = 0 then idTarifa := 0
          else idTarifa := qR1.FieldByName('E_ID').AsVariant;

        // cerca BONUS a taula de relacions_centres
        qR1.Close;
        qR1.SQL.Clear;
        qR1.SQL.Add('select E_ID from RELACIONS_CENTRE where d_taula=''S_BONUS'' and D_ID=:bonregistre and e_Centre=:idCentre');
        qR1.ParamByName('idcentre').AsInteger := idcentre;
        qR1.ParamByName('bonregistre').AsInteger := tD1.FieldByName('bonregistre').AsInteger;
        qR1.ExecQuery;
        if qR1.RecordCount = 0 then idBonus := 0
          else idBonus := qR1.FieldByName('E_ID').AsInteger;

        // crear VISITA_tractament
        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('INSERT INTO visitesTractaments (id, idVisita, idTractament, numVisites,'+
                    'unitats, importUnitari, percentDte, importDte, importMutua,'+
                    'idTarifaMutua, observacions, importTotal, perImpost, importImpost,'+
                    'bonus, sessionsConsumides, showObsTracFra, costMaterial, irpfMutua,'+
                    'netMutua, dasiclinicReg, traspas, concepte) VALUES (:id, :idVisita,'+
                    ':idTractament, :numVisites, :unitats, :importUnitari, :percentDte,'+
                    ':importDte, :importMutua, :idTarifaMutua, :observacions,'+
                    ':importTotal, :perImpost, :importImpost, :bonus, :sessionsConsumides,'+
                    '1, :costMaterial, :irpfMutua, :netMutua, :dasiclinicReg,'+
                    ':traspas, :concepte)');
        qE1.ParamByName('idVisita').AsInteger := idVisita;
        qE1.ParamByName('idTractament').AsInteger := idTractament;
        qE1.ParamByName('numVisites').AsInteger := tD1.FieldByName('NUM_VISITES').AsInteger;
        qE1.ParamByName('unitats').AsInteger := tD1.FieldByName('UNITATS').AsInteger;
        qE1.ParamByName('importUnitari').AsFloat := tD1.FieldByName('TRVIMPORT_UNITARI').AsFloat;
        qE1.ParamByName('percentDte').AsFloat := tD1.FieldByName('DTO_PERCENT').AsFloat;
        qE1.ParamByName('importDte').AsFloat := tD1.FieldByName('DTO_IMPORT').AsFloat;
        qE1.ParamByName('importMutua').AsFloat := tD1.FieldByName('IMPORTE_MUTUA').AsFloat;
        qE1.ParamByName('idTarifaMutua').AsInteger := idTarifa;
        qE1.ParamByName('observacions').AsString := tD1.FieldByName('OBSERVACIONS').AsString;
        qE1.ParamByName('importTotal').AsFloat := tD1.FieldByName('IMPORT_TOTAL').AsFloat;
        qE1.ParamByName('perImpost').AsFloat := tD1.FieldByName('IVAPERCENT').AsFloat;
        qE1.ParamByName('importImpost').AsFloat := tD1.FieldByName('IVAIMPORT').AsFloat;
        if idBonus > 0 then
          qE1.ParamByName('bonus').AsInteger := idbonus;
        qE1.ParamByName('sessionsConsumides').AsInteger := 1; // s'ha de mirar si hi ha m�s de una sessio
        qE1.ParamByName('costMaterial').AsFloat := tD1.FieldByName('COST_MATERIAL').AsFloat;
        qE1.ParamByName('irpfMutua').AsFloat := tD1.FieldByName('IRPF_MUTUA').AsFloat;
        qE1.ParamByName('netMutua').AsFloat := tD1.FieldByName('NETO_MUTUA').AsFloat;
        qE1.ParamByName('DasiclinicReg').AsInteger := tD1.FieldByName('trvregistre').AsInteger;
        qE1.ParamByName('traspas').AsString := 'TRASPAS_'+Trim(Edit4.Text)+Trim(Edit8.Text);
        if Trim(tD1.FieldByName('texte').AsString)>'' then
          qE1.ParamByName('concepte').AsString := tD1.FieldByName('texte').AsString;
        qE1.ExecSQL ;

        qE1.Close;
        qE1.SQL.Clear;
        qE1.SQL.Add('SELECT LAST_INSERT_ID() registre');
        qE1.ExecSQL ;
        idVisitaTractament := qE1.FieldByName('registre').AsInteger;

        qR2e.Close;
        qR2e.SQL.Clear;
        qR2e.SQL.Add('insert into RELACIONS_CENTRE (D_TAULA, D_ID, E_TAULA, E_ID, E_CENTRE) '+
                     'values (''VISITES_TRACTAMENTS'',:D_ID,''visites'',:E_ID,:idCentre)');
        qR2e.ParamByName('D_ID').AsInteger := tD1.FieldByName('TRVREGISTRE').AsInteger;
        qR2e.ParamByName('E_ID').AsInteger := idVisitaTractament;
        qR2e.ParamByName('idCentre').AsInteger := idCentre;
        qR2e.ExecQuery;

        tD1.Next;
      end;
      tD.Next;
    end;
    sbCites.Enabled := False;
    PassadaOK('VISITES');
  except
    raise;
  end;
end;

function TForm2.CogNoms(var Cadena: String; Separador : string) : String;
var p, r, p2 : integer;
    l : string;
begin
  p := Pos(Separador,Cadena);
  if p = 0 then Result := Cadena
  else begin
    // miro si hi ha noms compostos SI HI HA mes de 2 cops el seprador
    // nomes tenim en compte NOMS compostos perque ni ha m�s que cognoms
    r := 0;
    p2 := 0;
    for p := 1 to Length(Trim(Cadena)) do
    begin
      l := cadena[p];
      r := r + iif(Cadena[p]=separador,1,0);
      if ((r=2) and (p2=0)) then P2 := p;
    end;
    if r > 2 then
    begin
      Result := Trim(Copy(Cadena,0,P2));
      cadena := Trim(Copy(Cadena,P2+1,100));
    end else
    begin
      Result := Trim(Copy(Cadena,0,Pos(Separador,Cadena)-1));
      cadena := Trim(Copy(Cadena,Pos(Separador,Cadena)+1,100));
    end;
  end;
end;

function TForm2.DonemPass(Clau: string; var Password: String; var Salt : string) : Boolean;
var params : TStrings;
    URLConnection, Response : String;
    JSONObject : TJSONObject;
begin
  Result := False;
{
Per lo del password:
La petici� es fa com amb lo del certificat de SEOGA.
La url per enviar la petici� es  https://www.dasieclinic.com/api/v1.0/dc/encripta
S'ha de passar el par�metre 'password' i de valor la contrasenya de l'usuari.
La resposta es en JSON com lo de SEOGA i torna 2 par�metres: 'salt' i 'password'. Aquests es poden insertar directament en el  user.
}

  URLConnection := 'https://www.dasieclinic.com/api/v1.0/dc/encripta';

  params := TStringList.Create;
  params.Add('password='+Clau);
  try
    Response := NetHTTPClient1.post(URLConnection, params).ContentAsString;
    JSONObject := TJSONObject.ParseJSONValue(Response) as TJSONObject;
    Password := JSONObject.GetValue('password').Value;
    Salt := JSONObject.GetValue('salt').Value;

    Result := true;
  except
    on E:Exception  do begin
       showmessage(e.Message);
    end
  end;
end;

function TForm2.QuitarCaracteres(Str: String): String;
var
  i: Integer;
begin
  Result:= EmptyStr;
  for i:= 1 to Length(Str) do
    if Str[i] < #128 then
      Result:= Result + Str[i];
end;

function TForm2.base64encode(f:TStream):string;
const
  Base64Codes:array[0..63] of char=
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
  dSize=57*100;//must be multiple of 3
var
  d:array[0..dSize-1] of byte;
  i,l:integer;
begin
  Result:='';
  l:=dSize;
  while l=dSize do
   begin
    l:=f.Read(d[0],dSize);
    i:=0;
    while i<l do
     begin
      if i+1=l then
        Result:=Result+
          Base64Codes[  d[i  ] shr  2]+
          Base64Codes[((d[i  ] and $3) shl 4)]+
          '=='
      else if i+2=l then
        Result:=Result+
          Base64Codes[  d[i  ] shr  2]+
          Base64Codes[((d[i  ] and $3) shl 4) or (d[i+1] shr 4)]+
          Base64Codes[((d[i+1] and $F) shl 2)]+
          '='
      else
        Result:=Result+
          Base64Codes[  d[i  ] shr  2]+
          Base64Codes[((d[i  ] and $3) shl 4) or (d[i+1] shr 4)]+
          Base64Codes[((d[i+1] and $F) shl 2) or (d[i+2] shr 6)]+
          Base64Codes[  d[i+2] and $3F];
      inc(i,3);
      if ((i mod 57)=0) then Result:=Result+#13#10;
     end;
   end;
end;

end.
