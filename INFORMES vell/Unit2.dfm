object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Trasp'#224's DasiClinic -->>> Dasi eClinic'
  ClientHeight = 846
  ClientWidth = 1463
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 467
    Top = 20
    Width = 54
    Height = 13
    Caption = 'ID Registre'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1463
    Height = 846
    Align = alClient
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 116
      Width = 1461
      Height = 729
      Align = alClient
      Caption = 'Panel2'
      ParentBackground = False
      TabOrder = 0
      object Panel3: TPanel
        Left = 1
        Top = 1
        Width = 1459
        Height = 156
        Align = alTop
        TabOrder = 0
        object Label13: TLabel
          Left = 7
          Top = 119
          Width = 240
          Height = 29
          Caption = 'Esperant 5 minuts...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Memo3: TMemo
          Left = 492
          Top = 1
          Width = 966
          Height = 154
          Align = alRight
          Alignment = taCenter
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Lines.Strings = (
            
              'Compte !!!  les taules que es relacionen aqui baix no tenen enca' +
              'ra alguns camps...  revisar abans de fer cada trasp'#224's '
            'si el client esta utilitzant aquests camps'
            ''
            
              'USUARIS el professional (no te els camps ACCES A L'#39'AGENDA ON-LIN' +
              'E i SOLAMENT POT VEURE L'#39'AGENDA DEL PROFESSIONALS ASSIGNAT)'
            
              'USUARIS COMPTE potser a m'#233's de l'#39'acronim cal posar un comptador ' +
              '(p.e. per SUPERVISOR)'
            'BONOS (verure com es gestionen les sessions consumides)'
            'TEMA FOTO PACIENT, S'#39'ha d'#39'implementar per que es puji al s3'
            'TEMA DOCUMENTS escanejats a la fitxa del pacient =='
            'TEMA ORIGENS, falta implementar i posar les altres dades'
            
              'TEMA ALERTA AGENDA, s'#39'ha de mirar a quin camp es posa del eClini' +
              'c')
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
        object Memo4: TMemo
          Left = 234
          Top = 1
          Width = 258
          Height = 154
          Align = alRight
          Lines.Strings = (
            'REVISAR ANTECEDENT i ANTECEDENTS PACIENT')
          TabOrder = 3
        end
        object CheckBox1: TCheckBox
          Left = 22
          Top = 5
          Width = 184
          Height = 17
          Caption = 'Revisar Poblacions/Provincies/Pais '
          TabOrder = 0
        end
        object sbPoblacions: TJvXPButton
          Left = 49
          Top = 28
          Width = 121
          Height = 32
          Caption = 'Poblacions'
          Enabled = False
          TabOrder = 1
          OnClick = sbPoblacionsClick
        end
        object Button1: TButton
          Left = 4
          Top = 88
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 4
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 85
          Top = 87
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 5
          Visible = False
          OnClick = sbInformesClick
        end
      end
      object Panel5: TPanel
        Left = 1
        Top = 157
        Width = 1459
        Height = 159
        Align = alTop
        TabOrder = 1
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object sbEspe: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Especialitats i Categories'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbEspeClick
          end
          object sbTipusDoc: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Tipus documents'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbTipusDocClick
          end
          object sbEmpresa: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Empreses'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbEmpresaClick
          end
        end
        object Panel8: TPanel
          Left = 141
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object sbAntecedents: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Antecedents'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbAntecedentsClick
          end
          object sbProcedencies: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Procedencies'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbProcedenciesClick
          end
          object sbOrigens: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Origens'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbOrigensClick
          end
          object sbCies: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'CIES'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbCiesClick
          end
        end
        object Panel9: TPanel
          Left = 281
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object sbGrupsITtes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Grups i Tractaments'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbGrupsITtesClick
          end
          object sbTarifesGRP: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Grups de Tarifes'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbTarifesGRPClick
          end
          object sbTarifes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Tarifes'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbTarifesClick
          end
          object sbPreus: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Preus'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbPreusClick
          end
        end
        object Panel10: TPanel
          Left = 701
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 3
          object sbEntitats: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'ENTITATS'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbEntitatsClick
          end
          object sbFP: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Formes pagament'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbFPClick
          end
          object sbBancs: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = ' BANCS'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbBancsClick
          end
          object sbUsuaris: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Usuaris'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbUsuarisClick
          end
        end
        object Panel11: TPanel
          Left = 841
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 4
          object sbPacients: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Pacients'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbPacientsClick
          end
          object sbAntecedentsPAc: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Antecedents Pacient'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbAntecedentsPAcClick
          end
          object sbRelacionsC: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Relacions Confian'#231'a'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbRelacionsCClick
          end
          object sbTutors: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Tutors'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbTutorsClick
          end
        end
        object Panel12: TPanel
          Left = 981
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 5
          object sbCites: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Cites'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbCitesClick
          end
          object sbTipusCita: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Tipus de Cita'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbTipusCitaClick
          end
          object sbBoxes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Boxes'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbBoxesClick
          end
        end
        object Panel13: TPanel
          Left = 561
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 6
          object sbAbsencies: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Absencies i bloquejos'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbAbsenciesClick
          end
          object sbApertures: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Apertures'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbAperturesClick
          end
          object sbHoraris: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Festius i horaris'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbHorarisClick
          end
          object sbProf: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Professionals'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbProfClick
          end
        end
        object Panel14: TPanel
          Left = 1121
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 7
          object sbVisites: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Visites'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbVisitesClick
          end
          object sbEpisodis: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Episodis'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbEpisodisClick
          end
          object sbInformes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Informes'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbInformesClick
          end
          object sbImatgesDocs: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Imatges i documents'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbImatgesDocsClick
          end
        end
        object Panel15: TPanel
          Left = 421
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 8
          object sbBonus: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Bonus'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbBonusClick
          end
          object sbConConta: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Conceptes CONTA'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbConContaClick
          end
          object sbClasImg: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Classific. imatges API'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbClasImgClick
          end
        end
        object Panel16: TPanel
          Left = 1261
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 9
          object sbCarrecs: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'C'#224'rrecs(FRA)/Abon. Priv.'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbCarrecsClick
          end
          object sbCobros: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Cobraments'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbCobrosClick
          end
        end
        object Panel17: TPanel
          Left = 1401
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 10
          object sbCaixes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Caixes'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbCaixesClick
          end
          object sbFraMutues: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Factures Mutua'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbFraMutuesClick
          end
          object sbCobMutua: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Cobraments Mutua'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbCobMutuaClick
          end
        end
      end
      object Memo2: TMemo
        Left = 688
        Top = 316
        Width = 286
        Height = 412
        Align = alRight
        Ctl3D = False
        Lines.Strings = (
          'CAMPS QUE NO EXISTEIXEN A eCLINIC'
          ''
          '------ GRUPS TARIFES'
          'TRGGRUP                    codi'
          'TRGIMPORT_FIX'
          'TRGIMPORT_MINIM'
          'TRGIMPORT_MAXIM'
          ''
          '------ ESPECIALITATS'
          'ESPECIAL    indica si es odontologia'
          'CUENTA'
          'CENTROCOSTE'
          'ORDRE       no es fa servir'
          ''
          '------ GRUPS_TRACTAMENTS'
          'grtpeces, '
          'peptractamentbmp, '
          'grtobturacio, '
          'grtnivell, '
          'grtpont, '
          'grtprotesis, '
          'cuenta, '
          'ccoste, '
          ''
          '------ TRACTAMENTS'
          'trapeces,'
          'traminimfac,'
          'tracostmaterial,     indica si te cost de material 0/1'
          'traimatge,'
          'trapont,'
          'tracodi,'
          'ctrl_laboratori,'
          'traprotesis,'
          'estoc_minimo,'
          'travenda,'
          'agrupacio,'
          'grup,'
          'trabonus,             indica si es bonus'
          'trasessions,'
          ''
          '------- S_EMPRESES'
          'empcodi (NO CAL), emplogo (s'#39'ha de pujar manual '
          'fins que no estigui fet l'#39'API), sistema'
          ''
          '------- S_PROFESSIONALS'
          'PRFIMATGE, PRFPERCENTATGE, '
          'PERCENTATGE_MUTUA, IMPORT_FIX,'
          'IMPORT_MINIM, PRFPERCENTATGEMAT, '
          'TIPUS_LIQUIDACIO, '
          'ORDRE_PERCENTATGES, IRPF, ENTCODI, '
          'PRFAGENCIA, PRFDIGITS, '
          'PRFCOMPTE, ENTREGISTRE, PRFSERIE, '
          'HONORARIS_MODIFICAR, '
          'PRFORDREAGENDA, USA_BOX, USA_URGENCIAS, '
          'CENTRO_ORIGEN, '
          'COLABORADOR, AVISO_SMS, SINCRO_WEB, '
          'CITA_ONLINE, IDAGENDA, '
          'PRFIBAN  La majoria son del tema de liquidacions o '
          'cita on-line'
          ''
          '')
        ParentCtl3D = False
        ScrollBars = ssVertical
        TabOrder = 2
      end
      object DBGrid1: TDBGrid
        Left = 1
        Top = 316
        Width = 687
        Height = 412
        Align = alClient
        DataSource = DataSource1
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object Panel6: TPanel
        Left = 974
        Top = 316
        Width = 486
        Height = 412
        Align = alRight
        Caption = 'Panel6'
        TabOrder = 4
        object Memo1: TMemo
          Left = 1
          Top = 1
          Width = 484
          Height = 112
          Align = alTop
          Lines.Strings = (
            'Memo1')
          TabOrder = 0
        end
        object RichEdit1: TRichEdit
          Left = 1
          Top = 113
          Width = 484
          Height = 298
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Lines.Strings = (
            'RichEdit1')
          ParentFont = False
          TabOrder = 1
          Zoom = 100
        end
      end
      object RichView1: TRichView
        Left = 859
        Top = 322
        Width = 522
        Height = 451
        TabOrder = 5
        Visible = False
        DoInPaletteMode = rvpaCreateCopies
        RTFReadProperties.TextStyleMode = rvrsAddIfNeeded
        RTFReadProperties.ParaStyleMode = rvrsAddIfNeeded
        RVFOptions = [rvfoSavePicturesBody, rvfoSaveControlsBody, rvfoSaveBinary, rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveDocProperties, rvfoLoadDocProperties, rvfoSaveDocObjects, rvfoLoadDocObjects]
        Style = RVStyle1
      end
      object DBRichViewPDF: TDBRichView
        Left = 318
        Top = 322
        Width = 521
        Height = 514
        DataField = 'INFORME'
        DataSource = dsInformes
        TabOrder = 6
        Visible = False
        DoInPaletteMode = rvpaCreateCopies
        RTFReadProperties.TextStyleMode = rvrsAddIfNeeded
        RTFReadProperties.ParaStyleMode = rvrsAddIfNeeded
        RVFOptions = [rvfoSavePicturesBody, rvfoSaveControlsBody, rvfoSaveBinary, rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveDocProperties, rvfoLoadDocProperties]
        Style = RVStyle1
      end
      object Memo5: TMemo
        Left = 120
        Top = 408
        Width = 1198
        Height = 289
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Lines.Strings = (
          'REVISAR'
          ''
          
            'FORMES DE PAGAMENT (mirar tema d'#39'esborrar les creades per defect' +
            'e a eClinic)'
          ''
          
            'INFORMES - PLATILLES (revisar com passar ITEMS i relacionar els ' +
            'items del DasiClinic amb els del eClinic)'
          
            'INFORMES (ara els pujem com PDF, per'#242' lo seu seria que fossin te' +
            'xt html per que aix'#237' els poden tornar a editar. Cal mirar com pu' +
            'jar les imatges)'
          ''
          'FACTURAR BESTRETES al eClinic no hi es encara'
          ''
          
            'ABONAMENTS O RECTIFICATIVES (nosaltres fem una vista amb prfregi' +
            'stre 0...)'
          'AJUSTOS MUTUES (nosaltres fem una visita amb PACREGISTRE 0)')
        ParentFont = False
        TabOrder = 7
        Visible = False
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 1461
      Height = 115
      Align = alTop
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 16
        Width = 69
        Height = 13
        Caption = 'Firebird origen'
      end
      object Label1: TLabel
        Left = 467
        Top = 44
        Width = 47
        Height = 13
        Caption = 'ID Centre'
      end
      object Label2: TLabel
        Left = 923
        Top = 18
        Width = 379
        Height = 16
        AutoSize = False
        Caption = 'Cl'#237'nica Origen FIREBIRD'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 923
        Top = 47
        Width = 430
        Height = 16
        AutoSize = False
        Caption = 'Cl'#237'nica Destino MySQL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 732
        Top = 22
        Width = 121
        Height = 13
        Caption = 'Acronim Usuaris duplicats'
      end
      object Label8: TLabel
        Left = 635
        Top = 4
        Width = 600
        Height = 16
        Caption = 
          'COMPTE MIRAR CAMPS BUITS QUE S'#39'ENCRIPTEN NO PASSAR-LOS I REVISAR' +
          ' TAMBE TEXTOS RTF'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 8
        Top = 44
        Width = 24
        Height = 13
        Caption = 'TEST'
      end
      object Label4: TLabel
        Left = 467
        Top = 69
        Width = 47
        Height = 13
        Caption = 'ID Centre'
      end
      object Label10: TLabel
        Left = 8
        Top = 69
        Width = 61
        Height = 13
        Caption = 'PRODUCCIO'
      end
      object Label11: TLabel
        Left = 732
        Top = 71
        Width = 127
        Height = 13
        Caption = #218'ltim Pacregistre IMATGES'
      end
      object Label12: TLabel
        Left = 923
        Top = 71
        Width = 173
        Height = 13
        Caption = 'Pacients a passar en el mateix bucle'
      end
      object Label14: TLabel
        Left = 732
        Top = 94
        Width = 177
        Height = 13
        Caption = 'Ruta per generar informes temporals'
      end
      object Edit3: TEdit
        Left = 104
        Top = 14
        Width = 357
        Height = 21
        ReadOnly = True
        TabOrder = 0
        Text = 
          '172.16.0.204:/dades/bbdd/dasiclinic/desarrollo/dasiclinic_352_3.' +
          'fdb'
      end
      object Edit2: TEdit
        Left = 104
        Top = 41
        Width = 357
        Height = 21
        TabOrder = 1
        Text = 'eclinic-test.coglltc7cvjd.eu-west-1.rds.amazonaws.com'
      end
      object Edit9: TEdit
        Left = 527
        Top = 41
        Width = 46
        Height = 21
        ReadOnly = True
        TabOrder = 2
        Text = '13835'
      end
      object JvXPButton2: TJvXPButton
        Left = 621
        Top = 15
        Width = 99
        Caption = 'Connectar FIB'
        TabOrder = 3
        OnClick = JvXPButton2Click
      end
      object JvXPButton1: TJvXPButton
        Left = 621
        Top = 41
        Width = 99
        Caption = 'Connectar TEST'
        TabOrder = 4
        OnClick = JvXPButton1Click
      end
      object sbReiniciar: TJvXPButton
        AlignWithMargins = True
        Left = 1372
        Top = 4
        Width = 85
        Height = 107
        Caption = 'Reiniciar'
        Enabled = False
        TabOrder = 5
        Align = alRight
        OnClick = sbReiniciarClick
      end
      object Edit4: TEdit
        Left = 527
        Top = 15
        Width = 89
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
      end
      object Edit6: TEdit
        Left = 860
        Top = 19
        Width = 38
        Height = 21
        ReadOnly = True
        TabOrder = 7
        Text = 'HUM'
      end
      object ckExtern: TCheckBox
        Left = 732
        Top = 43
        Width = 140
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Es un trasp'#224's EXTERN'
        TabOrder = 8
      end
      object Edit8: TEdit
        Left = 527
        Top = 66
        Width = 46
        Height = 21
        ReadOnly = True
        TabOrder = 9
      end
      object JvXPButton3: TJvXPButton
        Left = 621
        Top = 66
        Width = 99
        Caption = 'Connectar PROD'
        TabOrder = 10
        OnClick = JvXPButton3Click
      end
      object Edit7: TEdit
        Left = 103
        Top = 66
        Width = 358
        Height = 21
        TabOrder = 11
        Text = 'eclinic-prod.coglltc7cvjd.eu-west-1.rds.amazonaws.com'
      end
      object Edit1: TEdit
        Left = 574
        Top = 52
        Width = 46
        Height = 21
        ReadOnly = True
        TabOrder = 12
      end
      object Edit5: TEdit
        Left = 860
        Top = 66
        Width = 57
        Height = 21
        TabOrder = 13
        Text = '0'
      end
      object sp1: TSpinEdit
        Left = 1101
        Top = 66
        Width = 40
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 14
        Value = 10
      end
      object CheckBox2: TCheckBox
        Left = 1145
        Top = 69
        Width = 147
        Height = 17
        Caption = 'AUTO-REINICIAR despr'#232's'
        Checked = True
        State = cbChecked
        TabOrder = 15
      end
      object Edit10: TEdit
        Left = 915
        Top = 91
        Width = 442
        Height = 21
        TabOrder = 16
        Text = 'C:\TRASPASOS\dasi\pruebas\'
      end
    end
  end
  object qE1: TMyQuery
    Connection = msEClinic
    Left = 79
    Top = 19
  end
  object FIBDades: TpFIBDatabase
    DBName = 
      '172.16.0.204:/dades/bbdd/dasiclinic/desarrollo/dasiclinic_351.fd' +
      'b'
    DBParams.Strings = (
      'User_Name=SYSDBA'
      'password=masterkey')
    DefaultTransaction = FIBPrincipal
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = []
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 8
    Top = 2
  end
  object FIBPrincipal: TpFIBTransaction
    DefaultDatabase = FIBDades
    TRParams.Strings = (
      'read'
      'rec_version'
      'read_committed')
    TPBMode = tpbDefault
    Left = 37
    Top = 2
  end
  object pFIBRelacions: TpFIBDatabase
    DBName = 
      '172.16.0.204:/dades/bbdd/dasiclinic/traspasos/dasieclinic/dasiec' +
      'linic.fdb'
    DBParams.Strings = (
      'User_Name=SYSDBA'
      'password=masterkey')
    DefaultTransaction = pFIBTrans_Relacions
    DefaultUpdateTransaction = TEscritura
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = []
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 8
    Top = 67
  end
  object pFIBTrans_Relacions: TpFIBTransaction
    DefaultDatabase = pFIBRelacions
    TRParams.Strings = (
      'read'
      'rec_version'
      'read_committed')
    TPBMode = tpbDefault
    Left = 37
    Top = 67
  end
  object qD1: TpFIBQuery
    Transaction = FIBPrincipal
    Database = FIBDades
    Left = 192
    Top = 64
    qoStartTransaction = True
  end
  object qR1: TpFIBQuery
    Transaction = pFIBTrans_Relacions
    Database = pFIBRelacions
    Left = 112
    Top = 64
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qR2e: TpFIBQuery
    Transaction = TEscritura
    Database = pFIBRelacions
    Left = 141
    Top = 64
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tDPob: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select distinct(p.pobregistre), pa.paia2, po.prvregistre, po.pob' +
        'nom from s_pacients p '
      
        '              left join s_poblacions po on p.pobregistre=po.pobr' +
        'egistre '
      
        '              left join s_paisos pa on po.pairegistre=pa.pairegi' +
        'stre '
      '              where p.pobregistre>0'
      'UNION'
      
        'select distinct(p.pobregistre), pa.paia2, po.prvregistre, po.pob' +
        'nom from s_professionals p '
      
        '              left join s_poblacions po on p.pobregistre=po.pobr' +
        'egistre '
      
        '              left join s_paisos pa on po.pairegistre=pa.pairegi' +
        'stre '
      '              where p.pobregistre>0'
      'UNION'
      
        'select distinct(p.pobregistre), pa.paia2, po.prvregistre, po.pob' +
        'nom from s_empreses p '
      
        '              left join s_poblacions po on p.pobregistre=po.pobr' +
        'egistre '
      
        '              left join s_paisos pa on po.pairegistre=pa.pairegi' +
        'stre '
      '              where p.pobregistre>0'
      'UNION'
      
        'select distinct(p.pobregistre), pa.paia2, po.prvregistre, po.pob' +
        'nom from TARIFES p '
      
        '              left join s_poblacions po on p.pobregistre=po.pobr' +
        'egistre '
      
        '              left join s_paisos pa on po.pairegistre=pa.pairegi' +
        'stre '
      '              where p.pobregistre>0')
    Transaction = FIBPrincipal
    Database = FIBDades
    AutoCommit = True
    Left = 8
    Top = 112
  end
  object msEClinic: TMyConnection
    Database = 'dasieclinicDB'
    Username = 'dasieclinic'
    Server = 'eclinic-prod.coglltc7cvjd.eu-west-1.rds.amazonaws.com'
    LoginPrompt = False
    Left = 8
    Top = 32
    EncryptedPassword = '96FFB4FF9EFF95FF94FFA8FF9AFF90FF96FFC7FFCCFFC8FFCDFF'
  end
  object TEscritura: TpFIBTransaction
    DefaultDatabase = pFIBRelacions
    Left = 66
    Top = 67
  end
  object DataSource1: TDataSource
    AutoEdit = False
    DataSet = tD
    Left = 40
    Top = 112
  end
  object tD: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from tarifes where trfregistre>=0')
    Transaction = FIBPrincipal
    Database = FIBDades
    AutoCommit = True
    Left = 272
    Top = 64
  end
  object tD1: TpFIBDataSet
    Transaction = FIBPrincipal
    Database = FIBDades
    Left = 304
    Top = 64
  end
  object RVStyle1: TRVStyle
    TextStyles = <
      item
        StyleName = 'Normal text'
        FontName = 'Arial'
        Unicode = True
        Size = 10
      end
      item
        StyleName = 'Heading'
        FontName = 'Arial'
        Style = [fsBold]
        Color = clBlue
        Unicode = True
        Size = 10
      end
      item
        StyleName = 'Subheading'
        FontName = 'Arial'
        Style = [fsBold]
        Color = clNavy
        Unicode = True
        Size = 10
      end
      item
        StyleName = 'Keywords'
        FontName = 'Arial'
        Style = [fsItalic]
        Color = clMaroon
        Unicode = True
        Size = 10
      end
      item
        StyleName = 'Jump 1'
        FontName = 'Arial'
        Style = [fsUnderline]
        Color = clGreen
        Unicode = True
        Jump = True
        Size = 10
      end
      item
        StyleName = 'Jump 2'
        FontName = 'Arial'
        Style = [fsUnderline]
        Color = clGreen
        Unicode = True
        Jump = True
        Size = 10
      end>
    ParaStyles = <
      item
        Tabs = <>
      end
      item
        StyleName = 'Centered'
        Alignment = rvaCenter
        Tabs = <>
      end>
    ListStyles = <>
    InvalidPicture.Data = {
      07544269746D617036100000424D361000000000000036000000280000002000
      0000200000000100200000000000001000000000000000000000000000000000
      0000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF000000FF000000FF00FFFF
      FF00FFFFFF000000FF000000FF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF000000FF000000FF00FFFF
      FF00FFFFFF000000FF000000FF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008080800080808000808080008080800080808000808080008080
      800080808000808080008080800080808000808080008080800080808000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000}
    StyleTemplates = <>
    Left = 1193
    Top = 775
  end
  object tInformes: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '    INFREGISTRE, INFTITULO, INFDATA, PACREGISTRE, USRREGISTRE,'
      '    INFORME'
      'FROM'
      '    INFORMES'
      'where(  infregistre=:infregistre'
      '     ) and (     INFORMES.INFREGISTRE = :OLD_INFREGISTRE'
      '     )'
      '        ')
    SelectSQL.Strings = (
      'SELECT'
      '    INFREGISTRE, INFTITULO, INFDATA, PACREGISTRE, USRREGISTRE,'
      '    INFORME'
      'FROM'
      '    INFORMES'
      'where infregistre=:infregistre    ')
    Transaction = FIBPrincipal
    Database = FIBDades
    Left = 320
    Top = 111
    object tInformesINFREGISTRE: TFIBIntegerField
      FieldName = 'INFREGISTRE'
    end
    object tInformesINFORME: TFIBMemoField
      FieldName = 'INFORME'
      BlobType = ftMemo
      Size = 8
    end
    object tInformesINFTITULO: TFIBStringField
      FieldName = 'INFTITULO'
      Size = 40
      EmptyStrToNull = True
    end
    object tInformesINFDATA: TFIBDateTimeField
      FieldName = 'INFDATA'
    end
    object tInformesPACREGISTRE: TFIBIntegerField
      FieldName = 'PACREGISTRE'
    end
    object tInformesUSRREGISTRE: TFIBIntegerField
      FieldName = 'USRREGISTRE'
    end
  end
  object dsInformes: TDataSource
    DataSet = tInformes
    Left = 372
    Top = 111
  end
  object gtHTMLEngine1: TgtHTMLEngine
    FileExtension = 'htm'
    FileDescription = 'HyperText Markup Language'
    InputXRes = 96
    InputYRes = 96
    ReferencePoint = rpBand
    EMailSettings.AuthenticationRequired = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ImageSettings.SourceDPI = 96
    ImageSettings.OutputImageFormat = ifJPEG
    Page.Width = 8.267700000000000000
    Page.Height = 11.692900000000000000
    Page.BinNumber = 0
    Preferences.AutoScroll = False
    Preferences.OptimizeForIE = False
    Navigator.LinkFont.Charset = DEFAULT_CHARSET
    Navigator.LinkFont.Color = clBlue
    Navigator.LinkFont.Height = -27
    Navigator.LinkFont.Name = 'Wingdings'
    Navigator.LinkFont.Style = []
    TransparentBackground = True
    TOCPageSettings.ItemFont.Charset = DEFAULT_CHARSET
    TOCPageSettings.ItemFont.Color = clWindowText
    TOCPageSettings.ItemFont.Height = -11
    TOCPageSettings.ItemFont.Name = 'Tahoma'
    TOCPageSettings.ItemFont.Style = []
    TOCPageSettings.TitleFont.Charset = DEFAULT_CHARSET
    TOCPageSettings.TitleFont.Color = clWindowText
    TOCPageSettings.TitleFont.Height = -11
    TOCPageSettings.TitleFont.Name = 'Tahoma'
    TOCPageSettings.TitleFont.Style = []
    TOCPageSettings.Title = 'Table Of Contents'
    Left = 1225
    Top = 703
  end
  object gtRichViewInterface1: TgtRichViewInterface
    Engine = gtHTMLEngine1
    DoBeginDoc = True
    DoEndDoc = True
    Left = 1329
    Top = 711
  end
  object RVReportHelper1: TRVReportHelper
    NoMetafiles = False
    PreviewCorrection = True
    RichView.BackgroundStyle = bsNoBitmap
    RichView.BottomMargin = 0
    RichView.RTFReadProperties.TextStyleMode = rvrsAddIfNeeded
    RichView.RTFReadProperties.ParaStyleMode = rvrsAddIfNeeded
    RichView.RVFOptions = [rvfoSavePicturesBody, rvfoSaveControlsBody, rvfoSaveBinary, rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveDocProperties, rvfoLoadDocProperties, rvfoSaveDocObjects, rvfoLoadDocObjects]
    RichView.TopMargin = 0
    Left = 1113
    Top = 727
  end
  object tD2: TpFIBDataSet
    Transaction = FIBPrincipal
    Database = FIBDades
    Left = 336
    Top = 64
  end
  object tD0: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from tarifes where trfregistre>=0')
    Transaction = FIBPrincipal
    Database = FIBDades
    AutoCommit = True
    Left = 240
    Top = 64
  end
  object qE2: TMyQuery
    Connection = msEClinic
    Left = 109
    Top = 19
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    HandleRedirects = True
    AllowCookies = True
    UserAgent = 'Embarcadero URI Client/1.0'
    OnValidateServerCertificate = NetHTTPClient1ValidateServerCertificate
    Left = 176
    Top = 136
  end
end
