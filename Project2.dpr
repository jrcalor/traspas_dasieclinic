program Project2;

uses
  Forms,
  Unit2 in 'Unit2.pas' {Form2},
  UFuncions in 'C:\componentsD2007\Funcions\UFuncions.pas',
  UFValors in 'C:\componentsD2007\Funcions\UFValors.pas' {FValors: TDataModule},
  UConstDC in 'C:\componentsD2007\Funcions\UConstDC.pas' {dmMensajes: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFValors, FValors);
  Application.CreateForm(TdmMensajes, dmMensajes);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
