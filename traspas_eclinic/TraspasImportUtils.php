<?php

namespace TraspasBundle\Model;

use RtfParser\Parser;
use RtfParser\Scanner;
use Symfony\Component\Finder\Finder;


class TraspasImportUtils
{

    public function rtfParse($text) {
        $scanner = new Scanner($text);
        $parser = new Parser($scanner);
        $text = '';
        $doc = $parser->parse();
        foreach ($doc->childNodes() as $node) {
            $text .= $node->text();
        }
        return $text;
    }

    private function read_doc($filename) {

        $fileHandle = fopen($filename, "r");
        $line = @fread($fileHandle, filesize($filename));
        $lines = explode(chr(0x0D),$line);
        $outtext = "";
        foreach($lines as $thisline)
        {
            $pos = strpos($thisline, chr(0x00));
            if (($pos !== FALSE)||(strlen($thisline)==0))
            {
            } else {
                $outtext .= $thisline." ";
            }
        }
//        $outtext = preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/","",$outtext);

        $chars = array(
            chr(0x01),
            chr(0x03),
            chr(0x04),
            chr(0x13),
            chr(0x14),
            chr(0x15),
            "\t",
        );

        $outtext = str_replace($chars, "", $outtext);

/*        $outtext = explode('Y,?'.chr(0x07), $outtext)[0];
        $outtext = str_replace('FORMTEXT', '', $outtext);
        $outtext = preg_replace('/\?\b/', '\'', $outtext);
        $outtext = trim($outtext);*/

        // esborra peu de pàgina
        /*$search = 'Tel. 93.636.37.37';
        $pospeu = strpos($outtext, $search);
        if ($pospeu !== false) {
            $outtext = substr($outtext, 0, $pospeu + strlen($search));
        }*/

        return $outtext;
    }

    private function read_docx($filename){

        $striped_content = '';
        $content = '';

        $zip = zip_open($filename);

        if (!$zip || is_numeric($zip)) return false;

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while

        zip_close($zip);


//        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $content = str_replace('</w:pPr></w:p>', "\r\n", $content);

        $striped_content = strip_tags($content);

        return $striped_content;
    }

    public function convertToText($filename, $traspas, $dir = 'files', $output = 'raw') {

        $path = __DIR__ . "/../../../web/uploads/traspas/" . $traspas . "/" . $dir . '/' . $filename;

        if(isset($path) && !file_exists($path)) {
            return "File Not exists";
        }

        $fileArray = pathinfo($path);
        $file_ext = $fileArray['extension'];
        if($file_ext == "doc" || $file_ext == "docx")
        {
            if($file_ext == "doc") {
                $content = $this->read_doc($path);
            } else {
                $content = $this->read_docx($path);
            }

            if ($output == 'html') {
                $content = nl2br($content);
                $content = preg_replace('/[\x00-\x1F\x7F\xCE\xBC\xE2\x89\xA5]/', '', $content);
            }

            return $content;

        } else {
            return "Invalid File Type";
        }
    }

    /**
     * Parse a csv file to array
     *
     * @return array
     * @throws \Exception
     *
     */
    public function parseCSV($traspas, $filename, $delimiter = ';')
    {
        $path = __DIR__ . "/../../../web/uploads/traspas/" . $traspas . "/";

        $ignoreFirstLine = true;
        $finder = new Finder();
        $finder->files()
            ->in($path)
            ->name($filename)
            ->files();
        foreach ($finder as $file) {
            $csv = $file;
        }
        if(empty($csv)){
            throw new \Exception("NO CSV FILE");
        }

        $rows = [];
        if (($handle = fopen($csv->getRealPath(), "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle, null, $delimiter)) !== FALSE) {
                $i++;
                if ($ignoreFirstLine && $i == 1) {
                    // llegim la primera linia amb el nom de les columnes
                    foreach ($data as $key => $value) {
                        $col[$key] = $value;
                    }
                    continue;
                }

                $row = [];
                foreach ($data as $key => $value) {
                    $row[$col[$key]] = $value;
                }

                $rows[] = $row;
            }
            fclose($handle);
        }
        return $rows;
    }

    /**
     *
     * Converteix formularis xml a html
     * http://localhost/dasieclinic/web/app_dev.php/parsexml/anabernabeu/script/d1s32014avm34ISAD
     *
     * @param $filename
     */
    public function parseXMLForm($traspas, $filename)
    {
        $path = __DIR__ . "/../../../web/uploads/traspas/" . $traspas . "/";

        $fileHandle = fopen($path . $filename, "r");
        $data = @fread($fileHandle, filesize($path . $filename));

        $file = new \SimpleXMLIterator($data);
        $file->next(); // saltem les labels

        $html = '';

        $form = new \SimpleXMLElement ( $file->asXML() );
//        dump($form);exit;

        foreach ( $form->children () as $field ) {



            $field = $form->current();
            $name = $field->getName();

/*            if (in_array($name, ['labels'])) {
                continue;
            }*/

            while ($field && !$field->hasChildren()) {
                $field = $field->getChildren();
                $html .= $this->parseXmlField($field);
            }
        }

//dump($field);


        dump($html);
        exit;
    }

    /**
     * @param $traspas
     * @param $dir
     * @return array|false
     */
    public function scandir($traspas, $dir = 'files') {

        $path = __DIR__ . "/../../../web/uploads/traspas/" . $traspas . "/" . $dir;
        $files = scandir($path);

        // s'esborra el . i el ..
        array_shift($files);
        array_shift($files);

        return $files;
    }


    /**
     * @param $field
     * @return string
     */
    private function parseXmlField($field)
    {
        $required = ($field['required'] == 'true') ? 'required' : null;
        $label = ($field['label']) ? $field['label'] : null;
        $type = ($field['tipo']) ? $field['tipo'] : null;

        $html = '';

        switch ($field['type']) {
            case 'select':

                if (isset ($field) && $field != '') {
                    $html .= $label;
                    $html .= '<select id="' . $field['id'] . '" ' . $required . '>';

                    $data = explode(',', $field);
                    foreach ($data as $value) {
                        $html .= '<option value="' . $value . '">' . $value . '</option>';
                    }
                    $html .= "</select>";
                }
                break;

            case 'field':
                $html = $label . ': ' . (string)$field;

                break;
        }

        return $html;

    }

}