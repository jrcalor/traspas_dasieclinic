<?php

namespace TraspasBundle\Model;

use DasiBundle\Funcions\AES;
use Doctrine\DBAL\DriverManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use TraspasBundle\Model\TraspasImportUtils;
use DatabaseBundle\Entity as DB;

class TraspasService extends TraspasEntities
{
    protected $em;
    protected $usuari;
    protected $queries;
    protected $params;
    protected $especialitat;
    public $env;
    public $root;
    public $uploadDir;
    public $hash;
    public $centre;
    public $clauValor;
    public $profService;
    public $episService;
    public $factService;
    protected $rtfReader;
    protected $htmlFormatter;
    protected $importUtils;

    public function __construct($info, $em, $usuari, $queries, $funcions, $profService, $episService, $factService, $arxiuService, $informeService, $legacyAgendaService)
    {
        ini_set('max_execution_time', '12000');
        ini_set('memory_limit', '2G');

        $this->traspas = $info['traspas'];
        $this->env = $info['env'];
        $this->root = $info['root'];
        $this->uploadDir = $info['uploadDir'];
        $this->clauValor = [];
        $this->em = $em;
        $this->usuari = $usuari;
        $this->queries = $queries;
        $this->funcions = $funcions;
        $this->profService = $profService;
        $this->episService = $episService;
        $this->factService = $factService;
        $this->arxiuService = $arxiuService;
        $this->informeService = $informeService;
        $this->legacyAgendaService = $legacyAgendaService;
        $this->rtfReader = new \RtfReader();
        $this->htmlFormatter = new \RtfHtml();
        $this->importUtils = new TraspasImportUtils();
    }

    public function setEntityManager($em)
    {
        $this->em = $em;
    }

    public function setDasiUsuari($usuari)
    {
        $this->usuari = $usuari;
    }

    public function setDasiQueries($queries)
    {
        $this->queries = $queries;
    }

    public function setDasiFuncions($dasiFuncions)
    {
        $this->dasiFuncions = $dasiFuncions;
    }

    public function setDasiProfessionals($professionalsSevice)
    {
        $this->profService = $professionalsSevice;
    }

    public function setCentre($id)
    {
        $this->centre = $this->em->getRepository('DatabaseBundle:Centres')
            ->find($id);
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    public function setEspecialitat($id)
    {
        $this->especialitat = $this->em->getRepository('DatabaseBundle:Especialitats')
            ->find($id);
    }

    public function setClauValor($clauValor)
    {
        $this->clauValor = $clauValor;
    }

    /*
     * Retorna conexió a base da dades origen
     */
    public function getOrigenConn($connectionParams)
    {
        $config = new \Doctrine\DBAL\Configuration();

        $conn = DriverManager::getConnection($connectionParams, $config);

        return $conn;
    }

    /*
     * Retorna conexió a base da dades destí
     */
    public function getDestiConn()
    {
/*        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = array(
            'dbname' => 'openclinica',
            'user' => 'clinica',
            'password' => 'sW67Clnvu3rBCWdC',
            'host' =>  '212.92.40.134',
            'port' => '5432',
            'driver' => 'pdo_pgsql',
        );
        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        return $conn;*/

    }



    /*
     * -------------------- FUNCIONS GENÈRIQUES DE TRASPÀS A eCLINIC ------------------------
     */

    /**
     * traspàs especialitats a eClinic
     * @param $especialitats array
     * @return string
     */
    public function serviceEspecialitats($especialitats)
    {
        $co = 0;
        foreach ($especialitats as $p) {
            $especialitat = $this->createEspecialitat($p);
            $this->funcions->addToLog('traspas', $this->traspas, 'Created especialitat ' . $especialitat->getNom() . ' (' . $especialitat->getIdEspecialitat() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' especialitats created';
    }

    /**
     * traspàs tags a eClinic (pel DC s'usa per traspassar els Origens del pacient)
     * @param $tags array
     * @return string
     */
    public function serviceTags($tags)
    {
        $co = 0;
        foreach ($tags as $p) {
            $tag = $this->createTag($p);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created tag ' . $tag->getNom() . ' (' . $tag->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' tags created';
    }


    /**
     * traspàs impostos a eClinic
     * @param $impostos array
     */
    /*    public function serviceImpostos($impostos)
        {
            $co = 0;
            foreach ($impostos as $p) {
                $impostos = $this->createImpost($p);

                $this->funcions->addToLog('traspas', $this->traspas, 'Created impost ' . $impost->getNom(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
                $co++;
            }
            $this->em->flush();
            $this->em->clear();

            return $co . ' impostos created';
        }*/

    /**
     * traspàs especialitats a eClinic
     * @param $especialitats array
     * @return string
     */
    public function serviceCategories($categories)
    {
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');

        $co = 0;
        foreach ($categories as $p) {
            // especialitat fk
            $especialitat = null;
            if (isset($p['idEspecialitat'])) {
                $especialitat = $especRepo->find($p['idEspecialitat']);
            } elseif (isset($p['especialitatReg'])) {
                $especialitat = $especRepo->findOneBy([
                    'dasiclinicReg' => $p['especialitatReg'],
                    'idCentre' => $this->centre]);
            } elseif (isset($p['especialitat'])) {
                $especialitat = $especRepo->findOneBy([
                    'nom' => $p['especialitat'],
                    'idCentre' => $this->centre]);
            }

            $categoria = $this->createCategoria($p, $especialitat);
            $this->funcions->addToLog('traspas', $this->traspas, 'Created especialitat ' . $categoria->getNom() . ' (' . $categoria->getEspecialitat()->getNom() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' categories created';
    }

    /**
     * traspàs tipus visita a eClinic
     * @param $tipusVisita array
     * @return string
     */
    public function serviceTipusVisita($registres)
    {
        $co = 0;
        foreach ($registres as $p) {
            $tipusVisita = $this->createTipusVisita($p);
            $this->funcions->addToLog('traspas', $this->traspas, 'Created tipusVisita ' . $tipusVisita->getNom() . ' (' . $tipusVisita->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' tipusVisita created';
    }


    /**
     * traspàs procedencies a eClinic
     * @param $procedencies array
     * @return string
     */
    public function serviceProcedencies($procedencies)
    {
        $co = 0;
        foreach ($procedencies as $p) {
            $procedencia = $this->createProcedencia($p);
            $this->funcions->addToLog('traspas', $this->traspas, 'Created procecencia ' . $procedencia->getNom() . ' (' . $procedencia->getIdProcedencia() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' procedencies created';
    }

    /**
     * traspàs antecedents (centre) a eClinic
     * @param $procedencies array
     * @return string
     */
    public function serviceAntecedents($antecedents)
    {
        $co = 0;
        foreach ($antecedents as $p) {
            $antecedent = $this->createAntecedent($p);
            $this->funcions->addToLog('traspas', $this->traspas, 'Created antecedent /centre) ' . $antecedent->getNom() . ' (' . $antecedent->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' antecedents (centre) created';
    }

    /**
     * traspàs boxes a eClinic
     * @param $boxes array
     * @return string
     */
    public function serviceBoxes($boxes)
    {
        $co = 0;

        $boxRepo = $this->em->getRepository('DatabaseBundle:Boxes');
        foreach ($boxes as $p) {

            // no tornem a crear si ja existeix el box
            $box = $boxRepo->findOneBy([
                'centre' => $this->centre,
                'dasiclinicReg' => $p['dasiclinicReg']
            ]);

            if (!$box) {
                $box = $this->createBox($p, $co + 1);
                $this->em->persist($box);
                $co++;

                $this->funcions->addToLog('traspas', $this->traspas, 'Created box ' . $box->getNom() . ' (' . $box->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            }

            //assigna horari a box
            // si dia setmana és 0 llavors horari de dilluns a divendres (DC)
            if ($p['diaSetmana'] == 0) {
                for ($i = 1; $i < 6; $i++) {
                    $p['diaSetmana'] = $i;
                    $horari = $this->createHorari($p);
                    $horari->setBoxes($box);
                }
            } else {
                $horari = $this->createHorari($p);
                $horari->setBoxes($box);
            }

        }

        $this->em->flush();
        $this->em->clear();

        return $co . ' box created';
    }

    /**
     * traspàs grup tractaments a eClinic
     * @param $grups array
     * @return string
     */
    public function serviceGrupTractaments($grups)
    {
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');

        $co = 0;
        foreach ($grups as $p) {
            $grup = $this->createGrupTractament($p);

            // especialitat fk
            $especialitat = null;
            if (isset($p['idEspecialitat'])) {
                $especialitat = $especRepo->find($p['idEspecialitat']);
            } elseif (isset($p['especialitatReg'])) {
                $especialitat = $especRepo->findOneBy([
                    'dasiclinicReg' => $p['especialitatReg'],
                    'idCentre' => $this->centre]);
            } elseif (isset($p['especialitat'])) {
                $especialitat = $especRepo->findOneBy([
                    'nom' => $p['especialitat'],
                    'idCentre' => $this->centre]);
            }

            if ($especialitat) {
                $grup->setEspecialitat($especialitat);
            } else {
                continue;
            }

            $this->funcions->addToLog('traspas', $this->traspas, 'Created grup tractament ' . $grup->getNom() . ' (' . $grup->getIdGrup() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' grups tractaments created';
    }


    /**
     * traspàs tractaments a eClinic
     * @param $tractaments array
     * @return string
     */
    public function serviceTractaments($tractaments)
    {
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');
        $grupTracRepo = $this->em->getRepository('DatabaseBundle:GrupTractaments');
        $impostRepo = $this->em->getRepository('DatabaseBundle:Impostos');
        $bonusRepo = $this->em->getRepository('DatabaseBundle:Bonus');

        $co = 0;
        foreach ($tractaments as $p) {
            $tractament = $this->createTractament($p);

            // especialitat fk
            $especialitat = null;
            if (isset($p['idEspecialitat'])) {
                $especialitat = $especRepo->find($p['idEspecialitat']);
            } elseif (isset($p['especialitatReg'])) {
                $especialitat = $especRepo->findOneBy([
                    'dasiclinicReg' => $p['especialitatReg'],
                    'idCentre' => $this->centre]);
            } elseif (isset($p['especialitat'])) {
                $especialitat = $especRepo->findOneBy([
                    'nom' => $p['especialitat'],
                    'idCentre' => $this->centre]);
            }

            if ($especialitat) {
                $tractament->setIdEspecialitat($especialitat);
            } else {
                continue;
            }

            // grupTractament fk
            $grup = null;
            if (isset($p['idGrupTractaments'])) {
                $grup = $grupTracRepo->find($p['idGrupTractaments']);
            } elseif (isset($p['especialitatReg'])) {
                $grup = $grupTracRepo->findOneBy([
                    'especialitat' => $especialitat->getIdEspecialitat(),
                    'dasiclinicReg' => $p['grupTractamentReg']
                ]);
            }

            if ($grup) {
                $tractament->setIdGrupTractaments($grup);
            }

            // impost fk
            if (isset($p['impost'])) {
                $impost = $impostRepo->find($p['id']);
                if ($impost) {
                    $tractament->setIdImpost($impost);
                }
            }

            // Pel traspàs de DC deixem la fk de bonus a null i la posarem al crear els bonus
            // bonus fk
            /*            $bonus = null;
                        if (isset($p['idBonus'])) {
                            $bonus = $bonusRepo->find($p['idBonus']);
                        } else {
                            // es crea el bonus associat al tractament
                            $bonus = $this->createBonus($p);
                        }
                        if ($bonus) {
                            $tractament->setBonus($bonus);
                        }*/

            $this->em->persist($tractament);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created tractament ' . $tractament->getNom() . ' (' . $tractament->getIdTractament() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' tractaments created';
    }


    /**
     * traspàs bonus a eClinic
     *
     * @param $bonuses
     * @return string
     */
    public function serviceBonus($bonuses)
    {
        $tracRepo = $this->em->getRepository('DatabaseBundle:Tractaments');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');

        $co = 0;
        foreach ($bonuses as $p) {
            $bonus = $this->createBonus($p);

            // pacient fk
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);

            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);

            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);
            }

            if ($pacient) {
                $bonus->setPacient($pacient);
            }
            $this->em->persist($bonus);

            // Tractament fk
            $tractament = null;
            if (isset($p['idTractament'])) {
                $tractament = $tracRepo->find($p['idTractament']);

            } elseif (isset($p['tractamentReg'])) {
                $tractament = $tracRepo->findOneBy([
                    'dasiclinicReg' => $p['tractamentReg'],
                    'idCentre' => $this->centre
                ]);
            }

            // lliguem el bonus al seu tractament
            if ($tractament) {
                $tractament->setBonus($bonus);
            } else {
                echo "no es troba el tractament - ";
                continue;
            }

            $this->funcions->addToLog('traspas', $this->traspas, 'Created bonus ' . $bonus->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' bonus created';
    }


    /**
     * traspàs tarifesMutues i tractamentsTarifes a eClinic
     *
     * @param $tarifesMutues
     * @param $tractamentsTarifes
     * @return string
     */
    public function serviceTarifesMutues($tarifesMutues, $tractamentsTarifes)
    {
        $coMut = 0;
        $coTra = 0;
        foreach ($tarifesMutues as $m) {
            $tarifa = $this->createTarifaMutua($m);
            $tarifa->addCentres($this->centre);
            $this->funcions->addToLog('traspas', $this->traspas, 'Created tarifa-mútua ' . $tarifa, 'INFO', array('co' => $coMut, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $coMut++;
            foreach ($tractamentsTarifes as $t) {

                if ( (isset($t['idTarifa']) && $t['idTarifa'] == $m['idTarifa']) ||
                    isset($t['tarifaReg']) && ($t['tarifaReg'] == $m['dasiclinicReg']) )    // pel dasiclinic
                {
                    $tarifaTractament = $this->createTractamentTarifa($tarifa, $t);
                    $this->funcions->addToLog('traspas', $this->traspas, 'Created tarifa/tractament ' . $tarifa , 'INFO', array('co' => $coTra, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
                    $coTra++;
                }
            }
        }
        $this->em->flush();
        $this->em->clear();

        return $coMut . ' tarifes/mútues i ' . $coTra . ' tractaments tarifes created';
    }

    /**
     * traspàs empreses a eClinic
     * @param $empreses array
     * @return string
     */
    public function serviceEmpreses($empreses)
    {
        /* TODO: taula de relació empresa_pacient per migracions DC */

        $co = 0;
        foreach ($empreses as $p) {
            $empresa = $this->createEmpresa($p);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created empresa ' . $empresa->getDadesEmpresa()->getNom() . ' (' . $empresa->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' empreses created';
    }

    /**
     * traspàs periodes a eClinic
     * @param $periodes array
     * @return string
     */
    public function servicePeriodes($periodes)
    {
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');

        $co = 0;
        foreach ($periodes as $p) {

            // professional fk
            $professional = null;
            if (isset($p['idProfessional'])) {
                $professional = $profRepo->find($p['idProfessional']);
            } elseif (isset($p['professionalReg'])) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $p['professionalReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['professional'])) {
                $professional = $profRepo->findOneBy([
                    'acronim' => substr($p['acronim'], 0, 5),
                    'centre' => $this->centre]);
            }

            if (!$professional) {
                continue;
            }

            $periode = $this->createPeriode($p);
            $periode->setProfessional($professional);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created periodos ' . $periode->getNom() . ' (' . $periode->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' periodes created';
    }

    /**
     * traspàs horaris a eClinic
     * @param $horaris array
     * @return string
     */
    public function serviceHoraris($horaris)
    {
        $periodeRepo = $this->em->getRepository('DatabaseBundle:Periodes');

        $co = 0;
        foreach ($horaris as $p) {
            $horari = $this->createHorari($p);

            // periode fk
            $periode = null;
            if (isset($p['idPeriode'])) {
                $periode = $periodeRepo->find($p['idPeriode']);
            } elseif (isset($p['periodeReg'])) {
                $periode = $periodeRepo->findOneBy([
                    'dasiclinicReg' => $p['periodeReg'],
                    'dasiclinicId' => $this->centre]);
            }

            if ($periode) {
                $horari->setPeriode($periode);
            } else {
                continue;
            }

            $this->funcions->addToLog('traspas', $this->traspas, 'Created horaris  (' . $horari->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' horaris created';
    }

    /**
     * traspàs festius a eClinic
     * @param $registres array
     * @return string
     */
    public function serviceFestius($registres)
    {
        $co = 0;
        foreach ($registres as $p) {
            $festiu = $this->createFestiu($p);
            $this->funcions->addToLog('traspas', $this->traspas, 'Created festiu ' . $festiu->getNom() . ' (' . $festiu->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' festius created';
    }

    /**
     * traspàs periodes absencia a eClinic
     * @param $periodes array
     * @return string
     */
    public function serviceBloquejosAgenda($bloquejos)
    {
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');

        $co = 0;
        foreach ($bloquejos as $p) {

            // professional fk
            $professional = null;
            if (isset($p['idProfessional'])) {
                $professional = $profRepo->find($p['idProfessional']);
            } elseif (isset($p['professionalReg'])) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $p['professionalReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['professional'])) {
                $professional = $profRepo->findOneBy([
                    'acronim' => substr($p['acronim'], 0, 5),
                    'centre' => $this->centre]);
            }

            if (!$professional) {
                continue;
            }

            $bloq = $this->createBloqueigAgenda($p);
            $bloq->setProfessional($professional);

            $this->funcions->addToLog(
                'traspas',
                $this->traspas,
                'Created bloquejos agenda ' . $bloq->getMotiu() . ' (' . $bloq->getId() . ')',
                'INFO',
                array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash)
            );
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' bloquejos agenda created';
    }

    /**
     * traspàs periodes absencia a eClinic
     * @param $periodes array
     * @return string
     */
    public function servicePeriodesAbsencia($periodes)
    {
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');

        $co = 0;
        foreach ($periodes as $p) {

            // professional fk
            $professional = null;
            if (isset($p['idProfessional'])) {
                $professional = $profRepo->find($p['idProfessional']);
            } elseif (isset($p['professionalReg'])) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $p['professionalReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['professional'])) {
                $professional = $profRepo->findOneBy([
                    'acronim' => substr($p['acronim'], 0, 5),
                    'centre' => $this->centre]);
            }

            if (!$professional) {
                continue;
            }

            $periode = $this->createPeriodeAbsencia($p);
            $periode->setProfessional($professional);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created periodos absencia' . $periode->getNom() . ' (' . $periode->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' periodes absencia created';
    }

    /**
     * traspàs professionals a eClinic
     * @param $professionals array
     * @return string
     */
    public function serviceProfessionals($professionals)
    {
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');
        $catRepo = $this->em->getRepository('DatabaseBundle:Categories');
        $empresaRepo = $this->em->getRepository('DatabaseBundle:Empresa');

        $co = 0;
        foreach ($professionals as $p) {
            $professional = $this->createProfessional($p);
            $this->em->persist($professional);

            // especialitat fk
            $especialitat = 0;
            if (isset($p['idEspecialitat'])) {
                $especialitat = $especRepo->find($p['idEspecialitat']);
            } elseif (isset($p['especialitatReg'])) {
                $especialitat = $especRepo->findOneBy([
                    'dasiclinicReg' => $p['especialitatReg'],
                    'idCentre' => $this->centre
                ]);
            } elseif (isset($p['especialitat'])) {
                $especialitat = $especRepo->findOneBy([
                    'nom' => $p['especialitat'],
                    'idCentre' => $this->centre
                ]);
            }

            if ($especialitat) {
                // categoria fk
                $categoria = 0;
                if (isset($p['idCategoria'])) {
                    $categoria = $catRepo->find($p['idCategoria']);
                } elseif (isset($p['categoriaReg'])) {
                    $categoria = $catRepo->findOneBy([
                        'especialitat' => $especialitat->getIdEspecialitat(),
                        'dasiclinicReg' => $p['categoriaReg']
                    ]);
                } elseif (isset($p['categoria'])) {
                    $categoria = $catRepo->findOneBy([
                        'idEspecialitat' => $especialitat->getIdEspecialitat(),
                        'nom' => $p['categoria']
                    ]);
                }
            }

            // Añadimos especialidad y categoria principal
            if ($especialitat) {
                $this->profService->addEspecialitat(
                    $especialitat->getIdEspecialitat(),
                    !$categoria ? 0 : $categoria->getIdCategoria(),
                    $professional,
                    true
                );
            }

            // empresa fk
            $empresa = null;
            if (isset($p['idEmpresa'])) {
                $empresa = $empresaRepo->find($p['idEmpresa']);
            } elseif (isset($p['idEmpresa'])) {
                $empresa = $empresaRepo->findOneBy([
                    'dasiclinicReg' => $p['empresaReg'],
                    'idCentre' => $this->centre]);
            }

            $this->em->persist($professional);
            $this->em->flush();

            // relacions de confiança agenda, visita, cursclinic pel Professional a TOTS els usuaris
            $this->profService->addRelacionsToAllUsers($professional);

            // periodos y horarios
            // si no es passen explícitament, creamos el periodo y horario per tot l'any corrent amb l'interval típic
            if (!isset($p['teHorari']) || $p['teHorari'] == false) {
                $this->profService->creaPeriodoYHorario($professional, 3, $p['interval'], date('Y'). '-01-01');
            }

            $this->funcions->addToLog('traspas', $this->traspas, 'Created professional ' . $professional->getAcronim() . ' (' . $professional->getIdProfessional() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' professionals created';
    }


    /**
     * traspàs cobertura mútues dels professionals a eClinic
     * @param $professionals array
     * @return string
     */
    public function serviceProfessionalsTarifes($mutuesProfessionals)
    {
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');
        $tarifaRepo = $this->em->getRepository('DatabaseBundle:TarifaMutua');

        $co = 0;
        foreach ($mutuesProfessionals as $p) {

            // cerquem la tarifa per FK o camp ccost
            $tarifa = null;
            if (isset($p['idTarifaMutua'])) {
                $tarifa = $tarifaRepo->find($p['idTarifaMutua']);
            } elseif (isset($p['codiMutua'])) {
                $tarifa = $tarifaRepo->findOneBy([
                    'ccoste' => $p['codiMutua'],
                    'centre' => $this->centre]);
            } elseif (isset($p['tarifaReg'])) {
                $tarifa = $tarifaRepo->findOneBy([
                    'dasiclinicReg' => $p['tarifaReg'],
                    'centre' => $this->centre]);
            }

            // professional fk
            $professional = null;
            if (isset($p['idProfessional'])) {
                $professional = $profRepo->find($p['idProfessional']);
            } elseif (isset($p['professional'])) {
                $professional = $profRepo->findOneBy([
                    'acronim' => substr($p['acronim'], 0, 5),
                    'centre' => $this->centre]);
            } elseif (isset($p['professionalReg'])) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $p['tarifaReg'],
                    'centre' => $this->centre]);
            }

            if ($tarifa && $professional) {

                $mutuaProfe = $this->createProfessionalsTarifaMutua($professional, $tarifa);
                $this->em->persist($mutuaProfe);

                $this->funcions->addToLog('traspas', $this->traspas, 'Created mutues - professional ' . $tarifa . ' - ' . $professional->getAcronim() . ' (' . $professional->getIdProfessional() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
                $co++;
            }

        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' mutues-professionals created';
    }


    /**
     * traspàs pacients a eClinic
     * @param $pacients array
     * @return string
     * @throws \Exception
     */
    public function servicePacients($pacients)
    {
        $procedenciaRepo = $this->em->getRepository('DatabaseBundle:Procedencies');
        $tarifaRepo = $this->em->getRepository('DatabaseBundle:TarifaMutua');
        $pacientRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $tagsRepo = $this->em->getRepository('DatabaseBundle:Tags');
        $clientRepo = $this->em->getRepository('DatabaseBundle:Clients');

        $batchSize = 50;
        $co = 0;
        $numhist = 0;

        // si historia auto, agafem l'últim número d'historia si hi ha pacients
        if (!isset($pacients[$co]['historia']) || $pacients[$co]['historia'] === 'auto') {
            $numhist = $pacientRepo->generaHistoria($this->centre);

        }
        foreach ($pacients as $p) {


            $pacient = $this->createPacient($p, $numhist);

            // cerquem la procedència per FK o per camp trapas
            $procedencia = null;
            if (isset($p['idProcedencia'])) {
                $procedencia = $procedenciaRepo->find($p['idProcedencia']);
            } elseif (isset($p['procedenciaReg'])) {
                $procedencia = $procedenciaRepo->findOneBy([
                    'dasiclinicReg' => $p['procedenciaReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['procedencia'])) {
                $procedencia = $procedenciaRepo->findOneBy([
                    'nom' => $p['procedencia'],
                    'centre' => $this->centre]);
            }
            if ($procedencia) {
                $pacient->setProcedencia($procedencia);
            }

            // cerquem l'origen de pacient per FK o per camp trapas
            $tag = null;
            if (!empty($p['idOrigenPac'])) {
                $tag = $tagsRepo->find($p['idOrigenPac']);

            } elseif (isset($p['origenReg'])) {
                $tag = $tagsRepo->findOneBy([
                        'dasiclinicReg' => $p['origenReg'],
                        'centre' => $this->centre]
                );
            }

            // DC: si el idRegistre = 0 no es passa
            if ($tag && $tag->getDasiclinicReg() >0) {
                $p['pacient'] = $pacient;
                $p['tag'] = $tag;

                $this->createPacientsTags($p);
            }

            // cerquem la tarifa per FK o camp ccost
            $tarifa = null;
            if (isset($p['idTarifaMutua'])) {
                $tarifa = $tarifaRepo->find($p['idTarifaMutua']);
            } elseif (isset($p['tarifaReg'])) {
                $tarifa = $tarifaRepo->findOneBy([
                    'dasiclinicReg' => $p['tarifaReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['tarifaNom'])) {
                $tarifa = $tarifaRepo->findOneBy([
                    'observacions' => trim($p['tarifaNom']),
                    'centre' => $this->centre]);
            } elseif (isset($p['codiMutua'])) {
                $tarifa = $tarifaRepo->findOneBy([
                    'observacions' => $p['codiMutua'],
                    'centre' => $this->centre]);
            }

            // si no s'ha trobat i passem paràmetre de crear tarifa/mutua
            if (!$tarifa && isset($p['tarifaNom'])) {
                $mutuaParams = [
                    'nom' => $p['tarifaNom'],
                    'esmutua' => true,
                    'observacions' => $p['tarifaNom']
                ];

                $tarifa = $this->createTarifaMutua($mutuaParams);
                $tarifa->addCentres($this->centre);
                $this->funcions->addToLog('traspas', $this->traspas, 'Created tarifa-mútua ' . $tarifa, 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            }

            if ($tarifa) {
                $pacient->addTarifaMutua($tarifa);
                $pacient->setTarifaPreferida($tarifa);
            }

            $this->em->persist($pacient);

            // si cal compartim el pacient amb un altra centre
            if (isset($p['shareWithCenter'])) {
                $this->sharePacientCentre($pacient, $p['shareWithCenter']);
            }

            // cerquem client/tutor per FK
            $client = null;
            if (isset($p['idClient'])) {
                $client = $clientRepo->find($p['idClient']);
            } elseif (isset($p['clientReg'])) {
                $client = $clientRepo->findOneBy([
                    'dasiclinicReg' => $p['clientReg'],
                    'centre' => $this->centre]);
            }
            if ($client) {
                $this->createClientAtPacient($client, $pacient);
            }


            $this->funcions->addToLog('traspas', $this->traspas, 'Created pacient history ' . $pacient->getHistoria(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
            $numhist++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' pacients created';
    }

    /**
     * traspàs clients a eClinic
     * @param $pacients array
     * @return string
     * @throws \Exception
     */
    public function serviceClients($clients)
    {
        $batchSize = 20;
        $co = 0;

        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');

        foreach ($clients as $p) {

            $client = $this->createClient($p, $co);

            $this->em->persist($client);

            // cerquem pacient per FK
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);

            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);

            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);
            }

            if ($pacient) {
                $this->createClientAtPacient($client, $pacient);
            }

            $this->funcions->addToLog(
                'traspas', $this->traspas,
                'Created client ' . $client->getId(),
                'INFO',
                [
                    'co' => $co,
                    'client_reg / id' => isset($p['clientReg']) ? $p['clientReg'] : $client->getId(),
                    'centre' => $this->centre->getIdCentre(),
                    'env' => $this->env,
                    'hash' => $this->hash
                ]
            );

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' clients created';
    }


    /**
     * traspàs antecedentsPacient a eClinic
     * @param $antecedents array
     * @return string
     */
    public function serviceAntecedentsPacient($antecedents)
    {
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $antRepo = $this->em->getRepository('DatabaseBundle:Antecedents');

        $batchSize = 20;
        $co = 0;
        $ordre = [];

        foreach ($antecedents as $p) {

            if (!isset($p['pacientReg']) && isset($p['historia'])) {
                $p['pacientReg'] = $p['historia'];
            }

            // en cas de no passar l'odre es crearem partint dels grups
            // veure traspàs de SINCONSUMIR
            if (!isset($p['ordre'])) {

                // contador ordre per cada pacient i grup d'antecedents
                if (isset($ordre[$p['pacientReg']][$p['grupAntecedent']])) {
                    $ordre[$p['pacientReg']][$p['grupAntecedent']]++;

                } else {
                    // si es tracta d'un subgrup i és el primer cop, es crea abans el subgrup
                    if (isset($p['subgrups'][$p['grupAntecedent']])) {
                        $subgrup['nom'] = $p['subgrups'][$p['grupAntecedent']];
                        $antecedent = $this->createAntecedentPacient($subgrup);
                        $antecedent->setOrdre($p['grupAntecedent']);

                        // pacient fk
                        $pacient = null;
                        if (isset($p['idPacient'])) {
                            $pacient = $pacRepo->find($p['idPacient']);
                        } elseif (isset($p['historia'])) {
                            $pacient = $pacRepo->findOneBy([
                                'historia' => $p['historia'],
                                'centre' => $this->centre]);
                        } elseif (isset($p['pacientReg'])) {
                            $pacient = $pacRepo->findOneBy([
                                'dasiclinicReg' => $p['pacientReg'],
                                'centre' => $this->centre]);
                        }
                        if ($pacient) {
                            $antecedent->setPacient($pacient);
                        } else {  // si no troba el pacient, salta
                            continue;
                        }

                        $this->em->persist($antecedent);
                    }

                    $ordre[$p['pacientReg']][$p['grupAntecedent']] = $p['grupAntecedent'] + 1;
                }
            }

            // recupera un antecedent existent o crea'l
            if (isset($p['antecedentReg'])) {
                $ant = $antRepo->findOneBy([
                    'dasiclinicReg' => $p['antecedentReg'],
                    'centre' => $this->centre]);
                if ($ant) {
                    $antpac['nom'] = $ant->getNom();
                    $antecedent = $this->createAntecedentPacient($antpac);
                }
            } else {
                // es crea
                $antecedent = $this->createAntecedentPacient($p);
            }

            if (!isset($p['ordre'])) {
                $antecedent->setOrdre($ordre[$p['pacientReg']][$p['grupAntecedent']]);
            }

            // pacient fk
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);
            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);
            }
            if ($pacient) {
                $antecedent->setPacient($pacient);
            } else {  // si no troba el pacient, salta
                continue;
            }

            $this->em->persist($antecedent);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created antecedent pacient history ' . $antecedent->getId(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' antecedents pacient created';
    }


    /**
     * traspàs cites i visites a eClinic
     *
     * @param $cites array, bool $creaVisites, bool $checkCitaExist
     * @return string
     */
    public function serviceCites($cites, $creaVisites = false, $checkCitaExist = false, $allowCitaSensePacient = false)
    {
        ini_set('memory_limit', 0);

        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');
        $tipusVisitaRepo = $this->em->getRepository('DatabaseBundle:TipusVisita');
        $tarifaRepo = $this->em->getRepository('DatabaseBundle:TarifaMutua');
        $tracRepo = $this->em->getRepository('DatabaseBundle:Tractaments');
        $episodiRepo = $this->em->getRepository('DatabaseBundle:Episodis');
        $boxRepo = $this->em->getRepository('DatabaseBundle:Boxes');

        $batchSize = 20;
        $co = 0;
        foreach ($cites as $p) {

            // si ja existeix la cita no fem res
            if ($checkCitaExist) {
                $query = $this->em->createQuery("SELECT c
                                    FROM DatabaseBundle:Cites c
                                    JOIN c.pacient p
                                    WHERE p.centre = :centre
                                    AND p.historia = :historia
                                    AND c.data = :data
                    ")
                    ->setParameters(array(
                        'centre' => $this->centre,
                        'historia' =>  $p['historia'],
                        'data'   => $p['data']
                    ));

                $result = $query->getResult();
                if (count($result)>0) {
                    $this->funcions->addToLog(
                        'traspas',
                        $this->traspas,
                        'Skipped cita ' . $p['data']->format('Y-m-d H:i:s') . ' (historia ' . $p['historia'] . ')',
                        'INFO',
                        array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash)
                    );
                    continue;
                }
            }

            $cita = $this->createCita($p);

            // pacient fk
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);
            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => (int)$p['historia'],
                    'centre' => $this->centre]);
            }

            if ($pacient) {
                $cita->setPacient($pacient);
            }

            if ($allowCitaSensePacient == false) {
                // si no troba el pacient, salta la cita
                if (!$pacient) {
                    $this->funcions->addToLog(
                        'traspas',

                    $this->traspas,
                        'Pacient not found. Skipped cita ' . $p['data']->format('Y-m-d H:i:s')
                        . ' (historia ' . (isset($p['historia']) ? $p['historia'] : '') . ')',
                        'INFO',
                        array('co' => $co,
                            'centre' => $this->centre->getIdCentre(),
                            'env' => $this->env,
                            'hash' => $this->hash
                        )
                    );
                    continue;
                }
            }

            // especialitat fk
            $especialitat = null;
            if (isset($p['idEspecialitat'])) {
                $especialitat = $especRepo->find($p['idEspecialitat']);
            } elseif (isset($p['especialitatReg'])) {
                $especialitat = $especRepo->findOneBy([
                    'dasiclinicReg' => $p['especialitatReg'],
                    'idCentre' => $this->centre]);
            } elseif (isset($p['especialitat'])) {
                $especialitat = $especRepo->findOneBy([
                    'nom' => $p['especialitat'],
                    'idCentre' => $this->centre]);
            }

            // professional fk
            $professional = null;
            if (isset($p['idProfessional'])) {
                $professional = $profRepo->find($p['idProfessional']);
            } elseif (isset($p['professionalReg'])) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $p['professionalReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['acronim'])) {
                $professional = $profRepo->findOneBy([
                    'acronim' => substr($p['acronim'], 0, 5),
                    'centre' => $this->centre]);
            } elseif (isset($p['professional_nom'])) {
                $professional = $profRepo->getProfessionalByNom(
                    $this->centre->getIdCentre(),
                    $p['professional_nom'],
                    $p['professional_cognom1'],
                    $p['professional_cognom2']
                );
            }

            // si no hem trobat el professional, posem el per defecte
            if (!$professional && isset($p['defaultProfessional'])) {
                $professional = $profRepo->find($p['defaultProfessional']);
            }

            if ($professional) {
                $cita->setProfessional($professional);

                // si no s'ha definit especialitat posem la principal del professional
                if (!$especialitat) {
                    $especialitat = $professional->getEspecialitatPrincipal();
                }
            }

            if ($especialitat) {
                $cita->setEspecialitat($especialitat);
            } /*else {
                exit('falta especialtiat reg' . $p['idEspecialitat']);
            }*/

            // tipus Visita fk
            $tipusVisita = null;
            if (isset($p['idTipusVisita'])) {
                $tipusVisita = $tipusVisitaRepo->find($p['idTipusVisita']);

            } elseif (isset($p['tipusVisitaReg'])) {
                $tipusVisita = $tipusVisitaRepo->findOneBy([
                'dasiclinicReg' => $p['tipusVisitaReg'],
                'centre' => $this->centre]);

            } elseif (isset($p['tipusVisita'])) {
                $tipusVisita = $tipusVisitaRepo->findOneBy([
                    'nom' => $p['tipusVisita'],
                    'centre' => $this->centre]);
            }
            if ($tipusVisita) {
                $cita->setTipusVisita($tipusVisita);
            }


            // box fk
            $box = null;
            if (isset($p['idBox'])) {
                $box = $boxRepo->find($p['idBox']);
            } elseif (isset($p['boxReg'])) {
                $box = $boxRepo->findOneBy([
                    'dasiclinicReg' => $p['boxReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['box'])) {
                $box = $boxRepo->findOneBy([
                    'codi' => $p['box'],
                    'centre' => $this->centre]);
            }
            if ($box) {
                $cita->setBox($box);
            }

            // tarifa fk
            $tarifaMutua = null;
            if (isset($p['idTarifaMutua'])) {
                $tarifaMutua = $tarifaRepo->find($p['idTarifaMutua']);
            } elseif (isset($p['tarifaReg'])) {
                $tarifaMutua = $tarifaRepo->findOneBy([
                    'dasiclinicReg' => $p['tarifaReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['codiMutua'])) {
                $tarifaMutua = $tarifaRepo->findOneBy([
                    'ccoste' => $p['codiMutua'],
                    'centre' => $this->centre]);
            }
            if ($pacient) {
                $cita->setTarifaCita($tarifaMutua);
            }

            // tractaments fk
            $tractament = null;
            if (isset($p['idTractament'])) {
                $tractament = $tracRepo->find($p['idTractament']);
            } elseif (isset($p['tractamentReg'])) {
                $tractament = $tracRepo->findOneBy([
                    'dasiclinicReg' => $p['tractamentReg'],
                    'idCentre' => $this->centre]);
            } elseif (isset($p['tractament'])) {
                $tractament = $tracRepo->findOneBy([
                    'codi' => $p['tractament'],
                    'idCentre' => $this->centre]);
            }
            if ($tractament) {
                $this->createCitaTractament($p, $cita, $tractament);
            }

            $this->em->persist($cita);

            // fem el set de datCrea després del persist per evitar el trigger default (prepersist)
            if (isset($p['dataCrea'])) {
                $cita->setDataCrea($p['dataCrea']);
            } else {
                $cita->setDataCrea($p['data']);
            }
            if (isset($p['dataPendent'])) {
                $cita->setDataPendent($p['dataPendent']);
            } else {
                $cita->setDataPendent($p['data']);
            }

            // si el pacient s'ha presentat crearem també un episodi i visita
            if ($creaVisites && $p['estatCita'] == 3 && $pacient) { // visitat

                // busquem episodi obert per pacient i especialitat
                if ($creaVisites == 'onlyOneEpisode') {
                    // un sol episodi, carreguem qualsevol episodi obert al pacient: multi-professional i multi-especialitat
                    $episodi = $episodiRepo->getEpisodiObertEspecialitat($pacient->getId());
                } else {
                    $episodi = $episodiRepo->getEpisodiObertEspecialitat($pacient->getId(), null, $especialitat->getIdEspecialitat());
                }
                // si no es troba l'episodi, n'obrim un
                if (!$episodi) {

                    $p['numEpisodi'] = $this->episService
                        ->generaNumeroEpisodi($this->centre, $pacient->getId());
                    $episodi = $this->createEpisodiFromCita($p, $cita);
                    $episodi->setPacient($pacient);
                    $episodi->setProfessional($professional);
                    $episodi->setEspecialitat($especialitat);

                    $this->em->persist($episodi);
                }

                $visita = $this->createVisitaFromCita($p, $cita, $episodi);
                $this->em->persist($visita);
            }

            if ($allowCitaSensePacient) {
                $historia = "sense_pacient";
            } else {
                $historia = isset($p['historia']) ? $p['historia'] : ($cita->getPacient() ? $cita->getPacient()->getHistoria() : exit('No es troba el pacient reg. ' . $p['dasiclinicReg']));
            }

            $this->funcions->addToLog('traspas', $this->traspas, 'Created cites ' . $p['data']->format('Y-m-d H:i:s') . ' (historia ' . $historia . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' cites created';

    }

    /**
     * traspàs episodis
     *
     * @param $episodis array
     * @return string
     */
    public function serviceEpisodis(array $episodis)
    {
        ini_set('memory_limit', 0);

        $episodiRepo = $this->em->getRepository('DatabaseBundle:Episodis');
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');

        $batchSize = 1000;
        $co = 0;
        foreach ($episodis as $p) {

            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);

            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);

            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);
            }
            if (!$pacient) {
                continue;
//                exit('no existeix el paciente reg ' .$p['pacientReg']);
            }

            $professional = null;
            if (isset($p['idProfessional'])) {
                $professional = $profRepo->find($p['idProfessional']);

            } elseif (isset($p['professionalReg'])) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $p['professionalReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['acronim'])) {
                $professional = $profRepo->findOneBy([
                    'acronim' => substr($p['acronim'], 0, 5),
                    'centre' => $this->centre]);
            }

            $especialitat = null;
            if (isset($p['idEspecialitat'])) {
                $especialitat = $especRepo->find($p['idEspecialitat']);
            } elseif (isset($p['especialitatReg'])) {
                $especialitat = $especRepo->findOneBy([
                    'dasiclinicReg' => $p['especialitatReg'],
                    'idCentre' => $this->centre
                ]);
            } elseif (isset($p['especialitat'])) {
                $especialitat = $especRepo->findOneBy([
                    'nom' => $p['especialitat'],
                    'idCentre' => $this->centre
                ]);
            }

            if (!$especialitat && $professional) {
                // si tenim professional l'agafem del professional
                $especialitat = $professional->getEspecialitatPrincipal();
            }

            // si no es passa, busquem el num. d'episodi (no té en compte l'especialitat)
            if (!isset($p['numEpisodi'])) {
                $p['numEpisodi'] = $this->episService
                    ->generaNumeroEpisodi($this->centre->getIdCentre(), $pacient->getId());
            }

            // crea l'episodi i assigna FK
            $episodi = $this->createEpisodi($p);
            $episodi->setPacient($pacient);
            $episodi->setProfessional($professional);
            $episodi->setEspecialitat($especialitat);

            $this->em->persist($episodi);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created episodis ' . $p['data']->format('Y-m-d H:i:s') . ' (historia ' . $pacient->getHistoria() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }

        $this->em->flush();
        $this->em->clear();

        return $co . ' episodis created';

    }

    /**
     * traspàs visites (genera episodis)
     *
     * @param $visites array, bool
     * @return string
     */
    public function serviceVisites($visites, $iniparams)
    {
        ini_set('memory_limit', 0);

        $forceCreaCita = isset($iniparams['forceCreaCita']) ? $iniparams['forceCreaCita'] : false;
        $onlyOneEpisode = isset($iniparams['onlyOneEpisode']) ? $iniparams['onlyOneEpisode'] : false;
        $onlyEpisodis = isset($iniparams['onlyEpisodis']) ? $iniparams['onlyEpisodis'] : false;
        $receptes = isset($iniparams['receptes']) ? $iniparams['receptes'] : false;
        $teMedicament = isset($iniparams['teMedicament']) ? $iniparams['teMedicament'] : false;

        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');
        $episodiRepo = $this->em->getRepository('DatabaseBundle:Episodis');
        $citaRepo = $this->em->getRepository('DatabaseBundle:Cites');
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');
        $tipusVisitaRepo = $this->em->getRepository('DatabaseBundle:TipusVisita');
        $tracRepo = $this->em->getRepository('DatabaseBundle:Tractaments');
        $boxRepo = $this->em->getRepository('DatabaseBundle:Boxes');
        $receRepo = $this->em->getRepository('DatabaseBundle:Receptes');

        $batchSize = 2000;
        $co = 0;
        foreach ($visites as $p) {

            // si passem episodiReg el recuperem
            if (isset($p['episodiReg'])) {
                 $episodi = $episodiRepo->findOneBy([
                    'dasiclinicReg' => $p['episodiReg'],
                    'traspas' => $this->hash
                ]);
            } else {
                $episodi = null;
            }

            // pacient fk
            if ($episodi) {
                $pacient = $episodi->getPacient();

            } else {
                $pacient = null;
                if (isset($p['idPacient'])) {
                    $pacient = $pacRepo->find($p['idPacient']);

                } elseif (isset($p['pacientReg'])) {
                    $pacient = $pacRepo->findOneBy([
                        'dasiclinicReg' => $p['pacientReg'],
                        'centre' => $this->centre]);

                } elseif (isset($p['historia'])) {
                    $pacient = $pacRepo->findOneBy([
                        'historia' => $p['historia'],
                        'centre' => $this->centre]);
                }
                if (!$pacient) {
                    continue;
//                exit('no existeix el paciente reg ' .$p['pacientReg']);
                }
            }

            // professional fk
            if ($episodi) {
                $professional = $episodi->getProfessional();

            } else {
                $professional = null;
                if (isset($p['idProfessional'])) {
                    $professional = $profRepo->find($p['idProfessional']);

                } elseif (isset($p['professionalReg'])) {
                    $professional = $profRepo->findOneBy([
                        'dasiclinicReg' => $p['professionalReg'],
                        'centre' => $this->centre]);
                } elseif (isset($p['acronim'])) {
                    $professional = $profRepo->findOneBy([
                        'acronim' => substr($p['acronim'], 0, 5),
                        'centre' => $this->centre]);
                }
            }

            // especialitat fk
            if ($episodi) {
                $especialitat = $episodi->getEspecialitat();

            } else {
                $especialitat = null;
                if (isset($p['idEspecialitat'])) {
                    $especialitat = $especRepo->find($p['idEspecialitat']);
                } elseif (isset($p['especialitatReg'])) {
                    $especialitat = $especRepo->findOneBy([
                        'dasiclinicReg' => $p['especialitatReg'],
                        'idCentre' => $this->centre
                    ]);
                } elseif (isset($p['especialitat'])) {
                    $especialitat = $especRepo->findOneBy([
                        'nom' => $p['especialitat'],
                        'idCentre' => $this->centre
                    ]);
                } elseif ($professional) {
                    // si tenim professional l'agafem del professional
                    $especialitat = $professional->getEspecialitatPrincipal();
                }
            }

            // mirem si la visita té cita o visita sense cita
            $cita = null;
            if (!isset($p['noCita'])) {

                // si no forcem crear cita, la busquem si passem in id
                if (!$forceCreaCita) {
                    if (isset($p['idCita'])) {
                        $cita = $citaRepo->find($p['idCita']);
                    } elseif (isset($p['citaReg'])) {
                        $cita = $citaRepo->findOneBy([
                            'dasiclinicReg' => $p['citaReg'],
                            'pacient' => $pacient->getId()
                        ]);
                    } elseif (isset($p['dataVisita'])) {  // recuperem la cita amb el pacient i data, si no trobem la cita dia absn/després, es crea

                        $dataCita = clone $p['dataVisita'];
                        $cita = $citaRepo->findOneBy([
                            'data' => $dataCita,
                            'pacient' => $pacient->getId()
                        ]);
                        /*if (!$cita) {
                            $cita = $citaRepo->findOneBy([
                                'data' => $dataCita->modify('+1 DAY'),
                                'pacient' => $pacient->getId()
                            ]);
                        }
                        if (!$cita) {
                            $cita = $citaRepo->findOneBy([
                                'data' => $dataCita->modify('-2 DAY'),
                                'pacient' => $pacient->getId()
                            ]);
                        }*/
                    }
                }

                // es crea la cita amb les dades de la visita si no la trobem
                if (!$cita && $forceCreaCita) {

                    $pCita = [];
                    $pCita['data'] = $p['dataVisita'];
                    $pCita['hora'] = $p['horaVisita'];
                    $pCita['temps'] = $p['temps'];
                    $pCita['estatCita'] = 3; // visitat
                    $pCita['dataVisitat'] = $p['dataVisita'];

                    if (isset($p['observacions'])) {
                        $pCita['observacions'] = $p['observacions'];
                    }
                    $cita = $this->createCita($pCita);

                    $cita->setPacient($pacient);

                    // TODO: passa especialitat per defecte
                    /*                    if ($p['idEspecialitat'] == 0) {
                                            $especialitat = $especRepo->find(9735);
                                        }*/

                    $cita->setEspecialitat($especialitat);

                    // busquem el professional d'altres cites del pacient
                    if (!$professional) {

                        $altresCites = $citaRepo->findOneBy(
                            ['pacient' => $pacient],
                            ['id' => 'DESC']
                        );

                        if ($altresCites) {
                            $professional = $altresCites->getProfessional();
                        } else {
                            $professional = $profRepo->find($p['profDefecte']);
                        }
                    }

                    $cita->setProfessional($professional);

                    // tipus Visita fk
                    $tipusVisita = null;
                    if (isset($p['idTipusVisita'])) {
                        $tipusVisita = $tipusVisitaRepo->find($p['idTipusVisita']);

                    } elseif (isset($p['tipusVisitaReg'])) {
                        $tipusVisita = $tipusVisitaRepo->findOneBy([
                            'dasiclinicReg' => $p['tipusVisitaReg'],
                            'centre' => $this->centre]);

                    } elseif (isset($p['tipusVisita'])) {
                        $tipusVisita = $tipusVisitaRepo->findOneBy([
                            'nom' => $p['tipusVisita'],
                            'centre' => $this->centre]);
                    }
                    if ($tipusVisita) {
                        $cita->setTipusVisita($tipusVisita);
                    }

                    $this->funcions->addToLog('traspas', $this->traspas, 'No existeix cita, es crea cita al ' . $pCita['data']->format('Y-m-d') . ' del pacient id ' . $pacient->getId(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
                    $this->em->persist($cita);
                }

                // tipus Visita fk
                $tipusVisita = null;
                if (isset($p['idTipusVisita'])) {
                    $tipusVisita = $tipusVisitaRepo->find($p['idTipusVisita']);

                } elseif (isset($p['tipusVisita'])) {
                    $tipusVisita = $tipusVisitaRepo->findOneBy([
                        'nom' => $p['tipusVisita'],
                        'centre' => $this->centre]);
                }
                if ($tipusVisita) {
                    $cita->setTipusVisita($tipusVisita);
                }

                if (!$cita) {
                    $this->funcions->addToLog('traspas', $this->traspas, 'err. al crear visita: No existeix la cita ' . $p['data']->format('Y-m-d') . ' del pacient id ' . $pacient->getId(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
                    continue;
                }

            } else {
                $p['noCita'] = true;
            }

            // març'20 - si no es troba cita, visita sense cita
            if (!$cita && !isset($p['noCita'])) {
//                exit('no existeix la cita reg ' . $p['data']->format('Y-m-d') . ' del pacient id ' . $pacient->getId());
                $p['noCita'] = true;
            }

            // agafem l'especialitat de la cita
            if (!$especialitat && $cita) {
                $especialitat = $cita->getEspecialitat();
            }

            // agafem l'especialitat del professional
            if (!$especialitat && $professional) {
                $especialitat = $professional->getEspecialitatPrincipal();
            }

            if (!$especialitat) {
                exit("no s'ha trobat una especialitat pel professional" . $p['professionalReg'] . " i la visita " . $p['data']->format('Y-m-d') . " del pacient id " . $pacient->getId());
            }

            // agafem el professional de la cita
            if (!$professional && $cita) {
                $professional = $cita->getProfessional();
            }

            if (!$professional) {
                continue;
                exit('no existeix el professional reg ' . $p['professionalReg'] . 'a la cita del ' . $p['data']->format('Y-m-d') . ' del pacient id ' . $pacient->getId());
            }

            if (!$episodi) {

                // recuperem o creem un episodi de la mateixa especialitat
                // o l'últim episodi obert (episodi multiespecialitat)
                // si estem generant només episodis, no recuperem l'anterior
                if (!$onlyEpisodis) {
                    $episodi = $this->getEpisodi($pacient->getId(), $onlyOneEpisode ? null : $especialitat->getIdEspecialitat(), $p);
                }

                if (!$episodi) {
                    // crea un nou episodi

                    // si no es passa, busquem el num. d'episodi (no té en compte l'especialitat)
                    if (!isset($p['numEpisodi'])) {
                        $p['numEpisodi'] = $this->episService
                            ->generaNumeroEpisodi($this->centre->getIdCentre(), $pacient->getId());
                    }

                    $episodi = $this->createEpisodi($p);
                    $episodi->setPacient($pacient);
                    $episodi->setProfessional($professional);
                    $episodi->setEspecialitat($especialitat);

                    $this->em->persist($episodi);
                    $this->em->flush(); // aquest flush és necessari, sinó et repeteix episodis
                }
            }

            // si no s'ha definit data de visita agafem la del episodi
            if (!isset($p['dataVisita'])) {
                $p['dataVisita'] = $p['data'];
            }

            // recuperem  una visita existent pel pacient i data
            $dataVisita = isset($p['dataVisita']) ? $p['dataVisita'] : $p['data'];

            $visita = $visitaRepo->findOneBy([
                'data' => $dataVisita,
                'pacient' => $pacient->getId(),
                'professional' => $professional->getId()
            ]);

            // si es troba visita actualitzem el contingut afegint el nou contingut
            if ($visita) {
//                dump('asd');exit;
                $this->updateVisita($visita, $p, true);

            } else {

                // si no s'ha definit l'hora visita mirem d'agafar-la de la cita
                if (!isset($p['horaVisita'])) {
                    if ($cita && $cita->getDataVisitat()) {
                        $horaVisita = date('H:i', $cita->getDataVisitat()->getTimeStamp());
                        $p['horaVisita'] = $this->h2m($horaVisita);
                    } else {
                        // posem per defecte les 9 del matí
                        $p['horaVisita'] = 540;
                    }
                }

                // si no s'ha definit temps de visita agafem el de la cita
                if (!isset($p['temps'])) {
                    if ($cita) {
                        $p['temps'] = $cita->getTemps();
                    } else {
                        // sinó posem per defecte 30 min
                        $p['temps'] = 30;
                    }
                }

                // sinó es troba visita es crea
                $visita = $this->createVisita($p);
                $visita->setPacient($pacient);

                // si passem professional diferent que el de l'episodi
                if (isset($p['professionalReg'])) {
                    if ($professional->getDasiclinicReg() !== $p['professionalReg']) {
                        $nouprof = $profRepo->findOneBy([
                            'dasiclinicReg' => $p['professionalReg'],
                            'centre' => $this->centre]);
                        if ($nouprof) {
                            $professional = $nouprof;
                        }
                    }
                }

                $visita->setProfessional($professional);
                $visita->setEpisodi($episodi);

                // posa especialitat del professional
                $novaesp = $professional->getEspecialitatPrincipal();
                if ($novaesp) {
                    $especialitat = $novaesp;
                }
                $visita->setEspecialitat($especialitat);
            }

            if ($cita) {
                $visita->setCita($cita);
            }

            // comprovem si és primera visita
            $teSuccessiva = $visitaRepo
                ->hasSuccessivaDia(
                    $episodi->getId(),
                    $pacient->getId()
                );

            if ($teSuccessiva) {
                $visita->setSuccessiva(1);
            } else {
                $visita->setSuccessiva(0);
            }

            // si passem camp observacionsCita les guardem a la cita
            if (isset($p['observacionsCita']) && !isset($p['noCita'])) {
                $cita->setObservacions($p['observacionsCita']);
            }

            // box fk,  sobreescribim el de la cita
            $box = null;
            if (isset($p['idBox'])) {
                $box = $boxRepo->find($p['idBox']);
            } elseif (isset($p['boxReg'])) {
                $box = $boxRepo->findOneBy([
                    'dasiclinicReg' => $p['boxReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['box'])) {
                $box = $boxRepo->findOneBy([
                    'codi' => $p['box'],
                    'centre' => $this->centre]);
            }
            if ($box && !isset($p['noCita'])) {
                $cita->setBox($box);
            }

            $this->em->persist($visita);

            // ----------------- si és recepta es crea ---------------------

            // recuperem la recepta que conté la visita, si no en té es crea
            if ($receptes) {
                $recepta = $receRepo->findOneByVisita($visita);
                if (!$recepta) {
                    $recepta = $this->createRecepta($p);
                    $recepta->setVisita($visita);
                    $this->em->persist($recepta);
                    $this->em->flush();
                }

                // es crea el medicament si es passen medicaments
                if ($teMedicament) {
                    $medicament = $this->createMedicament($p);
                    $this->em->persist($medicament);
                }
                //  i s'afegeix a la recepta
                $receptaMedicament = $this->createReceptaMedicament($p);
                $receptaMedicament->setRecepta($recepta);

                if ($teMedicament) {
                    $receptaMedicament->setMedicament($medicament);
                }
                $this->em->persist($receptaMedicament);

//                $this->em->flush();

                $this->funcions->addToLog('traspas', $this->traspas, 'Created receptes a visita ' . $visita->getData()->format('Y-m-d') . ' (historia ' . $pacient->getHistoria() . ')', 'INFO', array('co' => $co, 'pac_reg' => $pacient->getDasiclinicReg(), 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
            }

            // ----------- fi de receptes --------------------

            $historia = isset($p['historia']) ? $p['historia'] : ($visita->getPacient() ? $visita->getPacient()->getHistoria() : exit('No es troba el pacient reg. ' . $p['dasiclinicReg']));
            $this->funcions->addToLog(
                'traspas',
                $this->traspas,
                'Created visites ' . $visita->getData()->format('Y-m-d H:i:s') . ' (historia ' . $historia . ')',
                'INFO', array('co' => $co, 'pac_reg' => isset($p['pacientReg']) ? $p['pacientReg'] : $historia, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash)
            );

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
//                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' visites created';

    }


    /**
     * update visites (genera episodis)
     *
     * @param $visites array, bool
     * @return string
     */
    public function updateVisites($visites, $onlyOneEpisode = false, $onlyEpisodis = false)
    {
        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');

        $batchSize = 20;
        $co = 0;

        foreach ($visites as $p) {

            // visita fk
            $visita = null;
            if (isset($p['idVisita'])) {
                $visita = $visitaRepo->find($p['idVisita']);
            } elseif (isset($p['dasiclinicReg'])) {
                $visita = $visitaRepo->findOneBy([
                    'dasiclinicReg' => $p['dasiclinicReg'],
                    'traspas' => $this->hash
                ]);
            }

            // si es troba visita actualitzem el contingut afegint el nou contingut
            if ($visita) {
                $this->updateVisita($visita, $p, true);
            } else {
                continue;
            }

            $this->funcions->addToLog(
                'traspas',
                $this->traspas,
                'Updated visites id ' . $visita->getId(),
                'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash)
            );

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' updated';

    }


    /**
     * assigna tractaments a les visites (taula visitesTractaments)
     * (prèviament cal haver creat càrrecs si passem la FK)
     *
     * @param $visitesTractaments
     * @return string
     */
    public function serviceVisitesTractaments($visitesTractaments)
    {
        $visitesTractamentsRepo = $this->em->getRepository('DatabaseBundle:VisitesTractaments');
        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');
        $tracRepo = $this->em->getRepository('DatabaseBundle:Tractaments');
        $bonusRepo = $this->em->getRepository('DatabaseBundle:Bonus');
        $tarifaRepo = $this->em->getRepository('DatabaseBundle:TarifaMutua');
        $carrecRepo = $this->em->getRepository('DatabaseBundle:Carrec');

        $batchSize = 20;
        $co = 0;
        foreach ($visitesTractaments as $p) {

            // comprovem si ja s'ha creat i saltem
            $visitaTractament = $visitesTractamentsRepo->findOneBy([
                'dasiclinicReg' => $p['dasiclinicReg'],
                'traspas' => $this->hash]
            );

            $new = false;
            if (!$visitaTractament) {

                $new = true;

                // visita fk
                $visita = null;
                if (isset($p['idVisita'])) {
                    $visita = $visitaRepo->find($p['idVisita']);
                } elseif (isset($p['visitaReg'])) {
                    $visita = $visitaRepo->findOneBy([
                        'dasiclinicReg' => $p['visitaReg'],
                        'traspas' => $this->hash]);
                }
/*                if (!$visita) { // es permeten visitesTractaments sense visita
                    continue;
                }*/

                // tractaments fk
                $tractament = null;
                if (isset($p['idTractament'])) {
                    $tractament = $tracRepo->find($p['idTractament']);
                } elseif (isset($p['tractamentReg'])) {
                    $tractament = $tracRepo->findOneBy([
                        'dasiclinicReg' => $p['tractamentReg'],
                        'idCentre' => $this->centre]);
                } elseif (isset($p['tractament'])) {
                    $tractament = $tracRepo->findOneBy([
                        'codi' => $p['tractament'],
                        'idCentre' => $this->centre]);
                }
/*                if (!$tractament) {  // es permeten visitesTractaments sense tractament
                    continue;
                }*/

                $visitaTractament = $this->createVisitaTractament($p, $visita, $tractament);
                $this->em->persist($visitaTractament);
            }

            // bonus fk
            $bonus = null;
            if (isset($p['idBonus'])) {
                $bonus = $bonusRepo->find($p['idBonus']);
            } elseif (isset($p['bonusReg'])) {
                $bonus = $bonusRepo->findOneBy([
                    'dasiclinicReg' => $p['bonusReg'],
                    'traspas' => $this->hash]);
            }

            if ($bonus) {
                $visitaTractament->setBonus($bonus);
            }

            // tarifa fk
            $tarifaMutua = null;
            if (isset($p['idTarifaMutua'])) {
                $tarifaMutua = $tarifaRepo->find($p['idTarifaMutua']);
            } elseif (isset($p['tarifaReg'])) {
                $tarifaMutua = $tarifaRepo->findOneBy([
                    'dasiclinicReg' => $p['tarifaReg'],
                    'centre' => $this->centre]);
            }

            if ($tarifaMutua) {
                $visitaTractament->setTarifaMutua($tarifaMutua);
            }

            // carrec fk
            $carrec = null;
            if (isset($p['idCarrec'])) {
                $carrec = $carrecRepo->find($p['idCarrec']);
            } elseif (isset($p['carrecReg'])) {
                $carrec = $carrecRepo->findOneBy([
                    'dasiclinicReg' => $p['carrecReg'],
                    'centre' => $this->centre]);
            }

            // afegim al càrrec la visitaTractament
            if ($carrec) {
                if (!$visitaTractament->hasCarrec($carrec)) {
                    $visitaTractament->addCarrec($carrec);
                }
            }

            if ($new) {
                $this->funcions->addToLog(
                    'traspas',
                    $this->traspas,
                    'Created visitesTractaments ', // $visita->getData()->format('Y-m-d H:i:s'),
                    'INFO', array(
                        'co' => $co,
                        'visita_reg / carrec_reg' => isset($p['visitaReg']) ? $p['visitaReg'] : ($vista ? $visita->getId() : $p['carrecReg']),
                        'tractament_reg / id' => isset($p['tractamentReg']) ? $p['tractamentReg'] : ($tractament ? $tractament->getIdTractament() : null),
                        'centre' => $this->centre->getIdCentre(),
                        'env' => $this->env, 'hash' => $this->hash)
                );
            }
            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' visitesTractaments created';
    }

    /**
     * crea càrrecs
     *
     * @param $carrecs
     * @return string
     */
    public function serviceCarrecs($carrecs)
    {
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $visitaTractamentRepo = $this->em->getRepository('DatabaseBundle:VisitesTractaments');
        $tarifaRepo = $this->em->getRepository('DatabaseBundle:TarifaMutua');
        $empresaRepo = $this->em->getRepository('DatabaseBundle:Empresa');
        $clientRepo = $this->em->getRepository('DatabaseBundle:Clients');
        $grupTracRepo = $this->em->getRepository('DatabaseBundle:GrupTractaments');
        $formaPagamentRepo = $this->em->getRepository('DatabaseBundle:FormaPagamentClient');

        $batchSize = 20;
        $co = 0;
        foreach ($carrecs as $p) {

            $carrec = $this->createCarrec($p);

            // pacient fk
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);
            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);
            }
            if ($pacient) {
                $carrec->setPacient($pacient);
            } else {
                continue;   // no podem crear càrrecs que no tinguin pacient
            }

            // professional fk
            $professional = null;
            if (isset($p['idProfessional'])) {
                $professional = $profRepo->find($p['idProfessional']);
            } elseif (isset($p['professionalReg'])) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $p['professionalReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['acronim'])) {
                $professional = $profRepo->findOneBy([
                    'acronim' => substr($p['acronim'], 0, 5),
                    'centre' => $this->centre]);
            }

            if ($professional) {
                $carrec->setProfessional($professional);
            }

            // tarifa fk
            $tarifaMutua = null;
            if (isset($p['idTarifaMutua'])) {
                $tarifaMutua = $tarifaRepo->find($p['idTarifaMutua']);
            } elseif (isset($p['tarifaReg'])) {
                $tarifaMutua = $tarifaRepo->findOneBy([
                    'dasiclinicReg' => $p['tarifaReg'],
                    'centre' => $this->centre]);
            }

            if ($tarifaMutua) {
                $carrec->setTarifaMutua($tarifaMutua);
            }

            // empresa fk
            $empresa = null;
            if (isset($p['idEmpresa'])) {
                $empresa = $empresaRepo->find($p['idEmpresa']);
            } elseif (isset($p['idEmpresa'])) {
                $empresa = $empresaRepo->findOneBy([
                    'dasiclinicReg' => $p['empresaReg'],
                    'idCentre' => $this->centre]);
            }

            if ($empresa) {
                $carrec->setEmpresa($empresa);
            }

            // client fk
            $client = null;
            if (isset($p['idClient'])) {
                $client = $clientRepo->find($p['idClient']);
            } elseif (isset($p['clientReg'])) {
                $client = $clientRepo->findOneBy([
                    'dasiclinicReg' => $p['clientReg'],
                    'centre' => $this->centre]);
            }

            if ($client) {
                $carrec->setClient($client);
            }

            // grupTractament fk
            $grup = null;
            if (isset($p['idGrupTractaments'])) {
                $grup = $grupTracRepo->find($p['idGrupTractaments']);
            }

            if ($grup) {
                $carrec->setIdGrupTractaments($grup);
            }

            // formaPagament fk
            $formaPagament = null;
            if (isset($p['idFormaPagament'])) {
                $formaPagament = $formaPagamentRepo->find($p['idFormaPagament']);
            } elseif (isset($p['formaPagamentReg'])) {
                $formaPagament = $formaPagamentRepo->findOneBy([
                    'dasiclinicReg' => $p['formaPagamentReg'],
                    'centre' => $this->centre]);
            }

            if ($formaPagament) {
                $carrec->setFormaPagament($formaPagament);
            }

            // si és fra, es crea la fra (i tipus de càrrec CAR)
            if (isset($p['fraReg']) && $p['tipusCarrec'] == 'CAR') {

                // si no es passa num de fra i hi ha empresa agafem la serie
                if (!isset($p['codiFra']) && isset($p['codiCarrec'])) {
                    if ($empresa) {
                        $serie = str_pad($empresa->getSerieFac(), 2, "0", STR_PAD_LEFT);
                    } else {
                        $serie = '00';
                    }
                    $p['codiFra'] = $carrec->getExercici() . '/' . (string)$serie . str_pad($p['codiCarrec'], 4, "0", STR_PAD_LEFT);
                }
                $fra = $this->createFra($p);
                $this->em->persist($fra);
                $carrec->setFra($fra);
            }

            $this->em->persist($carrec);

            // si es passa UN visitaTractament, fk visitaTractament (DasiClinic)
            $visitaTractament = null;
            if (isset($p['idVisitaTractament'])) {
                $visitaTractament = $visitaTractamentRepo->find($p['idVisitaTractament']);
            } elseif (isset($p['visitaTractamentReg'])) {
                $visitaTractament = $visitaTractamentRepo->findOneBy([
                    'dasiclinicReg' => $p['visitaTractamentReg'],
                    'traspas' => $this->hash]);
            }

            // afegim al càrrec
            if ($visitaTractament) {
                $carrec->addVisitesTractaments($visitaTractament);
            }

            $this->funcions->addToLog(
                'traspas',
                $this->traspas,
                'Created carrec ' . $carrec->getDataCarrec()->format('Y-m-d H:i:s'),
                'INFO', array(
                    'co' => $co,
                    'carreg_reg / id' => isset($p['carrecReg']) ? $p['carrecReg'] : $carrec->getIdCarrec(),
                    'centre' => $this->centre->getIdCentre(),
                    'env' => $this->env, 'hash' => $this->hash)
            );

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' carrecs created';
    }


    /**
     * crea cobraments
     *
     * @param $cobraments
     * @return string
     */
    public function serviceCobraments($cobraments)
    {
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $carrecRepo = $this->em->getRepository('DatabaseBundle:Carrec');
        $cobramentRepo = $this->em->getRepository('DatabaseBundle:Cobrament');
        $formaPagamentRepo = $this->em->getRepository('DatabaseBundle:FormaPagamentClient');

        $batchSize = 1000;
        $co = 0;
        foreach ($cobraments as $p) {

            // comprovem si ja s'ha creat el cobrament i saltem
            $cobrament = $cobramentRepo->findOneBy([
                'dasiclinicReg' => $p['dasiclinicReg'],
                'centre' => $this->centre]
            );

            // carreguem càrrec associat si es passa
            $carrec = null;
            if (isset($p['idCarrec'])) {
                $carrec = $carrecRepo->find($p['idCarrec']);

            } elseif (isset($p['carrecReg'])) {
                $carrec = $carrecRepo->findOneBy([
                    'dasiclinicReg' => $p['carrecReg'],
                    'centre' => $this->centre]);
            }

            if (!$cobrament) {
                $cobrament = $this->createCobrament($p);

                // pacient fk
                $pacient = null;
                if (isset($p['idPacient'])) {
                    $pacient = $pacRepo->find($p['idPacient']);
                } elseif (isset($p['pacientReg'])) {
                    $pacient = $pacRepo->findOneBy([
                        'dasiclinicReg' => $p['pacientReg'],
                        'centre' => $this->centre]);
                } elseif (isset($p['historia'])) {
                    $pacient = $pacRepo->findOneBy([
                        'historia' => $p['historia'],
                        'centre' => $this->centre]);
                } elseif ($carrec) {
                    // agafem el pacient del càrrec associat
                    $pacient = $carrec->getPacient();
                }


                if ($pacient) {
                    $cobrament->setPacient($pacient);
                } else {
                    continue;
                }

                // formaPagament fk
                $formaPagament = null;
                if (isset($p['idFormaPagament'])) {
                    $formaPagament = $formaPagamentRepo->find($p['idFormaPagament']);
                } elseif (isset($p['formaPagamentReg'])) {
                    $formaPagament = $formaPagamentRepo->findOneBy([
                        'dasiclinicReg' => $p['formaPagamentReg'],
                        'centre' => $this->centre]);
                } elseif ($carrec) {
                    // agafem la forma de pagament del càrrec associat
                    $formaPagament = $carrec->getFormaPagament();
                }

                if ($formaPagament) {
                    $cobrament->setFormaPagament($formaPagament);
                }

                $this->em->persist($cobrament);
            }

            // si es tracta d'un càrrec al dipòsit modifiquem el cobrament
            // posant-li codi CDP enlloc de COB
            if ($carrec) {
                if ($carrec->getDiposit() < 0) {
                    $cobrament->setTipusCobrament('CDP');
                }
            }

            // es crea un registre de cobramentsCarrecs
            if ($carrec) {
                $cobramentCarrec = $this->createCobramentCarrec($p, $cobrament, $carrec);
                $this->em->persist($cobramentCarrec);
            }

            $this->funcions->addToLog(
                'traspas',
                $this->traspas,
                'Created cobrament ' . $cobrament->getDataCobrament()->format('Y-m-d H:i:s'),
                'INFO', array(
                    'co' => $co,
                    'cobrament_reg / id' => isset($p['dasiclinicReg']) ? $p['dasiclinicReg'] : $cobrament->getIdCobrament(),
                    'centre' => $this->centre->getIdCentre(),
                    'env' => $this->env, 'hash' => $this->hash)
            );

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' cobraments created';
    }


    /**
     * crea informes partinr de contingut HTML
     * @param $arxius array
     * @return string
     */
    public function serviceHTML2Informe($informes)
    {
        $pacientRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');

        $batchSize = 20;
        $co = 0;
        foreach ($informes as $p) {

            // cerquem el pacient per id / historia / dasiclinicReg
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacientRepo->find($p['idPacient']);

            } elseif (isset($p['historia'])) {
                $pacient = $pacientRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);

            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacientRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);
            }

            if (!$pacient) {
                continue;
            }

            // cerquem la visita per data i pacient
            $visita = null;
            $professional = null;
            if (isset($a['data'])) {
                $visita = $visitaRepo->findOneBy([
                    'data' => $p['data'],
                    'pacient' => $pacient]);

                $professional = $visita->getProfessional();
            }

            // professional fk
            if (!$professional) {
                if (isset($p['idProfessional'])) {
                    $professional = $profRepo->find($p['idProfessional']);
                } elseif (isset($p['professionalReg'])) {
                    $professional = $profRepo->findOneBy([
                        'dasiclinicReg' => $p['professionalReg'],
                        'centre' => $this->centre]);
                } elseif (isset($p['acronim'])) {
                    $professional = $profRepo->findOneBy([
                        'acronim' => substr($p['acronim'], 0, 5),
                        'centre' => $this->centre]);
                }
            }

            // generem l'informe sense plantilla
            $informeParams = [
                'titol' => $p['filename'],
                'html' => $p['html'],
                'pacient' => $pacient->getId(),
                'visita' => $visita ? $visita->getId() : null,
                'professional' => $professional->getIdProfessional(),
                'centre' => $this->centre->getIdCentre(),
                'usuari' => $this->centre->getClientDasi()->getUserMaster()->getUsuari()->getIdUsuari()
            ];

            $informe = $this->informeService->generaInformeSensePlantilla($informeParams);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created informes pacient history ' . $pacient->getHistoria(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' informes creats';
    }

    /**
     * passem arxius de una font externa (BD) a eCLinic (IBT)
     * arxius de text .doc com a informes del eClinic
     * @param $arxius array
     * @return string
     */
    public function serviceArxiu2Informe($arxius)
    {
        $pacientRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');
        $arxiuRepo = $this->em->getRepository('DatabaseBundle:ArxiusDoc');

        $batchSize = 20;
        $co = 0;
        foreach ($arxius as $a) {

            // cerquem el pacient per id o num historia
            if (isset($a['idPacient'])) {
                $pacient = $pacientRepo->find($a['idPacient']);
            } elseif (isset($a['historia'])) {
                $pacient = $pacientRepo->findOneBy([
                    'historia' => $a['historia'],
                    'centre' => $this->centre]);
            }

            if (!$pacient) {
                continue;
            }

            // cerquem la visita
            if (isset($a['data'])) {
                $visita = $visitaRepo->findOneBy([
                    'data' => $a['data'],
                    'pacient' => $pacient]);
            }

            if (!$visita) {
                continue;
            } else {
                $professional = $visita->getProfessional()->getIdProfessional();
            }

            // cerquem si l'arxiu ja s'ha pujat i el saltem
            $hasarxiu = $arxiuRepo->createQueryBuilder('a')
                ->where('a.name LIKE :nom')
                ->setParameter('nom', $a['filename'])
                ->andWhere('a.pacient = :pacient')
                ->setParameter('pacient', $pacient->getId())
                ->getQuery()
                ->getResult();

            if ($hasarxiu) {
                continue;
            }

            // guardem l'arxiu temporalment al server
            $downloadFile = $this->uploadDir . '/.files/' . $a['filename'];
            $downloadFile = str_replace('.dot', '.doc', $downloadFile); // si és dot el fem doc

            if (file_exists($downloadFile) and is_writable($downloadFile)) {
                unlink($downloadFile);
            }

            // si el fitxer és un .doc o .docx convertim a text i guardem com a informe
            if (strpos($a['filename'], '.doc')!==false) {

                $a['file'] = nl2br($a['file']);
                file_put_contents($downloadFile, $a['file']);

                $docObj = $this->importUtils();
                $txt = $docObj->convertToText($downloadFile);

                $informeParams = [
                    'titol' => $a['filename'],
                    'html' => $txt,
                    'pacient' => $pacient->getId(),
                    'visita' => $visita ? $visita->getId() : null,
                    'professional' => $professional,
                    'centre' => $this->centre->getIdCentre(),
                    'usuari' => $this->centre->getClientDasi()->getUserMaster()->getUsuari()->getIdUsuari()
                ];

                $informe = $this->informeService->generaInformeSensePlantilla($informeParams);
            } else {
                // si és un document normal el pugem al gestor documental
                file_put_contents($downloadFile, $a['file']);

                $params = [
                    'user' => $this->centre->getClientDasi()->getUserMaster(),
                    'pacient' => $pacient->getId(),
                    'visita' => $visita ? $visita->getId() : null
                ];

                // guardem l'arxiu a la ruta del pacient (es creen carpetes si cal)
                $this->arxiuService->saveArxiuDoc($a['filename'], $downloadFile, $params);
            }

            // esborrem el fitxer temporal del server
            unlink($downloadFile);

            $this->funcions->addToLog('traspas', $this->traspas, 'Uploaded arxiu pacient history ' . $pacient->getHistoria(), 'INFO', array('co' => $co, 'file' => $a['filename'], 'date' => $a['data']->format('d-m-Y'), 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' arxius pujats';
    }


    /**
     * arxius previament pujats al server passats a informe
     * arxius de text .doc com a informes del eClinic
     * @param $arxius array
     * @return string
     */
    public function serviceDoc2Informe($arxius)
    {
        $pacientRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');
        $arxiuRepo = $this->em->getRepository('DatabaseBundle:ArxiusDoc');

        $batchSize = 20;
        $co = 0;
        foreach ($arxius as $a) {

            // cerquem el pacient per id o num historia
            if (isset($a['idPacient'])) {
                $pacient = $pacientRepo->find($a['idPacient']);
            } elseif (isset($a['historia'])) {
                $pacient = $pacientRepo->findOneBy([
                    'historia' => $a['historia'],
                    'centre' => $this->centre]);
            }

            if (!$pacient) {
                continue;
            }

            // cerquem la visita
            $visita = null;
/*            if (isset($a['data'])) {
                $visita = $visitaRepo->findOneBy([
                    'data' => $a['data'],
                    'pacient' => $pacient]);
            }

            if (!$visita) {
                continue;
            } else {
                $professional = $visita->getProfessional()->getIdProfessional();
            } */

            // neteja caractrers de control
            $titol = preg_replace('/[\x00-\x1F\x7F\xCE\xBC\xE2\x89\xA5]/', '', $a['titol']);

            // també convertim caracters codificats en html i els decodifiquem novament
            $titol = htmlentities($titol);
            $titol = html_entity_decode(utf8_decode($titol));

            $html = $this->importUtils->convertToText($a['filename'], $this->traspas, 'files', 'html');

            $informeParams = [
                'titol' => $titol,
                'html' => $html,
                'pacient' => $pacient->getId(),
                'visita' => $visita ? $visita->getId() : null,
                'professional' => $a['professional'],
                'centre' => $this->centre->getIdCentre(),
                'usuari' => $this->centre->getClientDasi()->getUserMaster()->getUsuari()->getIdUsuari()
            ];

            $informe = $this->informeService->generaInformeSensePlantilla($informeParams);

            $this->funcions->addToLog('traspas', $this->traspas, 'Created informe pacient history ' . $pacient->getHistoria(), 'INFO', array('co' => $co, 'file' => $a['filename'], 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' arxius pujats';
    }

    /**
     * traspasa imatges i documents a eClinic desde dasiClinic
     * @param $arxius array
     * @return string
     */
    public function serviceArxius($arxius)
    {
        $pacientRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');
        $tagRepo = $this->em->getRepository('DatabaseBundle:Tags');

        $batchSize = 20;
        $co = 0;
        foreach ($arxius as $p) {

            // pacient fk
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacientRepo->find($p['idPacient']);
            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacientRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['historia'])) {
                $pacient = $pacientRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);
            }
/*            if (!$pacient) {
                var_dump($p);exit;
                continue;
            }*/

            // visita fk
            $visita = null;
            if (isset($p['idVisita'])) {
                $visita = $visitaRepo->find($p['idVisita']);
            } elseif (isset($p['visitaReg'])) {
                $visita = $visitaRepo->findOneBy([
                    'dasiclinicReg' => $p['visitaReg'],
                    'traspas' => $this->hash]);
            } elseif (isset($p['data'])) {
                $visita = $visitaRepo->findOneBy([
                    'data' => $p['data'],
                    'pacient' => $pacient]);
            }

            if ($visita && !$pacient) {
                $pacient = $visita->getPacient();
            }

            if (!$pacient) {
                continue;
            }

            // tag fk
            $tags = [];
            if (isset($p['idTag'])) {
                $tags[] = $tagRepo->find($p['idTag']);
            } elseif (isset($p['tagReg'])) {
                $tags[] = $tagRepo->findOneBy([
                    'dasiclinicReg' => $p['tagReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['tag'])) {
                $tags[] = $tagRepo->findOneBy([
                    'nom' => $p['tag'],
                    'pacient' => $pacient]);
            }

            // cerquem si l'arxiu ja s'ha pujat i el saltem
            /*            $hasarxiu = $arxiuRepo->createQueryBuilder('a')
                            ->where('a.name LIKE :nom')
                            ->setParameter('nom', $p['filename'])
                            ->andWhere('a.pacient = :pacient')
                            ->setParameter('pacient', $pacient->getId())
                            ->getQuery()
                            ->getResult();

                        if ($hasarxiu) {
                            continue;
                        }*/

            $downloadFile = isset($p['path']) ? $p['path'] : $this->uploadDir . '/.files' . $p['filename'];

            // si és folder, salta'l   <=> crea carpeta si no té fitxer adjunt (dasiclinic)
            if (isset($p['imatgeReg']) && $p['imatgeReg'] == 0 && isset($p['documentReg']) && $p['documentReg'] == 0) {
//                $user = $this->centre->getClientDasi()->getUserMaster();
//                $this->arxiuService->createFolders($p['filename'], $user, $pacient->getId(), $visita);
                continue;
            }

            // si passem un arxiu i ja existeix l'arxiu temporal al servidor, l'esborrem
            if (isset($p['file']) && file_exists($downloadFile) && is_writable($downloadFile)) {
                unlink($downloadFile);
            }

            // carreguem el file de fk (dasiclinic) si no es passa l'arxiu directament
            $size = null;
            if (isset($p['path'])) {
                $size = true;
            } elseif (isset($p['file'])) {
                $size = file_put_contents($downloadFile, $p['file']);
            } else {
                if (isset($p['imatgeReg']) && $p['imatgeReg'] > 0 && $p['imgFile']) {
                    $size = file_put_contents($downloadFile, $p['imgFile']);
                } elseif (isset($p['documentReg']) && $p['documentReg'] > 0 && $p['docFile']) {
                    $size = file_put_contents($downloadFile, $p['docFile']);
                }
            }
            if (is_null($size)) {
                exit("no s'ha trobat el fitxer a pujar amb reg " . $p['dasiclinicReg']);
            }

            // si volem crear només la paperera, 'createRecycleBin' => true,
            $params = [
                'user' => $this->centre->getClientDasi()->getUserMaster()->getId(),
                'pacient' => $pacient->getId(),
                'visita' => $visita ? $visita->getId() : null,
                'descripcio' => $p['descripcio'],
                'tags' => $tags,
                'defaultFolders' => false   // no es creen les carpetes de imatges i documents per defecte
            ];

            // guardem l'arxiu a la ruta del pacient (es creen carpetes si cal)
            $this->arxiuService->saveArxiuDoc($p['filename'], $downloadFile, $params);

            // esborrem el fitxer temporal del server
            if (isset($p['file']) && file_exists($downloadFile) && is_writable($downloadFile)) {
                unlink($downloadFile);
            }

            $this->funcions->addToLog('traspas', $this->traspas, 'Uploaded arxiu pacient history ' . $pacient->getHistoria(), 'INFO', array('co' => $co, 'file' => $p['filename'], 'date' => $p['data']->format('d-m-Y'), 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' arxius pujats';
    }

    /**
     * traspàs medicaments (genera receptes)
     *
     * @param $medicaments array, bool
     * @return string
     */
    public function serviceMedicaments($medicaments)
    {
        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');
        $receRepo = $this->em->getRepository('DatabaseBundle:Receptes');

        $batchSize = 20;
        $co = 0;
        foreach ($medicaments as $p) {

            // es crea el medicament
            $medicament = $this->createMedicament($p);
            $this->em->persist($medicament);

            $this->em->flush();

            $this->funcions->addToLog('traspas', $this->traspas, 'Created medicament ' . $medicament->getNameSpeciality(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' medicaments created';

    }


    /**
     * traspàs receptes passant medicaments i visites (genera receptes)
     *
     * @param $medicaments array, bool
     * @return string
     */
    public function serviceReceptes($medicaments, $teMedicament = true)
    {
        $visitaRepo = $this->em->getRepository('DatabaseBundle:Visites');
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');
        $receRepo = $this->em->getRepository('DatabaseBundle:Receptes');

        $batchSize = 20;
        $co = 0;
        foreach ($medicaments as $p) {

            // pacient fk
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);
            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);
            }
            if (!$pacient) {
                continue;
//                exit('no existeix el paciente reg ' .$p['pacientReg']);
            }

            // visita fk
            $visita = null;
            if (isset($p['idVisita'])) {
                $visita = $visitaRepo->find($p['idVisita']);
            } elseif (isset($p['visitaReg'])) {
                $visita = $visitaRepo->findOneBy([
                    'dasiclinicReg' => $p['visitaReg'],
                    'pacient' => $pacient->getId()
                ]);
            } else {  // recuperem la visita amb el pacient i data, si no trobem la visita busquem la visita anterior més propera
                $dataVisita = clone $p['dataVisita'];
                $visita = $visitaRepo->findOneBy([
                    'data' => $dataVisita,
                    'pacient' => $pacient->getId()
                ]);
                if (!$visita) {
                    $visitesAnt = $visitaRepo->createQueryBuilder('v')
                        ->where('v.pacient = :pacient')
                        ->andwhere('v.data <= :data')
                        ->setParameters([
                            'pacient' => $pacient->getId(),
                            'data' => $dataVisita,
                        ])
                        ->getQuery()
                        ->getResult();

                    if ($visitesAnt) {
                        $visita = $visitesAnt[0];
                    }
                }

                if (!$visita) {
                    // o la última visita del pacient
                    $visitesAnt = $visitaRepo->createQueryBuilder('v')
                        ->where('v.pacient = :pacient')
                        ->orderBy('v.data', 'DESC')
                        ->setParameters([
                            'pacient' => $pacient->getId()
                        ])
                        ->getQuery()
                        ->getResult();

                    if ($visitesAnt) {
                        $visita = $visitesAnt[0];
                    }
                }

                // es crea la cita amb les dades de la visita si no la trobem
                if (!$visita) {
                    $this->funcions->addToLog('traspas', $this->traspas, 'No existeix visita pel medicament ' . $p['name_speciality'] . ' al ' . $p['dataVisita']->format('Y-m-d') . ' del pacient id ' . $pacient->getId(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
                    continue;
                }
            }

            // recuperem la recepta que conté la visita, si no en té es crea
            $recepta = $receRepo->findOneByVisita($visita);
            if (!$recepta) {
                $recepta = $this->createRecepta($p);
                $recepta->setVisita($visita);
                $this->em->persist($recepta);
            }

            // es crea el medicament si es passen medicaments
            if ($teMedicament) {
                $medicament = $this->createMedicament($p);
                $this->em->persist($medicament);
            }
            //  i s'afegeix a la recepta
            $receptaMedicament = $this->createReceptaMedicament($p);
            $receptaMedicament->setRecepta($recepta);

            if ($teMedicament) {
                $receptaMedicament->setMedicament($medicament);
            }
            $this->em->persist($receptaMedicament);

            $this->em->flush();

            $this->funcions->addToLog('traspas', $this->traspas, 'Created receptes ' . $medicament->getNameSpeciality() . ' a visita ' . $visita->getData()->format('Y-m-d') . ' (historia ' . $pacient->getHistoria() . ')', 'INFO', array('co' => $co, 'pac_reg' => $pacient->getDasiclinicReg(), 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' medicaments created';

    }

    /**
     * traspàs bancs (crea bancCompte i bancEntitat)
     *
     * @param $bancs array
     * @return string
     */
    public function serviceBancs($bancs)
    {
        $visitaRepo = $this->em->getRepository('DatabaseBundle:BancEntitat');

        $batchSize = 20;
        $co = 0;
        foreach ($bancs as $p) {

            $entitat = $this->createBancEntitat($p);
            $this->em->persist($entitat);

            $bancCompte = $this->createBancCompte($p, $entitat);

            $this->em->persist($bancCompte);

            $this->funcions->addToLog(
                'traspas',
                $this->traspas,
                'Created bancCompte ' . $bancCompte->getNom(),
                'INFO',
                [
                    'co' => $co,
                    'centre' => $this->centre->getIdCentre(),
                    'env' => $this->env,
                    'hash' => $this->hash
                ]
            );

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' bancCompte created';

    }



    /**
     * traspàs formes de pagament
     *
     * @param $formesPagament array
     * @return string
     */
    public function serviceFormesPagament($formesPagament)
    {
        $bancCopmteRepo = $this->em->getRepository('DatabaseBundle:BancCompte');
        $formesPagamentRepo = $this->em->getRepository('DatabaseBundle:FormaPagamentClient');

        $batchSize = 20;
        $co = 0;
        foreach ($formesPagament as $p) {

            // fk bancCompte
            if (isset($p['idBancCompte'])) {
                $bancCopmte = $bancCopmteRepo->find($p['idBancCompte']);
            } else {
                $bancCopmte = $bancCopmteRepo->findOneBy([
                    'dasiclinicReg' => $p['formaPagoPare'],
                    'centre' => $this->centre
                ]);
            }

            // mirem si ja existeix la forma de pagament primaria buscant pel pare
            $formaPagament = $formesPagamentRepo->findOneBy([
                'pare' => $p['formaPagoPare'],
                'centre' => $this->centre
                ]);

            // si existeix actualitzem el registre
            if ($formaPagament) {
                $formaPagament = $this->updateFormaPagamentClient($formaPagament, $p, $bancCopmte);
            } else {

                $formaPagament = $this->createFormaPagamentClient($p, $bancCopmte);
                $this->em->persist($formaPagament);
            }

            $this->funcions->addToLog(
                'traspas',
                $this->traspas,
                'Created formaPagament ' . $formaPagament->getNom(),
                'INFO',
                [
                    'co' => $co,
                    'centre' => $this->centre->getIdCentre(),
                    'env' => $this->env,
                    'hash' => $this->hash
                ]
            );

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' formaPagament created';

    }


    /*
     * ----------------------------- updates i d'altres perles
     */

    /**
     * Comparteix un pacient amb un centre passat
     * @param $paciemt
     * @param $centre
     */
    public function sharePacientCentre($pacient, $centre) {

        if (!is_object($pacient)) {
            $centre = $this->em->getRepository('DatabaseBundle:Pacients')->find($pacient);
        }

        if (!is_object($centre)) {
            $centre = $this->em->getRepository('DatabaseBundle:Centres')->find($centre);
        }

        $pacientCentre = $this->createPacientCentre($pacient, $centre);

        $this->em->persist($pacientCentre);
        $this->em->flush();
    }


    /**
     * actualitza pacients a eClinic
     * @param $pacients array
     * @return string
     */
    public function updatePacients($pacients, $concatFields = [])
    {
        $pacientRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $procedenciaRepo = $this->em->getRepository('DatabaseBundle:Procedencies');
        $tagsRepo = $this->em->getRepository('DatabaseBundle:Tags');
        $tarifaRepo = $this->em->getRepository('DatabaseBundle:TarifaMutua');

        $batchSize = 20;
        $co = 0;
        foreach ($pacients as $p) {

            $pacient = null;

            // cerquem el pacient per id / historia / dasiclinicReg / dasiclinicId / dni o nomComplert
            // per aquest ordre, segons el camp que es passi
            if (isset($p['idPacient'])) {
                $pacient = $pacientRepo->find($p['idPacient']);

            } elseif (isset($p['historia'])) {
                $pacient = $pacientRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);

            } elseif (isset($p['dasiclinicReg'])) {
                $pacient = $pacientRepo->findOneBy([
                    'dasiclinicReg' => $p['dasiclinicReg'],
                    'centre' => $this->centre]);

            } elseif (isset($p['dasiclinicId'])) {
                $pacient = $pacientRepo->findOneBy([
                    'dasiclinicId' => $p['dasiclinicId'],
                    'centre' => $this->centre]);

            } elseif (isset($p['numDocument']) && $p['numDocument'] !== '') {
                // busquem per dni
                $pacient = $pacientRepo
                    ->findByNameOrPhoneOrDni(
                        $this->centre,
                        ['dni' => $p['numDocument']],
                        'dni'
                    );

            } elseif (isset($p['nomCognoms'])) {
                // busquem per nomCognoms
                $pacient = $pacientRepo
                    ->findByNameOrPhoneOrDni(
                        $this->centre,
                        ['nomCognoms' => $p['nomCognoms']],
                        'name'
                    );
            }

            // actualitzem el pacient amb les dades passades si es troba
            // a $concatFields opcionalment podem passar camps on volem concatenar la informació
            // en cas de que hi hagi més d'un registre que actualitzi el mateix pacient
            if ($pacient) {
                $pacient = $this->updatePacient($pacient, $p, $concatFields);
            } else {
                continue;
            }

            // cerquem la procedència per FK o per camp trapas
            $procedencia = null;
            if (isset($p['idProcedencia'])) {
                $procedencia = $procedenciaRepo->find($p['idProcedencia']);
            } elseif (isset($p['procedencia'])) {
                $procedencia = $procedenciaRepo->findOneBy([
                    'traspas' => $p['procedencia'],
                    'centre' => $this->centre]);
            }
            if ($procedencia) {
                $pacient->setProcedencia($procedencia);
            }

            // cerquem l'origen de pacient per FK o per camp trapas
            $tag = null;
            if (isset($p['origenReg'])) {
                $tag = $tagsRepo->findOneBy([
                        'dasiclinicReg' => $p['origenReg'],
                        'centre' => $this->centre]
                );

                // DC: si el idRegistre = 0 no es passa
                if ($tag && $tag->getDasiclinicReg() >0) {
                    $p['pacient'] = $pacient;
                    $p['tag'] = $tag;

                    $this->createPacientsTags($p);
                }
            }

            // cerquem la tarifa per FK o camp ccost
            $tarifa = null;
            if (isset($p['idTarifaMutua'])) {
                $tarifa = $tarifaRepo->find($p['idTarifaMutua']);
            } elseif (isset($p['codiMutua'])) {
                $tarifa = $tarifaRepo->findOneBy([
                    'ccoste' => $p['codiMutua'],
                    'centre' => $this->centre]
                );
            } elseif (isset($p['tarifaReg'])) {
                $tarifa = $tarifaRepo->findOneBy([
                        'dasiclinicReg' => $p['tarifaReg'],
                        'centre' => $this->centre]
                );
            }
            if ($tarifa) {
                $pacient->addTarifaMutua($tarifa);
                $pacient->setTarifaPreferida($tarifa);
            }

            $this->funcions->addToLog('traspasUpdate', $this->traspas, 'Updated pacient history ' . $pacient->getHistoria(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' pacients updated';
    }

    /**
     * traspàs clients a eClinic
     * @param $pacients array
     * @return string
     * @throws \Exception
     */
    public function updateClients(array $pacients)
    {
        $batchSize = 20;
        $co = 0;

        $clientAtPacRepo = $this->em->getRepository('DatabaseBundle:ClientsAtPacients');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');

        foreach ($pacients as $p) {

            // cerquem pacient per FK
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);

            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);

            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => $p['historia'],
                    'centre' => $this->centre]);
            }

            if ($pacient) {

                // busquem els clients associats al pacient
                $clients = $clientAtPacRepo->findBy([
                    'pacient_id' => $pacient->getId()
                ]);

                foreach ($clients as $client) {
                    $client = $client->getClientId();
                    if ($client->getRelacio() === 'Mare') {
                        $client = $this->updateClient($client, $p);
                    }
                    $this->funcions->addToLog(
                        'traspas', $this->traspas,
                        'Updated client ' . $client->getId(),
                        'INFO',
                        [
                            'co' => $co,
                            'client_reg / id' => isset($p['clientReg']) ? $p['clientReg'] : $client->getId(),
                            'centre' => $this->centre->getIdCentre(),
                            'env' => $this->env,
                            'hash' => $this->hash
                        ]
                    );
                }
            }

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();
exit;
        return $co . ' clients updated';
    }


    /**
     * update cites
     *
     * @param $cites array, bool $creaVisites, bool $checkCitaExist
     * @return string
     */
    public function updateCites($cites, $creaVisites = false, $checkCitaExist = false, $allowCitaSensePacient = false)
    {
        $citaRepo = $this->em->getRepository('DatabaseBundle:Cites');
        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');
        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $especRepo = $this->em->getRepository('DatabaseBundle:Especialitats');
        $tipusVisitaRepo = $this->em->getRepository('DatabaseBundle:TipusVisita');
        $tarifaRepo = $this->em->getRepository('DatabaseBundle:TarifaMutua');
        $tracRepo = $this->em->getRepository('DatabaseBundle:Tractaments');
        $boxRepo = $this->em->getRepository('DatabaseBundle:Boxes');

        $batchSize = 20;
        $co = 0;
        foreach ($cites as $p) {

            $cita = null;
            if (isset($p['idCita'])) {
                $cita = $citaRepo->find($p['idCita']);
            } elseif (isset($p['dasiclinicReg'])) {
                $cita = $citaRepo->findOneBy([
                    'dasiclinicReg' => $p['dasiclinicReg'],
                    'centre' => $this->centre]);
            }

            if (!$cita) {
                continue;
            }
/*
            // pacient fk
            $pacient = null;
            if (isset($p['idPacient'])) {
                $pacient = $pacRepo->find($p['idPacient']);
            } elseif (isset($p['pacientReg'])) {
                $pacient = $pacRepo->findOneBy([
                    'dasiclinicReg' => $p['pacientReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['historia'])) {
                $pacient = $pacRepo->findOneBy([
                    'historia' => (int)$p['historia'],
                    'centre' => $this->centre]);
            }

            if ($pacient) {
                $cita->setPacient($pacient);
            }

            // especialitat fk
            $especialitat = null;
            if (isset($p['idEspecialitat'])) {
                $especialitat = $especRepo->find($p['idEspecialitat']);
            } elseif (isset($p['especialitatReg'])) {
                $especialitat = $especRepo->findOneBy([
                    'dasiclinicReg' => $p['especialitatReg'],
                    'idCentre' => $this->centre]);
            } elseif (isset($p['especialitat'])) {
                $especialitat = $especRepo->findOneBy([
                    'nom' => $p['especialitat'],
                    'idCentre' => $this->centre]);
            }

            // professional fk
            $professional = null;
            if (isset($p['idProfessional'])) {
                $professional = $profRepo->find($p['idProfessional']);
            } elseif (isset($p['professionalReg'])) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $p['professionalReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['acronim'])) {
                $professional = $profRepo->findOneBy([
                    'acronim' => substr($p['acronim'], 0, 5),
                    'centre' => $this->centre]);
            } elseif (isset($p['professional_nom'])) {
                $professional = $profRepo->getProfessionalByNom(
                    $this->centre->getIdCentre(),
                    $p['professional_nom'],
                    $p['professional_cognom1'],
                    $p['professional_cognom2']
                );
            }

            if ($professional) {
                $cita->setProfessional($professional);

                // si no s'ha definit especialitat posem la principal del professional
                if (!$especialitat) {
                    $especialitat = $professional->getEspecialitatPrincipal();
                }
            }

            if ($especialitat) {
                $cita->setEspecialitat($especialitat);
            }

            // tipus Visita fk
            $tipusVisita = null;
            if (isset($p['idTipusVisita'])) {
                $tipusVisita = $tipusVisitaRepo->find($p['idTipusVisita']);
            } elseif (isset($p['tipusVisita'])) {
                $tipusVisita = $tipusVisitaRepo->findOneBy([
                    'nom' => $p['tipusVisita'],
                    'centre' => $this->centre]);
            }
            if ($tipusVisita) {
                $cita->setTipusVisita($tipusVisita);
            }

            // box fk
            $box = null;
            if (isset($p['idBox'])) {
                $box = $boxRepo->find($p['idBox']);
            } elseif (isset($p['boxReg'])) {
                $box = $boxRepo->findOneBy([
                    'dasiclinicReg' => $p['boxReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['box'])) {
                $box = $boxRepo->findOneBy([
                    'codi' => $p['box'],
                    'centre' => $this->centre]);
            }
            if ($box) {
                $cita->setBox($box);
            }
*/
            // tarifa fk
            $tarifaMutua = null;
            if (isset($p['idTarifaMutua'])) {
                $tarifaMutua = $tarifaRepo->find($p['idTarifaMutua']);
            } elseif (isset($p['tarifaReg'])) {
                $tarifaMutua = $tarifaRepo->findOneBy([
                    'dasiclinicReg' => $p['tarifaReg'],
                    'centre' => $this->centre]);
            } elseif (isset($p['codiMutua'])) {
                $tarifaMutua = $tarifaRepo->findOneBy([
                    'ccoste' => $p['codiMutua'],
                    'centre' => $this->centre]);
            }
            if ($tarifaMutua) {
                $cita->setTarifaCita($tarifaMutua);
            }
/*
            // tractaments fk
            $tractament = null;
            if (isset($p['idTractament'])) {
                $tractament = $tracRepo->find($p['idTractament']);
            } elseif (isset($p['tractamentReg'])) {
                $tractament = $tracRepo->findOneBy([
                    'dasiclinicReg' => $p['tractamentReg'],
                    'idCentre' => $this->centre]);
            } elseif (isset($p['tractament'])) {
                $tractament = $tracRepo->findOneBy([
                    'codi' => $p['tractament'],
                    'idCentre' => $this->centre]);
            }
            if ($tractament) {
                $this->createCitaTractament($p, $cita, $tractament);
            }
*/

            $this->funcions->addToLog('traspas', $this->traspas, 'Updated cites (idCita ' . $cita->getId() . ')', 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

            if (($co % $batchSize) === 0) {
                $this->em->flush();
                $this->em->clear();
                $this->setCentre($p['centre']);
            }
            $co++;
        }
        $this->em->flush();
        $this->em->clear();

        return $co . ' cites updated';

    }


    // posa un tractament a les cites entre un intèrval de dates
    public function serviceTractamentACites($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $tractament = $this->em->getRepository('DatabaseBundle:Tractaments')
            ->find($params['tractament']);

        $repoCites = $this->em->getRepository('DatabaseBundle:Cites');
        $query = $repoCites->createQueryBuilder('c')
            ->join('c.professional', 'p')
            ->where('p.centre = :centre')
            ->andwhere('c.data BETWEEN :dataIni AND :dataFi')
            ->setParameters([
                'centre' => $this->centre,
                'dataIni' => $params['dataIni'],
                'dataFi' => $params['dataFi']
            ])
            ->getQuery();

        $cites = $query->getResult();
// dump($cites);exit;

        foreach ($cites as $cita) {

            $citesTractaments = new DB\CitesTractaments();
            $citesTractaments->setCita($cita);
            $citesTractaments->setTractament($tractament);
            $citesTractaments->setUnitats(1);
            $this->em->persist($citesTractaments);
        }

        $this->em->flush();

        return 'ok';

    }

    /**
     * Afegeix relacions de confiança als professionals passats per tos els usuaris del centre
     * (per ex. si no s'han creat relacions als professionals inactius al moment de crear l'usuari)
     * @param User $user
     * @return string
     */
    public function addRelacionsToAllUsers($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');

        $co = 0;
        foreach ($params['professionals'] as $idProf) {

            $professional = $profRepo->find($idProf);

            if ($professional) {

                $this->profService->addRelacionsToAllUsers($professional, $this->centre->getIdCentre());
                $this->funcions->addToLog('traspasUpdate', $this->traspas, 'RC creada per tots els usuaris amb el professional ' . $professional->getIdProfessional(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));
                $co++;
            }

        }

        return 'ok';
    }

    /**
     * actulitza saldo pacients
     */
    public function updateSaldoPacients($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $pacRepo = $this->em->getRepository('DatabaseBundle:Pacients');
        $repofra = $this->em->getRepository('DatabaseBundle:Fra');

        $pacients = $pacRepo->findByCentre($params['centre']);

        $batchSize = 100;
        $co = 0;
        foreach ($pacients as $p) {
            if ($p) {
                $repofra->updateSaldoPacient($p, false);

                $this->funcions->addToLog('traspasUpdate', $this->traspas, 'actualizat saldo del pacient amb historia ' . $p->getHistoria(), 'INFO', array('co' => $co, 'centre' => $this->centre->getIdCentre(), 'env' => $this->env, 'hash' => $this->hash));

                if (($co % $batchSize) === 0) {
                    $this->em->flush();
                    $this->em->clear();
//                    $this->setCentre($p['centre']);
                }
                $co++;
            }
        }

        return 'ok';
    }

    /* ---------------------------------------------------------------------------------------------- */


    /*
     * retorna un episodi existent per un pacient o especialitat
     */
    public function getEpisodi($pacient, $especialitat)
    {
        $repoEpisodi = $this->em->getRepository('DatabaseBundle:Episodis');
        $episodi = $repoEpisodi->getEpisodiObertEspecialitat($pacient, null, $especialitat);

        if ($episodi) {
            return $episodi;
        } else {
            return null;
        }
    }

    /*
     * Normalitzem els valors de traspàs de camps al tipus correctes per les funcions de traspàs al eClinic
     */
    public function parseValues($params, $normRow, $value, $output = '')
    {

        if (!is_array($params)) {
            $arrParams[] = $params;
        } elseif (isset($params['id'])) {
            $arrParams[] = $params;
        } else {
            $arrParams = $params;
        }

        $origValue = $value;
        $today = new \DateTime('today');

        foreach ($arrParams as $e) {

            // tipus de sortida i salt de linia (per defecte raw)
            $saltLinia = "\r\n";
            if ($output == 'html' && isset($e['id']) && in_array($e['id'], [
                    'anamnesis',
                    'exploracio',
                    'judici',
                    'evolucio',
                    'comentaris'
                    ])) {

                $saltLinia = '<br/>';
                $value = nl2br($value);

            } elseif ((isset($e['output']) && $e['output'] == 'html')) {
                $saltLinia = '<br/>';
                $value = nl2br($value);
            }

            // multipliquem salt de linia o no en fem
            if (isset($e['saltlinia'])) {
                if ($e['saltlinia'] > 0) {
                    for ($i = 1; $i <= $e['saltlinia']; $i++) {
                        $saltLinia .= $saltLinia;
                    }

                } else {
                    $saltLinia = '';
                }
            }

            if (is_array($e)) {

                // mirem si label
                $label = null;
                if (isset($e['label'])) {
                    $label = $e['label'];
                }

                if (isset($e['type'])) {

                    switch ($e['type']) {

                        case 'clauValor':
                            // obtenim valor de clauValor si s'ha definit
                            if (is_numeric($value)) {
                                $value = intval($value);
                            } else {
                                $value = trim($value);
                            }

                            if (array_key_exists('failValue', $e)) {
                                $failValue = $e['failValue'];
                            } else {
                                $failValue = $value;
                            }

                            $value = isset($this->clauValor[$e['id']][$value]) ? $this->clauValor[$e['id']][$value] : $failValue;

                            break;

                        case 'jsonArray':
                            $arrTmp = json_decode($value);
                            if ($arrTmp[1]) {
                                $value = $arrTmp[0] . ': ' . $arrTmp[1];
                            } else {
                                $value = '';
                            }
                            /*
                             * de moment només un valor
                             * foreach ($arrTmp as $j) {
                                $value .= key($j)
                            }*/
                            break;

                        case 'entity2String':
                            // obtenim una entity passant una id d'una fk de la bd externa i convertim a string
                            if (isset($e['class']) && $value) {
                                if (isset($e['idFk']) && $value) {
                                    $arrTmp = $this->em->getRepository('DatabaseBundle:' . $e['class'])
                                        ->findBy([
                                            'centre' => $this->centre->getIdCentre(),
                                            $e['idFk'] => $value
                                        ]);
                                    if ($arrTmp) {
                                        $value = (string)$arrTmp[0];
                                    } else {
                                        $value = '';
                                    }
                                } else {
                                    $value = (string)$this->em->getRepository('DatabaseBundle:' . $e['class'])->find($value);
                                }
                            }
                            break;

                        case 'rtf':
                            // parse rtf, nova llibreria, TODO: NO VA, DESINSTALAR-LA
//                            dump($value);exit;
//                            dump($this->importUtils->rtfParse($value));exit;

                            // neteja text abans de parsejat a rtf
                            $value = preg_replace('/[\x00-\x09\x0B\x0C\x0E\x80\x8A\xE2\x97\x0E-\x1F\x7F]/', '', $value);

                            // decodifiquem camps rtf
                            $this->rtfReader->Parse($value);
                            if (isset($e['output']) && $e['output'] == 'raw') {
                                $value = html_entity_decode(strip_tags($this->htmlFormatter->Format($this->rtfReader->root)));
                            } else {
                                $value = nl2br(nl2br(html_entity_decode(strip_tags($this->htmlFormatter->Format($this->rtfReader->root)))));
                                //                                $value = str_replace('<br>', '<br><br>', $value);
                            }

                            // es necessari tornar a fer el preg_replace, el parse genera caracters de control
                            $value = preg_replace('/[\x00-\x09\x0B\x0C\x0E\x80\x8A\xE2\x97\x0E-\x1F\x7F]/', '', $value);

                            // esborra tots els caracters especials tractant la cadena com UTF8
                            // p{Cc} es la propiedad Unicode para los caracteres de control,
                            // y la u hace que tanto la expresión regular como la cadena de destino se traten como utf-8.
                            $value = preg_replace("/p{Cc}+/u", "", $value);

                            break;

                        case 'html_p':
                            $value = '<p>' . trim($value) . '</p>';
                            break;

                        case 'html_strong':
                            $value = '<strong>' . trim($value) . '</strong>';
                            break;

                        case 'utf8encode':
                            $value = utf8_encode($value);
                            break;

                        case 'utf8decode':
                            $value = utf8_decode($value);
                            break;

                        case 'htmlentities&utf8decode':

                            // neteja caractrers de control
                            $value = preg_replace('/[\x00-\x09\x0B\x0C\x0E\x80\x8A\xE2\x97\x0E-\x1F\x7F]/', '', $value);

                            $value = utf8_encode($value);
                            $value = htmlentities($value);
                            $value = html_entity_decode(utf8_decode($value));

                            break;

                        case 'trim':
                            if (isset($e['string'])) {
                                if (isset($e['replace'])) {
                                    $replace = $e['replace'];
                                } else {
                                    $replace = '';
                                }
                                $value = str_replace($e['string'], $replace, $value);
                            } else {
                                if (isset($e['replace'])) {
                                    $value = trim($value, $e['replace']);
                                } else {
                                    $value = trim($value);
                                }
                            }
                            break;

                        case 'ltrim':
                            if (isset($e['replace'])) {
                                $value = ltrim($value, $e['replace']);
                            } else {
                                $value = ltrim($value);
                            }
                            break;

                        case 'rtrim':
                            if (isset($e['replace'])) {
                                $value = rtrim($value, $e['replace']);
                            } else {
                                $value = rtrim($value);
                            }
                            break;

                        case 'strpadleft':
                            if (isset($e['string']) && isset($e['length'])) {
                                $value = str_pad($value, $e['length'], $e['string'], STR_PAD_LEFT);
                            }
                            break;

                        case 'pregReplace':
                            if (isset($this->clauValor[$e['id']])) {
                                $patterns = array_keys($this->clauValor[$e['id']]);
                                $replaces = array_values($this->clauValor[$e['id']]);
                                $value = preg_replace($patterns, $replaces, $value);
                            }
                            break;

                        case 'cleanControlChar':

                            // neteja caractrers de control
                            $value = preg_replace('/[\x00-\x1F\x7F\xCE\xBC\xE2\x89\xA5]/', '', $value);

                            // també convertim caracters codificats en html i els decodifiquem novament
                            $value = htmlentities($value);
                            $value = html_entity_decode(utf8_decode($value));

                            break;

                        case 'decimalwithpoint':
                            $value = str_replace(',', '.', $value);

                            break;

                        case 'bool':
                            if ($value !== '' && !is_null($value)) {
                                if ($value == 1 || $value == '1' || $value == 'S' || $value == 's' || $value == 'Sí') {
                                    $value = true;
                                } else {
                                    $value = false;
                                }
                            } else {
                                $value = false;
                            }
                            break;

                        case 'stringBool':
                            if ($value !== '' && !is_null($value)) {
                                $value = 'Sí';
                            } else {
                                $value = 'No';
                            }
                            break;

                        case 'choice':
                            if (isset($e['values'][$value])) {
                                $value = $e['values'][$value];
                            } else {
                                $value = null;
                            }
                            break;

                        case 'zeroIsNull':
                            if ($value <= 0) {
                                $value = null;
                            }
                            break;

                        case 'isNotNumericZero':
                            if (!is_numeric ($value)) {
                                $value = 0;
                            }
                            break;

                        case 'not':
                            $value = !$value;
                            break;


                        case 'notEmpty':
                            if ($value !== '' && !is_null($value)) {
                                $value = true;
                            } else {
                                $value = false;
                            }
                            break;

                        case 'ucwords':
                            $value = ucwords($value);
                            break;

                        case 'decHex':
                            if (trim($value)!='' || !$value) {
                                $value = intval(trim($value));
                                if (is_int($value)) {
                                    $value = dechex($value);
                                }
                            }
                            break;

                        /*                        case 'Tcolor2hex':
                                                    $value = intval($value);
                                                    if (is_int($value)) {
                                                        $value = str_pad($value, 9, '0', STR_PAD_LEFT)
                                                        $value = sprintf("#%03x%03x%03x", 13, 0, 255);($value);
                                                    }
                                                    break;*/

                        case 'sexe':    // Hombre-Home / Mujer-Dona
                            if ($value !== '') {
                                switch (strtoupper($this->funcions->quitaAcentos($value))) {
                                    case 'H':
                                    case 'HOMBRE':
                                    case 'VARON':
                                    default:
                                        $value = 'm';
                                        break;
                                    case 'M':
                                    case 'D':
                                    case 'MUJER':
                                        $value = 'f';
                                        break;
                                }
                            }
                            break;

                        case 'sexe2': // (M)asculino - (F)emenino / (V)aron - (H)embra
                            if ($value !== '') {
                                switch ($value) {
                                    case 'M':
                                    case 'Masculino':
                                    case 'V':
                                    default:
                                        $value = 'm';
                                        break;
                                    case 'F':
                                    case 'Femenino':
                                    case 'H':
                                        $value = 'f';
                                        break;
                                }
                            }
                            break;


                        case 'date':    // opcionalment es pot passar format
                            if (!is_object($origValue)) {
                                if (isset($e['format'])) {
                                    $dateFormat = $e['format'];
                                    $parsedDate = $origValue;

                                } else { // per defecte, segons número de caracters
                                    $origValue = str_replace('/', '-', $origValue);

                                    if (strlen($origValue) > 10) {
                                        $dateFormat = 'Y-m-d H:i';
                                        $parsedDate = substr($origValue, 0, 10) . ' 00:00';
                                    } else {
                                        $dateFormat = 'Y-m-d';
                                        $parsedDate = substr($origValue, 0, 10);
                                    }
                                }

                                $value = \DateTime::createFromFormat($dateFormat, $parsedDate);

                                if ($value == false || !$this->funcions->isValidDateFromString($parsedDate, $dateFormat, $value)) {
                                    $value = null;
                                }

                            } else {
                                $value = null;
                            }
                            break;

                        case 'diaSetmanaDC':
                            switch ($value) {
                                case 0:
                                    $value = 0;
                                    break;
                                case 1:
                                    $value = 7;
                                    break;
                                case 8:
                                    $value = 1;
                                    break;
                                default:
                                    $value--;
                                    break;
                            }
                            break;

                        case 'stringDate': // retorna format d-m-Y
                            $value = str_replace('/', '-', $value);
                            $value = \DateTime::createFromFormat('Y-m-d H:i', substr($value, 0, 10) . ' 00:00');
                            if ($value == false) {
                                $value = '';
                            } else {
                                $value = $value->format('d-m-Y');
                            }
                            break;

                        case 'stringDateDMY':
                        case 'stringDateDMyy':
                            if ($value == false) {
                                $value = '';
                            } else {
                                $dataaux = explode(' ', $value);

                                if (count($dataaux)>1) {
                                    list($dataaux, $trash) = $dataaux; // despreciem l'hora
                                } else {
                                    $dataaux = $value;
                                }
                                $dataaux = explode('-', str_replace('/', '-', $dataaux));
                                if (count($dataaux)>2 && strlen($dataaux[2]) == 2) {
                                    // si any de dos xifres posem l'any quatre xitres
                                    // si l'anu resultat és més gran que l'any actual, prefix '19'
                                    if (intval('20' . $dataaux[2]) > intval(date("Y"))) {
                                        $dataaux[2] =  '19' . $dataaux[2];
                                    } else {
                                        $dataaux[2] =  '20' . $dataaux[2];
                                    }
                                }
                                $dataaux = implode('-', $dataaux);

                                $value = \DateTime::createFromFormat('!d-m-Y', $dataaux);
                            }

                            if (is_bool($value)) {
                                $value = '';
                            }

                            break;

                        case 'year2date':
                            if ($value == false) {
                                $value = '';
                            } else {

                                $value = \DateTime::createFromFormat('Y', $value);
                            }
                            break;


                        case 'horaCita':
                            if ($origValue !== '') {
                                if (strpos($origValue, ' ') !== false) {
                                    $arrDiaHora = explode(' ', $origValue);
                                    $value = substr($arrDiaHora[1], 0, 5);
                                }
                                $value = $this->h2m($value);
                            }
                            break;

/*       TODO:                  case 'diffTimeDate':
                            if (!is_object($origValue)) {
                                if (isset($e['format'])) {
                                    $dateFormat = $e['format'];

                                } else { // per defecte retornem minuts
                                    $dateFormat = 'i';
                                }

                                $start_date = new DateTime('2007-09-01 04:10:58');
                                $since_start = $start_date->diff(new DateTime('2012-09-11 10:25:00'));

                                $value = \DateTime::createFromFormat($dateFormat, $parsedDate);

                                if ($value == false) {
                                    $value = null;
                                }
                            } else {
                                $value = null;
                            }
                            break;*/

                        case 'estatCita':
                            if ($value === false || !$value) {
                                if ($normRow['data'] >= $today) {
                                    $value = 0; // pendiente
                                } else {
                                    $value = 4; // no presentado
                                }
                            } else {
                                $value = 3; // visitado
                            }
                            break;

                        case 'tempsCita':
                            if ($value == 0 && $e['default']) {
                                $value = $e['default'];
                            }
                            break;


                    }
                }

                if (($value == '' || !$value) && isset($e['default'])) {
                    $value = $e['default'];
                }

                if ($value !== '' && !is_null($value)) {
                    if ($label && trim($value) !== '') {
                        $value = $label . ': ' . $value . $saltLinia;
                    }
                    if (isset($e['id'])) {
                        if (isset($normRow[$e['id']])) {
                            $normRow[$e['id']] .= $saltLinia . $value;
                        } else {
                            $normRow[$e['id']] = $value;
                        }
                    }
                }
            } else {
                if ($value !== '' && !is_null($value) && strcasecmp($value,'NULL')) {
                    if (isset($normRow[$e])) {
                        $normRow[$e] .= $saltLinia . $value;
                    } else {
                        $normRow[$e] = $value;
                    }
                }
            }
        }

        return $normRow;

    }

    /**
     * Crea un UploadedFile passant la ruta d'un fitxer i el seu nom.
     *
     * @param $blob
     * @param $filename
     * @return UploadedFile
     */
    public function createFileFromPath($path, $filename)
    {
        $filename = strtolower($filename);
        $path = $this->uploadDir . '/.files/' . $path;

        // obtenim el tamany en bytes
        $size = filesize($path);

        // obtenim el mime del fitxer
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $path);
        finfo_close($finfo);

        $file = new UploadedFile($path, $filename, $mime, $size, 0);

        return $file;
    }

    /**
     * Crea un UploadedFile passant un binari blob i un nom de fitxer.
     *
     * @param $blob
     * @param $filename
     * @return UploadedFile
     */
    public function createFileFromBlob($blob, $filename)
    {
        $filename = strtolower($filename);
        $path = $this->uploadDir . '/.files/' . $filename;

        // pugem el fitxer al server i obtenim el tamany en bytes
        $size = file_put_contents($path, $blob);

        // obtenim el mime del fitxer
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $path);
        finfo_close($finfo);

        $file = new UploadedFile($path, $filename, $mime, $size, 0);

        return $file;
    }

    // associa a una entitat un fitxer d'imatge (redimensiona i crea miniatura)
    public function putImageAndThumbnailOnEntity($entity, $image, $resizeW = 1024, $resizeH = 600)
    {

        if (null !== $image) {
            $fs = $this->arxiuService->s3fs;
            $imageresize = $this->arxiuService->imageResize;

            // Redimensionamos la imagen
            $imageresize->resize($image, $resizeW, $resizeH);

            // Si tenia una anterior la eliminamos
            $fs->delete($entity->getPath());

            $key = $fs->create_key();

            // Subimos la imagen
            if ($fs->upload($key, $image) !== false) {
                $entity->setPath($key);
            }

            if(method_exists($entity, 'getPathMini')) {

                // Redimensionamos la imagen a miniatura
                $imageresize->resize($image, 200, 80);

                // eliminamos la miniatura anterior
                $fs->delete($entity->getPathMini());

                // Subimos la miniatura
                if ($fs->upload($key . '_mini', $image) !== false) {
                    $entity->setPathMini($key . '_mini');
                }
            }
        }
    }

    // get zona passada per nom
    public function getZonaByName($nom, $nivell = 2, $pais = 'ES')
    {
        $repo = $this->em->getRepository('DatabaseBundle:Zones');

        // busquem primer coincidència exacte, sinó parcial
        $query = $repo->createQueryBuilder('z')
            ->where('z.pais = :pais')
            ->andWhere('z.nivell = :nivell')
            ->andWhere('z.nom = :nom')
            ->setParameters([
                'pais' => $pais,
                'nivell' => $nivell,
                'nom' => $nom
            ])
            ->getQuery();
        $zona = $query->getResult();

        if (!$zona) {
            $query = $repo->createQueryBuilder('z')
                ->where('z.pais = :pais')
                ->andWhere('z.nivell = :nivell')
                ->andWhere('z.nom LIKE :nom')
                ->setParameters([
                    'pais' => $pais,
                    'nivell' => $nivell,
                    'nom' => '%' . $nom . '%'
                ])
                ->getQuery();
            $zona = $query->getResult();
        }

        return isset($zona[0]) ? $zona[0] : null;
    }

    // get paisIso per nom
    public function getPaisByName($nom)
    {
        $pais = $this->em->getRepository('DatabaseBundle:Paisos')
            ->findOneBy(['nom'=> $nom]);

        if ($pais) {
            return $pais->getIso();
        } else {
            return null;
        }
    }

    // get pacient per atributs (nif, mòvil, tel, o nom i cognoms)
    // TODO: revisar si funciona
    public function getPacientByAtt($p)
    {
        $aes = new AES();
        $dbh = $this->em->getConnection();
        $sql = '
            SELECT p.idPacient as id, p.historia, per.nomComplert,
                   CONCAT(per.nom, " ", per.cognom1, " ", per cognom2) nomCognoms
            FROM pacients p
            JOIN persones per on per.idPersona = p.idPersona
            WHERE p.idCentre = :centre
            ';

            if (isset($p['nomCognoms'])) {

                // AES password
                $key = $aes->getkey();
                $sth = $dbh->prepare($sql . '
            AND CONVERT(AES_DECRYPT(per.nomComplert2, :key) USING utf8) LIKE :nomCognoms'
                );

                $sth->bindParam(':key', $key, \PDO::PARAM_STR);
                $sth->bindParam(':nom', $p['nomCognoms'], \PDO::PARAM_STR);
            }


            $sth->bindParam(':centre', $this->centre, \PDO::PARAM_INT);


            $sth->execute();

            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            $result = $aes->arrayAES_Decrypt($result, array('nomComplert'));


        return isset($result[0]) ? $result[0] : null;
    }

    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    // Convierte [hh:mm] a minutos
    public function h2m($time)
    {
        if($time==0) {
            return 0;
        }

        $t = explode(":", $time);

        return $t[0]*60 + $t[1];
    }

    // Convierte minutos a [hh:mm]
    public function m2h($time)
    {
        if ($time==0) {
            return '00:00';
        }

        // validem l'string passat com a hora
        $time = $this->validateStringTime($time);

        if ($time) {

            // return true;
            $h = floor($time / 60);
            $h = strlen($h)<2 ? '0'.$h : $h;
            $m = floor($time % 60);
            $m = strlen($m)<2 ? '0'.$m : $m;

            return $h.':'.$m;

        } else {
            return null;
        }

    }

    public function validateStringTime($time)
    {
        // validem l'string passat com a hora
        $dateObj = \DateTime::createFromFormat('d.m.Y H:i', "10.10.2010 " . $time);

        if ($dateObj !== false && $dateObj && $dateObj->format('G') ==
            intval($time)) {

            // return true;
            return trim($time);
        }
        else {

            //return false;
            return null;
        }
    }

    public function getPreview($data)
    {
        if ($this->env == 'DEV') {
            dump($data);exit;
        } else {
            echo ('<pre>');
            var_dump($data);
            echo('</pre>');
            exit;
        }
    }




}
