<?php

namespace TraspasBundle\Model;

use Doctrine\DBAL\DriverManager;
use DatabaseBundle\Entity as DB;
use Symfony\Component\Validator\Constraints\DateTime;

class tenis extends dasiclinicGeneric
{
    /*
     * ********************** Traspàs tenis **************************************
     * Programa original DasiClinic
     */

    /*
     * ordre d'execució traspàs: /traspas/tenis/info
    */

    const traspasNom = 'tenis';
    const traspasHash = 'kd8923hglas8sa90248filsdfi';
    const traspasInfo =
        ['ordre_execucio_traspas' => [
            '/traspas/tenis/especialitats/d1s32014avm34ISAD',
            '/traspas/tenis/festius/d1s32014avm34ISAD',
            '/traspas/tenis/empreses/d1s32014avm34ISAD',

            '/traspas/tenis/professionals/d1s32014avm34ISAD',

            '/traspas/tenis/periodes/d1s32014avm34ISAD',
            '/traspas/tenis/horaris/d1s32014avm34ISAD',
            '/traspas/tenis/periodesAbsencia/d1s32014avm34ISAD',

            '/traspas/tenis/tarifes/d1s32014avm34ISAD',
            '/traspas/tenis/professionalsTarifes/d1s32014avm34ISAD',

            '/traspas/tenis/procedencies/d1s32014avm34ISAD',
            '/traspas/tenis/origens/d1s32014avm34ISAD',
            '/traspas/tenis/clients/d1s32014avm34ISAD',
            '/traspas/tenis/pacients/d1s32014avm34ISAD',

            '/traspas/tenis/grupTractaments/d1s32014avm34ISAD',
            '/traspas/tenis/tractaments/d1s32014avm34ISAD',
            '/traspas/tenis/tarifes/d1s32014avm34ISAD', // TODO: falta desenv. peridicitat de tarifes/tractaments
//            '/update/tenis/pacients2/d1s32014avm34ISAD',   // update pacients => posa tarifaMutua i posa origens

            '/traspas/tenis/boxes/d1s32014avm34ISAD',
            '/traspas/tenis/cites/d1s32014avm34ISAD',
            '/traspas/tenis/bloquejosAgenda/d1s32014avm34ISAD', // LIMITAR EL TRASPÀS DE BLOQUEJOS A L'ULTIM ANY

//            '/update/tenis/cites2/d1s32014avm34ISAD',      // update cites => posa tarifaMutua

            '/traspas/tenis/visites/d1s32014avm34ISAD',

//            '/update/tenis/visites2/d1s32014avm34ISAD',    // update visites => observacions de la visita

//            '/traspas/tenis/arxius/d1s32014avm34ISAD',     // es passa manulament de 500 en 500 (peta de momoria la query)

            '/traspas/tenis/carrecs/d1s32014avm34ISAD',
            '/traspas/tenis/visitesTractaments/d1s32014avm34ISAD',

            '/traspas/tenis/bancs/d1s32014avm34ISAD',
            '/traspas/tenis/formesPagament/d1s32014avm34ISAD',
            '/traspas/tenis/cobraments/d1s32014avm34ISAD',

        ]];

    public function getParams()
    {

        $params['DEV'] = [
            'nom' => self::traspasNom,
            'hash' => self::traspasHash,
            'info' => self::traspasInfo,
            'centre' => 6875,
            'professional' => null,
            'especialitat' => null,
            'conn' =>[
                'dbname' => 'tenis',
                'user' => 'root',
                'password' => '37215zx',
                'host' =>  'localhost',
                'port' => '3306',
                'driver' => 'pdo_mysql',
                'charset' => 'utf8',
            ]
        ];

        $params['PROD'] = [
            'nom' => self::traspasNom,
            'hash' => self::traspasHash,
            'info' => self::traspasInfo,
            'centre' => 13283,
            'professional' => null,
            'especialitat' => null,
            'conn' =>[
                'dbname' => 'tenis',
                'user' => 'dasieclinic',
                'password' => 'iKajkWeoi8372',
                'host' =>  'eclinic-prod.coglltc7cvjd.eu-west-1.rds.amazonaws.com',
                'port' => '3306',
                'driver' => 'pdo_mysql',
                'charset' => 'utf8',
            ]
        ];

        return $params[$this->env];
    }

    // traspassa especialitats
    public function traspasEspecialitats($params)
    {
        $result = $this->dcEspecialitats($params);
        return $result;
    }

    // traspassa categories
    public function traspasCategories($params)
    {
        $result = $this->dcCategories($params);
        return $result;
    }

    // traspassa empreses
    public function traspasEmpreses($params)
    {
        $result = $this->dcEmpreses($params);

        return $result;
    }

    // traspassa professionals
    public function traspasProfessionals($params)
    {
        $result = $this->dcProfessionals($params);

        return $result;
    }

    // traspassa professionals
    public function traspasProfessionalsTarifes($params)
    {
        $result = $this->dcProfessionalsTarifes($params);

        return $result;
    }

    // traspassa periodes
    public function traspasPeriodes($params)
    {
        $result = $this->dcPeriodes($params);

        return $result;
    }

    // traspassa horaris
    public function traspasHoraris($params)
    {
        $result = $this->dcHoraris($params);

        return $result;
    }

    // traspassa festius
    public function traspasFestius($params)
    {
        $result = $this->dcFestius($params);

        return $result;
    }

    // traspassa bloquejosAgenda
    public function traspasBloquejosAgenda($params)
    {
        $result = $this->dcBloquejosAgenda($params);

        return $result;
    }

    // traspassa periodes absencia
    public function traspasPeriodesAbsencia($params)
    {
        $result = $this->dcPeriodesAbsencia($params);

        return $result;
    }

    // traspassa tarifesMutues
    public function traspasTarifesMutues($params)
    {
        $result = $this->dcTarifesMutues($params);

        return $result;
    }


    // traspassa procedències
    public function traspasProcedencies($params)
    {

        // opcionalment passem una query i un llistat d'equivalència entre camps

//        $sql = '';  per defecte agafa la genèrica a dasiclinicGeneric
        // normalize field keys from array tractaments data to properties of entity (opcional)
        /*        $equiv = [
                    'Codi' => 'codi',
                    'Descripció' => 'nom'
                ];
        */
        $result = $this->dcProcedencies($params);

        return $result;

    }

    // traspassa origens de pacients (tags)
    public function traspasOrigens($params)
    {
        $result = $this->dcOrigens($params);

        return $result;

    }

    // traspassa clients/tutors
    public function traspasClients($params)
    {
        $result = $this->dcClients($params);

        return $result;
    }

    // traspassa pacients
    public function traspasPacients($params)
    {
        $result = $this->dcPacients($params);

        return $result;
    }

    // traspassa pacients
    public function traspasPacients2($params)
    {
        $result = $this->dcPacients2($params);

        return $result;
    }

    // traspassa grupTractaments
    public function traspasGrupTractaments($params)
    {
        $result = $this->dcGrupTractaments($params);

        return $result;
    }

    // traspassa tractaments
    public function traspasTractaments($params)
    {
        $result = $this->dcTractaments($params);

        return $result;
    }

    // traspassa bonus i associa a pacients
    public function traspasBonus($params)
    {
        $result = $this->dcBonus($params);

        return $result;
    }

    // traspassa boxes
    public function traspasBoxes($params)
    {
        $result = $this->dcBoxes($params);

        return $result;
    }

    // traspassa cites
    public function traspasCites($params)
    {
        $result = $this->dcCites($params);

        return $result;
    }

    // update cites
    public function traspasCites2($params)
    {
        $result = $this->dcCites2($params);

        return $result;
    }

    // traspassa visites
    public function traspasVisites($params)
    {
        // treballen sense episodis, n'obrim un per totes les visites d'un pacient
        $onlyOneEpisode = true;
        $result = $this->dcVisites($params, $onlyOneEpisode);

        return $result;
    }

    // update visites
    public function traspasVisites2($params)
    {
        $result = $this->dcVisites2($params);

        return $result;
    }

    // traspassa visitesTractaments
    public function traspasVisitesTractaments($params)
    {
        $result = $this->dcVisitesTractaments($params);

        return $result;
    }

    // traspassa carrecs
    public function traspasCarrecs($params)
    {
        $result = $this->dcCarrecs($params);

        return $result;
    }

    // traspassa bancs
    public function traspasBancs($params)
    {
        $result = $this->dcBancs($params);

        return $result;
    }

    // traspassa formesPagament
    public function traspasFormesPagament($params)
    {
        $result = $this->dcFormesPagament($params);

        return $result;
    }

    // traspassa cobraments
    public function traspasCobraments($params)
    {
        $result = $this->dcCobraments($params);

        return $result;
    }

    // traspassa tags arxius
    public function traspasTags($params)
    {
        $result = $this->dcTagsArxius($params);

        return $result;
    }

    // traspassa arxius
    public function traspasArxius($params)
    {
        $result = $this->dcArxius($params);

        return $result;
    }

    // traspassa informes (sense plantilla)
    public function traspasInformes($params)
    {
        $result = $this->dcInformes($params);

        return $result;
    }


    // relacions amb professionals inactius (JMG maig'18)
    public function traspasScript($params)
    {
        $params['professionals'] = [

        ];

        $result = $this->addRelacionsToAllUsers($params);

        return $result;
    }


}
