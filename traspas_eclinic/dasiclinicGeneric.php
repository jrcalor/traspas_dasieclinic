<?php

namespace TraspasBundle\Model;

class dasiclinicGeneric extends TraspasService
{
    /*
     * ********************** Traspàs genèric DasiClinic ver. 352 **************************************
     */

    /*
     * clauValor definim taules d'equivalència entre un valor origin i el valor al eClinic
     */
    const clauValor = [
        'iva' => [
            0 => 7,     // exento
            1 => 1,     // iva
            2 => 2,     // reducido
            3 => 9      // super reducido
        ],
        'estatCita' => [
            0 => 0,     // pendiente
            1 => 1,     // sala espera
            2 => 3,     // visitado/procesado
            3 => 4,     // no presentado
            4 => 2      // en curs
        ],
        'tipusCarrec' => [
            0 => 'CAR',
            1 => 'CAR',  // FRA, però tb és un CAR
            2 => 'MUT',
            3 => 'ANT',
            4 => 'ABO',
            5 => 'RET'
        ],
        'formaPagoPare' => [
            0 => 'tpag_efectivo',
            1 => 'tpag_targeta1',
            2 => 'tpag_talon',
            3 => 'tpag_targeta2',
            4 => 'tpag_financiacion',
            5 => 'tpag_cargo_dep',
            6 => 'tpag_ent_efec'
        ],
        'tipusInforme' => [
            0 => 3,  // informe administratiu
            1 => 2      // informe mèdic
        ]
    ];

    // traspassa especialiats
    public function dcEspecialitats($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }



        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_especialitats');

        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

//        dump($result);exit;

        $equiv = [
            'MODREGISTRE' => 'dasiclinicReg',
            'MODNOM' => 'nom',
            'ACTIVO' => 'actiu',
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceEspecialitats($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }

    // traspassa categories
    public function dcCategories($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_categories');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

//        dump($result);exit;

        $equiv = [
            'CATREGISTRE' => 'dasiclinicReg',
            'CATNOM' => 'nom',
            'MODREGISTRE' => 'especialitatReg',
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            if ($row['CATNOM'] == '') {
                continue;
            }
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceCategories($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }


    // traspassa empreses
    public function dcEmpreses($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_empreses');
        $sql['q'] = str_replace('[[dc]]', $params['conn']['dbname'], $sql['q']);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

//        dump($result);exit;

        $equiv = [
            'ACTIVA' => 'actiu',
            'EMPREGISTRE' => 'empresaReg',
            'EMPNOM' => 'nom',
            'EMPRAOSOCIAL' => 'cognom1',
            'EMPADRECA' => 'direccio1',
            'EMPCCP' => 'cp',
            'EMPPOBLACIO' => 'poblacio',
            'EMPPROVINCIA' => 'provincia',
            'EMPPAIS' => 'pais',
            'EMPTELEFON1' => 'telefon1',
            'EMPTELEFON2' => 'telefon2',
            'EMPNIF' => 'numDocument',
            'EMPWEB' => 'web',
            'EMPIRPF' => 'irpf',
            'EMPOBSERVACIONS' => 'observacions',
            'EMPEMAIL' => 'email',
            'EMPLOGO' => 'logo',
            'SERIE_FRA' => 'serieFac',
            'SERIE_FRE' => 'serieRec',
            'DIGITS_CONTA' => 'contaDigits'
        ];

        //   'POBREGISTRE' => 'poblacio'

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceEmpreses($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }

    // traspassa procedències
    public function dcProcedencies($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_procedencies');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // normalize field keys from array tractaments data to properties of entity
        $equiv = [
            'PROCREGISTRE' => 'dasiclinicReg',
            'PROCNOM' => 'nom'
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceProcedencies($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }
        return $result;

    }

    // traspassa Antecedents del centre
    public function dcAntecedents($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_antecedents');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // normalize field keys from array tractaments data to properties of entity
        $equiv = [
            'ANTREGISTRE' => 'dasiclinicReg',
            'tipus' => 'tipus',
            'DESCRIPCION' => 'nom'
        ];

        $normalizedArr = [];

        $grupFarm = 100000; // primer grup Fàrmacs (després suma de mil en mil)
        $grupaAnte = 200000; // primer grup Antecedents
        $ordres = [];

        // bucle pels subgrups
        foreach ($result as $row) {

            if (!in_array(strtoupper($row['tipus']), ['GRUP_FARM', 'GRUP_ANTE'])) {
                continue;
            }

            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }

            // posem l'ordre del eClinic
            switch (strtoupper($normRow['tipus'])) {
                case 'GRUP_FARM':
                    $grupFarm+= 1000;
                    $ordre = $grupFarm;
                    break;
                case 'GRUP_ANTE':
                    $grupaAnte+= 1000;
                    $ordre = $grupaAnte;
                    break;

            }

            $normRow['ordre'] = $ordre;

            $normalizedArr[] = $normRow;

            $ordres[$normRow['dasiclinicReg']] = $ordre;
        }


        // bucle pels items
        foreach ($result as $row) {

            if (!in_array(strtoupper($row['tipus']), ['ITEM_FARM', 'ITEM_ANTE'])) {
                continue;
            }

            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }

            $ordres[$row['PADRE']]++;
            $normRow['ordre'] = $ordres[$row['PADRE']];

            $normalizedArr[] = $normRow;
        }

//dump($normalizedArr);exit;

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceAntecedents($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }
        return $result;

    }

    public function dcAntecedentsPacient($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_antecedents_pacient');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;

        // normalize field keys from array tractaments data to properties of entity
        $equiv = [
            'ANTREGISTRE' => 'dasiclinicReg',
            'PACREGISTRE' => 'pacientReg',
            'DESCRIPCION' => 'nom',
            'TEXTE' => 'observacions',
            'tipus' => 'tipus',
        ];

        $normalizedGrupsArr = [];

        // grups segons PADRE
        $grups = [
            'GRUP_ANTE_DES' => 100000,  // 1
            'GRUP_FARM_DES' => 200000,  // 2
            'GRUP_ANTE' => 300000,      // 3
            'GRUP_ANTE_FAM' => 400000,  // 4
            'GRUP_FARM' => 500000       // 5
        ];

        $ordres = [];
        $conull = 0;

        // bucle pels subgrups
        foreach ($result as $row) {

            // saltem els items
            if ($row['tipus'] === 'item') {
                continue;
            }

            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }

            $normRow['tipus'] = strtoupper($normRow['tipus']);

            // posem l'ordre del eClinic

            // si no trobem el pacient, es crea el grup
            if (!isset($pacientGrup[$normRow['pacientReg']][$normRow['tipus']])) {
                $pacientGrup[$normRow['pacientReg']][$normRow['tipus']] = $grups[$normRow['tipus']];
            }

            $pacientGrup[$normRow['pacientReg']][$normRow['tipus']] += 1000;

            $ordre = $pacientGrup[$normRow['pacientReg']][$normRow['tipus']];

            if (!$ordre) {
                $conull++;
            }

            $normRow['ordre'] = $ordre;
            $normRow['centre'] = $params['centre'];

            $normalizedGrupsArr[] = $normRow;

            $ordres[$normRow['pacientReg']][$normRow['dasiclinicReg']] = $ordre;
        }

//        dump($conull);exit;
//dump($normalizedGrupsArr);exit;

        $normalizedItemsArr = [];

        // bucle pels items
        foreach ($result as $row) {

            if (strtoupper($row['tipus']) !== 'ITEM') {
                continue;
            }

            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }

            $ordres[$normRow['pacientReg']][$row['PADRE']]++;

            $ordre = $ordres[$normRow['pacientReg']][$row['PADRE']];

            if (!$ordre) {
                $conull++;
            }

            $normRow['ordre'] = $ordre;
            $normRow['centre'] = $params['centre'];

            $normalizedItemsArr[] = $normRow;
        }
//        dump($conull);exit;
//dump($normalizedItemsArr);exit;

        $normalizedArr = $normalizedGrupsArr + $normalizedItemsArr;

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceAntecedentsPacient($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }
        return $result;
    }


    // traspassa tipus de visita
    public function dcTipusVisita($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_procedencies');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

        // normalize field keys
        $equiv = [
            'Codi' => 'nom',
            'Descripció' => 'descripcio'
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceTipusVisita($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }
        return $result;

    }

    /* traspassa boxes

    * totes els boxes tenen el mateix horari, per aixo es declara general per a tots
    * son els estregistre -1 i -2 de s_estructura
    * -1 es de dilluns adivendres i -2 dissabtes
     */
    public function dcBoxes($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_boxes');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // normalize field keys from array tractaments data to properties of entity
        $equiv = [
            'BOXREGISTRE'       => 'dasiclinicReg',
            'BOXNOM'            => 'codi',
            'BOXDESCRIPCION'    => 'nom',
//            'BOXCOLOR'          => ['id' => 'color', 'type' => 'decHex'],
            'BOXORDRE'          => 'ordre',
            'ESTHORA1' => 'matiInici',
            'ESTHORA2' => 'matiFinal',
            'ESTHORA3' => 'tardaInici',
            'ESTHORA4' => 'tardaFinal',
            'ESTDIA' => ['id' => 'diaSetmana', 'type' => 'diaSetmanaDC'],
            'ESTINTERVAL' => 'interval',
            'ESTACTIVAT1' => ['id' => 'actiu', 'type' => 'bool'],
            'ESTACTIVAT2' => 'mati',
            'ESTACTIVAT3' => 'tarda',
        ];

        /* TODO: camps faltants
         * BOXLIMITE
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceBoxes($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }



        return $result;

    }


    // traspassa grupTractaments
    public function dcGrupTractaments($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_grup_tractaments');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

//        dump($result);exit;

        $equiv = [
            'GRTREGISTRE' => 'dasiclinicReg',
            'GRTNOM' => 'nom',
            'MODREGISTRE' => 'especialitatReg'
        ];

        /*
         * TODO: camps faltants
         *
         * GRTPECES
         * PEPTRACTAMENTBMP
         * GRTOOBTURACIO
         * GRTNIVELL
         * GRTPONT
         * GRTPROTESIS
         * CUENTA
         * CCOSTE
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {

                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }

            }

            // no introduim el grup 0 de tractaments
            if ($normRow['dasiclinicReg'] == 0) {
                continue;
            }

            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceGrupTractaments($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }

    // traspassa bonus
    /*
     * TODO: falta relacionar bonus amb pacients compartits, taula DC: tractaments_bonus_comp => eclinic bonusPacients
     */
    public function dcBonus($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_bonus');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;

        // normalize field keys from array tractaments data to properties of entity
        $equiv = [
            'BONREGISTRE' => 'dasiclinicReg',
            'INICIAT' => ['id' => 'dataIniciat', 'type' => 'date'],
            'SESSIONS_INICIALS' => 'sessions',
            'SESSIONS_UTILITZADES' => 'consumides',
            'CANCELAT' => ['id' => 'dataCancelat', 'type' => 'date'],
            'CANCELAT_MOTIU' => 'motiuCancelat',
            'TRAREGISTRE' => 'tractamentReg',
            'PACREGISTRE' => 'pacientReg',
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }
            $normalizedArr[] = $normRow;
        }
//dump($normalizedArr);exit;
        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceBonus($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }


    // traspassa tractaments
    public function dcTractaments($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
            $this->setClauValor(self::clauValor);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_tractaments');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // normalize field keys from array tractaments data to properties of entity
        $equiv = [
            'TRAREGISTRE' => 'dasiclinicReg',
            'TRANOM' => 'nom',
            'TRACODI' => 'codi',
            'TRAMINUTS' => 'temps',
            'TRAPREUCOSTMATERIAL' => 'costMaterial',
            'TRAPVP' => 'import',
            'INACTIVO' => 'actiu',
            'TRAIVA' => ['id' => 'impost', 'type' => 'clauValor'],
            'TRABONUS' => 'bonusReg',
            'GRTREGISTRE' => 'grupTractamentReg',
            'MODREGISTRE' => 'especialitatReg',
        ];

        /*
         * TODO: camps faltants
         *
         * TRAVENDA -> indica l'article de venda
         * TRANIVELL
         * TRAPECES
         * TRAPONT
         * TRAPBTURACIO
         * PEPTRACTAMENTBMP
         * CTRL_LABORATORI
         * TRAPROTESIS
         * ESTOC_MINIMO
         * TRAMINIMFAC
         * TRAIMATGE ?????????????????????
         * GRT
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // no introduim el grup 0 de tractaments
            if ($normRow['dasiclinicReg'] == 0) {
                continue;
            }

            $normRow['actiu'] = !$normRow['actiu'];  // invertim inactiu pq sigui actiu
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceTractaments($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }


    /*
     * Traspàs de tarifes/mútues i tractaments associats
     * (és necessari primer passar els tractaments)
     * TODO: cal periodificar les tarifes/mutues (al DC tenen el periode a tarifesTractaments)
     */
    public function dcTarifesMutues($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_tarifesmutues');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // normalize field keys from array tarifesMutues data to properties of entity
        $equiv = [
            'TRFREGISTRE' => 'dasiclinicReg',
            'TRFDESCRIPCIO' => 'nom',
            'TRFNIF' => 'numDocument',
            'TRFADRECA' => 'direccio1',
            'TRFCP' => 'cp',
            'POBNOM' => 'poblacio',
            'TRFTELEFON1' => 'telefon1',
            'TRFTELEFON2' => 'telefon2',
            'TRFFAX' => 'fax',
            'TRFEMAIL' => 'email',
            'TRFOBSERVACIONS' => 'observacions',
            'MODIFICA_PRECIO_MUTUA' => 'modificapreumutua',
            'INACTIVA' => ['id' => 'actiu', 'type' => 'not'],       // not inverteix el valor del camp
            'CUENTA' => 'cuenta',
            'CCOSTE' => 'ccoste'
        ];

        $tarifesArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            $normRow['esmutua'] = true;     // per defecte són totes mutua

            $tarifesArr[] = $normRow;
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_tarifestractaments');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // normalize field keys from array tarifesTractaments data to properties of entity
        $equiv = [
            'TRFREGISTRE' => 'tarifaReg',
            'TRAREGISTRE' => 'tractamentReg',
            'PREU' => 'preu',
            'FACTURAR_MUTUA' => 'facturarMutua',
            'IMPORT_MUTUA' => 'importMutua',
            'PAGAR_MUTUA' => 'pagarMutua',
            'INACTIVO' => ['id' => 'actiu', 'type' => 'not'],
            'CODI_MUTUA' => 'codiMutua',
            'CONCEPTE_MUTUA' => 'concepteMutua',
            'COMISION_FIJA_PRF' => 'comisioFixaPrf',
            'PIDE_VOLANTE' => 'demanaVolant'
        ];

        $tractamentsTarifesArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }
            $tractamentsTarifesArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($tarifesArr);
                $this->getPreview($tractamentsTarifesArr);
                break;
            case 'traspas':
                $result = $result = $this->serviceTarifesMutues($tarifesArr, $tractamentsTarifesArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }

    // traspassa periodes
    public function dcPeriodes($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_periodes');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;

        $equiv = [
            'PERREGISTRE' => 'dasiclinicReg',
            'PERNOM' => 'nom',
            'PERINICI' => ['id' => 'inici', 'type' => 'date'],
            'PERFINAL' => ['id' => 'final', 'type' => 'date'],
            'PRFREGISTRE' => 'professionalReg',
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }
            $normRow['dasiclinicId'] = $params['centre'];   // guardarem el centre a periode per recuperar-lo a horaris
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->servicePeriodes($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }

    // traspassa horaris
    public function dcHoraris($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_horaris');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;

        $equiv = [
            'ESTREGISTRE' => 'dasiclinicReg',
            'ESTHORA1' => 'matiInici',
            'ESTHORA2' => 'matiFinal',
            'ESTHORA3' => 'tardaInici',
            'ESTHORA4' => 'tardaFinal',
            'ESTDIA' => ['id' => 'diaSetmana', 'type' => 'diaSetmanaDC'],
            'ESTINTERVAL' => 'interval',
            'ESTACTIVAT1' => ['id' => 'actiu', 'type' => 'bool'],
            'ESTACTIVAT2' => 'mati',
            'ESTACTIVAT3' => 'tarda',
            'PERREGISTRE' => 'periodeReg'
        ];
/*
 *   TODO: a tenir en compte pel futur, límit de num. cites per hora o franja
 *   "LIMIT_HORA" => "2"
    "LIMIT_MATI" => "0"
    "LIMIT_TARDA" => "3"
    "ESTINTERVAL_T" => null
    "FEXREGISTRE" => null
 */
        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceHoraris($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }

    // traspassa festius
    public function dcFestius($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_festius');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;

        $equiv = [
            'FESDATA' => ['id' => 'data', 'type' => 'date'],
            'FESNOM' => 'nom',
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceFestius($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // traspassa bloquejos agenda
    // (són cites amb pacregistre = -1)
    public function dcBloquejosAgenda($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
            $this->setClauValor(self::clauValor);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_bloquejos_agenda');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'CITREGISTRE'   => 'dasiclinicReg',
            'CITDATA'       => ['id' => 'data', 'type' => 'date'],
            'CITHORA'       => ['id' => 'hora', 'type' => 'horaCita'],
            'CITTEMPS'      => ['id' => 'temps', 'type' => 'tempsCita', 'default' => 60],
            'CITOBSERVACIONS' => ['id' => 'descripcio', 'output' => 'raw'],
            'PRFREGISTRE'   => 'professionalReg'
        ];

        $normalizedArr = [];

        while ($row = $stmt->fetch()) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':

                $result = $this->serviceBloquejosAgenda($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }


    // traspassa periodes absencia
    public function dcPeriodesAbsencia($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_periodes_absencia');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;

        $equiv = [
            'FESREGISTRE' => 'dasiclinicReg',
            'FESNOM' => 'nom',
            'FESDATAINICI' => ['id' => 'inici', 'type' => 'date'],
            'FESDATAFINAL' => ['id' => 'final', 'type' => 'date'],
            'PRFREGISTRE' => 'professionalReg',
            'FESTIPO' => 'tipus',

        ];

        $normalizedArr = [];

        $profRepo = $this->em->getRepository('DatabaseBundle:Professionals');

        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // si no és tot el dia (0), llavors és matí (1) o tarda (2) i guarda també l'hora
            // agafem hores inici i fi de matí o tarda
            if ($normRow['tipus'] > 0) {
                $professional = $profRepo->findOneBy([
                    'dasiclinicReg' => $normRow['professionalReg'],
                    'centre' => $this->centre]);

                $horari = $this->legacyAgendaService->get_horario($professional, $normRow['inici']);

                switch ($normRow['tipus']) {
                    case 1:
                        if ($horari) {
                            $normRow['horaIni'] = $horari['matiInici'];
                            $normRow['horaFi'] = $horari['matiFinal'];
                        } else {
                            $normRow['horaIni'] = 480;
                            $normRow['horaFi'] = 840;
                        }
                        break;

                    case 2:
                        if ($horari) {
                            $normRow['horaIni'] = $horari['matiInici'];
                            $normRow['horaFi'] = $horari['matiFinal'];
                        } else {
                            $normRow['horaIni'] = 960;
                            $normRow['horaFi'] = 1140;
                        }
                        break;
                }

            }

            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->servicePeriodesAbsencia($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }

    // traspassa professionals
    public function dcProfessionals($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_professionals');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // normalize field keys from array professional data to properties of entity
        $equiv = [
            'PRFREGISTRE'   => 'dasiclinicReg',
            'PRFCODI'       => 'acronim',
            'PRFNOM'        => 'nom',
            'PRFCOGNOMS'    => 'cognoms',
            'DNI'           => ['id' => 'numDocument', 'type' => 'cleanSpecChar'],
            'POBNOM'        => 'poblacio',
            'PRFADRECA'    => 'direccio1',
            'PRFCCP'         => 'cp',
            'PRFTELEFON1'   => 'telefon1',
            'PRFTELEFONS2'   => 'telefon2',
            'PRFEMAIL'      => 'email',
            'PRFOBSERVACIONS' => ['id' => 'observacions', 'type' => 'rtf', 'output' => 'raw'],
            'PRFBS_AGENDA'  => 'obsAgenda',
            'COLEGIADO'     => 'numColegiat',
            'MODREGISTRE'   => 'especialitatReg',
            'ACTIVO'        => 'actiu',
            'PRFCOLOR'      => ['id' => 'color', 'type' => 'decHex'],
            'PRFIMATGE'     => 'foto',
            'CATREGISTRE'   => 'categoriaReg',
            'EMPREGRISTRE'  => 'empresaReg'

            /*
             * TODO:
             * 'USABOX'
             * 'USAURGENCIAS'
             * 'AVISO_SMS'
             * 'CENTRO_ORIGEN'
             * 'COLABORADOR'
             * 'PRFPERCENTATGE' =>
             * 'PERCENTATGE_MUTUA' =>
             * 'IMPORT_FIX'
             * 'IPORT_MINIM'
             * 'PRFPERCENTATGEMAT'
             * 'TIPUS_LIQUIDACO'
             * 'ORDRE_PERCENTATGES'
             * 'IRPF' (ara al eclinic el té l'empresa)
             * 'ENTCODI' (entitat bancaria)'
             * 'ENTREGISTRE'
             * 'PRFAGENCIA'
             * 'PRFDIGITS'
             * 'PRFCOMPTE'
             * 'PRFSERIE'
             * 'PRFIBAN'
             * 'CITA_ONLINE'
             * 'IDAGENDA'
             *
             */

        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            $normRow['teHorari'] = true;
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceProfessionals($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    public function dcProfessionalsTarifes($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_professionalstarifes');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // normalize field keys from array professional data to properties of entity
        $equiv = [
            'PRFREGISTRE'     => 'professionalReg',
            'TRFREGISTRE'     => 'tarifaReg',
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }

            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceProfessionalsTarifes($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }


    // traspassa origens de pacients
    public function dcOrigens($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_pacientsorigens');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'ORGREGISTRE'  => 'dasiclinicReg',
            'ORGNOM'  => 'nom',
//            'OTROS' => 'TODO'
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normRow['grup'] = 'PAC_ORIGEN_TYPE';   // passem el grup del tag per defecte
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceTags($normalizedArr);
                break;
            case 'update':
//                $result = $this->updateTags($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }



    // traspassa pacients
    public function dcPacients($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_pacients');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'PACREGISTRE'   => 'dasiclinicReg',
            'PAC_HISTORIA_VISIBLE'  => 'historia',
            'PACNOM'        => 'nom',
            'PACCOGNOM1'    => 'cognom1',
            'PACCOGNOM2'    => 'cognom2',
            'PACNIF'        => 'numDocument',
            'PACADRECA'     => 'direccio1',
            'PACCP'         => 'cp',
            'POBNOM'        => 'poblacio',
            'PACTELEFON1'   => 'telefon1',
            'PACTELEFON2'   => 'telefon2',
            'FOTOGRAFIA'    => 'foto',
            'PACEMAIL'      => 'email',
            'PACDATAINICI'  => ['id'=>'dataAlta', 'type' => 'date'],
            'MODIFICADO_FECHA' => ['id'=>'dataMod', 'type' => 'date'],
            'PACNAIXEMENT'  => ['id'=>'dataNaixement', 'type' => 'date'],
            'PACLOPD'       => ['id' => 'lopd', 'type' => 'bool'],
            'PACSEXE'       => ['id' => 'sexe', 'type' => 'sexe'],
            'AVISO_SMS'     => 'sms',
            'EMAILING'      => 'mailing',
            'PACVOLFACTURA'     => 'fraPacient',
            'PACSALDO'      => 'saldo',

            'PACOBSERVACIONS'    => ['id' => 'observacions', 'type' => 'rtf', 'output' => 'raw'],
            /* comentar 'type' => 'rtf', només a traspas NO DASICLINIC */
            'PACPROFESSIO'      => ['id' => 'observacions', 'label' => 'Professión', 'output' => 'raw'],
            'PACESTCIVIL'   =>  ['id' => 'observacions', 'label' => 'Estado civil', 'output' => 'raw'],
            'PACFILLS   '      => ['id' => 'observacions', 'label' => 'Hijos', 'output' => 'raw'],
            'PACPOLISSA'      => ['id' => 'observacions', 'label' => 'Póliza', 'output' => 'raw'],
            'PACNSS'      => ['id' => 'observacions', 'label' => 'NSS', 'output' => 'raw'],
            'PROCREGISTRE'  => 'procedenciaReg',
            'TRFREGISTRE'  => 'tarifaReg',
            'ORGREGISTRE' => 'origenReg'
        ];


        /*
         * --- camps que no són al eclinic
         *
         * PACIDIOMA
         * PACFRATUTOR
         * PACDESCOMPTE
         * PACFILLS
         * PACESTCIVIL
         * PACESPAC
         * PACREMITENT
         * ACUREGISTRE
         * PACOBS_SITBUCAL
         *
         *     "PAC_TE_HISTORIA" => "1"
         *      "PAC_HISTORIA_VISIBLE" => "1"
         *
         *DOM_NOM" => "GARCIA VÁZQUEZ, CARLOS"
        "DOM_ADRECA" => "ELS PINS, 111 -CASA"
        "DOM_POBREGISTRE" => "4184"
        "DOM_CP" => "08470"
        "DOM_TITULAR" => "GARCIA VÁZQUEZ, CARLOS"
        "DOM_ENTREGISTRE" => "0"
        "DOM_AGENCIA" => ""
        "DOM_DIGITS" => ""
        "DOM_COMPTE" => ""
        "DOM_DNI" => ""
        "DOM_ERROR" => ""
        "DOM_ENTCODI" => ""
         *
         * "EMPRESA_TIPO_PAC" => "0"
            "EMPRESA_TIPO" => "0"
            "EMPRESA_REF" => ""
            "EMPRESA_GL" => ""
         *
         *
         * CENTRO_ORIGEN
         *
         * PACTUTOR => és el PACREGISTRE del pacient que fa de tutor (mateixa taula pacients i tutors)
         * PACTUTOR2
         * TODO: crear entitat client/tutor sobre els pacient del DC resultats de la query següient:
         * SELECT * FROM summae.s_pacients where pactutor>0
         *
         * "PDCREGISTRE" => "0"
         * "CMRREGISTRE" => "0"
         * CENTRE => null   (TODO: multicentre?)
         *
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->servicePacients($normalizedArr);
                break;
            case 'update':
                $result = $this->updatePacients($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // update pacients
    public function dcPacients2($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_pacients');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // equivalència de columnes de la taula amb els camps del eClinic
/*        $equiv = [
            'PACREGISTRE'   => 'dasiclinicReg',
            'TRFREGISTRE'  => 'tarifaReg',
        ];*/

/*        $equiv = [
            'PACREGISTRE'   => 'dasiclinicReg',
            'ORGREGISTRE'  => 'origenReg',
        ];*/

        $equiv = [
            'PACREGISTRE'   => 'dasiclinicReg',
            'POBNOM'  => 'poblacio',
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->servicePacients($normalizedArr);
                break;
            case 'update':
                $result = $this->updatePacients($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }


    // traspassa clients/tutors
    public function dcClients($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_clients');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
//dump($result);exit;
        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'PACREGISTRE'   => 'dasiclinicReg',
            'PACNOM'        => 'nom',
            'PACCOGNOM1'    => 'cognom1',
            'PACCOGNOM2'    => 'cognom2',
            'PACNIF'        => 'numDocument',
            'PACADRECA'     => 'direccio1',
            'PACCP'         => 'cp',
            'POBNOM'        => 'poblacio',
            'PACTELEFON1'   => 'telefon1',
            'PACTELEFON2'   => 'telefon2',
            'FOTOGRAFIA'    => 'foto',
            'PACEMAIL'      => 'email',
            'PACDATAINICI'  => ['id'=>'dataAlta', 'type' => 'date'],
            'MODIFICADO_FECHA' => ['id'=>'dataMod', 'type' => 'date'],
            'PACNAIXEMENT'  => ['id'=>'dataNaixement', 'type' => 'date'],
            'PACLOPD'       => ['id' => 'lopd', 'type' => 'bool'],
            'PACSEXE'       => ['id' => 'sexe', 'type' => 'sexe'],
            'AVISO_SMS'     => 'sms',
            'EMAILING'      => 'mailing',
            'PACVOLFACTURA'     => 'fraPacient',
            'PACSALDO'      => 'saldo',

            'PACOBSERVACIONS'    => ['id' => 'observacions', 'type' => 'rtf', 'output' => 'raw'],
            'PACPROFESSIO'      => ['id' => 'observacions', 'label' => 'Professión', 'output' => 'raw'],
            'PACESTCIVIL'   =>  ['id' => 'observacions', 'label' => 'Estado civil', 'output' => 'raw'],
            'PACFILLS   '      => ['id' => 'observacions', 'label' => 'Hijos', 'output' => 'raw'],
            'PACPOLISSA'      => ['id' => 'observacions', 'label' => 'Póliza', 'output' => 'raw'],
            'PACNSS'      => ['id' => 'observacions', 'label' => 'NSS', 'output' => 'raw'],
            'PROCREGISTRE'  => 'procedenciaReg',
            'TRFREGISTRE'  => 'tarifaReg',
            'ORGREGISTRE' => 'origenReg'
        ];


        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceClients($normalizedArr);
                break;
            case 'update':
//                $result = $this->updateClients($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }


    // traspassa ctes
    public function dcCites($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
            $this->setClauValor(self::clauValor);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_cites');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

//        $result = $stmt->fetchAll();

//        dump(array_slice($result, 100));exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'CITREGISTRE'   => 'dasiclinicReg',
            'CITDATA'       => ['id' => 'data', 'type' => 'date'],
            'EMISION'       => ['id' => 'dataCrea', 'type' => 'date'],
            'EXERCICI'      => 'exercici',
            'CITHORA'       => ['id' => 'hora', 'type' => 'horaCita'],
            'CITVHORA'      => 'horaVisita',
            'CITTEMPS'      => ['id' => 'temps', 'type' => 'tempsCita', 'default' => 60],
            'CITESTAT'      => ['id' => 'estatCita', 'type' => 'clauValor'],
            'CITEXTRA'      => 'urgencia',
            'CITTIPUS'      => 'tipusVisita',
            'CITOBSERVACIONS' => ['id' => 'observacions', 'output' => 'raw'],
            'PRFREGISTRE'   => 'professionalReg',
            'PACREGISTRE'   => 'pacientReg',
            'TRAREGISTE'    => 'tractamentReg',
            'BOXREGISTRE'   => 'boxReg',
            'TRFREGISTRE'   => 'tarifaReg',
            'MODREGISTRE'   => 'especialitatReg'
        ];

        $normalizedArr = [];

        while ($row = $stmt->fetch()) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];

            // només traspàs NO_DASICLINIC
/*            $normRow['especialitat'] = $params['especialitat'];
            $normRow['data'] = new \DateTime('today');*/

            // si no es troba horavisita posem la mateixa de la cita (visita no agendada)
            if (isset($normRow['horaVisita'])) {
                $horaVisita = $this->validateStringTime($normRow['horaVisita']);
            } else {
                $horaVisita = $this->m2h($normRow['hora']);
            }

            if(!$horaVisita) {
                continue;
            }

            $normRow['dataVisitat'] = new \DateTime($normRow['data']->format('Y-m-d') . ' ' . $horaVisita);

            // visita processada si estat és 2
            if ($normRow['estatCita'] == 2) {
                $normRow['estatAdm'] = 1;
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':

                $result = $this->serviceCites($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // update ctes (afegeix TarifaMutua)
    public function dcCites2($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
            $this->setClauValor(self::clauValor);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_cites');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

//        $result = $stmt->fetchAll();

//        dump(array_slice($result, 100));exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'CITREGISTRE'   => 'dasiclinicReg',
            'TRFREGISTRE'   => 'tarifaReg'
        ];

        $normalizedArr = [];

        while ($row = $stmt->fetch()) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'update':

                $result = $this->updateCites($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // traspassa episodis
    public function dcEpisodis($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_episodis');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
// dump($result);exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'EPIREGISTRE'   => 'dasiclinicReg',
            'EPIDATA'       => ['id' => 'data', 'type' => 'date'],
            'EPIDATA_FI'       => ['id' => 'dataAlta', 'type' => 'date'],
            'EPINUMERO_PAC' => 'numEpisodi',

            'anamnesis'    => ['id' => 'anamnesis', 'type' => 'rtf', 'output' => 'raw'],
            'exploracio'    => ['id' => 'exploracio', 'type' => 'rtf', 'output' => 'raw'],
            'proves_complementaries'    => ['id' => 'comentaris', 'type' => 'rtf', 'output' => 'raw'],
            'diagnostic'    => ['id' => 'diagnostic', 'type' => 'rtf', 'output' => 'raw'],
            'judici'    => ['id' => 'judici', 'type' => 'rtf', 'output' => 'raw'],
            'tractament'    => ['id' => 'comentaris', 'type' => 'rtf', 'output' => 'raw'],
            'evolucio'    => ['id' => 'evolucio', 'type' => 'rtf', 'output' => 'raw'],
            'evolucio_fisio'    => ['id' => 'evolucio', 'type' => 'rtf', 'output' => 'raw'],

            'PACREGISTRE'     => 'pacientReg',
            'PRFREGISTRE'    => 'professionalReg',
            'MODREGISTRE'   => 'especialitatReg',
            'CITREGISTRE'   => 'citaReg',
        ];

        /*
         * TODO camps pendents:
         * EPIMALATIA
         * CENTRO_ORIGEN
         *
         * FRAREGISTRE
         * CIE_FACTURADO
         * IMPORTE_FACTURADO
         *
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $e = $equiv[$key];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

//        dump($normalizedArr);exit;

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceEpisodis($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // traspassa visites
    public function dcVisites($params, $onlyOneEpisode = false)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_visites');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
// dump($result);exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'VISREGISTRE'   => 'dasiclinicReg',
            'VISDATA'       => ['id' => 'data', 'type' => 'date'],
            'VISOBSERVACIONS'   =>  ['id' => 'observacionsVisita', 'output' => 'html', 'type' => 'htmlentities&utf8decode'],  // cleanControlChar
            'OBS_MED'       =>  ['id' => 'observacionsVisita', 'output' => 'html', 'type' => 'htmlentities&utf8decode'],  // cleanControlChar

            'OBS_ADM'       => ['id' => 'observacionsCita', 'output' => 'html', 'type' => 'htmlentities&utf8decode'], // cleanControlChar

            'PACREGISTRE'     => 'pacientReg',
            'PRFREGISTRE'    => 'professionalReg',
            'BOXREGISTRE'   => 'boxReg',
            'CITREGISTRE'   => 'citaReg',
            'EPIREGISTRE'   => 'episodiReg',
        ];

        /*
         * TODO camps pendents:
         * VISTANCAT
         * VISINFORME
         * VISCARREGADA
         * COBRAT
         * TOTAL
         * TOTAL_MUTUA
         * FACTURADA
         * CARREGISTRE
         * CARREGISTRE_MUTUA
         * TRACTAMENTS_UNITATS ????
         *
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceVisites($normalizedArr, $onlyOneEpisode);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // update visites
    public function dcVisites2($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_visites');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

//        $result = $stmt->fetchAll();
//        dump($result);exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'VISREGISTRE'   => 'dasiclinicReg',
            'VISOBSERVACIONS'   =>  ['id' => 'observacionsVisita', 'output' => 'html', 'type' => 'htmlentities&utf8decode']
        ];

        $normalizedArr = [];
        while ($row = $stmt->fetch()) {

            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'update':
                $result = $this->updateVisites($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // traspassa visitesTractaments
    public function dcVisitesTractaments($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_visites_tractaments');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

// dump($result);exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'TRVREGISTRE'   => 'dasiclinicReg',
            'VISREGISTRE'   => 'visitaReg',
            'TRAREGISTRE'   => 'tractamentReg',
            'CARREGISTRE'   => 'carrecReg',
            'TRFREGISTRE'   => 'tarifaReg',
            'BONREGISTRE'   => 'bonusReg',

            'OBSERVACIONS'  =>  'observacionsTrac',
            'NUM_VISITES'   => 'numVisites',
            'TRVIMPORT_UNITARI' => 'importUnitari',
            'UNITATS'       =>  'unitatsTrac',
            'DTO_PERCENT'   =>  'percenDte',
            'DTO_IMPORT'    =>  'importDte',
            'IMPORT_TOTAL'  =>  'importTotal',
            'COST_MATERIAL'  =>  'costMaterial',
            'IVAPERECENT'   =>  'perImpost',
            'IVAIMPORT'     =>  'importImpost',
            'IMPORTE_MUTUA' =>  'importMutua',
            'TOTAL_MUTUA'   =>  'totalMutua',
            'NETO_MUTUA'     => 'netMutua'

        ];

        /*
         * TODO camps pendents:
         * PREREGISTRE => pressupostReg
         * PTRREGISTRE ?????
         * TEXTE    => important per odontología (guarda el num de la peça)
         * PECA1
         * PECA2
         * TRVOBT2 4 5 6 8 ????
         * TANCAT ??
         *
         * COST_MATERIAL_PRF
         * PENDENT_MATERIAL
         * TE_MATERIAL
         * MATERIAL_PAC (magatzem)
         * MATERIAL_MUT (magatzem)
         * MAETRIAL_PRF_MUTUA  (magatzem)
         *
         * PRFREGISTRE (no cal)
         * PACREGISTRE (no cal)
         * ENVREGISTRE
         * IVAREGISTRE (no cal)
         * DATA (ja tenim data al càrrec)
         * TRACODI (no cal)
         * PENDIENTE_ENVIO
         * NUM_VOLANTE
         * MVEREGISTRE => el tenim al càrrec
         *
         * CARREGISTRE_MUTUA (de la taula de càrrecs mutua)
         * TOTAL_MUTUA
         * BASE_IMP_MUTUA
         * IVAIMPORT_MUTUA
         * IRPF_MUTUA
         * NETO_MUTUA
         * FACTURA_MUTUA
         *
         * PFRAREGISTRE
         * FACTURA_MANUAL
         *
         * APLICA_COMISION_CONCEPTO
         * HONORARIS_PROVISIONAL
         * HONORARIS_MODIFICA
         *
         * COBRADO_PAC
         * COBRADO_MUT
         * COFREGISTRE
         *
         * TIPUS_LIQUIDACIO
         * LIQREGISTRE
         * LIQ_COMISION
         * LIQ_COMISION_MUTUA
         * LIQ_FIJO
         * LIQ_COMISION_BANCO
         * LIQ_AJUSTES_MUTUA
         * LIQ_IMPORTE
         * LIQ_A_LIQUIDAR
         *
         * CON_SESION
         * CONCEPTE_MANUAL_EMP
         *
         * MAGRESITRE
         *
         */

        $normalizedArr = [];
        while ($row = $stmt->fetch()) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceVisitesTractaments($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // traspassa carrecs
    // la query fa un join entre carregs i carregs linies
    public function dcCarrecs($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
            $this->setClauValor(self::clauValor);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_carrecs');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
// dump($result);exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'CCREG'   => 'dasiclinicReg',
            'ESTAT'         => ['id' => 'tipusCarrec', 'type' => 'clauValor'],
            'CARDATA'       => ['id' => 'dataCarrec', 'type' => 'date'],
            'EXERCICI'      => 'exercici',
            'CODI'          => 'codiFra',
            'CARGO'         => [
                'id' => 'codiCarrec',
                'type' => 'strpadleft',
                'string' => '0',
                'length' => 7
            ],
            'TOTAL'         => 'import',
            'COST_MATERIAL' => 'costMaterial',
            'TOTAL_COBRAT'  => 'totalCobrat',
            'PENDENT_COBRAR'    => 'pendentCobrar',
            'TOTAL_LIQUIDACIO' => 'totalLiquidacio',
            'TOTAL_BASE'        => 'totalBase',
            'BASE_EXENTA'       => 'baseExenta',
            'IMPORT_IVA'      => 'totalImpost',
            'DEPOSITO'          => 'diposit',
            'PACREGISTRE'     => 'pacientReg',
            'PRFREGISTRE'    => 'professionalReg',
            'FRAREGISTRE'   => ['id' => 'fraReg', 'type' => 'zeroIsNull'],
            'FRADATA'       => ['id' => 'dataFra', 'type' => 'date'],
            'TRFREGISTRE'   => 'tarifaReg',
            'TUTOR'         => 'clientReg',
            'EMPREGISTRE'   =>  'empresaReg',
            'CONTA'         => 'conta',
            'CONCEPTE'      => 'concepte'
        ];

        /*
         * TODO camps pendents:
         *
         * SERIE (juntament amb l'exercici serveixen per muntar CODI: 'exercici + la serie + el numero)
         * MATERIAL_PROFESSIONAL
         * COST_MATERIAL_PRF
         * PREREGISTRE
         * PENDENT_MATERIAL
         * LIQUIDAT
         * LIQUIDACIO_PAGADA
         * QUOREGISTRE
         * TOTAL_IVA
         *
         * MUTUA (bool, si és mutua)
         *
         * NUMERO_DESPOSITO (tb es per formar el CODI)
         *
         * RECTIFICA_FRA_EXERCICI (?)
         * RESTIFICA_BESTRETA (?)
         *
         * OJO! a CARREGS_LINEAS hi ha else següents camps:
         *
         * unitats
         * import_unitari
         * import_total
         * poriva
         * import_iva
         * base
         * dto_import
         * dto_percent
         *
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // dataDoc serà igual a dataCarrec
            $normRow['dataDoc'] = $normRow['dataCarrec'];

            // si no és tipus CAR, posa el codi tal i com ve
            if ($normRow['tipusCarrec'] !== 'CAR') {
                $normRow['codiFra'] = str_replace('/A', '/A/', $normRow['codiFra']);
                $normRow['codiFra'] = str_replace('/R', '/R/', $normRow['codiFra']);

                $normRow['codiCarrec'] = $normRow['codiFra'];

                // si té càrrec el dipòsit posem el mateix import a total
                if ($normRow['diposit'] > 0) {
                    $normRow['import'] = $normRow['diposit'];

                    // si té dipòsit però no és anticipo, és càrrec al dipòsit
                    // posem l'import del dipòsit en negatiu
                    // **** ULL, crec que al DC ja venen en negatiu
/*                    if ($normRow['tipusCarrec'] !== 'ANT') {
                        $normRow['diposit'] *= -1;
                    }*/
                }
            }



            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

//dump($normalizedArr);exit;
        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceCarrecs($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // traspassa bancs (crea bancCompte i bancEntitat)
    public function dcBancs($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_bancs');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

//        dump($result);exit;

        $equiv = [
            'BANREGISTRE' => 'dasiclinicReg',
            'ENTREGISTRE' => 'bancEntitatReg',
            'ENTITAT' => 'entitat',
            'ENTCODI' => 'entCodi',
            'ENTNOM' => 'entNom',
            'ENTBIC' => 'entBIC',
            'AGENCIA' => 'agencia',
            'CC' => 'cc',
            'DESCRIPCIO' => 'nom',
            'DIGITS' => 'digits',
            'CUENTA' => 'compte',
            'CCOSTE' => 'ccost',
            'IBAN' => 'iban',
            'SUFIJO' => 'sufix'
        ];

        /*
         * TODO camps pendents:
         *
         * EMPREGISTRE (empresa)
         *
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }

            if (isset($normRow['compte'])) {
                $normRow['compte'] = $normRow['digits'] . $normRow['compte'];
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceBancs($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }


    // traspassa formesPagament (crea bancCompte i bancEntitat subordinats)
    public function dcFormesPagament($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
            $this->setClauValor(self::clauValor);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_formes_pagament');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

//        dump($result);exit;

        $equiv = [
            'FORMA_PAGO' => ['id' => 'formaPagoPare', 'type' => 'clauValor', 'failValue' => null],
            'NOMBRE' => 'nom',
            'COMISION' => 'perComisio',
            'BANREGISTRE' => 'bancCompteReg',
            'DEPOSITO' => 'diposit',
            'INACTIVA' => 'inactiu'
        ];

        /**
         * TODO camps pendents:
         *
         * CUENTA
         * TIPO_CONTA
         * SISTEMA
         * GRUP
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // guardem el id de la forma de pagament del DC a dasiclincReg
            $normRow['dasiclinicReg'] = $row['FORMA_PAGO'];

            // si l'id és 0 (CAIXA) setegem idBancCompte a 1
            if ($normRow['bancCompteReg'] == 0) {
                $normRow['idBancCompte'] = 1;
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];

            $normalizedArr[] = $normRow;
        }

//        dump($normalizedArr);exit;

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceFormesPagament($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }

    // traspassa cobraments i crea cobramentsCarrecs
    public function dcCobraments($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
            $this->setClauValor(self::clauValor);
        }

        // la query fa un join entre cobraments i cobramentsCarregs
        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_cobraments');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
// dump($result);exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [

            // camps de COBRAMENTS
            'COBREG'   => 'dasiclinicReg',
            'PACREGISTRE'   => 'pacientReg',
            'COBRO_TEXTO'   => 'codi',
            'COBDATA'       => ['id' => 'dataCobrament', 'type' => 'date'],
            'ULTMODIFICACIO' => ['id' => 'dataModificat', 'type' => 'date'],
            'EXERCICI'      => 'exercici',
            'TOTAL'         => 'importCobrat',
            'DEPOSITO'      => 'diposit',
            'FORMA_PAGO'    => 'formaPagamentReg',
            'COMISION_BANCO' => 'comisioBanc',
            'TOTAL_LIQUIDACIO' => 'totalLiquidacio',
            'LIQUIDAT'          => 'liquidat',
            'LIQUIDACIO_PAGADA' => 'liquidacioPagada',
            'ES_DEPOSITO'   => 'esDiposit',
            'ES_ANTICIPO'   => 'esAnticipo',
            'ES_REINTEGRO'  => 'esReintegro',

            // camps de COBRAMENTS_CARREGS
//            'CCCOB' => 'cccobReg',
            'CCCAR'   => 'carrecReg',
            'IMPORT_PARCIAL' => 'importParcial',
            'DEPOSITO_PARCIAL' => 'dipositParcial',
            'MULTI' => 'multi'
        ];

        /**
         * TODO traspàs de taula COBRAMENTS_BESTRETES  nova ver. 352
         *
         * <<<< Per quan al eClinic es puguin facturar les bestretes >>>>>
         * relaciona quins pagaments de quines bestretes s'ha cobrat
         *
         *  TODO camps pendents:
         *
         * USRREGISTRE --> no es passar perquè no es traspassen usuaris
         * PRFREGISTRE --> no cal pq es pot agafar del càrrec
         * PENDENT_MATERIAL
         * COST_MATERIAL_IRPF
         * LIQREGISTRE (FK de Liquidacions)
         * FRAREGISTRE (???)
         * FRACODI ???
         *
         * NUMERO -> es un comptador de rebuts
         * per exemple si un carrec es el 123 i te 3 cobraments
         * seran el 123/1, 123/2 i 123/3
         *
         * COBRO_NUMERO ???

         *
         * CIERRA -> moviment de conta?
         * CONTA -> contabilitzat?
         *
         * QUOREGISTRE
         *
         * PREREGISTRE (pressupostos)
         */

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            /**
             * TODO: pels càrrecs al dipòsit  posar importParcial en negatiu i l'idCarrec del anticipo (bestreta)
             * i tipusCobrament a CDP
             */

            // setegem tipusCobrament
            $normRow['tipusCobrament'] = 'COB';

            if (isset($normRow['esAnticipo']) && $normRow['esAnticipo']) {
                $normRow['tipusCobrament'] = 'ANT';

            } elseif (isset($normRow['esReintegro']) && $normRow['esReintegro']) {
                $normRow['tipusCobrament'] = 'RET';
            }

            // si hi ha data de última modificació del cobrament guardem aquesta
            if (isset($normRow['dataModificat'])) {
                $normRow['dataCobrament'] = $normRow['dataModificat'];
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

//dump($normalizedArr);exit;
        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceCobraments($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }


    // traspassa tags arxius
    public function dcTagsArxius($params)
    {

        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_tags_arxius');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

//        dump($result);exit;

        $equiv = [
            'CLAREGISTRE' => 'dasiclinicReg',
            'CLANOM' => 'nom'
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $equiv)) {
                    $normRow[$equiv[$key]] = $value;
                }
            }
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceTags($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;

    }


    // traspassa arxius
    public function dcArxius($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_arxius');
        $sql = str_replace('[[dc]]', strtolower($params['conn']['dbname']), $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
// dump($result);exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
            'MINREGISTRE'   =>  'dasiclinicReg',
            'IMGREGISTRE'   =>  'imatgeReg',
            'DOCREGISTRE'   =>  'documentReg',
            'PACREGISTRE'   =>  'pacientReg',
            'CLAREGISTRE'   =>  'tagReg',
            'PARE'          =>  'parent',
            'IMGDESCRIPCIO' =>  'descripcio',
            'IMGDATA'       =>  ['id' => 'data', 'type' => 'date'],
            'IMGNOM'        =>  'filename',
            'IMAGEN'        =>  'imgFile',
            'DOCUMENT'      =>  'docFile'
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;

        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':
                $result = $this->serviceArxius($normalizedArr, true, true);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

    // traspassa informes (sense plantilla)
    public function dcInformes($params)
    {
        // seteja info general del client/centre
        if (!$this->centre) {
            $this->setCentre($params['centre']);
            $this->setHash($params['hash']);
        }

        $sql = isset($params['sql']) ? $params['sql'] : $this->queries->getQuery('dc_informes');
        $sql = str_replace('[[dc]]', $params['conn']['dbname'], $sql);
        $sql = strtolower($sql['q']);

        // Execute query a bd origen
        $conn = $this->getOrigenConn($params['conn']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $resultAll = $stmt->fetchAll();

//        $result = array_slice($resultAll, 12801, 1);
//        dump($result);exit;

        // equivalència de columnes de la taula amb els camps del eClinic
        $equiv = [
//            'INFREGISTRE'   => 'dasiclinicReg',
            'MODREGISTRE'   => 'especialitatReg',
            'INFTIPODOCUMENTO'  => ['id' => 'tags', 'type' => 'clauValor'],
            'INFDATA'      => ['id' => 'dataAlta', 'type' => 'date'],
            'INFTITULO' => 'titol',
            'PRFREGISTRE'   => 'professionalReg',
            'VISREGISTRE'   => 'visitaReg',
            'PACREGISTRE'   => 'pacientReg',
            'INFORME'   => ['id' => 'contingut', 'type' => 'rtf', 'output' => 'html']
        ];

        $normalizedArr = [];
        foreach ($result as $row) {
            $normRow = [];
            foreach ($row as $key => $value) {
                if (array_key_exists(strtoupper($key), $equiv)) {
                    $e = $equiv[strtoupper($key)];
                    $normRow = $this->parseValues($e, $normRow, $value);
                }
            }

            // info repetitiva
            $normRow['centre'] = $params['centre'];
            $normalizedArr[] = $normRow;
        }

        switch ($params['action']) {
            case 'preview':
                $this->getPreview($normalizedArr);
                break;
            case 'traspas':

                $result = $this->serviceHTML2Informe($normalizedArr);
                break;
            default:
                $result = 'Acció inexistent';
                break;
        }

        return $result;
    }

}
