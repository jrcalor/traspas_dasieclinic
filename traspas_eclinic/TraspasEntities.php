<?php

namespace TraspasBundle\Model;

use DatabaseBundle\Entity as DB;

class TraspasEntities
{
    // ------------------- CREACIÓ UNITARIA D'ENTITATS -----------------------------------------

    /**
     * create Especialitat entity
     * @param $p array
     * @return DB\Especialitats
     */
    public function createEspecialitat($p)
    {
        $especialitat = new DB\Especialitats();
        $especialitat->setNom($p['nom']);
        $especialitat->setIdCentre($this->centre);

        if (isset($p['actiu'])) {
            $especialitat->setActiu($p['actiu']);
        }

        if (isset($p['dasiclinicReg'])) {
            $especialitat->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($especialitat);

        return $especialitat;
    }

    /**
     * create Tag entity
     * @param $p array
     * @return DB\Tag
     */
    public function createTag($p)
    {
        $tag = new DB\Tags();
        $tag->setNom($p['nom']);
        $tag->setCentre($this->centre);

        $grup = $p['grup'] ?? 'ARXIUSDOC_DOC_TYPE';

        $tag->setGrup($grup);

        if (isset($p['dasiclinicReg'])) {
            $tag->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($tag);

        return $tag;
    }


    /**
     * create PacientsTags entity
     *
     * @param $p
     * @return DB\PacientsTags
     */
    public function createPacientsTags($p)
    {

        $pacientTag = new DB\PacientsTags();
        $pacientTag->setPacient($p['pacient']);
        $pacientTag->setTag($p['tag']);

        $nom = $p['valor'] ?? $p['tag']->getNom();
        $pacientTag->setValor($nom);

        if (isset($p['dataAlta'])) {
            $pacientTag->setDataAlta($p['dataAlta']);
        }

        $this->em->persist($pacientTag);

        return $pacientTag;
    }


    /**
     * create Impost entity
     * @param $p array
     * @return DB\Impostos
     */
/*    public function createImpost($p)
    {
        $impost = new DB\Impostos();
        $impost->setNom($p['nom']);
        $impost->setIdCentre($this->centre);

        if (isset($p['actiu'])) {
            $especialitat->setActiu($p['actiu']);
        }

        if (isset($p['dasiclinicReg'])) {
            $especialitat->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($especialitat);

        return $especialitat;
    }*/

    /**
     * create Categoria entity
     * @param $p array
     * @return DB\Categories
     */
    public function createCategoria($p, $especialitat)
    {
        $categoria = new DB\Categories();
        $categoria->setNom($p['nom']);
        $categoria->setEspecialitat($especialitat);

        if (isset($p['dasiclinicReg'])) {
            $categoria->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($categoria);

        return $categoria;
    }

    /**
     * create Procedencia entity
     * @param $p array
     * @return DB\Procedencies
     */
    public function createProcedencia($p)
    {
        $procedencia = new DB\Procedencies();
        $procedencia->setNom($p['nom']);
        $procedencia->setCentre($this->centre);
        $procedencia->setTraspas($this->hash);

        if (isset($p['dadesAdicionals'])) {
            $procedencia->setDadesAdicionals(true);
        } else {
            $procedencia->setDadesAdicionals(false);
        }

        if (isset($p['dasiclinicReg'])) {
            $procedencia->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($procedencia);

        return $procedencia;
    }

    /**
     * create TipusVisita entity
     * @param $p array
     * @return DB\TipusVisita
     */
    public function createTipusVisita($p)
    {
        $tipusVisita = new DB\TipusVisita();
        $tipusVisita->setNom($p['nom']);
        if (isset($p['descripcio'])) {
            $tipusVisita->setDescripcio($p['descripcio']);
        }
        if (isset($p['color'])) {
            $tipusVisita->setColor($p['color']);
        }
        if (isset($p['dasiclinicReg'])) {
            $tipusVisita->setDasiclinicReg($p['dasiclinicReg']);
        }

        $tipusVisita->setCentre($this->centre);

        $this->em->persist($tipusVisita);

        return $tipusVisita;
    }

    /**
     * create Pacient entity
     * @param $p array
     * @return DB\Pacients
     * @throws \Exception
     */
    public function createPacient($p, $co)
    {
        // Creació del pracient
        $pacient = new DB\Pacients();

        $pacient->setCentre($this->centre);
        $pacient->setTraspas($this->hash);

        // creacio de persona
        if (!isset($p['idTipusDoc']) && !isset($p['tipusDocument'])) {
            $p['idTipusDoc'] = 1;
        }
        $persona = $this->createPersona($p);
        $pacient->setPersona($persona);

        // si historia és auto posem un valor seqüencial
        if (!isset($p['historia']) || $p['historia'] === 'auto') {
            $pacient->setHistoria($co);
        } else {

            if (is_null($p['historia'])) {
                $p['historia'] = 0;
            }
            $pacient->setHistoria($p['historia']);
        }

        if (isset($p['fileFoto'])) {
            $file = $this->createFileFromPath($p['fileFoto'], 'foto'.$co);
            $this->putImageAndThumbnailOnEntity($pacient, $file->getPathName(), 260, 260);

        } elseif (isset($p['blobFoto'])) {
            $file = $this->createFileFromBlob($p['blobFoto'], 'foto'.$co);
            $this->putImageAndThumbnailOnEntity($pacient, $file, 260, 260);
        }
        if (isset($p['actiu'])) {
            $pacient->setActiu($p['actiu']);
        }
        if (isset($p['dataAlta'])) {
            $pacient->setDataAlta($p['dataAlta']);
        } else {
            $pacient->setDataAlta(new \DateTime('today'));
        }
        if (isset($p['dataMod'])) {
            $pacient->setDataMod($p['dataMod']);
        }
        if (isset($p['dataNaixement'])) {
            $pacient->setDataNaixement($p['dataNaixement']);
        }
        if (isset($p['dataBaixa'])) {
            $pacient->setDataBaixa($p['dataBaixa']);
            $pacient->setActiu(false);
        }
        if (isset($p['motiuBaixa'])) {
            $pacient->setMotiuBaixa($p['motiuBaixa']);
            $pacient->setActiu(false);
        }
        if (isset($p['sexe'])) {
            $pacient->setSexe(strtolower($p['sexe']));
        }
        if (isset($p['sms'])) {
            $pacient->setSms($p['sms']);
        }
        if (isset($p['smsPubli'])) {
            $pacient->setSmsPubli($p['smsPubli']);
        }
        if (isset($p['mailing'])) {
            $pacient->setMailing($p['mailing']);
        }

        if (isset($p['lopd'])) {
            $pacient->setLopd($p['lopd']);
        }

        if (isset($p['fraPacient'])) {
            $pacient->setFraPacient($p['fraPacient']);
        }

        if (isset($p['iban'])) {
            $pacient->setIban($p['iban']);
        }

        if (isset($p['observacions'])) {
            $pacient->setObservacions($p['observacions']);
        }
        if (isset($p['altresDades'])) {
            $pacient->setAltresDades($p['altresDades']);
        }

        if (isset($p['dasiclinicReg'])) {
            $pacient->setDasiclinicReg($p['dasiclinicReg']);
        }

        if (isset($p['dasiclinicId'])) {
            $pacient->setDasiclinicId($p['dasiclinicId']);
        }

        return $pacient;
    }


    /**
     * create relationship between a patient and a center
     * @param $pacient
     * @param $centre
     * @return DB\PacientsCentres
     */
    public function createPacientCentre($pacient, $centre)
    {
        $paccentre = new DB\PacientsCentres();

        $paccentre->setPacient($pacient);
        $paccentre->setCentre($centre);

        return $paccentre;
    }

    /**
     * create Clients entity
     * @param $p array
     * @return DB\Clients
     * @throws \Exception
     */
    public function createClient($p, $co)
    {
        $client = new DB\Clients();

        $client->setCentre($this->centre);
        $client->setTraspas($this->hash);

        // creacio de persona
        if (!isset($p['idTipusDoc']) && !isset($p['tipusDocument'])) {
            $p['idTipusDoc'] = 1;
        }
        $persona = $this->createPersona($p);
        $client->setPersona($persona);

        if (isset($p['fileFoto'])) {
            $file = $this->createFileFromPath($p['fileFoto'], 'foto'.$co);
            $this->putImageAndThumbnailOnEntity($client, $file->getPathName(), 260, 260);
        } elseif (isset($p['blobFoto'])) {
            $file = $this->createFileFromBlob($p['blobFoto'], 'foto'.$co);
            $this->putImageAndThumbnailOnEntity($client, $file, 260, 260);
        }

        if (isset($p['actiu'])) {
            $client->setActiu($p['actiu']);
        }

        if (isset($p['dataAlta'])) {
            $client->setDataAlta($p['dataAlta']);
        } else {
            $client->setDataAlta(new \DateTime('today'));
        }

        if (isset($p['empresa'])) {
            $client->setEmpresa($p['empresa']);
        }

        if (isset($p['fax'])) {
            $client->setFax($p['fax']);
        }

        if (isset($p['relacio'])) {
            $client->setRelacio($p['relacio']);
        }

        if (isset($p['iban'])) {
            $client->setIban($p['iban']);
        }

        if (isset($p['percIRPF'])) {
            $client->setPercIRPF($p['percIRPF']);
        }

        if (isset($p['observacions'])) {
            $client->setObservacions($p['observacions']);
        }

        if (isset($p['dasiclinicReg'])) {
            $client->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $client;
    }

    /**
     * @param $client
     * @param $pacient
     * @param bool $defecte
     * @return DB\ClientsAtPacients
     */
    public function createClientAtPacient($client, $pacient, $defecte = true)
    {
        $clientPacient = new DB\ClientsAtPacients();

        $clientPacient->setPacientId($pacient);
        $clientPacient->setClientId($client);
        $clientPacient->setDefecte($defecte);

        $this->em->persist($clientPacient);

        return $clientPacient;
    }


    /**
     * create Pacient entity
     * @param $pacient
     * @param $p
     * @param $concatFields
     * @return mixed
     */
    public function updatePacient($pacient, $p, $concatFields)
    {
        $persona = $this->updatePersona($pacient->getPersona(), $p);

        if (isset($p['lopd'])) {
            $pacient->setLopd($p['lopd']);
        }
        if (isset($p['dataAlta'])) {
            $pacient->setDataAlta($p['dataAlta']);
        }
        if (isset($p['dataNaixement'])) {
            $pacient->setDataNaixement($p['dataNaixement']);
        }
        if (isset($p['sexe'])) {
            $pacient->setSexe(strtolower($p['sexe']));
        }

        if (isset($p['iban'])) {
            $pacient->setIban($p['iban']);
        }

        if (isset($p['observacions'])) {
            if (in_array('observacions', $concatFields)) {
                $p['observacions'] = $pacient->getObservacions() . chr(13) . chr(10) . $p['observacions'];
            }
            $pacient->setObservacions($p['observacions']);
        }

        if (isset($p['altresDades'])) {
            if (in_array('altresDades', $concatFields)) {
                $p['altresDades'] = $pacient->getAltresDades() . chr(13) . chr(10) . $p['altresDades'];
            }
            $pacient->setAltresDades($p['altresDades']);
        }

        return $pacient;
    }

    /**
     * create Pacient entity
     * @param $pacient
     * @param $p
     * @param $concatFields
     * @return mixed
     */
    public function updateClient($client, $p)
    {
        $persona = $this->updatePersona($client->getPersona(), $p);

        if (isset($p['iban'])) {
            $client->setIban($p['iban']);
        }

        return $client;
    }

    /**
     * create Antecedents entity
     * @param $p array
     * @return DB\Antecedents
     */
    public function createAntecedent($p)
    {
        $antecedent = new DB\Antecedents();

        $antecedent->setNom($p['nom']);
        $antecedent->setOrdre($p['ordre']);
        $antecedent->setCentre($this->centre);

        if (isset($p['dasiclinicReg'])) {
            $antecedent->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($antecedent);

        return $antecedent;
    }

    /**
     * create AntecedentsPacient entity
     * @param $p array
     * @return DB\AntecedentsPacient
     */
    public function createAntecedentPacient($p)
    {
        $antecedent = new DB\AntecedentsPacient();

        $antecedent->setNom($p['nom']);

        if ($p['ordre']) {
            $antecedent->setOrdre($p['ordre']);
        }

        if ($p['observacions']) {
            $antecedent->setObservacions($p['observacions']);
        }

        $antecedent->setCentre($this->centre);

        return $antecedent;
    }

    /**
     * create Receptes entity
     * @param $p array
     * @return DB\Receptes
     */
    public function createRecepta($p)
    {
        $recepta = new DB\Receptes();
        if (isset($p['comentaris'])) {
            $recepta->setComentaris($p['comentaris']);
        }

        $recepta->setTraspas($this->hash);

        return $recepta;
    }

    /**
     * create Medicaments entity
     * @param $p array
     * @return DB\Medicaments
     */
    public function createMedicament($p)
    {
        $medicament = new DB\Medicaments();

        if (!isset($p['pais'])) {
            $p['pais'] = 'ES';
        }
        $pais = $this->em->getRepository('DatabaseBundle:Paisos')->findOneByIso($p['pais']);
        $medicament->setPais($pais);

        if (isset($p['id_speciality'])) {
            $medicament->setIdSpeciality($p['id_speciality']);
        }
        if (isset($p['name_speciality'])) {
            $medicament->setNameSpeciality($p['name_speciality']);
        }
        if (isset($p['comentaris'])) {
            $medicament->setComentaris($p['comentaris']);
        }
        if (isset($p['dosage_form'])) {
            $medicament->setDosageForm($p['dosage_form']);
        }
        if (isset($p['package'])) {
            $medicament->setPackage($p['package']);
        }
        if (isset($p['id_package'])) {
            $medicament->setIdPackage($p['id_package']);
        }
        if (isset($p['id_laboratory'])) {
            $medicament->setIdLaboratory($p['id_laboratory']);
        }
        if (isset($p['name_laboratory'])) {
            $medicament->setNameLaboratory($p['name_laboratory']);
        }
        if (isset($p['code_atc'])) {
            $medicament->setCodeAtc($p['code_atc']);
        }
        if (isset($p['name_atc'])) {
            $medicament->setNameAtc($p['name_atc']);
        }
        if (isset($p['national_code'])) {
            $medicament->setNationalCode($p['national_code']);
        }

        if (isset($p['dataAlta'])) {
            $medicament->setFechaAlta($p['fecha_alta']);
        }
        if (isset($p['dataBaixa'])) {
            $medicament->setFechaBaja($p['fecha_baja']);
        }
        if (isset($p['baja'])) {
            $medicament->setBaja($p['baja']);
        } else {
            $medicament->setBaja(false);
        }

        $medicament->setCentre($this->centre);

        return $medicament;
    }


    /**
     * create receptesMedicaments entity
     * @param $p array
     * @return DB\ReceptesMedicaments
     */
    public function createReceptaMedicament($p)
    {
        $receptaMed = new DB\ReceptesMedicaments();

        if (isset($p['medicamentNom'])) {
            $receptaMed->setMedicamentNom($p['medicamentNom']);
        }
        if (isset($p['posologia'])) {
            $receptaMed->setPosologia($p['posologia']);
        }
        if (isset($p['unitatsEnvasos'])) {
            $receptaMed->setUnitatsEnvasos($p['unitatsEnvasos']);
        }
        if (isset($p['duradaTractament'])) {
            $receptaMed->setDuradaTractament($p['duradaTractament']);
        }
        if (isset($p['unitatsPosologia'])) {
            $receptaMed->setUnitatsPosologia($p['unitatsPosologia']);
        }
        if (isset($p['pautaPosologia'])) {
            $receptaMed->setPautaPosologia($p['pautaPosologia']);
        }
        if (isset($p['instPacient'])) {
            $receptaMed->setInstPacient($p['instPacient']);
        }
        if (isset($p['alertaFarmaceutic'])) {
            $receptaMed->setAlertaFarmaceutic($p['alertaFarmaceutic']);
        }

        return $receptaMed;

    }

    /**
     * create Box entity
     * @param $p array
     * @return DB\Boxes
     */
    public function createBox($p, $co)
    {
        $box = new DB\Boxes();
        $box->setCodi($p['codi']);
        if (isset($p['nom'])) {
            $box->setNom($p['nom']);
        } else {
            $box->setNom($p['codi']);
        }
        $box->setCentre($this->centre);

        if (isset($p['actiu'])) {
            $box->setActiu($p['actiu']);
        } else {
            $box->setActiu(true);
        }

        if (isset($p['ordre'])) {
            $box->setOrdre($p['ordre']);
        } else {
            $box->setOrdre($co);
        }

        if (isset($p['color'])) {
            $box->setColor('#'.$p['color']);
        } else {
            $box->setColor('#'.$this->random_color());
        }

        if (isset($p['dasiclinicReg'])) {
            $box->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $box;
    }

    /**
     * create GrupTractament entity
     * @param $p array
     * @return DB\GrupTractaments
     */
    public function createGrupTractament($p)
    {
        $grup = new DB\GrupTractaments();
        $grup->setNom($p['nom']);

        if (isset($p['dasiclinicReg'])) {
            $grup->setDasiclinicReg($p['dasiclinicReg']);
        }

        if (isset($p['serieFac'])) {
            $grup->serieFac($p['serieFac']);
        }

        if (isset($p['serieRec'])) {
            $grup->serieRec($p['serieRec']);
        }

        if (isset($p['serieFacMut'])) {
            $grup->serieFacMut($p['serieFacMut']);
        }

        if (isset($p['serieRecMut'])) {
            $grup->serieRecMut($p['serieRecMut']);
        }


        $this->em->persist($grup);

        return $grup;
    }


    /**
     * create Tractament entity
     * @param $p array
     * @return DB\Tractaments
     */
    public function createTractament($p)
    {

        $tractament = new DB\Tractaments();

        $tractament->setNom($p['nom']);

        // si no es passa codi li posem el mateix del nom
        if (isset($p['codi'])) {
            $tractament->setCodi($p['codi']);
        } else {
            $tractament->setCodi($p['nom']);
        }

        $tractament->setCentre($this->centre);

        if (isset($p['comentaris'])) {
            $tractament->setComentaris($p['comentaris']);
        }

        if (!isset($p['temps'])) {
            $p['temps'] = 0;
        }
        $tractament->setDuracio($p['temps']);

        if (isset($p['import'])) {
            $tractament->setImport($p['import']);
        }

        if (!isset($p['actiu'])) {
            $p['actiu'] = true;
        }
        $tractament->setActiu($p['actiu']);

        if (isset($p['costMaterial'])) {
            $tractament->setCostMaterial($p['costMaterial']);
        }

        if (isset($p['dasiclinicReg'])) {
            $tractament->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $tractament;
    }

    /**
     * create Bonus entity
     * @param $p array
     * @return DB\Bonus
     */
    public function createBonus($p)
    {
        $bonus = new DB\Bonus();

        if (isset($p['dataCreat'])) {
            $bonus->setDataCreat($p['dataCreat']);
        }

        if (isset($p['dataIniciat'])) {
            $bonus->setDataIniciat($p['dataIniciat']);
        }

        if (isset($p['dataCaducitat'])) {
            $bonus->setDataCaducitat($p['dataCaducitat']);
        }

        if (isset($p['dataCancelat'])) {
            $bonus->setDataCancelat($p['dataCancelat']);
        }

        if (isset($p['motiuCancelat'])) {
            $bonus->setMotiuCancelat($p['motiuCancelat']);
        }

        if (isset($p['sessions'])) {
            $bonus->setSessions($p['sessions']);
        }

        if (isset($p['consumides'])) {
            $bonus->setConsumides($p['consumides']);
        }

        if (isset($p['mesosCaduca'])) {
            $bonus->setMesosCaduca($p['mesosCaduca']);
        }

        if (isset($p['dasiclinicReg'])) {
            $bonus->setDasiclinicReg($p['dasiclinicReg']);
        }

        $bonus->setTraspas($this->hash);

        return $bonus;
    }


    /**
     * create TarifaMutua entity
     * @param $p array
     * @return DB\TarifaMutua
     * @throws \Exception
     */
    public function createTarifaMutua($p)
    {
        // creacio de persona
        if (!isset($p['idTipusDoc']) && !isset($p['tipusDocument'])) {
            $p['idTipusDoc'] = 3;
        }
        $persona = $this->createPersona($p);

        // Creació tarifa/mutua
        $tarifa = new DB\TarifaMutua();
        $tarifa->setPersona($persona);
        $tarifa->setEsmutua($p['esmutua']);

        $tarifa->setDataCreat(new \DateTime('today'));
        $tarifa->setDataInici(new \DateTime('today'));
        $tarifa->setCentre($this->centre);

        if (isset($p['observacions'])) {
            $tarifa->setObservacions($p['observacions']);
        }

        if (isset($p['modificapreumutua'])) {
            $tarifa->setModificapreumutua($p['modificapreumutua']);
        }

        if (isset($p['actiu'])) {
            $tarifa->setActiu($p['actiu']);
        }

        if (isset($p['cuenta'])) {
            $tarifa->setCuenta($p['cuenta']);
        }

        if (isset($p['ccoste'])) {
            $tarifa->setCcoste($p['ccoste']);
        }

        if (isset($p['dasiclinicReg'])) {
            $tarifa->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($tarifa);
        $this->em->flush();

        return $tarifa;
    }

    /**
     * create TarifesTractaments entity
     * @param $p array
     * @return DB\TarifaTractament
     */
    public function createTractamentTarifa($tarifa, $t)
    {
        $tarifaTractamentRepo = $this->em->getRepository('DatabaseBundle:TarifaTractament');

        $newEntity = new DB\TarifaTractament();

        $newEntity->setIdTarifaMutua($tarifa);

        $repo = $this->em->getRepository('DatabaseBundle:Tractaments');
        $tractament = null;

        // cerquem el tractament per codi o FK
        if (isset($t['idTractament'])) {
            $tractament = $repo->find($t['idTractament']);
        } elseif (isset($t['codi'])) {
            $tractament = $repo->findOneByCodi($t['codi']);
        } elseif (isset($t['tractamentReg'])) {
            $tractament = $repo->findOneBy([
                'dasiclinicReg' => $t['tractamentReg'],
                'idCentre' => $this->centre]);
        }

        if ($tractament) {

            // si ja existeix l'esborrem i es torna a crear (parche no peridifica tarifes/mutues DC)
            $hasTarifaTractament = $tarifaTractamentRepo->find([
                'idTarifaMutua' => $tarifa->getIdTarifaMutua(),
                'idTractament' => $tractament->getIdTractament()
            ]);

            if ($hasTarifaTractament) {
                $this->em->remove($hasTarifaTractament);
            }

            $newEntity = new DB\TarifaTractament();

            $newEntity->setIdTarifaMutua($tarifa);
            $newEntity->setIdTractament($tractament);

            if (isset($t['codiMutua'])) {
                $newEntity->setCodiMutua($t['codiMutua']);
            }

            if (isset($t['concepteMutua'])) {
                $newEntity->setConcepteMutua($t['concepteMutua']);
            }

            if (isset($t['importMutua'])) {
                $newEntity->setImportMutua($t['importMutua']);
            }

            if (isset($t['facturarMutua'])) {
                $newEntity->setFacturarMutua($t['facturarMutua']);
            }

            if (isset($t['pagarMutua'])) {
                $newEntity->setPagarMutua($t['pagarMutua']);
            }

            if (isset($t['preu'])) {
                $newEntity->setPreu($t['preu']);
            }

            if (isset($t['comisioFixaPrf'])) {
                $newEntity->setComisioFixaPrf($t['comisioFixaPrf']);
            }

            if (isset($t['demanaVolant'])) {
                $newEntity->setDemanaVolant($t['demanaVolant']);
            }

            if (isset($t['preu'])) {
                $newEntity->setPreu($t['preu']);
            }

            if (isset($t['actiu'])) {
                $newEntity->setActiu(true);
            }

            $this->em->persist($newEntity);
        }

        return $newEntity;
    }

    /**
     * create ProfessionalsTarifaMutua entity
     *
     * @param $professional
     * @param $tarifa
     * @return DB\ProfessionalsTarifaMutua
     */
    public function createProfessionalsTarifaMutua($professional, $tarifa) {

        $newEntity = DB\ProfessionalsTarifaMutua::create($professional, $tarifa);

        return $newEntity;
    }

    /**
     * create Empresa entity
     * @param $p array
     * @return DB\Empresa
     */
    public function createEmpresa($p)
    {
        // creacio de persona
        if (!isset($p['idTipusDoc']) && !isset($p['tipusDocument'])) {
            $p['idTipusDoc'] = 3;
        }
        $persona = $this->createPersona($p);

        // Creació de la empresa
        $empresa = new DB\Empresa();
        $empresa->setDadesEmpresa($persona);
        $empresa->setCentre($this->centre);

        if (isset($p['actiu'])) {
            $empresa->setActiu($p['actiu']);
        }

        if (isset($p['observacions'])) {
            $empresa->setComentaris($p['observacions']);
        }

        if (isset($p['web'])) {
            $empresa->setWeb($p['observacions']);
        }

        if (!isset($p['serieFac'])) {
            $p['serieFac'] = 0;
        }
        $empresa->setSerieFac($p['serieFac']);

        if (!isset($p['serieRec'])) {
            $p['serieRec'] = 50;
        }
        $empresa->setSerieRec($p['serieRec']);

        if (!isset($p['serieFacMut'])) {
            $p['serieFacMut'] = 20;
        }
        $empresa->setSerieFacMut($p['serieFacMut']);

        if (!isset($p['serieRecMut'])) {
            $p['serieRecMut'] = 70;
        }
        $empresa->setSerieRecMut($p['serieRecMut']);

        if (isset($p['irpf'])) {
            $empresa->setPercIRPF($p['irpf']);
        }

        if (isset($p['contaDigits'])) {
            $empresa->setContaDigits($p['contaDigits']);
        }

        if (isset($p['dasiclinicReg'])) {
            $empresa->setDasiclinicReg($p['dasiclinicReg']);
        }

        if (isset($p['logo'])) {
            $file = $this->createFileFromBlob($p['logo'], 'logo');
            $this->putImageAndThumbnailOnEntity($empresa, $file);
        }

        $this->em->persist($empresa);
        $this->em->flush();

        return $empresa;
    }

    /**
     * create Periode entity
     * @param $p array
     * @return DB\Periodes
     */
    public function createPeriode($p)
    {
        // Creació d'un periode
        $periode = new DB\Periodes();
        $periode->setNom($p['nom']);
        $periode->setInici($p['inici']);
        $periode->setFinal($p['final']);
        $periode->setNom($p['nom']);

        if (isset($p['actiu'])) {
            $periode->setActiu($p['actiu']);
        }

        if (isset($p['observacions'])) {
            $periode->setObservacions($p['observacions']);
        }

        if (isset($p['dasiclinicReg'])) {
            $periode->setDasiclinicReg($p['dasiclinicReg']);
        }

        if (isset($p['dasiclinicId'])) {
            $periode->setDasiclinicId($p['dasiclinicId']);
        }

        $this->em->persist($periode);
        $this->em->flush();

        return $periode;
    }

    /**
     * create Horari entity
     * @param $p array
     * @return DB\Horaris
     */
    public function createHorari($p)
    {
        // Creació d'un horari
        $horari = new DB\Horaris();

        $horari->setSetmana(0);
        $horari->setDiaSetmana($p['diaSetmana']);
        $horari->setActiu($p['actiu']);
        $horari->setInterval($p['interval']);

        $horari->setMati($p['mati']);
        $horari->setTarda($p['tarda']);

        $horari->setMatiInici($p['matiInici']);
        $horari->setMatiFinal($p['matiFinal']);
        $horari->setTardaInici($p['tardaInici']);
        $horari->setTardaFinal($p['tardaFinal']);

        if (isset($p['dasiclinicReg'])) {
            $horari->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($horari);
        $this->em->flush();

        return $horari;
    }

    /**
     * create DiesFestius entity
     * @param $p array
     * @return DB\DiesFestius
     */
    public function createFestiu($p)
    {
        $festiu = new DB\DiesFestius();
        $festiu->setNom($p['nom']);
        $festiu->setData($p['data']);
        $festiu->setAny($p['data']->format("Y"));
        $festiu->setIdCentre($this->centre);

        if (isset($p['periodicitat'])) {
            $festiu->setPeriodicitat($p['periodicitat']);
        } else {
            $festiu->setPeriodicitat(0);
        }

        $this->em->persist($festiu);

        return $festiu;
    }


    /**
     * create PeriodeAbsencia entity
     * @param $p array
     * @return DB\PeriodesAbsencia
     */
    public function createPeriodeAbsencia($p)
    {
        // Creació d'un periode
        $periode = new DB\PeriodesAbsencia();
        $periode->setNom($p['nom']);
        $periode->setInici($p['inici']);
        $periode->setFinal($p['final']);
        $periode->setNom($p['nom']);
        $periode->setTipus($p['tipus']);

        // si ve del dasiClinic i no és tot el dia (0), llavors és matí (1) o tarda (2) i guarda també l'hora
        if ($p['tipus'] > 0) {
            $periode->setHoraInici($p['horaIni']);
            $periode->setHoraFinal(($p['horaFi']));
        }

        if (isset($p['actiu'])) {
            $periode->setActiu($p['actiu']);
        }

        if (isset($p['observacions'])) {
            $periode->setObservacions($p['observacions']);
        }

        if (isset($p['dasiclinicReg'])) {
            $periode->setDasiclinicReg($p['dasiclinicReg']);
        }

        $this->em->persist($periode);
        $this->em->flush();

        return $periode;
    }

    /**
     * create BloquejosAgenda entity
     * @param $p array
     * @return DB\BloquejosAgenda
     */
    public function createBloqueigAgenda($p)
    {
        $bloq = new DB\BloquejosAgenda();

        $bloq->setData($p['data']);
        $bloq->setHora($p['hora']);
        $bloq->setTemps($p['temps']);

        if (isset($p['descripcio'])) {
            $bloq->setMotiu($p['descripcio']);
        }

        if (isset($p['allDay'])) {
            $bloq->setAllDay($p['allDay']);
        }

        if (isset($p['dasiclinicReg'])) {
            $bloq->setDasiclinicId($p['dasiclinicReg']);
        }

        $this->em->persist($bloq);
        $this->em->flush();

        return $bloq;
    }


    /**
     * create Professional entity
     * @param $p array
     * @return DB\Professionals
     */
    public function createProfessional($p)
    {
        // creacio de persona
        if (!isset($p['idTipusDoc']) && !isset($p['tipusDocument'])) {
            $p['idTipusDoc'] = 1;
        }
        $persona = $this->createPersona($p);

        // Creació del professional
        $professional = new DB\Professionals();
        $professional->setPersona($persona);
        $professional->setCentre($this->centre);

        if (isset($p['acronim'])) {
            $professional->setAcronim($this->clean(strtoupper($p['acronim'])));
        } else {
            $professional->setAcronim($this->clean(strtoupper(substr($persona->getNom(), 0, 3) . substr($persona->getCognom1(), 0, 3))));
        }

        if (isset($p['color'])) {
            $professional->setColorAgenda('#'.$p['color']);
        } else {
            $professional->setColorAgenda('#'.$this->random_color());
        }

        if (isset($p['foto'])) {
            $file = $this->createFileFromBlob($p['foto'], 'foto');
            $this->putImageAndThumbnailOnEntity($professional, $file);
        }

        if (isset($p['actiu'])) {
            $professional->setActiu($p['actiu']);
        }

        if (isset($p['observacions'])) {
            $professional->setObservacions($p['observacions']);
        }

        if (isset($p['obsAgenda'])) {
            $professional->setObsAgenda($p['obsAgenda']);
        }

        if (isset($p['numColegiat'])) {
            $professional->setNumColegiat($p['numColegiat']);
        }

        if (isset($p['dasiclinicReg'])) {
            $professional->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $professional;
    }


    /**
     * create Persones entity
     * @param $p array
     * @return DB\Persones
     */
    public function createPersona($p)
    {
        $persona = new DB\Persones();

        if (isset($p['cognomsNom'])) {
            $nomaux = explode(',', trim($p['cognomsNom']));
            $p['cognoms'] = $nomaux[0];
            if (isset($nomaux[1])) {
                $p['nom'] = $nomaux[1];
            }

        } elseif (isset($p['nomCognoms'])) {

            $parts = explode(' ', trim($p['nomCognoms']));
            $numParts = count($parts);
            if ($numParts > 2) {
                $p['cognom2'] = $parts[$numParts-1];
                $p['cognom1'] = $parts[$numParts-2];
                $p['nom'] = '';
                for ($i = 0; $i < $numParts-2; $i++) {
                    $p['nom'] .= $parts[$i] . ' ';
                }

            } else {

                if (isset($parts[2])) {
                    $p['cognom2'] = $parts[2];
                }
                if (isset($parts[1])) {
                    $p['cognom1'] = $parts[1];
                }
                if (isset($parts[0])) {
                    $p['nom'] = $parts[0];
                }
            }
        } elseif (isset($p['nomCognomsMixed'])) {

            // primer mira si a ve tot el nom complert (més de 2 paraules), sinó camp nom i cognoms separats
            $parts = explode(' ', trim($p['nomCognomsMixed']));
            $numParts = count($parts);
            if ($numParts > 2) {
                $p['cognom2'] = $parts[$numParts-1];
                $p['cognom1'] = $parts[$numParts-2];
                $p['nom'] = '';
                for ($i = 0; $i < $numParts-2; $i++) {
                    $p['nom'] .= $parts[$i] . ' ';
                }
            }
        }

        if (isset($p['nom'])) {
            $persona->setNom(trim($p['nom']));
        }

        // separa cognoms si existeix
        if (isset($p['cognoms'])) {
            $cognet = [];
            $cog = explode(' ', trim($p['cognoms']));
            foreach ($cog as $valor) {
                if ($valor !== 'de' && $valor !== 'del' && $valor !== 'la') {
                    $cognet[] = $valor;
                }
            }
            $persona->setCognom1(trim($cognet[0]));
            unset($cognet[0]);

            $cog2 = implode(' ', $cognet);
            $persona->setCognom2(trim($cog2));
        } else {
            if (isset($p['cognom1'])) {
                $persona->setCognom1(trim($p['cognom1']));
            }
            if (isset($p['cognom2'])) {
                $persona->setCognom2(trim($p['cognom2']));
            }
        }

        // si telefon1 és mobil i no hi ha mobil passa'l a aquesta columna
        if (isset($p['telefon1']) && in_array(substr($p['telefon1'], 0, 1), ['6', '7'])) {
            // si telefon1 és mobil i mobil és fixe, intercanvia'ls
            if (isset($p['mobil'])) {
                if (!in_array(substr($p['mobil'], 0, 1), ['6', '7'])) {
                    $tempTel = $p['mobil'];
                    $p['mobil'] = $p['telefon1'];
                    $p['telefon1'] = $tempTel;
                }
            } else {
                $p['mobil'] = $p['telefon1'];
                unset($p['telefon1']);
            }
        }

        // telefons i mobil
        if (isset($p['mobil'])) {
            $p['mobil'] = trim(str_replace('.', '', $p['mobil']));
            $persona->setMobil($p['mobil']);
        }
        if (isset($p['telefon1'])) {
            $p['telefon1'] = trim(str_replace('.', '', $p['telefon1']));
            $fdigit = substr($p['telefon1'], 0, 1);
            if (!$persona->getMobil() && in_array($fdigit, array('6', '7'))) {
                $persona->setMobil($p['telefon1']);
            } else {
                $persona->setTelefon1($p['telefon1']);
            }
        }

        if (isset($p['telefon2'])) {
            $p['telefon2'] = trim(str_replace('.', '', $p['telefon2']));
            $fdigit = substr($p['telefon2'], 0, 1);
            if (!$persona->getMobil() && in_array($fdigit, array('6', '7'))) {
                $persona->setMobil($p['telefon2']);
            } elseif (!$persona->getTelefon1()) {
                $persona->setTelefon1($p['telefon2']);
            } else {
                $persona->setTelefon2($p['telefon2']);
            }
        }

        if (isset($p['numDocument'])) {
            $persona->setNumDocument(strtoupper($p['numDocument']));
            $repo = $this->em->getRepository('DatabaseBundle:TipusDocument');

            // si passem id tipus document del eClinic busquem pel idPare
            $tipusDocu = null;

            if (isset($p['idTipusDocument'])) {
                $tipusDocu = $repo->findOneBy([
                    'idTipus' => $p['idTipusDocument'],
                    'idCentre' => $this->centre]);
            } elseif  (isset($p['idTipusDoc'])) {
                $tipusDocu = $repo->findOneBy([
                    'idPare' => $p['idTipusDoc'],
                    'idCentre' => $this->centre]);
            } elseif (isset($p['tipusDocument'])) {
                $tipusDocu = $repo->findOneBy([
                    'nom' => $p['tipusDocument'],
                    'idCentre' => $this->centre]);
            }
            if ($tipusDocu) {
                $persona->setTipusDocument($tipusDocu);
            }
        }

        // email
        if (isset($p['email'])) {
            $email = new DB\Emails();
            $email->setEmail(strtolower($p['email']));
            $persona->setEmail($email);
        }

        // adreça
        $adreca = new DB\Adreces();

        if (isset($p['direccio1'])) {
            $adreca->setDireccio1(trim($p['direccio1']));
        }

        if (isset($p['direccio2'])) {
            $adreca->setDireccio1(trim($p['direccio2']));
        }

        if (isset($p['cp'])) {
            if (strlen($p['cp']) == 4) {
                $p['cp'] = '0' . $p['cp'];
            }
            $adreca->setCP($p['cp']);
        }

        if (isset($p['pais'])) {
            $pais = $this->getPaisByName($p['pais']);
        } else {
            $pais = $this->centre->getDadesCentre()->getAdreca()->getPais();
        }
        $adreca->setPais($pais);

        $zona = null;

        if (isset($p['poblacio'])) {
            $zona = $this->getZonaByName($p['poblacio'], 2, $pais);
        } elseif (isset($p['provincia'])) {
            $zona = $this->getZonaByName($p['provincia'], 1, $pais);
        }

        if ($zona) {
            $adreca->setZona($zona);
        }

        $persona->setAdreca($adreca);

        return $persona;
    }

    /**
     * update Persones entity
     * @param $p array
     * @return DB\Persones
     */
    public function updatePersona($persona, $p)
    {
        if (isset($p['cognomsNom'])) {
            $p['cognom1'] = '';
            $p['cognom2'] = '';
            $nomaux = explode(',', trim($p['cognomsNom']));
            $p['cognoms'] = $nomaux[0];
            if (isset($nomaux[1])) {
                $p['nom'] = $nomaux[1];
            }

        } elseif (isset($p['nomCognoms'])) {
            $p['cognom1'] = '';
            $p['cognom2'] = '';
            $parts = explode(' ', trim($p['nomCognoms']));

            $numParts = count($parts);
            if ($numParts > 2) {
                $p['cognom2'] = $parts[$numParts-1];
                $p['cognom1'] = $parts[$numParts-2];
                $p['nom'] = '';
                for ($i = 0; $i < $numParts-2; $i++) {
                    $p['nom'] .= $parts[$i] . ' ';
                }

            } else {

                if (isset($parts[2])) {
                    $p['cognom2'] = $parts[2];
                }
                if (isset($parts[1])) {
                    $p['cognom1'] = $parts[1];
                }
                if (isset($parts[0])) {
                    $p['nom'] = $parts[0];
                }
            }
        } elseif (isset($p['nomCognomsMixed'])) {
            $p['cognom1'] = '';
            $p['cognom2'] = '';

            // primer mira si a ve tot el nom complert (més de 2 paraules), sinó camp nom i cognoms separats
            $parts = explode(' ', trim($p['nomCognomsMixed']));
            $numParts = count($parts);
            if ($numParts > 2) {
                $p['cognom2'] = $parts[$numParts-1];
                $p['cognom1'] = $parts[$numParts-2];
                $p['nom'] = '';
                for ($i = 0; $i < $numParts-2; $i++) {
                    $p['nom'] .= $parts[$i] . ' ';
                }
            }
        }

        if (isset($p['nom'])) {
            $persona->setNom(trim($p['nom']));
        }

        // separa cognoms si existeix
        if (isset($p['cognoms'])) {
            $cognet = [];
            $cog = explode(' ', trim($p['cognoms']));
            foreach ($cog as $valor) {
                if ($valor !== 'de' && $valor !== 'del' && $valor !== 'la') {
                    $cognet[] = $valor;
                }
            }
            $p['cognom1'] = trim($cognet[0]);
            unset($cognet[0]);

            $cog2 = implode(' ', $cognet);
            $p['cognom2'] = trim($cog2);
        }

        if (isset($p['cognom1'])) {
            $persona->setCognom1(trim($p['cognom1']));
        }
        if (isset($p['cognom2'])) {
            $persona->setCognom2(trim($p['cognom2']));
        }


        // telefons i mobil
        if (isset($p['mobil'])) {
            $p['mobil'] = trim(str_replace('.', '', $p['mobil']));
            if (!$persona->getMobil()) {
                $persona->setMobil($p['mobil']);
            } elseif(!$persona->getTelefon1()) {
                $persona->setTelefon1($p['mobil']);
            } elseif(!$persona->getTelefon2()) {
                $persona->setTelefon2($p['mobil']);
            }
        }
        if (isset($p['telefon1'])) {
            $p['telefon1'] = trim(str_replace('.', '', $p['telefon1']));
            $fdigit = substr($p['telefon1'], 0, 1);
            if (!isset($p['mobil']) || (isset($p['mobil']) && $p['mobil'] && $p['mobil'] !== "" && in_array($fdigit, array('6', '7')))) {
                $persona->setMobil($p['telefon1']);
            } else {
                $persona->setTelefon1($p['telefon1']);
            }
        }
        if (isset($p['telefon2'])) {
            $p['telefon2'] = trim(str_replace('.', '', $p['telefon2']));
            $fdigit = substr($p['telefon2'], 0, 1);
            if (!isset($p['mobil']) || (isset($p['mobil']) && $p['mobil'] && $p['mobil'] !== "" && in_array($fdigit, array('6', '7')))) {
                $persona->setMobil($p['telefon2']);
            } else {
                $persona->setTelefon2($p['telefon2']);
            }
        }

        if (isset($p['numDocument'])) {
            $persona->setNumDocument($p['numDocument']);
            $repo = $this->em->getRepository('DatabaseBundle:TipusDocument');

            // si passem id tipus document del eClinic busquem pel idPare
            $tipusDocu = null;
            if (isset($p['idTipusDoc'])) {
                $tipusDocu = $repo->findOneBy([
                    'idPare' => $p['idTipusDoc'],
                    'centre' => $this->centre]);
            } elseif (isset($p['tipusDocument'])) {
                $tipusDocu = $repo->findOneBy([
                    'nom' => $p['tipusDocument'],
                    'centre' => $this->centre]);
            }
            if ($tipusDocu) {
                $persona->setTipusDocument($tipusDocu);
            }
        }

        // email
        if (isset($p['email'])) {
            $email = new DB\Emails();
            $email->setEmail(strtolower($p['email']));
            $persona->setEmail($email);
        }

        // adreça
        $adreca = $persona->getAdreca();

        if ($adreca) {

            if (isset($p['direccio1'])) {
                $adreca->setDireccio1($p['direccio1']);
            }

            if (isset($p['direccio2'])) {
                $adreca->setDireccio1($p['direccio2']);
            }

            if (isset($p['cp'])) {
                $adreca->setCP($p['cp']);
            }

            if (isset($p['poblacio'])) {
                $zona = $this->getZonaByName($p['poblacio']);
                if ($zona) {
                    $adreca->setZona($zona);
                }
            }
            $persona->setAdreca($adreca);

        }

        return $persona;
    }


    /**
     * create Cites entity
     * @param $p array
     * @return DB\Cites
     */
    public function createCita($p)
    {
        $cita = new DB\Cites();

        $cita->setCentre($this->centre);

        $cita->setData($p['data']);
        $cita->setExercici($p['data']->format("Y"));
        $cita->setMes($p['data']->format("m"));
        $cita->setHora($p['hora']);
        $cita->setTemps($p['temps']);

        if (isset($p['estatCita']) && is_int($p['estatCita'])) {
            $cita->setEstat($p['estatCita']);
        }
        if (isset($p['estatAdm']) && is_int($p['estatAdm'])) {
            $cita->setEstatAdm($p['estatAdm']);
        }
        if (isset($p['dataCancelat'])) {
            $cita->setDataCancelat($p['dataCancelat']);
        }
        if (isset($p['dataConfirmat'])) {
            $cita->setDataConfirmat($p['dataConfirmat']);
        }
        if (isset($p['dataNoPresentat'])) {
            $cita->setDataNp($p['dataNoPresentat']);
        }
        if (isset($p['dataVisitat'])) {
            $cita->setDataVisitat($p['dataVisitat']);
        }
        if (isset($p['dataEnCurs'])) {
            $cita->setDataCurs($p['dataEnCurs']);
        }
        if (isset($p['dataEspera'])) {
            $cita->setDataEspera($p['dataEspera']);
        }

        if (isset($p['urgencia'])) {
            $cita->setUrgencia($p['urgencia']);
        }
        if (isset($p['observacions'])) {
            $cita->setObservacions($p['observacions']);
        }
        if (isset($p['obsDestaca'])) {
            $cita->setObsDestaca($p['obsDestaca']);
        }

        if (isset($p['dasiclinicReg'])) {
            $cita->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $cita;
    }

    /**
     * create CitesTractaments entity
     * @param $p array
     * @return DB\CitesTractaments
     */
    public function createCitaTractament($p, $cita, $tractament)
    {
        $citesTractaments = new DB\CitesTractaments();

        $citesTractaments->setCita($cita);
        $citesTractaments->setTractament($tractament);

        if (isset($p['unitatsTrac'])) {
            $citesTractaments->setUnitats($p['unitatsTrac']);
        }
        if (isset($p['observacionsTrac'])) {
            $citesTractaments->setObservacions($p['observacionsTrac']);
        }

        return $citesTractaments;
    }


    /**
     * create VisitesTractaments entity
     * @param $p array
     * @return DB\VisitesTractaments
     */
    public function createVisitaTractament($p, $visita, $tractament)
    {
        $visitesTractaments = new DB\VisitesTractaments();

        if ($visita) {
            $visitesTractaments->setVisita($visita);
        }

        if ($tractament) {
            $visitesTractaments->setTractament($tractament);
        }

        if (!isset($p['numVisites'])) {
            $p['numVisites'] = 1;
        }
        $visitesTractaments->setNumVisites($p['numVisites']);

        if (isset($p['concepte'])) {
            $visitesTractaments->setConcepte($p['concepte']);
        }

        if (isset($p['observacionsTrac'])) {
            $visitesTractaments->setObservacions($p['observacionsTrac']);
        }

        if (!isset($p['unitatsTrac'])) {
            $p['unitatsTrac'] = 1;
        }
        $visitesTractaments->setUnitats($p['unitatsTrac']);

        if (isset($p['importTotal'])) {
            $visitesTractaments->setImportTotal($p['importTotal']);
        }

        if (isset($p['importUnitari'])) {
            $visitesTractaments->setImportUnitari($p['importUnitari']);
        } else {
            $visitesTractaments->setImportUnitari($p['importTotal']);
        }

        if (isset($p['percentDte'])) {
            $visitesTractaments->setPercentDte($p['percentDte']);
        }
        if (isset($p['importDte'])) {
            $visitesTractaments->setImportDte($p['importDte']);
        }

        if (isset($p['importMutua'])) {
            $visitesTractaments->setImportMutua($p['importMutua']);
        }
        if (isset($p['irpfMutua'])) {
            $visitesTractaments->setIrpfMutua($p['irpfMutua']);
        }
        if (isset($p['netMutua'])) {
            $visitesTractaments->setNetMutua($p['netMutua']);
        }
        if (isset($p['perImpost'])) {
            $visitesTractaments->setPerImpost($p['perImpost']);
        }
        if (isset($p['importImpost'])) {
            $visitesTractaments->setImportImpost($p['importImpost']);
        }
        if (isset($p['costMaterial'])) {
            $visitesTractaments->setCostMaterial($p['costMaterial']);
        }
        if (isset($p['showObsTracFra'])) {
            $visitesTractaments->setUnitats($p['showObsTracFra']);
        }

        if (isset($p['bonus'])) {
            $visitesTractaments->setBonus($p['bonus']);
        }
        if (isset($p['sessionsConsumides'])) {
            $visitesTractaments->setSessionsConsumides($p['sessionsConsumides']);
        }

        // ------ aquestes propietats no s'estàn usant actualment al eClinic -----
        if (isset($p['estat'])) {
            $visitesTractaments->setEstat($p['estat']);
        }
        if (isset($p['dataIni'])) {
            $visitesTractaments->setDataIni($p['dataIni']);
        }
        if (isset($p['dataFi'])) {
            $visitesTractaments->setDataFi($p['dataFi']);
        }
        // ----------------------------------

        if (isset($p['dasiclinicReg'])) {
            $visitesTractaments->setDasiclinicReg($p['dasiclinicReg']);
        }

        $visitesTractaments->setTraspas($this->hash);

        return $visitesTractaments;
    }

    /**
     * create Carrec entity
     * @param $p array
     * @return DB\Carrec
     */
    public function createCarrec($p)
    {
        $carrec = new DB\Carrec();

        $carrec->setCentre($this->centre);

        $carrec->setTipusCarrec($p['tipusCarrec']);

        // si no es passa codi de càrrec agafem seqüencial
        if (!isset($p['codiCarrec'])) {
            // carreguem el num de contador, segons el tipus de càrreg i centre
            $contador = $this->factService->getContador($p['tipusCarrec'], null, $this->centre)->contador;
            // muntem el codi de document partint del tipusCarrec i num. d'ordre
            $p['codiCarrec'] = $this->factService->getCodi($p['tipusCarrec'], $contador->getContador());

            $contador->setContador($contador->getContador()+1);
            $this->em->persist($contador);
        }

        $carrec->setCodi($p['codiCarrec']);

        if (isset($p['concepte'])) {
            $carrec->setConcepte($p['concepte']);
        }

        $carrec->setImport($p['import']);
        $carrec->setTotalBase($p['totalBase']);


        $carrec->setDataCarrec($p['dataCarrec']);
        $carrec->setDataDoc($p['dataDoc']);

        if (!isset($p['exercici'])) {
            $p['exercici'] = $p['dataCarrec']->format("Y");
        }
        $carrec->setExercici($p['exercici']);
        $carrec->setMes($p['dataCarrec']->format("m"));

        $carrec->setTotalCobrat($p['totalCobrat']);
        $carrec->setPendentCobrar($p['pendentCobrar']);

        if (isset($p['diposit'])) {
            $carrec->setDiposit($p['diposit']);
        }

        if (isset($p['totalImpost'])) {
            $carrec->setTotalImpost($p['totalImpost']);
        }

        if (isset($p['irpf'])) {
            $carrec->setIrpf($p['irpf']);
        }

        if (isset($p['dataExport'])) {
            $carrec->setDataExport($p['dataExport']);
        }

        // no s'estan usant al eClinic pel moment -----------
        if (isset($p['baseExenta'])) {
            $carrec->setBaseExenta($p['baseExenta']);
        }
        if (isset($p['deposito'])) {
            $carrec->setDiposit($p['deposito']);
        }
        if (isset($p['conta'])) {
            $carrec->setConta($p['conta']);
        }
        if (isset($p['costMaterial'])) {
            $carrec->setCostMaterial($p['costMaterial']);
        }
        if (isset($p['totalLiquidacio'])) {
            $carrec->setTotalLiquidacio($p['totalLiquidacio']);
        }
        if (isset($p['liquidat'])) {
            $carrec->setLiquidat($p['liquidat']);
        }
        // -------------------------------------------------

        if (isset($p['dasiclinicReg'])) {
            $carrec->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $carrec;
    }

    /**
     * create Fra entity
     * @param $p array
     * @return DB\Fra
     */
    public function createFra($p)
    {
        $fra = new DB\Fra();

        $fra->setCodi($p['codiFra']);
        $fra->setDataDoc($p['dataFra']);

        if (isset($p['proforma'])) {
            $fra->setProforma($p['proforma']);
        }

        if (isset($p['notaPeu'])) {
            $fra->setNotaPeu($p['notaPeu']);
        }

        if (isset($p['observacionsFra'])) {
            $fra->setObservacions($p['observacionsFra']);
        }

        if (isset($p['groupTrac'])) {
            $fra->setGroupTrac($p['groupTrac']);
        }

        if (isset($p['periodeIni'])) {
            $fra->setPeriodeIni($p['periodeIni']);
        }

        if (isset($p['periodeFi'])) {
            $fra->setPeriodeFi($p['periodeFi']);
        }

        /**
         * no s'estan usant al eClinic pel moment -----------
         *
         * irpfPer
         * irpfImport
         * import
         * total
         * cobrat
         * importCobrat
         */

        if (isset($p['fraReg'])) {
            $fra->setDasiclinicReg($p['fraReg']);
        }

        return $fra;
    }


    /**
     * create Cobrament entity
     * @param $p array
     * @return DB\Cobrament
     */
    public function createCobrament($p)
    {
        $cobrament = new DB\Cobrament();

        $cobrament->setTipusCobrament($p['tipusCobrament']);

        // si no es passa codi de càrrec agafem seqüencial
        if (!isset($p['codi'])) {
            // carreguem el num de contador, segons el tipus de càrreg i centre
            $contador = $this->factService->getContador($p['tipusCobrament'], null, $this->centre)->contador;
            // muntem el codi de document partint del tipusCarrec i num. d'ordre
            $p['codi'] = $this->factService->getCodi($p['tipusCobrament'], $contador->getContador());

            $contador->setContador($contador->getContador()+1);
            $this->em->persist($contador);
        }

        $cobrament->setCodi($p['codi']);

        $cobrament->setImportCobrat($p['importCobrat']);
        $cobrament->setEsDiposit($p['esDiposit']);
        $cobrament->setDataCobrament($p['dataCobrament']);

        if (!isset($p['exercici'])) {
            $p['exercici'] = $p['dataCobrament']->format("Y");
        }

        $cobrament->setExercici($p['exercici']);
        $cobrament->setMes($p['dataCobrament']->format("m"));

        if (isset($p['observacions'])) {
            $cobrament->setObservacions($p['observacions']);
        }

        if (isset($p['comisioBanc'])) {
            $cobrament->setComisioBanc($p['comisioBanc']);
        }

        if (isset($p['comisioBanc'])) {
            $cobrament->setObservacions($p['totalLiquidacio']);
        }

        if (isset($p['liquidat'])) {
            $cobrament->setLiquidat($p['liquidat']);
        }

        if (isset($p['liquidacioPagada'])) {
            $cobrament->setLiquidacioPagada($p['liquidacioPagada']);
        }

        if (isset($p['liquidacioPagada'])) {
            $cobrament->setLiquidacioPagada($p['liquidacioPagada']);
        }

        if (isset($p['dasiclinicReg'])) {
            $cobrament->setDasiclinicReg($p['dasiclinicReg']);
        }

        $cobrament->setCentre($this->centre);

        return $cobrament;
    }

    /**
     * create Cobrament entity
     * @param $p array
     * @return DB\Cobrament
     */
    public function createCobramentCarrec($p, $cobrament, $carrec, $carrecDiposit = null)
    {
        $cc = new DB\CobramentCarrec();

        $cc->setIdCobrament($cobrament);
        $cc->setIdCarrec($carrec);

        $p['importParcial'] = $p['importCobrat'];

        $cc->setImportParcial($p['importParcial']);

        if (isset($p['dipositParcial'])) {
            $cc->setDipositParcial($p['dipositParcial']);
        }

        // si no es passa carrecDiposit és el mateix que carrec
        if (!$carrecDiposit) {
            $carrecDiposit = $carrec;
        }
        $cc->setIdCarrecDiposit($carrecDiposit);

        return $cc;
    }
    // ---------------------------------- Auxiliars de facturació --------------------------- //

    public function createBancEntitat($p)
    {
        $entitat = new DB\BancEntitat();

        $entitat->setNom($p['entNom']);
        $entitat->setEntitat($p['entCodi']);
        if (isset($p['entBIC'])) {
            $entitat->setBic($p['entBIC']);
        }
        $entitat->setCentre($this->centre);

        return $entitat;
    }

    public function createBancCompte($p, $entitat)
    {
        $compte = new DB\BancCompte();

        $compte->setNom($p['nom']);
        $compte->setEntitat($entitat);
        $compte->setIban($p['iban']);

        if (isset($p['ccontable'])) {
            $compte->setCcontable($p['cc']);
        }

        if (isset($p['ccost'])) {
            $compte->setCcost($p['ccost']);
        }

        $compte->setCentre($this->centre);

        return $compte;
    }

    public function createFormaPagamentClient($p, $compte)
    {
        $fp = new DB\FormaPagamentClient();

        $fp->setNom($p['nom']);
        $fp->setBancCompte($compte);
        $fp->setPerComisio($p['perComisio']);
        $fp->setDiposit($p['diposit']);
        $fp->setPare($p['formaPagoPare']);
        $fp->setCentre($this->centre);

        if (isset($p['inactiu'])) {
            $fp->setInactiu($p['inactiu']);
        } else {
            $fp->setInactiu(false);
        }

        if (isset($p['dasiclinicReg'])) {
            $fp->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $fp;
    }

    public function updateFormaPagamentClient($fp, $p, $compte)
    {
        $fp->setNom($p['nom']);
        $fp->setBancCompte($compte);
        $fp->setPerComisio($p['perComisio']);
        $fp->setDiposit($p['diposit']);
        $fp->setPare($p['formaPagoPare']);

        if (isset($p['inactiu'])) {
            $fp->setInactiu($p['inactiu']);
        } else {
            $fp->setInactiu(false);
        }

        if (isset($p['dasiclinicReg'])) {
            $fp->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $fp;
    }


    // ----------------------------- fi de auxiliars de Facturació ---------------------------------- //



    /**
     * create Episodis entity
     * @param $p array
     * @return DB\Episodis
     */
    public function createEpisodi($p)
    {
        $episodi = new DB\Episodis();

        $episodi->setNumEpisodi($p['numEpisodi']);

        if (!isset($p['data'])) {
            $data = $p['dataVisita'];
        } else {
            $data = $p['data'];
        }

        $episodi->setDataInici($data);
        $episodi->setExercici($data->format("Y"));
        $episodi->setMes($data->format("m"));

        if (isset($p['dataAlta'])) {
            $episodi->setDataAlta($p['dataAlta']);
            $episodi->setActiu(false);

        } else {
            $episodi->setActiu(true);
        }

        if (isset($p['anamnesis'])) {
            $episodi->setAnamnesis($p['anamnesis']);
        }
        if (isset($p['exploracio'])) {
            $episodi->setExploracio($p['exploracio']);
        }
        if (isset($p['judici'])) {
            $episodi->setJudici($p['judici']);
        }
        if (isset($p['evolucio'])) {
            $episodi->setEvolucio($p['evolucio']);
        }
        if (isset($p['comentaris'])) {
            $episodi->setComentaris($p['comentaris']);
        }

        if (isset($p['episodiReg'])) {
            $episodi->setDasiclinicReg($p['episodiReg']);
        }  elseif (isset($p['dasiclinicReg'])) {
            $episodi->setDasiclinicReg($p['dasiclinicReg']);
        }

        $episodi->setTraspas($this->hash);

        return $episodi;
    }


    /**
     * create Episodis from Cita entity
     * @param $p array
     * @return DB\Episodis
     */
    public function createEpisodiFromCita($p, $cita)
    {
        $episodi = new DB\Episodis();

        $pacient = $cita ->getPacient();

        $episodi->setNumEpisodi($p['numEpisodi']);

        if (!isset($p['data'])) {
            $data = $p['dataVisita'];
        } else {
            $data = $p['data'];
        }

        $episodi->setDataInici($data);
        $episodi->setExercici($data->format("Y"));
        $episodi->setMes($data->format("m"));
        $episodi->setActiu(true);
        $episodi->setPacient($pacient);
        $episodi->setProfessional($cita->getProfessional());
        $episodi->setEspecialitat($cita->getEspecialitat());

        if (isset($p['anamnesis'])) {
            $episodi->setAnamnesis($p['anamnesis']);
        }
        if (isset($p['exploracio'])) {
            $episodi->setExploracio($p['exploracio']);
        }
        if (isset($p['judici'])) {
            $episodi->setJudici($p['judici']);
        }
        if (isset($p['evolucio'])) {
            $episodi->setEvolucio($p['evolucio']);
        }
        if (isset($p['comentaris'])) {
            $episodi->setComentaris($p['comentaris']);
        }

        if (isset($p['dasiclinicReg'])) {
            $episodi->setDasiclinicReg($p['dasiclinicReg']);
        }

        return $episodi;
    }

    /**
     * create Visita entity
     * @param $p array
     * @return DB\Visites
     */
    public function createVisita($p)
    {
        $visita = new DB\Visites();

        $visita->setData($p['dataVisita']);
        $visita->setHora($p['horaVisita']);
        $visita->setTemps($p['temps']);

        $visita->setExercici($p['dataVisita']->format("Y"));
        $visita->setMes($p['dataVisita']->format("m"));

        if (isset($p['observacionsVisita'])) {
            $visita->setObservacions($p['observacionsVisita']);
        } elseif (isset($p['observacions'])) {
            $visita->setObservacions($p['observacions']);
        }

        if (isset($p['provesComplementaries'])) {
            $visita->setProvesComplementaries($p['provesComplementaries']);
        }

        if (isset($p['visitaReg'])) {
            $visita->setDasiclinicReg($p['visitaReg']);
        }  elseif (isset($p['dasiclinicReg'])) {
            $visita->setDasiclinicReg($p['dasiclinicReg']);
        }

        $visita->setTraspas($this->hash);

        return $visita;
    }

    /**
     * update Visita entity
     * @param $p array
     * @return DB\Visites
     */
    public function updateVisita($visita, $p, $appendContent = false)
    {
        if (isset($p['dataVisita'])) {
            $visita->setData($p['dataVisita']);
        }
        if (isset($p['horaVisita'])) {
            $visita->setHora($p['horaVisita']);
        }
        if (isset($p['temps'])) {
            $visita->setTemps($p['temps']);
        }

        if (isset($p['observacionsVisita'])) {

            if ($appendContent) {
                $p['observacionsVisita'] = $visita->getObservacions() . '<br/>' . $p['observacionsVisita'];
            }
            $visita->setObservacions($p['observacionsVisita']);

        }

        if (isset($p['provesComplementaries'])) {

            if ($appendContent) {
                $p['provesComplementaries'] = $visita->getProvesComplementaries() . '<br/>' . $p['provesComplementaries'];
            }
            $visita->setProvesComplementaries($p['provesComplementaries']);
        }

        if (isset($p['visitaReg'])) {
            $visita->setDasiclinicReg($p['visitaReg']);
        }  elseif (isset($p['dasiclinicReg'])) {
            $visita->setDasiclinicReg($p['dasiclinicReg']);
        }

        $visita->setTraspas($this->hash);

        return $visita;
    }


    /**
     * create Visita from cita entity
     * @param $p array
     * @return DB\Visites
     */
    public function createVisitaFromCita($p, $cita, $episodi)
    {
        // crea una visita partint de al cita i afegeix tractaments de la cita
        $visita = $this->factService->createVisita($cita, $episodi, false);

        $cita->setExercici($p['data']->format("Y"));
        $cita->setMes($p['data']->format("m"));

        if (isset($p['observacionsVisita'])) {
            $visita->setObservacions($p['observacionsVisita']);
        }
        if (isset($p['provesComplementaries'])) {
            $visita->setProvesComplementaries($p['provesComplementaries']);
        }

        if (isset($p['dasiclinicReg'])) {
            $visita->setDasiclinicReg($p['dasiclinicReg']);
        }

        $visita->setTraspas($this->hash);

        return $visita;
    }

    // Removes special chars.
    function clean($string) {

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

}