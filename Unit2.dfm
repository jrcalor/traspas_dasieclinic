object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Trasp'#224's DasiClinic -->>> Dasi eClinic'
  ClientHeight = 846
  ClientWidth = 1560
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 467
    Top = 20
    Width = 54
    Height = 13
    Caption = 'ID Registre'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1560
    Height = 846
    Align = alClient
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 1558
      Height = 127
      Align = alTop
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 16
        Width = 69
        Height = 13
        Caption = 'Firebird origen'
      end
      object Label1: TLabel
        Left = 467
        Top = 44
        Width = 47
        Height = 13
        Caption = 'ID Centre'
      end
      object Label2: TLabel
        Left = 927
        Top = 18
        Width = 379
        Height = 16
        AutoSize = False
        Caption = 'Cl'#237'nica Origen FIREBIRD'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 927
        Top = 47
        Width = 430
        Height = 16
        AutoSize = False
        Caption = 'Cl'#237'nica Destino MySQL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 733
        Top = 22
        Width = 121
        Height = 13
        Caption = 'Acronim Usuaris duplicats'
      end
      object Label8: TLabel
        Left = 635
        Top = 4
        Width = 600
        Height = 16
        Caption = 
          'COMPTE MIRAR CAMPS BUITS QUE S'#39'ENCRIPTEN NO PASSAR-LOS I REVISAR' +
          ' TAMBE TEXTOS RTF'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 8
        Top = 44
        Width = 24
        Height = 13
        Caption = 'TEST'
      end
      object Label4: TLabel
        Left = 467
        Top = 69
        Width = 47
        Height = 13
        Caption = 'ID Centre'
      end
      object Label10: TLabel
        Left = 8
        Top = 69
        Width = 61
        Height = 13
        Caption = 'PRODUCCIO'
      end
      object Label11: TLabel
        Left = 733
        Top = 61
        Width = 94
        Height = 13
        Caption = #218'ltim Pac. IMATGES'
      end
      object Label12: TLabel
        Left = 927
        Top = 71
        Width = 173
        Height = 13
        Caption = 'Pacients a passar en el mateix bucle'
      end
      object Label14: TLabel
        Left = 744
        Top = 107
        Width = 177
        Height = 13
        Caption = 'Ruta per generar informes temporals'
      end
      object Label15: TLabel
        Left = 733
        Top = 84
        Width = 102
        Height = 13
        Caption = #218'ltim Pac. INFORMES'
      end
      object Edit3: TEdit
        Left = 103
        Top = 14
        Width = 357
        Height = 21
        ReadOnly = True
        TabOrder = 0
        Text = 
          '172.16.0.204/3050:/dades/bbdd/dasiclinic/cliniques/mchover/DASIC' +
          'LINIC.FDB'
      end
      object Edit2: TEdit
        Left = 104
        Top = 41
        Width = 357
        Height = 21
        TabOrder = 1
        Text = 'eclinic-test.coglltc7cvjd.eu-west-1.rds.amazonaws.com'
      end
      object Edit9: TEdit
        Left = 527
        Top = 41
        Width = 46
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object JvXPButton2: TJvXPButton
        Left = 621
        Top = 15
        Width = 99
        Caption = 'Connectar FIB'
        TabOrder = 3
        OnClick = JvXPButton2Click
      end
      object JvXPButton1: TJvXPButton
        Left = 621
        Top = 41
        Width = 99
        Caption = 'Connectar TEST'
        TabOrder = 4
        OnClick = JvXPButton1Click
      end
      object sbReiniciar: TJvXPButton
        AlignWithMargins = True
        Left = 1469
        Top = 4
        Width = 85
        Height = 119
        Caption = 'Reiniciar'
        Enabled = False
        TabOrder = 5
        Align = alRight
        OnClick = sbReiniciarClick
      end
      object Edit4: TEdit
        Left = 527
        Top = 15
        Width = 89
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
      end
      object Edit6: TEdit
        Left = 860
        Top = 19
        Width = 38
        Height = 21
        ReadOnly = True
        TabOrder = 7
        Text = 'TCH'
      end
      object ckExtern: TCheckBox
        Left = 733
        Top = 40
        Width = 140
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Es un trasp'#224's EXTERN'
        TabOrder = 8
      end
      object Edit8: TEdit
        Left = 527
        Top = 68
        Width = 46
        Height = 21
        ReadOnly = True
        TabOrder = 9
        Text = '13873'
      end
      object JvXPButton3: TJvXPButton
        Left = 621
        Top = 66
        Width = 99
        Caption = 'Connectar PROD'
        TabOrder = 10
        OnClick = JvXPButton3Click
      end
      object Edit7: TEdit
        Left = 103
        Top = 66
        Width = 358
        Height = 21
        TabOrder = 11
        Text = 'eclinic-prod.coglltc7cvjd.eu-west-1.rds.amazonaws.com'
      end
      object Edit1: TEdit
        Left = 574
        Top = 52
        Width = 46
        Height = 21
        ReadOnly = True
        TabOrder = 12
      end
      object Edit5: TEdit
        Left = 860
        Top = 58
        Width = 57
        Height = 21
        TabOrder = 13
        Text = '0'
      end
      object sp1: TSpinEdit
        Left = 1104
        Top = 66
        Width = 40
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 14
        Value = 10
      end
      object CheckBox2: TCheckBox
        Left = 1145
        Top = 69
        Width = 147
        Height = 17
        Caption = 'AUTO-REINICIAR despr'#232's'
        Checked = True
        State = cbChecked
        TabOrder = 15
      end
      object Edit10: TEdit
        Left = 927
        Top = 104
        Width = 437
        Height = 21
        TabOrder = 16
        Text = 'C:\TRASPASOS\dasi\pruebas\'
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 128
      Width = 1558
      Height = 717
      Align = alClient
      Caption = 'Panel2'
      ParentBackground = False
      TabOrder = 0
      object Panel3: TPanel
        Left = 1
        Top = 1
        Width = 1556
        Height = 156
        Align = alTop
        TabOrder = 0
        object Label13: TLabel
          Left = 7
          Top = 119
          Width = 240
          Height = 29
          Caption = 'Esperant 5 minuts...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Memo3: TMemo
          Left = 589
          Top = 1
          Width = 966
          Height = 154
          Align = alRight
          Alignment = taCenter
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Lines.Strings = (
            
              'Compte !!!  les taules que es relacionen aqui baix no tenen enca' +
              'ra alguns camps...  revisar abans de fer cada trasp'#224's '
            'si el client esta utilitzant aquests camps'
            ''
            
              'USUARIS el professional (no te els camps ACCES A L'#39'AGENDA ON-LIN' +
              'E i SOLAMENT POT VEURE L'#39'AGENDA DEL PROFESSIONALS ASSIGNAT)'
            
              'USUARIS COMPTE potser a m'#233's de l'#39'acronim cal posar un comptador ' +
              '(p.e. per SUPERVISOR)'
            'BONOS (verure com es gestionen les sessions consumides)'
            'TEMA FOTO PACIENT, S'#39'ha d'#39'implementar per que es puji al s3'
            'TEMA DOCUMENTS escanejats a la fitxa del pacient =='
            'TEMA ORIGENS, falta implementar i posar les altres dades'
            
              'TEMA ALERTA AGENDA, s'#39'ha de mirar a quin camp es posa del eClini' +
              'c')
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
        object Memo4: TMemo
          Left = 331
          Top = 1
          Width = 258
          Height = 154
          Align = alRight
          Lines.Strings = (
            'REVISAR ANTECEDENT i ANTECEDENTS PACIENT')
          TabOrder = 3
        end
        object CheckBox1: TCheckBox
          Left = 22
          Top = 5
          Width = 184
          Height = 17
          Caption = 'Revisar Poblacions/Provincies/Pais '
          TabOrder = 0
        end
        object sbPoblacions: TJvXPButton
          Left = 49
          Top = 28
          Width = 121
          Height = 32
          Caption = 'Poblacions'
          Enabled = False
          TabOrder = 1
          OnClick = sbPoblacionsClick
        end
        object Button1: TButton
          Left = 4
          Top = 88
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 4
          Visible = False
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 85
          Top = 88
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 5
          Visible = False
          OnClick = sbInformesClick
        end
        object Button3: TButton
          Left = 4
          Top = 64
          Width = 75
          Height = 25
          Caption = 'Arreglo emails'
          TabOrder = 6
          Visible = False
          OnClick = Button3Click
        end
      end
      object Panel5: TPanel
        Left = 1
        Top = 157
        Width = 1556
        Height = 159
        Align = alTop
        TabOrder = 1
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object sbEspe: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Especialitats i Categories'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbEspeClick
          end
          object sbTipusDoc: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Tipus documents'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbTipusDocClick
          end
          object sbEmpresa: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Empreses'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbEmpresaClick
          end
        end
        object Panel8: TPanel
          Left = 141
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object sbAntecedents: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Antecedents'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbAntecedentsClick
          end
          object sbProcedencies: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Procedencies'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbProcedenciesClick
          end
          object sbOrigens: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Origens'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbOrigensClick
          end
          object sbCies: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'CIES'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbCiesClick
          end
        end
        object Panel9: TPanel
          Left = 281
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object sbGrupsITtes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Grups i Tractaments'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbGrupsITtesClick
          end
          object sbTarifesGRP: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Grups de Tarifes'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbTarifesGRPClick
          end
          object sbTarifes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Tarifes'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbTarifesClick
          end
          object sbPreus: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Preus'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbPreusClick
          end
        end
        object Panel10: TPanel
          Left = 701
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 3
          object sbEntitats: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'ENTITATS'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbEntitatsClick
          end
          object sbFP: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Formes pagament'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbFPClick
          end
          object sbBancs: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = ' BANCS'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbBancsClick
          end
          object sbUsuaris: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Usuaris'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbUsuarisClick
          end
        end
        object Panel11: TPanel
          Left = 841
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 4
          object sbPacients: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Pacients'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbPacientsClick
          end
          object sbAntecedentsPAc: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Antecedents Pacient'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbAntecedentsPAcClick
          end
          object sbRelacionsC: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Relacions Confian'#231'a'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbRelacionsCClick
          end
          object sbTutors: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Tutors'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbTutorsClick
          end
        end
        object Panel12: TPanel
          Left = 981
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 5
          object sbCites: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Cites'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbCitesClick
          end
          object sbTipusCita: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Tipus de Cita'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbTipusCitaClick
          end
          object sbBoxes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Boxes'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbBoxesClick
          end
          object sbBonusPac: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Bonus Pacient'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbBonusPacClick
          end
        end
        object Panel13: TPanel
          Left = 561
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 6
          object sbAbsencies: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Absencies i bloquejos'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbAbsenciesClick
          end
          object sbApertures: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Apertures'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbAperturesClick
          end
          object sbHoraris: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Festius i horaris'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbHorarisClick
          end
          object sbProf: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Professionals'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbProfClick
          end
        end
        object Panel14: TPanel
          Left = 1121
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 7
          object sbVisites: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Visites'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbVisitesClick
          end
          object sbEpisodis: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Episodis'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbEpisodisClick
          end
          object sbInformes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Informes'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbInformesClick
          end
          object sbImatgesDocs: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 120
            Width = 134
            Height = 33
            Caption = 'Imatges i documents'
            Enabled = False
            TabOrder = 3
            Align = alTop
            OnClick = sbImatgesDocsClick
          end
        end
        object Panel15: TPanel
          Left = 421
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 8
          object sbConConta: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Conceptes CONTA'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbConContaClick
          end
          object sbClasImg: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Classific. imatges API'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbClasImgClick
          end
        end
        object Panel16: TPanel
          Left = 1261
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 9
          object sbCarrecs: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'C'#224'rrecs(FRA)/Abon. Priv.'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbCarrecsClick
          end
          object sbCobros: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Cobraments'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbCobrosClick
          end
        end
        object Panel17: TPanel
          Left = 1401
          Top = 1
          Width = 140
          Height = 157
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 10
          object sbCaixes: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 134
            Height = 33
            Caption = 'Caixes'
            Enabled = False
            TabOrder = 0
            Align = alTop
            OnClick = sbCaixesClick
          end
          object sbFraMutues: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 42
            Width = 134
            Height = 33
            Caption = 'Factures Mutua'
            Enabled = False
            TabOrder = 1
            Align = alTop
            OnClick = sbFraMutuesClick
          end
          object sbCobMutua: TJvXPButton
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 134
            Height = 33
            Caption = 'Cobraments Mutua'
            Enabled = False
            TabOrder = 2
            Align = alTop
            OnClick = sbCobMutuaClick
          end
        end
      end
      object Memo2: TMemo
        Left = 785
        Top = 316
        Width = 286
        Height = 400
        Align = alRight
        Ctl3D = False
        Lines.Strings = (
          'CAMPS QUE NO EXISTEIXEN A eCLINIC'
          ''
          '------ GRUPS TARIFES'
          'TRGGRUP                    codi'
          'TRGIMPORT_FIX'
          'TRGIMPORT_MINIM'
          'TRGIMPORT_MAXIM'
          ''
          '------ ESPECIALITATS'
          'ESPECIAL    indica si es odontologia'
          'CUENTA'
          'CENTROCOSTE'
          'ORDRE       no es fa servir'
          ''
          '------ GRUPS_TRACTAMENTS'
          'grtpeces, '
          'peptractamentbmp, '
          'grtobturacio, '
          'grtnivell, '
          'grtpont, '
          'grtprotesis, '
          'cuenta, '
          'ccoste, '
          ''
          '------ TRACTAMENTS'
          'trapeces,'
          'traminimfac,'
          'tracostmaterial,     indica si te cost de material 0/1'
          'traimatge,'
          'trapont,'
          'tracodi,'
          'ctrl_laboratori,'
          'traprotesis,'
          'estoc_minimo,'
          'travenda,'
          'agrupacio,'
          'grup,'
          'trabonus,             indica si es bonus'
          'trasessions,'
          ''
          '------- S_EMPRESES'
          'empcodi (NO CAL), emplogo (s'#39'ha de pujar manual '
          'fins que no estigui fet l'#39'API), sistema'
          ''
          '------- S_PROFESSIONALS'
          'PRFIMATGE, PRFPERCENTATGE, '
          'PERCENTATGE_MUTUA, IMPORT_FIX,'
          'IMPORT_MINIM, PRFPERCENTATGEMAT, '
          'TIPUS_LIQUIDACIO, '
          'ORDRE_PERCENTATGES, IRPF, ENTCODI, '
          'PRFAGENCIA, PRFDIGITS, '
          'PRFCOMPTE, ENTREGISTRE, PRFSERIE, '
          'HONORARIS_MODIFICAR, '
          'PRFORDREAGENDA, USA_BOX, USA_URGENCIAS, '
          'CENTRO_ORIGEN, '
          'COLABORADOR, AVISO_SMS, SINCRO_WEB, '
          'CITA_ONLINE, IDAGENDA, '
          'PRFIBAN  La majoria son del tema de liquidacions o '
          'cita on-line'
          ''
          '')
        ParentCtl3D = False
        ScrollBars = ssVertical
        TabOrder = 2
      end
      object DBGrid1: TDBGrid
        Left = 1
        Top = 316
        Width = 278
        Height = 400
        Align = alClient
        DataSource = DataSource1
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object Panel6: TPanel
        Left = 1071
        Top = 316
        Width = 486
        Height = 400
        Align = alRight
        Caption = 'Panel6'
        TabOrder = 4
        object Memo1: TMemo
          Left = 1
          Top = 1
          Width = 484
          Height = 112
          Align = alTop
          Lines.Strings = (
            'Memo1')
          TabOrder = 0
        end
        object RichEdit1: TRichEdit
          Left = 1
          Top = 113
          Width = 484
          Height = 286
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Lines.Strings = (
            'RichEdit1')
          ParentFont = False
          TabOrder = 1
          Zoom = 100
        end
      end
      object RichView1: TRichView
        Left = 859
        Top = 322
        Width = 522
        Height = 451
        TabOrder = 5
        Visible = False
        DoInPaletteMode = rvpaCreateCopies
        RTFReadProperties.TextStyleMode = rvrsAddIfNeeded
        RTFReadProperties.ParaStyleMode = rvrsAddIfNeeded
        RVFOptions = [rvfoSavePicturesBody, rvfoSaveControlsBody, rvfoSaveBinary, rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveDocProperties, rvfoLoadDocProperties, rvfoSaveDocObjects, rvfoLoadDocObjects]
        Style = RVStyle1
      end
      object DBRichViewPDF: TDBRichView
        Left = 396
        Top = 346
        Width = 521
        Height = 514
        DataField = 'INFORME'
        DataSource = dsInformes
        TabOrder = 6
        Visible = False
        DoInPaletteMode = rvpaCreateCopies
        RTFReadProperties.TextStyleMode = rvrsAddIfNeeded
        RTFReadProperties.ParaStyleMode = rvrsAddIfNeeded
        RVFOptions = [rvfoSavePicturesBody, rvfoSaveControlsBody, rvfoSaveBinary, rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveDocProperties, rvfoLoadDocProperties]
        Style = RVStyle1
      end
      object Memo5: TMemo
        Left = 694
        Top = 346
        Width = 624
        Height = 351
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Lines.Strings = (
          'REVISAR'
          ''
          'BONOS (S'#39'ha de crear un "bono master" de cada bono '#191'? )'
          ''
          
            'FORMES DE PAGAMENT (mirar tema d'#39'esborrar les creades per defect' +
            'e a '
          'eClinic)'
          ''
          
            'INFORMES - PLATILLES (revisar com passar ITEMS i relacionar els ' +
            'items del '
          'DasiClinic amb els del eClinic)'
          
            'INFORMES (ara els pujem com PDF, per'#242' lo seu seria que fossin te' +
            'xt html '
          
            'per que aix'#237' els poden tornar a editar. Cal mirar com pujar les ' +
            'imatges)'
          ''
          'FACTURAR BESTRETES al eClinic no hi es encara'
          ''
          
            'ABONAMENTS O RECTIFICATIVES (nosaltres fem una vista amb prfregi' +
            'stre '
          '0...)'
          'AJUSTOS MUTUES (nosaltres fem una visita amb PACREGISTRE 0)')
        ParentFont = False
        TabOrder = 7
        Visible = False
      end
      object SRichViewEdit1: TSRichViewEdit
        Left = 279
        Top = 316
        Width = 506
        Height = 400
        Cursor = crIBeam
        Align = alRight
        Constraints.MinWidth = 300
        Color = 10070188
        TabOrder = 8
        ExternalRVStyle = RVStyle1
        HScrollBarSchemeIndex = 0
        VScrollBarSchemeIndex = 0
        Version = 'v9.1'
        PageProperty.PageWidth = 210.000000000000000000
        PageProperty.PageHeight = 297.000000000000000000
        PageProperty.LeftMargin = 20.000000000000000000
        PageProperty.RightMargin = 20.000000000000000000
        PageProperty.TopMargin = 20.000000000000000000
        PageProperty.BottomMargin = 20.000000000000000000
        PageProperty.PageNoFont.Charset = DEFAULT_CHARSET
        PageProperty.PageNoFont.Color = clWindowText
        PageProperty.PageNoFont.Height = -11
        PageProperty.PageNoFont.Name = 'Arial'
        PageProperty.PageNoFont.Style = []
        PageProperty.PageBreak.Text = 'Page break'
        PageProperty.HeaderY = 100.000000000000000000
        PageProperty.FooterY = 100.000000000000000000
        ViewProperty.MarginBounds = [srvmbLeft, srvmbRight, srvmbTop, srvmbBottom]
        ViewProperty.MarginsRectVisible = True
        ViewProperty.HintFont.Charset = DEFAULT_CHARSET
        ViewProperty.HintFont.Color = clBlack
        ViewProperty.HintFont.Height = -13
        ViewProperty.HintFont.Name = 'Tahoma'
        ViewProperty.HintFont.Style = []
        ViewProperty.HintPrefixText = 'Page '
        ViewProperty.ZoomPanelFont.Charset = DEFAULT_CHARSET
        ViewProperty.ZoomPanelFont.Height = -11
        ViewProperty.ZoomPanelFont.Name = 'Arial'
        ViewProperty.ZoomPercent = 57.808563232421880000
        ViewProperty.ZoomPercentEdit = 100.000000000000000000
        ViewProperty.ZoomPercentIN = 31.433660507202150000
        ViewProperty.ZoomPercentOUT = 100.000000000000000000
        ViewProperty.ZoomPanelVisible = True
        ViewProperty.MainTitleFont.Charset = DEFAULT_CHARSET
        ViewProperty.MainTitleFont.Height = -11
        ViewProperty.MainTitleFont.Name = 'Tahoma'
        ViewProperty.MainTitleFont.Style = []
        ViewProperty.HeaderTitleFont.Charset = DEFAULT_CHARSET
        ViewProperty.HeaderTitleFont.Height = -11
        ViewProperty.HeaderTitleFont.Name = 'Tahoma'
        ViewProperty.HeaderTitleFont.Style = []
        ViewProperty.FooterTitleFont.Charset = DEFAULT_CHARSET
        ViewProperty.FooterTitleFont.Height = -11
        ViewProperty.FooterTitleFont.Name = 'Tahoma'
        ViewProperty.FooterTitleFont.Style = []
        ViewProperty.UseVCLThemes = False
        LineNumberProperty.Font.Charset = DEFAULT_CHARSET
        LineNumberProperty.Font.Height = -8
        LineNumberProperty.Font.Name = 'Tahoma'
        LineNumberProperty.Font.Style = []
        BackgroundProperty.TransparentColor = clBlack
        BackgroundProperty.PercentMiddle = 80.000000000000000000
        BackgroundProperty.FillType = srvtfGradientV
        BackgroundProperty.PicturePosition = srvtpTopLeft
        BackgroundProperty.Picture.Data = {
          07544269746D617036C20100424D36C201000000000036000000280000003F01
          000078000000010018000000000000C201000000000000000000000000000000
          0000E3BB9BE3BB9B000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9B000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9B000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9B000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9B000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDFBCA0DDB89B000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB496DDB89CDFBCA0DCB697000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DEBA9EDEBA9EDCB697000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DCB697DDB89BDFBCA0DDB89BDCB697
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDEB596DEB596DEB596DEB596DEB596DEB596DEBA9CE0BBA0
          DCB6980000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDEB596DEB596DEB596DEB596DEB596DEB596DEB596DCB697
          DEBA9EDEBA9E0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697
          DCB697DDB89BE0BCA0DEB99CDCB6970000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697
          DCB697DCB697DCB697DEBA9CE0BDA2DCB6980000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697
          DCB697DCB697DCB697DCB697DCB698E0BC9FE0BC9FDCB6980000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697
          DCB697DCB697DCB697DCB697DCB697DCB697DDB899E0BDA2DEBA9CDCB6970000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697
          DCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697DDB89BE0BDA2DDB8
          9BDCB69700000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697
          DCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697E0BA
          9EE0BCA0DEB79A00000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697
          DCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB697DCB6
          97DCB698E0BC9FE0BCA0DCB69800000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698
          DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB6
          98DCB698DCB698DEB89AE0BDA2E0BC9FDCB69800000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698
          DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB6
          98DCB698DCB698DCB698DCB698DEB99CE0BDA2DEBA9EDCB69800000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698
          DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB6
          98DCB698DCB698DCB698DCB698DCB698DCB698DEBA9EE0BDA2DEB99CDCB69800
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698
          DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB6
          98DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698E0BC9FE1BEA2DE
          B89ADCB698000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698
          DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB6
          98DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DDB899E0
          BCA0E0BCA0DEB89A000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698
          DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB6
          98DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DCB698DC
          B698DDB899E0BCA0E0BCA0DDB899DCB697000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79A
          DEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB7
          9ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADE
          B79ADEB79ADEB79ADEB99CE0BDA2E0BCA0DEB89A000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BDEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79A
          DEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB7
          9ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADE
          B79ADEB79ADEB79ADEB79ADEB79ADEB99CE2BFA4E0BC9FDEB79A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE2BCA0DEBA9CDEB79ADEB79ADEB79ADEB79ADEB79ADEB79A
          DEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB7
          9ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADE
          B79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEBA9CE2BEA2E0BC9FDEB79A
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE3C0A6E3C0A6E2BFA4E0BC9FDEB89ADEB79ADEB79ADEB79A
          DEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB7
          9ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADE
          B79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEBA9CE2BEA2
          E0BC9FDEB79A0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE3C0A6E3C0A6E3C0A6E3C0A6E2C0A4E2BCA0DEB99CDEB79A
          DEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB7
          9ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADE
          B79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79ADEB79A
          E0BC9FE2BFA4DEBA9CDEB79A0000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE3C0A6E3C0A6E3C0A6E3C0A6E3C0A6E3C0A6E3C0A6E2BFA4
          E0BC9FDEB99CDEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB8
          9ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADE
          B89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89A
          DEB89ADEB89AE0BCA0E2BFA4DEBA9CDEB89A0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE3C0A6E3C0A6E3C0A6E3C0A6E3C0A6E3C0A6E3C0A6E3C0A6
          E3C0A6E3C0A6E2BEA2DEBA9CDEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB8
          9ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADE
          B89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89A
          DEB89ADEB89ADEB89ADEB89AE0BCA0E2BFA4DEBA9CDEB89A0000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6
          E4C0A6E4C0A6E4C0A6E4C0A6E3C0A6E2BCA0DEB99CDFB899DFB899DFB899DFB8
          99DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DF
          B899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899
          DFB899DFB899DFB899DFB899DFB899DFB899E0BCA0E2BFA4E0BA9CDFB8990000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6
          E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E2BEA2E0BA9CDFB899DFB8
          99DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DF
          B899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899
          DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899E0BCA0E2BFA4E0BA
          9CDFB89900000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6
          E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E3C0A6E2BC
          A0DEB99CDFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DF
          B899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899
          DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899E0BC
          A0E2BFA4E0BA9CDFB89900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6
          E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0
          A6E4C0A6E2BFA4E0BA9EDFB899DFB899DFB899DFB899DFB899DFB899DFB899DF
          B899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899
          DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB899DFB8
          99DFB899E0BCA0E2BFA4E0BC9FDFB89900000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6
          E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0A6E4C0
          A6E4C0A6E4C0A6E4C0A6E3C0A6E2BCA0E0B99CDEB89ADEB89ADEB89ADEB89ADE
          B89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89A
          DEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB89ADEB8
          9ADEB89ADEB89ADEB89AE0BCA0E2BFA4E0BCA0DEB89A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6
          E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2
          A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E2BFA4E0BCA0DEB99CDEB99CDE
          B99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99C
          DEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB9
          9CDEB99CDEB99CDEB99CDEB99CDEB99CE0BCA0E2BFA4E0BCA0DEB99C00000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6
          E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2
          A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E2BFA4E0
          BA9CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99C
          DEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB9
          9CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CDEB99CE0BA9EE2BFA4E0BCA0DE
          B99CDEB698000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6
          E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2
          A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4
          C2A6E4C0A4E2BCA0E0BA9CE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9B
          E0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA
          9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9EE2
          BFA4E2BEA2E0BA9C000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6
          E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2
          A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4C2A6E4
          C2A6E4C2A8E4C2A8E4C2A8E2BFA4E2BC9EE0BA9BE0BA9BE0BA9BE0BA9BE0BA9B
          E0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA
          9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0BA9BE0
          BA9BE0BA9CE2BFA4E2BEA2E0BA9CDFB899000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8
          E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2
          A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4
          C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C0A6E2BFA4E0BA9CE0B99CE0B99C
          E0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B9
          9CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0
          B99CE0B99CE0B99CE0BA9CE2BFA4E2BFA4E0BA9CE0B89A000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8
          E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2
          A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4
          C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E5C3AAE4C0A6E2BCA0
          E0BA9CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B9
          9CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0B99CE0
          B99CE0B99CE0B99CE0B99CE0B99CE0BA9CE2BEA2E2BFA4E0BA9EE0B89A000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E6C3A8E6C3A8
          E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3
          A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6
          C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E5C3AAE5C3AAE5C3AA
          E6C3A8E2BFA4E0BC9FE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA
          9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0
          BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE2BEA2E2C0A4E0BC9F
          E0BA9B0000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E6C3A8E6C3A8
          E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3
          A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6
          C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E5C3AAE5C3AAE5C3AA
          E5C3AAE5C3AAE5C3AAE4C2A6E2BFA4E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA
          9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0
          BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE2BCA0
          E3C0A6E2BCA0E0BA9C0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE4C2A6E4C2A8E4C2A8E4C2A8E4C2A8E4C2A8E6C3A8E6C3A8
          E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3
          A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6
          C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E5C3AAE5C3AAE5C3AA
          E5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE4C0A6E2BCA0E0BA9CE0BA9CE0BA
          9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0
          BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9C
          E0BA9CE0BC9FE2C0A4E2BFA4E0BA9CDFB8990000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A7E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3
          A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6
          C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C4A8E6C4A8E6C4A8E4BFA4E2BC
          A0E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0
          BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9C
          E0BA9CE0BA9CE0BA9CE2BA9CE2BFA4E2BFA4E2BA9CE0B89A0000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3
          AAE5C3AAE6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6
          C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C3A8E2BFA4E2BA9EE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0
          BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9C
          E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE2BFA4E2BFA4E2BCA0E0B99CDEB6
          9800000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3
          AAE5C3AAE6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6
          C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C1A6E2BEA2E2BA9EE0BA9CE0BA9CE0BA9CE0BA9CE0
          BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9C
          E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE2BCA0E4C0A6E2BC
          A0E0BA9C00000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E5C3AAE5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4
          A8E5C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E4C0A4E2BEA0E2BC9DE0BA9CE0
          BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9C
          E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE2BC
          A0E3C0A6E2BFA4E2BC9DE0B89A00000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E5C3AAE5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4
          A8E5C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E4C0A4E2
          BEA0E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9C
          E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA
          9CE0BA9CE2BA9EE3C0A6E3C0A6E2BA9EE0BA9B00000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E5C3AAE5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4
          A8E5C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C3A8E4C0A4E2BC9EE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9C
          E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA
          9CE0BA9CE0BA9CE0BA9CE2BC9DE2BFA4E3C0A6E2BEA0E0BA9C00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AA
          E5C3AAE5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE5C3AAE2BFA4E2BA9EE0BA9CE0BA9CE0BA9CE0BA9C
          E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA
          9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE2BEA0E3C0A6E2BFA4E0BA9EE0
          B89A000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AAE5C3AA
          E5C3AAE5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4A8E5C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C3A8E2BEA2E0BA9EE0BA9C
          E0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA
          9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE0BA9CE2BA9EE3C0A6E3
          C0A6E2BCA0E0BA9C000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8E6C3A8
          E6C3A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C1A6
          E2BEA2E2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA
          9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2BA9CE2
          BA9EE2BFA4E4C0A6E2BEA2E0BA9CE0B89A000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4A8E5C2A5E2BEA0E2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC
          9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2
          BC9DE2BC9DE2BC9DE2BEA0E4C2A6E4C0A4E2BC9EE0BA9B000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4A8E5C2A5E2BCA0E2BC9DE2BC9DE2BC9DE2BC
          9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2
          BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9EE4C0A4E4C2A6E2BEA0E0BA9CDFB899
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6
          C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4A8E5C2A5E2BCA0E2BC
          9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2
          BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BC9DE2BEA2E4C2A6E2BFA4
          E2BC9EE2B99A0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          A8E4C0A4E2BCA0E2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2
          BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BCA0
          E2BCA0E4C2A6E4C0A6E4C0A6DFB8990000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4A8E4BFA4E2BCA0E2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2
          BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9E
          E2BA9EE2BA9EE2BC9EE2BC9EE2BFA4E2BC9EE2B99A0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4A8E4BFA4E2BCA0E2BA9EE2BA9EE2
          BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9EE2BA9E
          E2BA9EE2BA9EE2BA9EE2BA9EE2BCA0E4C0A6E4C0A6E2BCA0E0BA9C0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5AC
          E7C5ACE7C5ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE6C4A8E4C0A2E2
          BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9E
          E2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE4BFA4E4BFA4E2BC
          9EE0BA9B00000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8
          E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4A8E6C4
          A8E6C4A8E6C4A8E6C4A8E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5AC
          E7C5ACE7C5ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE6C4A7E4C0A2E2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9E
          E2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE4C2
          A6E5C2A5E2BEA0E2BC9DE2B99900000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5ACE7C5ACE7C5ACE7C5
          ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5AC
          E7C5ACE7C5ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE6C3A8E4C0A4E2BCA0E2BC9EE2BC9EE2BC9EE2BC9E
          E2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC
          9EE2BC9EE2BC9EE2BC9EE3BE9FE3BE9FE0BA9C00000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5ACE7C5ACE7C5ACE7C5
          ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5AC
          E7C5ACE7C5ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE6C3A8E4C0A4E2BCA0E2BC9E
          E2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC
          9EE2BC9EE2BC9EE2BC9EE2BC9EE5C2A5E4C2A8E4BFA4E2BA9EE2B99A00000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5ACE7C5ACE7C5ACE7C5
          ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5AC
          E7C5ACE7C5ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE6C3A8
          E4C0A4E2BCA0E2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC
          9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE4BFA4E5C2A5E5C2A5E3BE9FE2
          BA9C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5ACE7C5ACE7C5ACE7C5
          ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5AC
          E7C5ACE7C5ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE6C3A8E4C0A4E2BCA0E2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC
          9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BCA0E4C0A4E5
          C3AAE4BFA4E2BC9EE2B99A000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C6AAE6C6AAE6C6AA
          E6C6AAE6C6AAE6C6AAE6C6AAE6C6AAE6C6AAE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE6C4A8E4C0A4E2BEA0E2BC9EE2BC9EE2BC
          9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2BC9EE2
          BC9EE2BEA0E5C2A5E5C2A5E2BEA2E2BC9DE2B999000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AA
          E6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4
          AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6
          C4AAE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE8C6AAE8
          C6AAE8C6AAE8C6AAE8C6AAE8C6AAE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6
          ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8
          C6ACE8C6ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE6C4A8E6C1A6E4BE
          A0E3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3
          BE9FE3BE9FE3BE9FE3BE9FE4C0A4E6C2A6E5C2A5E4BEA0E2BA9CE2B999000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5AC
          E7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5
          ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6
          ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE7C5ACE6C1A6E4BDA1E3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3
          BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE4BDA1E5C2A5E6C3A8E4C0A4E3BE9F
          E2BA9C0000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5AC
          E7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5
          ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6
          ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE7C5ACE6C1A6E4BDA1E3BE9FE3BE9FE3BE9FE3BE9FE3
          BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE4BEA2E6C3A8
          E6C2A6E4C0A4E2BC9EE2BA9A0000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5AC
          E7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5
          ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6
          ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE7C6ACE6C3A8E4BEA2E3BE9FE3
          BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9F
          E3BE9FE4C0A4E6C2A6E6C2A6E4BEA2E4BB9CE2B99A0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE6C4AAE7C5ACE7C5AC
          E7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5
          ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7
          C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C5ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6
          ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE7C6ACE6
          C3A8E4BEA2E3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9F
          E3BE9FE3BE9FE3BE9FE3BE9FE4C0A4E6C3A8E5C2A5E4BDA1E2BA9C0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C6AAE6C6AAE6C6AAE6C6AAE6C6AAE6C6AAE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C7AEE6C4A8E4C0A4E3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9F
          E3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE4BEA0E6C4A7E6C4A8E5C2A5E3BE
          9FE2BA9A00000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C6AAE6C6AAE6C6AAE6C6AAE6C6AAE6C6AAE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C7AEE6C4A8E5C2A5E2BEA0E3BE9FE3BE9F
          E3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE4BFA4E6C3
          A8E6C4A8E5C2A5E3BE9FE2BA9C00000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE6C6AAE6C6AAE6C6AAE6C6AAE6C6AAE6C6AAE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7
          C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE6C4AAE5C2A5
          E4BEA2E3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE9FE3BE
          9FE3BE9FE5C2A5E6C3A8E6C3A8E4C0A4E2BC9EE2BA9A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C6ACE6C2A6E6BFA2E4BEA0E4BEA0E4BEA0E4BEA0E4BEA0E4BEA0E4BE
          A0E4BEA0E4BEA0E4BEA0E4BEA0E6C1A6E6C3A8E6C3A8E4C0A2E2BC9DE2BA9A00
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C7AEE6C4A8E4C0A4E4BEA0E4BEA0E4BEA0E4BE
          A0E4BEA0E4BEA0E4BEA0E4BEA0E4BEA0E4BDA1E4BEA0E6C1A6E6C4AAE6C3A8E4
          BEA2E2BC9EE2BA9A000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE6C4A8E6C1A6E4BE
          A0E4BEA0E4BEA0E4BEA0E4BEA0E4BEA0E4BDA1E4BDA1E4BDA1E4BDA1E6BFA2E6
          C2A6E6C4AAE6C2A6E4BDA1E4BB9CE2B99A000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8
          C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8
          C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8AD
          E8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE6C4AAE6C2A6E6BFA2E4BEA0E4BEA0E4BEA0E4BEA0E4BEA0E4BEA0E4BEA0E4
          BEA0E4BEA0E4C0A2E6C4A7E6C4AAE6C4A7E6BFA2000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFE8C7AEE6C4A8E4C0A4E4BEA0E4BEA0E4BEA0E4BEA0E4
          BEA2E4BEA2E4BEA2E4BEA2E4BEA2E4C0A2E6C3A8E7C6ACE6C3A8E6BFA2000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC9AFE8C8AEE6C4AAE6C2A6E4BEA0E4
          BEA2E4BEA2E4BEA2E4BEA2E4BEA2E4BEA2E4BEA2E4BEA2E4C0A4E6C4A8E7C6AC
          E6C3A8E6BFA20000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6AC
          E7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6ACE7C6
          ACE7C6ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFE7
          C5ACE6C4A7E4C0A2E4BEA2E4BEA2E4BEA2E4BEA2E4BEA2E4BEA2E4BEA2E4BEA2
          E4C0A4E6C4A8E6C4AAE6C3A8E6BFA20000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFE8C7AEE6C4AAE6C0A4E6BFA2E6BFA2E6BFA2E6BFA2E6BFA2
          E6BFA2E6BFA2E6BFA2E6C0A4E8C4AAE8C6AAE6C3A8E6BFA20000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC8AFE8C6AAE6C3A8E4C0A2E6BFA2
          E6BFA2E6BFA2E6BFA2E6BFA2E6BFA2E6BFA2E6C1A6E6C4A8E8C4AAE6C3A8E6BF
          A200000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6ACE8C6AC
          E8C6ACE8C6ACE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8
          C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEA
          C8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8B0EAC8B0EAC8B0EAC8B0EAC8B0EAC8B0EAC8
          B0EAC8B0EAC8B0EAC8B0EAC8B0EAC8B0EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEACAB1EACAB1EACAB1EACAB1EACAB1EACAB1E8C7AE
          E8C3A9E6C1A6E6BFA2E6BFA2E6BFA2E6BFA2E6BFA2E6BFA2E6BFA2E6C0A4E6C4
          A8E7C5ACE6C4A8E6BFA400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEACAB0EACAB0EACAB0EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1
          EACAB1EACAB1EAC9AFE8C6ACE6C4A7E4C0A2E4C0A2E4C0A2E4C0A2E4C0A2E4C0
          A2E4C0A2E6C1A6E6C4A8E8C6ACE6C4AAE6C1A600000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C8ADE8C8ADE8C8ADE8C8ADE8C8ADE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEACAB0EACAB0EACAB0EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1
          EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1E8C7AEE8C3A9E6C1A6E4C0A2E4C0
          A2E4C0A2E4C0A2E6C0A2E6C0A2E6C1A6E8C6A9E7C5ACE7C5ACE6C1A600000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C8ADE8C8ADE8C8ADE8C8ADE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEACAB0EACAB0EACAB0EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1
          EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EAC9AFE8C6
          AAE6C4A7E4C0A2E6C0A2E6C0A2E6C0A2E6C0A2E6C0A2E6C0A4E6C4A8E7C5ACE8
          C6ACE6C4A7E4BEA0000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C8ADE8C8ADE8C8ADE8C8ADE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEACAB0EACAB0EACAB0EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1
          EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACAB1EACA
          B1EACAB1EACAB1E8C7AEE8C4AAE6C2A6E6C0A2E6C0A2E6C0A2E6C0A4E6C0A4E6
          C0A4E6C4A8E8C6AAE8C6AAE6C4A7E4C0A2000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C8ADE8C8ADE8C8ADE8C8ADE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEBC8AFEBC8AFEBC8AF
          EBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8
          AFEBC8AFEBC8AFEBC8AFEBC8AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EACAB0E8C6ACE8C4A8E6C0A4E6C0A4E6
          C0A4E8C1A4E8C1A4E8C1A4E6C4A8E8C6AAE8C6AAE8C3A9E6C0A2000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AE
          E8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7AEE8C7
          AEE8C7AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEA
          C8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEBC8AFEBC8AFEBC8AF
          EBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8
          AFEBC8AFEBC8AFEBC8AFEBC8AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB2EBCAB2
          EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCA
          B2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EAC8AFEA
          C5AAE6C2A6E7C2A5E7C2A5E7C2A5E7C2A5E7C2A5E6C2A6E8C6AAE8C7AEE8C4AA
          E7C2A50000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1E8C8AEE8C6AAE7C2A5E7C2A5E7C2A5E7C2A5E7C2A5E8C2A6
          E8C6AAE8C6ACE8C6ACE6C4A7E6BFA20000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EACAB0E8C7AEE8C6A9E7C2A5E7C2A5
          E7C2A5E7C2A5E7C2A5E8C4AAE8C6ACE8C6ACE8C6A9E6C0A20000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EAC9AF
          E8C6AAE8C2A6E7C2A5E7C2A5E7C2A5E7C2A5E8C4A8E8C6ACE8C6ACE8C6AAE8C1
          A400000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2ECCCB3
          ECCCB3ECCCB3EBCBB1E8C8AEE8C6AAE8C2A6E7C2A5E7C2A5E7C2A5E8C2A6E8C6
          AAE8C8ADE8C7AEE6C4A7E6BFA200000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AE
          E8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8
          AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8
          C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEE8C8AEEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEA
          C8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8B0EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCA
          B2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EB
          CAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCCB2EBCCB2EBCCB2ECCCB3
          ECCCB3ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4EACAB1E8C8AEEAC5AAE6C4A7E7C2
          A5E7C2A5E6C4A7EAC5AAE8C7AEE8C7AEE8C4AAE6C0A2E4BEA000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AF
          EAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEA
          C8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AFEBC8AF
          EBC8AFEBC8AFEBC8AFEBC8AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCA
          B2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EB
          CAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCCB2EBCCB2EBCCB2ECCCB3
          ECCCB3ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4EBCA
          B1EAC6ACE8C3A9E8C2A6E8C2A6E8C2A6E8C3A9E8C7AEEAC8AEEAC6ACE8C2A6E6
          BEA0000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC9AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EB
          CCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB3
          ECCCB3ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3EBC9AFE8C7AEEAC5A8E8C2A6E8C2A6E8C4A8EAC6AAEA
          C8AEEAC8AEE8C6A9E8C1A4000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC9AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EB
          CCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB3
          ECCCB3ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCCB3EBCAB0EAC6ACE8C4A8E8
          C2A6E8C2A6E8C6AAEAC8AEEAC8AEEAC6ACE8C2A6E6BFA2000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC9AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EB
          CCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB3
          ECCCB3ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3EC
          CCB3EACAAFEAC6ACE8C4A8E8C2A6E8C4A8EAC6ACEAC8AEEAC8AEEAC5A8E8C1A4
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8
          AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEA
          C8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC9AE
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EB
          CCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB3ECCCB3
          ECCCB3ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3EC
          CDB3ECCDB3ECCDB3ECCDB3ECCCB3EAC9AFEAC6AAE8C4A6E8C2A6E8C6AAEAC8AE
          EAC8AEEAC6ACE8C2A6E6BFA20000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEA
          C8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2
          EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCA
          B2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EB
          CAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EB
          CCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4ECCCB4ECCCB4
          ECCCB4ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6ECCDB6ECCDB6ECCD
          B6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6EC
          CDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6EBCCB2EBC9AFEAC6AC
          E8C4A8E8C3A9EAC6ACEBC9AFEAC8AFEAC6AAE8C1A4E6BEA00000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AEEAC8AE
          EAC8AEEAC8AEEAC8AEEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8
          AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEA
          C8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AFEAC8AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2
          EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCA
          B2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EB
          CAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCAB2EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EB
          CCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4ECCCB4ECCCB4
          ECCCB4ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6ECCDB6ECCDB6ECCD
          B6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6EC
          CDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6
          ECCDB6EBCCB2EBC9AFEAC6ACE8C3A9EAC5AAEAC8AEEBC9AFEAC8AFE8C3A9E6BF
          A200000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC9AEEAC9AEEAC9AEEAC9AEEAC9AE
          EAC9AEEAC9AEEAC9AEEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9
          AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEA
          C9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEACAB0EACA
          B0EACAB0EACAB0EACAB0EACAB0EACAB0EACAB0EACAB0EACAB0EACAB0EACAB0EA
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EB
          CCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4EBCCB4ECCDB3ECCDB3
          ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6ECCEB4ECCEB4ECCE
          B4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4EC
          CEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB6
          ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCDB3EBCAB0EAC8ACE8C6A9E8C6ACEAC9
          AFEAC9AFEAC8ACE8C2A6E6C0A000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC9AEEAC9AEEAC9AEEAC9AEEAC9AE
          EAC9AEEBC9AEEBC9AEEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2EC
          CAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2
          ECCAB2ECCCB2ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4EC
          CCB4ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3
          ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6EECDB4ECCEB4ECCEB4ECCE
          B4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4EC
          CEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB6
          ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCDB3ECCA
          B0EAC8ACE8C6AAEBC9AEEBC9AFEBCAB0EAC6AAE8C2A4E8BFA000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AEEAC8AEEAC8AEEAC9AEEAC9AEEAC9AEEAC9AEEAC9AE
          EAC9AEEBC9AEEBC9AEEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2EC
          CAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2
          ECCAB2ECCCB2ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4EC
          CCB4ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3
          ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6EECDB4ECCEB4ECCEB4ECCE
          B4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4EC
          CEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB6
          ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCE
          B6ECCEB6ECCEB6EDCDB3ECCAB0EAC8ACEAC8ACEBC9AFECCAB0EBCAB0EAC5A8E6
          C0A2E8BFA0000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB2EB
          CAB2EBCAB2EBCAB2EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCAB2EBCAB2EBCAB2EBCA
          B2EBCAB2EBCAB2EBCAB2EBCAB2ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB4ECCCB4ECCCB4ECCC
          B4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4EC
          CCB4ECCCB4ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6ECCDB6ECCDB6
          ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EC
          CEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6
          ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6EDCFB8EDCFB8EDCDB3ECCAB2EBCAB0EBC8AFEB
          CAB1EBCAB1EBC9AEE8C4A8E6C0A2000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB2EB
          CAB2EBCAB2EBCAB2EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCAB2EBCAB2EBCAB2EBCA
          B2EBCAB2EBCAB2EBCAB2EBCAB2ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB4ECCCB4ECCCB4ECCC
          B4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4EC
          CCB4ECCCB4ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6ECCDB6ECCDB6
          ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EC
          CEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6
          ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EC
          CEB4ECCAB2EBCAB1EBC9AFEBCAB1ECCAB2EAC6ACE8C2A6E6BFA2000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC8AFEAC8AFEAC8AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEB
          C9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB2EB
          CAB2EBCAB2EBCAB2EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCAB2EBCAB2EBCAB2EBCA
          B2EBCAB2EBCAB2EBCAB2EBCAB2ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB4ECCCB4ECCCB4ECCC
          B4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4ECCCB4EC
          CCB4ECCCB4ECCCB4ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6ECCDB6ECCDB6
          ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EC
          CEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6
          ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8ED
          CFB8EECFB8EECFB8EECFB8EECEB6EDCCB3ECCAB2ECCAB2EBCAB1EBCAB1EAC6AC
          E7C2A5E6BFA20000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEAC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EB
          CCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2
          EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB4ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3EC
          CDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6ECCDB6ECCDB6
          ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EC
          CEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6
          ECCEB6ECCEB6ECCEB6ECCEB6EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCF
          B8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8ED
          D0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EECFB8EDCDB3EDCCB3
          ECCCB3EBCBB1EBCBB1EAC6AAE7C2A5E6C0A00000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AFEAC9AF
          EAC9AFEAC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EB
          CCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2
          EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB4ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3EC
          CDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB6ECCDB6ECCDB6
          ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6ECCDB6EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EC
          CEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6
          ECCEB6ECCEB6ECCEB6EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCF
          B8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8EDCFB8ED
          D0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7
          EED0B8EED0B8EDCDB3EDCDB3EDCDB3ECCDB3EBCBB0E8C6AAE8C2A2E6C0A00000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9
          AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EB
          CAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EC
          CAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2
          ECCAB2ECCAB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCC
          B2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2EC
          CCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2
          ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3
          EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCDB3EDCDB3EDCDB3EDCD
          B3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3ED
          CDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EECDB4EECDB4EECDB4
          EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EECEB6EECEB6EECEB6EECEB6EECEB6EE
          CEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6
          EECEB6EECEB6EECEB6EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECF
          B8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8ED
          D0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EDD0B7EED0B6
          EED0B6EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EECEB6EECEB6EDCDB3ECCA
          B0EAC6AAEAC2A3E8C0A000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1ECCAB2ECCAB2EC
          CAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB4ECCCB4ECCCB4EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3
          EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCDB3EDCDB3EDCDB3EDCD
          B3EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EE
          CDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4
          EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EECEB6EECFB8EECFB8EECFB8EECFB8EE
          CFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8
          EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECF
          B8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EE
          D0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8
          EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EFD0BAEFD0BAEFD0BAEFD0
          BAEECFB8EECFB8ECCEB4ECC9B0EAC6AAE8C1A4E6BFA200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1ECCAB2ECCAB2EC
          CAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB4ECCCB4EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3
          EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCDB3EDCDB3EDCDB3EDCD
          B3EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EE
          CDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4
          EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EECEB6EECFB8EECFB8EECFB8EECFB8EE
          CFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8
          EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECF
          B8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EE
          D0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8
          EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EFD0BAEFD0BAEFD0BAEFD0
          BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEECEB6ECCAB2EAC6AAE8C1A4E6
          BFA2000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBC9AF
          EBC9AFEBC9AFEBC9AFEBC9AFEBC9AFEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB1EBCAB1EB
          CAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1ECCAB2ECCAB2EC
          CAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB4ECCCB4EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3
          EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCDB3EDCDB3EDCDB3EDCD
          B3EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EE
          CDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4
          EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EECEB6EECFB8EECFB8EECFB8EECFB8EE
          CFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8
          EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECF
          B8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EE
          D0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8
          EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EFD0BAEFD0BAEFD0BAEFD0
          BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD2BAEFD0BAEE
          CFB8EDCCB3EAC6AAE8C1A4E6BFA2000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCBB0EBCBB0EBCBB0EBCBB0EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCCB2EBCCB2
          EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB2ECCCB2EC
          CCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCDB3ECCDB3EC
          CDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3
          ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3EC
          CDB3ECCDB3ECCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3
          EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCD
          B3EECDB4EECDB4EECDB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4EC
          CEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4
          ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6EDCFB8EECFB8EECFB8EECFB8EECFB8EECFB8EE
          CFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8
          EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EED0
          B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EE
          D0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8
          EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EFD2B9EFD2B9EFD2B9EFD2
          B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEF
          D0BAEFD2BAF0D4BCF0D2BCEFD0BAEECEB4ECC8AEE9C4A5E8C0A0000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0
          EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCAB0EBCA
          B0EBCAB0EBCAB0EBCAB0EBCAB0EBCBB0EBCBB0EBCBB0EBCBB0EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCCB2EBCCB2
          EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB2EC
          CCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB2ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCDB3EC
          CDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3
          ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3EC
          CDB3ECCDB3ECCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3
          EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCD
          B3EECDB4EECDB4EECDB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4EC
          CEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4
          ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCEB4ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6EDCFB8EECFB8EECFB8EECFB8EECFB8EECFB8EE
          CFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8
          EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EED0
          B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EE
          D0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8
          EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EFD2B9EFD2B9EFD2B9EFD2
          B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD0BAEFD0BAEFD0BAEFD0BAEF
          D0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD2BAF0D3BEF0D4BEEFD2BCEDCEB7EBC9AF
          E8C3A40000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1
          EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCB
          B1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCCB2EBCCB2
          EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCC
          B2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2EBCCB2ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCDB3EC
          CDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3
          ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCD
          B3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3ECCDB3EC
          CDB3ECCDB3ECCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3
          EDCDB3EDCDB3EDCDB3EDCDB3EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECD
          B4EECDB4EECDB4EECDB4ECCEB4ECCEB4ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6EC
          CEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6
          ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCEB6ECCE
          B6ECCEB6ECCEB6ECCEB6ECCEB6EDCFB8EECFB8EECFB8EECFB8EECFB8EECFB8EE
          CFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8
          EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EED0
          B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EE
          D0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EED0B8EFD2B9
          EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2
          B9EFD2B9EFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEF
          D0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD2BD
          EFD4BFEFD3BDEED1B9EBCBB2E9C4A9E7C0A30000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E3BB9BE3BB9BEBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1
          EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCAB1EBCA
          B1EBCAB1EBCAB1EBCAB1EBCAB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EB
          CBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1EBCBB1ECCAB2ECCAB2
          ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCA
          B2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2EC
          CAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCAB2ECCCB2ECCCB2
          ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCC
          B3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3EC
          CCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3ECCCB3
          ECCCB3ECCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCC
          B3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCCB3EDCDB3ED
          CDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3
          EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCD
          B3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3ED
          CDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3EDCDB3
          EDCDB3EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECDB4EECD
          B4EECDB4EECDB4EECEB4EECEB4EECEB4EECEB6EECEB6EECEB6EECEB6EECEB6EE
          CEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6
          EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECEB6EECE
          B6EECEB6EECEB6EECEB6EECEB6EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EE
          CFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8
          EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECFB8EECF
          B8EED0B8EED0B8EFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEF
          D0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD0BAEFD2B9EFD2B9EFD2B9EFD2B9EFD2B9
          EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9EFD2B9F0D2B9F0D2B9F0D2
          B9F0D2B9F0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0
          D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BAF0D1BA
          F0D1BAF0D1BAF0D2BAF0D3BEF2D6BFF0D5C0F0D3BEEECEB6ECC8ACE8C2A4E8C0
          A0E8BFA0E8BFA0E8BFA0E8BFA0E8BFA0E8BFA0E8BFA0E8BFA0E8BFA0E8BFA000
          0000}
        BackgroundProperty.Visible = True
        MenuHButtons = <
          item
            ImageIndexNormal = 0
            ImageIndexMove = 0
            ImageIndexDown = 0
            ImageIndexDisable = 0
            Caption = 'Normal'
            Hint = 'Normal'
            ShowHint = True
            Down = True
            GroupIndex = 1
          end
          item
            ImageIndexNormal = 1
            ImageIndexMove = 1
            ImageIndexDown = 1
            ImageIndexDisable = 1
            Caption = 'Web'
            Hint = 'Web'
            ShowHint = True
            GroupIndex = 1
          end
          item
            ImageIndexNormal = 2
            ImageIndexMove = 2
            ImageIndexDown = 2
            ImageIndexDisable = 2
            Caption = 'Break Pages'
            Hint = 'Break Pages'
            ShowHint = True
            GroupIndex = 1
          end>
        MenuVButtons = <
          item
            ImageIndexNormal = 0
            ImageIndexMove = 0
            ImageIndexDown = 0
            ImageIndexDisable = 0
            Caption = 'Up'
          end
          item
            ImageIndexNormal = 1
            ImageIndexMove = 1
            ImageIndexDown = 1
            ImageIndexDisable = 1
            Caption = 'Select'
          end
          item
            ImageIndexNormal = 2
            ImageIndexMove = 2
            ImageIndexDown = 2
            ImageIndexDisable = 2
            Caption = 'Down'
          end>
        MenuHorizontal.PenFrame.Color = 12937777
        MenuHorizontal.ButtonWidth = 15
        MenuVertical.PenFrame.Color = 12937777
        MenuVertical.ButtonHeight = 15
        MenuVertical.Align = srvtbaBottom
        CaretBlinkTime = 530
        ReadOnly = True
        UseStyleTemplates = True
        RVColor = clWhite
        RVOptions = [rvoAllowSelection, rvoScrollToEnd, rvoTagsArePChars, rvoAutoCopyText, rvoAutoCopyRVF, rvoAutoCopyImage, rvoAutoCopyRTF, rvoFormatInvalidate, rvoDblClickSelectsWord, rvoRClickDeselects, rvoFastFormatting]
        RVEditorOptions = [rvoCtrlJumps, rvoWantTabs]
        RTFOptions = [rvrtfDuplicateUnicode, rvrtfSaveJpegAsJpeg, rvrtfSavePngAsPng, rvrtfSaveDocParameters, rvrtfSaveHeaderFooter]
        RTFReadProperties.SkipHiddenText = False
        RVFOptions = [rvfoSavePicturesBody, rvfoSaveControlsBody, rvfoIgnoreUnknownPicFmt, rvfoIgnoreUnknownCtrls, rvfoConvUnknownStylesToZero, rvfoConvLargeImageIdxToZero, rvfoSaveBinary, rvfoSaveBack, rvfoLoadBack, rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveLayout, rvfoLoadLayout, rvfoSaveDocProperties, rvfoLoadDocProperties, rvfoCanChangeUnits]
        TabNavigation = rvtnNone
        ExplicitLeft = 254
        ExplicitTop = 604
      end
    end
  end
  object Edit11: TEdit
    Left = 861
    Top = 82
    Width = 57
    Height = 21
    TabOrder = 1
    Text = '0'
  end
  object qE1: TMyQuery
    Connection = msEClinic
    Left = 79
    Top = 19
  end
  object FIBDades: TpFIBDatabase
    DBName = 
      '172.16.0.204:/dades/bbdd/dasiclinic/desarrollo/dasiclinic_351.fd' +
      'b'
    DBParams.Strings = (
      'User_Name=SYSDBA'
      'password=masterkey')
    DefaultTransaction = FIBPrincipal
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = []
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 8
    Top = 2
  end
  object FIBPrincipal: TpFIBTransaction
    DefaultDatabase = FIBDades
    TRParams.Strings = (
      'read'
      'rec_version'
      'read_committed')
    TPBMode = tpbDefault
    Left = 37
    Top = 2
  end
  object pFIBRelacions: TpFIBDatabase
    DBName = 
      '172.16.0.204:/dades/bbdd/dasiclinic/traspasos/dasieclinic/dasiec' +
      'linic.fdb'
    DBParams.Strings = (
      'User_Name=SYSDBA'
      'password=masterkey')
    DefaultTransaction = pFIBTrans_Relacions
    DefaultUpdateTransaction = TEscritura
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = []
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 8
    Top = 67
  end
  object pFIBTrans_Relacions: TpFIBTransaction
    DefaultDatabase = pFIBRelacions
    TRParams.Strings = (
      'read'
      'rec_version'
      'read_committed')
    TPBMode = tpbDefault
    Left = 37
    Top = 67
  end
  object qD1: TpFIBQuery
    Transaction = FIBPrincipal
    Database = FIBDades
    Left = 192
    Top = 64
    qoStartTransaction = True
  end
  object qR1: TpFIBQuery
    Transaction = pFIBTrans_Relacions
    Database = pFIBRelacions
    Left = 112
    Top = 64
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qR2e: TpFIBQuery
    Transaction = TEscritura
    Database = pFIBRelacions
    Left = 141
    Top = 64
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tDPob: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select distinct(p.pobregistre), pa.paia2, po.prvregistre, po.pob' +
        'nom from s_pacients p '
      
        '              left join s_poblacions po on p.pobregistre=po.pobr' +
        'egistre '
      
        '              left join s_paisos pa on po.pairegistre=pa.pairegi' +
        'stre '
      '              where p.pobregistre>0'
      'UNION'
      
        'select distinct(p.pobregistre), pa.paia2, po.prvregistre, po.pob' +
        'nom from s_professionals p '
      
        '              left join s_poblacions po on p.pobregistre=po.pobr' +
        'egistre '
      
        '              left join s_paisos pa on po.pairegistre=pa.pairegi' +
        'stre '
      '              where p.pobregistre>0'
      'UNION'
      
        'select distinct(p.pobregistre), pa.paia2, po.prvregistre, po.pob' +
        'nom from s_empreses p '
      
        '              left join s_poblacions po on p.pobregistre=po.pobr' +
        'egistre '
      
        '              left join s_paisos pa on po.pairegistre=pa.pairegi' +
        'stre '
      '              where p.pobregistre>0'
      'UNION'
      
        'select distinct(p.pobregistre), pa.paia2, po.prvregistre, po.pob' +
        'nom from TARIFES p '
      
        '              left join s_poblacions po on p.pobregistre=po.pobr' +
        'egistre '
      
        '              left join s_paisos pa on po.pairegistre=pa.pairegi' +
        'stre '
      '              where p.pobregistre>0')
    Transaction = FIBPrincipal
    Database = FIBDades
    AutoCommit = True
    Left = 8
    Top = 112
  end
  object msEClinic: TMyConnection
    Database = 'dasieclinicDB'
    Username = 'dasieclinic'
    Server = 'eclinic-prod.coglltc7cvjd.eu-west-1.rds.amazonaws.com'
    LoginPrompt = False
    Left = 8
    Top = 32
    EncryptedPassword = '96FFB4FF9EFF95FF94FFA8FF9AFF90FF96FFC7FFCCFFC8FFCDFF'
  end
  object TEscritura: TpFIBTransaction
    DefaultDatabase = pFIBRelacions
    Left = 66
    Top = 67
  end
  object DataSource1: TDataSource
    AutoEdit = False
    DataSet = tD
    Left = 40
    Top = 112
  end
  object tD: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from tarifes where trfregistre>=0')
    Transaction = FIBPrincipal
    Database = FIBDades
    AutoCommit = True
    Left = 272
    Top = 64
  end
  object tD1: TpFIBDataSet
    Transaction = FIBPrincipal
    Database = FIBDades
    Left = 304
    Top = 64
  end
  object RVStyle1: TRVStyle
    TextStyles = <
      item
        StyleName = 'Normal text'
        FontName = 'Arial'
        Unicode = True
        Size = 10
      end
      item
        StyleName = 'Heading'
        FontName = 'Arial'
        Style = [fsBold]
        Color = clBlue
        Unicode = True
        Size = 10
      end
      item
        StyleName = 'Subheading'
        FontName = 'Arial'
        Style = [fsBold]
        Color = clNavy
        Unicode = True
        Size = 10
      end
      item
        StyleName = 'Keywords'
        FontName = 'Arial'
        Style = [fsItalic]
        Color = clMaroon
        Unicode = True
        Size = 10
      end
      item
        StyleName = 'Jump 1'
        FontName = 'Arial'
        Style = [fsUnderline]
        Color = clGreen
        Unicode = True
        Jump = True
        Size = 10
      end
      item
        StyleName = 'Jump 2'
        FontName = 'Arial'
        Style = [fsUnderline]
        Color = clGreen
        Unicode = True
        Jump = True
        Size = 10
      end>
    ParaStyles = <
      item
        Tabs = <>
      end
      item
        StyleName = 'Centered'
        Alignment = rvaCenter
        Tabs = <>
      end>
    ListStyles = <>
    InvalidPicture.Data = {
      07544269746D617036100000424D361000000000000036000000280000002000
      0000200000000100200000000000001000000000000000000000000000000000
      0000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF000000FF000000FF00FFFF
      FF00FFFFFF000000FF000000FF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF000000FF000000FF00FFFF
      FF00FFFFFF000000FF000000FF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C00000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008080800080808000808080008080800080808000808080008080
      800080808000808080008080800080808000808080008080800080808000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000}
    StyleTemplates = <>
    Left = 361
    Top = 223
  end
  object tInformes: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '    INFREGISTRE, INFTITULO, INFDATA, PACREGISTRE, USRREGISTRE,'
      '    INFORME'
      'FROM'
      '    INFORMES'
      'where(  infregistre=:infregistre'
      '     ) and (     INFORMES.INFREGISTRE = :OLD_INFREGISTRE'
      '     )'
      '        ')
    SelectSQL.Strings = (
      'SELECT'
      '    INFREGISTRE, INFTITULO, INFDATA, PACREGISTRE, USRREGISTRE,'
      '    INFORME'
      'FROM'
      '    INFORMES'
      'where infregistre=:infregistre    ')
    Transaction = FIBPrincipal
    Database = FIBDades
    Left = 320
    Top = 111
    object tInformesINFREGISTRE: TFIBIntegerField
      FieldName = 'INFREGISTRE'
    end
    object tInformesINFORME: TFIBMemoField
      FieldName = 'INFORME'
      BlobType = ftMemo
      Size = 8
    end
    object tInformesINFTITULO: TFIBStringField
      FieldName = 'INFTITULO'
      Size = 40
      EmptyStrToNull = True
    end
    object tInformesINFDATA: TFIBDateTimeField
      FieldName = 'INFDATA'
    end
    object tInformesPACREGISTRE: TFIBIntegerField
      FieldName = 'PACREGISTRE'
    end
    object tInformesUSRREGISTRE: TFIBIntegerField
      FieldName = 'USRREGISTRE'
    end
  end
  object dsInformes: TDataSource
    DataSet = tInformes
    Left = 372
    Top = 111
  end
  object RVReportHelper1: TRVReportHelper
    NoMetafiles = False
    PreviewCorrection = True
    RichView.BackgroundStyle = bsNoBitmap
    RichView.BottomMargin = 0
    RichView.RTFReadProperties.TextStyleMode = rvrsAddIfNeeded
    RichView.RTFReadProperties.ParaStyleMode = rvrsAddIfNeeded
    RichView.RVFOptions = [rvfoSavePicturesBody, rvfoSaveControlsBody, rvfoSaveBinary, rvfoSaveTextStyles, rvfoSaveParaStyles, rvfoSaveDocProperties, rvfoLoadDocProperties, rvfoSaveDocObjects, rvfoLoadDocObjects]
    RichView.TopMargin = 0
    Left = 249
    Top = 167
  end
  object tD2: TpFIBDataSet
    Transaction = FIBPrincipal
    Database = FIBDades
    Left = 336
    Top = 64
  end
  object tD0: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from tarifes where trfregistre>=0')
    Transaction = FIBPrincipal
    Database = FIBDades
    AutoCommit = True
    Left = 240
    Top = 64
  end
  object qE2: TMyQuery
    Connection = msEClinic
    Left = 109
    Top = 19
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    HandleRedirects = True
    AllowCookies = True
    UserAgent = 'Embarcadero URI Client/1.0'
    OnValidateServerCertificate = NetHTTPClient1ValidateServerCertificate
    Left = 176
    Top = 136
  end
  object gtScaleRichViewInterface1: TgtScaleRichViewInterface
    Engine = gtPDFEngine1
    DoBeginDoc = True
    DoEndDoc = True
    Left = 360
    Top = 168
  end
  object RVAControlPanel1: TRVAControlPanel
    DialogFontName = 'Tahoma'
    RVFFilter = 'RichView Files (*.rvf)|*.rvf'
    RVStylesFilter = 'RichView Styles (*.rvst)|*.rvst'
    RVStylesExt = 'rvst'
    DefaultExt = 'rvf'
    RVFormatTitle = 'RichView Format'
    DefaultFileName = 'Untitled.rvf'
    XMLFilter = 'XML Files (*.xml)|*.xml'
    DefaultMargin = 10
    DefaultDocParameters.PageWidth = 210.000000000000000000
    DefaultDocParameters.PageHeight = 297.000000000000000000
    DefaultDocParameters.Units = rvuMillimeters
    DefaultDocParameters.LeftMargin = 20.000000000000000000
    DefaultDocParameters.RightMargin = 20.000000000000000000
    DefaultDocParameters.TopMargin = 20.000000000000000000
    DefaultDocParameters.BottomMargin = 20.000000000000000000
    DefaultDocParameters.HeaderY = 50.000000000000000000
    DefaultDocParameters.FooterY = 50.000000000000000000
    Language = 'Spanish'
    UnitsDisplay = rvuMillimeters
    UnitsProgram = rvstuTwips
    PixelBorders = True
    Header.Text = '- &p -'
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -13
    Header.Font.Name = 'Arial'
    Header.Font.Style = []
    Footer.Font.Charset = DEFAULT_CHARSET
    Footer.Font.Color = clWindowText
    Footer.Font.Height = -13
    Footer.Font.Name = 'Arial'
    Footer.Font.Style = []
    Left = 440
    Top = 224
  end
  object gtPDFEngine1: TgtPDFEngine
    FileExtension = 'pdf'
    FileDescription = 'Adobe PDF Files'
    InputXRes = 96
    InputYRes = 96
    ReferencePoint = rpBand
    EMailSettings.AuthenticationRequired = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ImageSettings.SourceDPI = 96
    ImageSettings.OutputImageFormat = ifJPEG
    Page.Width = 8.267700000000000000
    Page.Height = 11.692900000000000000
    Page.BinNumber = 0
    FontEncoding = feWinAnsiEncoding
    DigitalSignature.FieldProperties.Visible = True
    DigitalSignature.FieldProperties.PageNumber = 1
    DigitalSignature.FieldProperties.FieldAppearenceOptions = [sfaoShowName, sfaoShowReason, sfaoShowLocation, sfaoShowDate, sfaoShowLabels]
    DigitalSignature.SignatureProperties.DateTime = 44533.737024050930000000
    PDFVersion = PDF_VER14
    Left = 266
    Top = 221
  end
  object DCP_cast1281: TDCP_cast128
    Id = 7
    Algorithm = 'Cast128'
    MaxKeySize = 128
    BlockSize = 64
    Left = 526
    Top = 225
  end
  object DCP_md51: TDCP_md5
    Id = 16
    Algorithm = 'MD5'
    HashSize = 128
    Left = 602
    Top = 225
  end
  object DCP_sha2561: TDCP_sha256
    Id = 28
    Algorithm = 'SHA256'
    HashSize = 256
    Left = 682
    Top = 225
  end
end
