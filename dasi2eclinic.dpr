program dasi2eclinic;

uses
  Forms,
  Unit2 in 'Unit2.pas' {Form2},
  UFValors in 'C:\componentsD2007\Funcions\UFValors.pas' {FValors: TDataModule},
  UConstDC in 'C:\componentsD2007\Funcions\UConstDC.pas' {dmMensajes: TDataModule},
  UFuncions in 'C:\ComponentsDX10\Funcions\UFuncions.pas',
  dmActionsSRV in 'C:\Components\EditorNou_DX\dmActionsSRV.pas' {srvActionsResource: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFValors, FValors);
  Application.CreateForm(TsrvActionsResource, srvActionsResource);
  Application.CreateForm(TdmMensajes, dmMensajes);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
